﻿using System;
using System.IO;
using CDP;
using CDP.Protocols;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

/// <summary>
/// Notice: All tests are running on mac.
/// </summary>
public class PerformanceTest {
	TestResult testResult;
	TextWriter Console;

	[SetUp]
	public void SetUp() {
		testResult = new TestResult(nameof(PerformanceTest));
		Console = TestContext.Out;
	}

	[Test]
	public void GetMetrics() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			chrome.Performance.Enable();
			var result = chrome.Performance.GetMetrics();
			Assert.NotNull(result);
			testResult.WriteJson(nameof(GetMetrics), result);
		}
	}

	protected HeadlessChrome CreateHeadlessChrome() {
		HeadlessChrome chrome = new HeadlessChrome(
			HeadlessChromeTest.ChromePath
		);
		chrome.ThrowExceptionOnSend = true;
		//SetupLogAddedEventHandler(chrome);
		return chrome;
	}

	protected void SetupLogAddedEventHandler(HeadlessChrome chrome) {
		if (chrome == null) {
			return;
		}
		chrome.LogAdded += (sender, e) => {
			Console.WriteLine("Log: {0}", e.Message);
		};
	}
}
