﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Text.RegularExpressions;

public class TestHelper {
	public static int GetChromeProcessCount() {
		string s = StartProcess("ps ax");
		if (string.IsNullOrEmpty(s) == false) {
			s = StartProcess(@"grep -i chrome", s);
		}
		if (string.IsNullOrEmpty(s) == false) {
			s = StartProcess(@"grep -v -i helper", s);
		}
		//s = StartProcess(@"grep \\sS\\s", s);
		//s = Grep(s, "^(?!.*(Helper)).*$");
		return GetLineCount(s);
	}

	public static int GetLineCount(string text) {
		if (string.IsNullOrEmpty(text)) {
			return 0;
		}
		if (text.Contains(Environment.NewLine)) {
			Regex regex = new Regex(Environment.NewLine);
			MatchCollection matches = regex.Matches(text);
			if (matches == null) {
				return 1;
			}
			return matches.Count + 1;
		}
		return 1;
	}

	public static string Grep(string text, string pattern) {
		Regex regex = new Regex(pattern);
		List<string> lines = new List<string>();
		using (StringReader reader = new StringReader(text)) {
			string line;
			while ((line = reader.ReadLine()) != null) {
				if (regex.Match(line).Success) {
					lines.Add(line);
				}
			}
		}
		return string.Join(
			Environment.NewLine,
			lines.ToArray()
		);
	}

	public static string StartProcess(string command, string input = null) {
		string arguments = string.Empty;
		if (command.Contains(" ")) {
			string[] commands = command.Split(' ');
			command = commands[0];
			arguments = string.Join(" ", commands, 1, commands.Length - 1);
		}
		ProcessStartInfo startInfo = new ProcessStartInfo() {
			FileName = command,
			Arguments = arguments,
			CreateNoWindow = true,
			UseShellExecute = false,
			RedirectStandardInput = true,
			RedirectStandardOutput = true
		};
		using (Process process = Process.Start(startInfo)) {
			if (string.IsNullOrEmpty(input) == false) {
				process.StandardInput.Write(input);
				process.StandardInput.Close();
			}
			StreamReader reader = process.StandardOutput;
			List<string> lines = new List<string>();
			string line;
			while ((line = reader.ReadLine()) != null) {
				lines.Add(line);
			}
			return string.Join(
				Environment.NewLine,
				lines.ToArray()
			);
		}
	}
}
