﻿using System;
using System.IO;
using CDP;
using CDP.Protocols;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

/// <summary>
/// Notice: All tests are running on mac.
/// </summary>
[TestFixture]
public class RuntimeTest {
	TestResult testResult;
	TextWriter Console;

	[SetUp]
	public void SetUp() {
		testResult = new TestResult(nameof(RuntimeTest));
		Console = TestContext.Out;
	}

	[Test]
	public void ConsoleAPICalled() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			RuntimeDomain.ConsoleAPICalledEventArgs args = null;
			chrome.Runtime.ConsoleAPICalled += (sender, e) => {
				args = e;
			};
			chrome.Start();
			chrome.Runtime.Enable();
			chrome.Page.SetDocumentContent(
				chrome.FrameId,
				"<script>console.log('test')</script>"
			);
			chrome.Delay(500);
			Assert.IsNotNull(args);
			Assert.IsNotEmpty(args.Args);
			Assert.AreEqual("test", args.Args[0].Value);
			testResult.WriteJson(nameof(ConsoleAPICalled), args);
		}
	}

	[Test]
	public void ExceptionThrown() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			RuntimeDomain.ExceptionThrownEventArgs args = null;
			chrome.Runtime.ExceptionThrown += (sender, e) => {
				args = e;
			};
			chrome.Start();
			chrome.Runtime.Enable();
			chrome.Page.SetDocumentContent(
				chrome.FrameId,
				"<script>aa</script>"
			);
			chrome.Delay(500);
			Assert.IsNotNull(args);
			testResult.WriteJson(nameof(ExceptionThrown), args);
		}
	}

	protected HeadlessChrome CreateHeadlessChrome() {
		HeadlessChrome chrome = new HeadlessChrome(
			HeadlessChromeTest.ChromePath
		);
		chrome.ThrowExceptionOnSend = true;
		//SetupLogAddedEventHandler(chrome);
		return chrome;
	}

	protected void SetupLogAddedEventHandler(HeadlessChrome chrome) {
		if (chrome == null) {
			return;
		}
		chrome.LogAdded += (sender, e) => {
			Console.WriteLine("Log: {0}", e.Message);
		};
	}
}
