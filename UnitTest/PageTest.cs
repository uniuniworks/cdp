﻿using System;
using System.IO;
using CDP;
using CDP.Protocols;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

/// <summary>
/// Notice: All tests are running on mac.
/// </summary>
[TestFixture]
public class PageTest {
	TestResult testResult;
	TextWriter Console;

	[SetUp]
	public void SetUp() {
		testResult = new TestResult(nameof(PageTest));
		Console = TestContext.Out;
	}

	[Test]
	public void FrameNavigated() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			PageDomain.FrameNavigatedEventArgs args = null;
			chrome.Page.FrameNavigated += (sender, e) => {
				args = e;
			};

			string url = "http://google.com/";
			chrome.Start();
			chrome.Page.Disable();
			chrome.Page.Navigate(url);
			chrome.Delay(500);
			Assert.IsNull(args);

			chrome.Page.Enable();
			chrome.Page.Navigate(url);
			chrome.Delay(500);
			Assert.IsNotNull(args);
			Assert.IsNotNull(args.Frame);
			Assert.IsTrue(args.Frame.Url.Contains("google.com"));
			testResult.WriteJson(nameof(FrameNavigated), args);
		}
	}

	[Test]
	public void BringToFront() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			Assert.DoesNotThrow(() => {
				chrome.Page.BringToFront();
			});
		}
	}

	[Test]
	public void CaptureScreenshot() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			chrome.Page.Navigate("http://google.com/");
			var png = chrome.Page.CaptureScreenshot();
			Assert.NotNull(png);
			Assert.IsNotEmpty(png.Data);
			testResult.Write(nameof(CaptureScreenshot) + ".png", png.Data);
		}
	}

	[Test]
	public void CaptureSnapshot() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			chrome.Page.Navigate("http://google.com/");
			var result = chrome.Page.CaptureSnapshot();
			Assert.NotNull(result);
			testResult.WriteJson(nameof(CaptureSnapshot), result);
		}
	}

	[Test]
	public void ClearDeviceMetricsOverride() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			chrome.Page.ClearDeviceMetricsOverride();
		}
	}

	[Test]
	public void ClearDeviceOrientationOverride() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			chrome.Page.ClearDeviceOrientationOverride();
		}
	}

	[Test]
	public void ClearGeolocationOverride() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			chrome.Page.ClearGeolocationOverride();
		}
	}

	[Test]
	public void CreateIsolatedWorld() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var result = chrome.Page.CreateIsolatedWorld(
				chrome.FrameId
			);
			Assert.NotNull(result);
			testResult.WriteJson(nameof(CreateIsolatedWorld), result);
		}
	}

	[Test]
	public void GetInstallabilityErrors() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var result = chrome.Page.GetInstallabilityErrors();
			Assert.NotNull(result);
			testResult.WriteJson(nameof(GetInstallabilityErrors), result);
		}
	}

	[Test]
	public void GetCookies() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var result = chrome.Page.GetCookies();
			Assert.NotNull(result);
			testResult.WriteJson(nameof(GetCookies), result);
		}
	}

	[Test]
	public void GetFrameTree() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var result = chrome.Page.GetFrameTree();
			Assert.IsNotNull(result);
			Assert.IsNotNull(result.FrameTree);
			testResult.WriteJson(nameof(GetFrameTree), result);
		}
	}

	[Test]
	public void GetLayoutMetrics() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var result = chrome.Page.GetLayoutMetrics();
			Assert.NotNull(result);
			testResult.WriteJson(nameof(GetLayoutMetrics), result);
		}
	}

	[Test]
	public void GetNavigationHistory() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			chrome.Page.Navigate("http://google.com");
			var result = chrome.Page.GetNavigationHistory();
			Assert.IsNotNull(result);
			testResult.WriteJson(nameof(GetNavigationHistory), result);
		}
	}

	[Test]
	public void ResetNavigationHistory() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			chrome.Page.Navigate("http://google.com");
			chrome.Page.ResetNavigationHistory();
			var result = chrome.Page.GetNavigationHistory();
			Assert.IsNotNull(result);
			testResult.WriteJson(nameof(ResetNavigationHistory), result);
		}
	}

	/*
	[Test]
	public void GetResourceContent() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			chrome.Page.Navigate("http://google.com");
			var history = chrome.Page.GetNavigationHistory();
			var result = chrome.Page.GetResourceContent(
				chrome.FrameId,
				history.Entries[history.CurrentIndex].Url
			);
			Assert.IsNotNull(result);
			testResult.WriteJson(nameof(GetResourceContent), result);
		}
	}
	//*/

	[Test]
	public void GetResourceTree() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var result = chrome.Page.GetResourceTree();
			Assert.IsNotNull(result);
			Assert.IsNotNull(result.FrameTree);
			Assert.IsNotNull(result.FrameTree.Frame);
			testResult.WriteJson(nameof(GetResourceTree), result);
		}
	}

	[Test]
	public void Navigate() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			Assert.DoesNotThrow(() => {
				var result = chrome.Page.Navigate("http://google.com");
				testResult.WriteJson(nameof(Navigate), result);
			});
		}
	}

	[Test]
	public void PrintToPDF() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			chrome.Page.Navigate("http://google.com");
			chrome.Delay(500);
			var result = chrome.Page.PrintToPDF(
				scale: 0.5,
				transferMode: "ReturnAsBase64"
			);
			Assert.IsNotNull(result);
			testResult.Write(nameof(PrintToPDF) + ".pdf", result.Data);
		}
	}

	[Test]
	public void Reload() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			Assert.DoesNotThrow(() => {
				chrome.Page.Reload();
			});
		}
	}

	[Test]
	public void SetFontFamilies() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var sizes = new PageDomain.FontFamilies {
				Standard = ""
			};
			chrome.Page.SetFontFamilies(sizes);
		}
	}

	[Test]
	public void SetFontSizes() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var sizes = new PageDomain.FontSizes {
				Standard = 12
			};
			chrome.Page.SetFontSizes(sizes);
		}
	}

	[Test]
	public void SetDocumentContent() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			chrome.Page.SetDocumentContent(
				chrome.FrameId,
				"<html><body>Test test</body></html>");
			var png = chrome.Page.CaptureScreenshot();
			Assert.NotNull(png);
			Assert.IsNotEmpty(png.Data);
			testResult.Write(nameof(SetDocumentContent) + ".png", png.Data);
		}
	}

	protected HeadlessChrome CreateHeadlessChrome() {
		HeadlessChrome chrome = new HeadlessChrome(
			HeadlessChromeTest.ChromePath
		);
		chrome.ThrowExceptionOnSend = true;
		//SetupLogAddedEventHandler(chrome);
		return chrome;
	}

	protected void SetupLogAddedEventHandler(HeadlessChrome chrome) {
		if (chrome == null) {
			return;
		}
		chrome.LogAdded += (sender, e) => {
			Console.WriteLine("Log: {0}", e.Message);
		};
	}
}
