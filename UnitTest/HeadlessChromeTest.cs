﻿using System;
using System.IO;
using CDP;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

#if NET40
using System.Threading.Tasks;
#endif // NET40

/// <summary>
/// Notice: All tests are running on mac.
/// </summary>
[TestFixture]
public class HeadlessChromeTest {
	/// <summary>
	/// Chrome app path.
	/// </summary>
	public static readonly string ChromePath;
	TextWriter Console;

	static HeadlessChromeTest() {
		if (Environment.OSVersion.Platform == PlatformID.Win32NT) {
			ChromePath = HeadlessChrome.DefaultAppPathWindows;
		} else {
			ChromePath = HeadlessChrome.DefaultAppPathMac;
		}
	}

	[SetUp]
	public void SetUp() {
		Console = TestContext.Out;
	}

	[Test]
	public void Constuctor() {
		HeadlessChrome chrome;
		Assert.Catch<ArgumentNullException>(() => {
			chrome = new HeadlessChrome(null);
		});
		Assert.DoesNotThrow(() => {
			chrome = new HeadlessChrome("");
			chrome.Close();
		});
		Assert.DoesNotThrow(() => {
			chrome = new HeadlessChrome(ChromePath);
			chrome.Close();
		});
	}

	[Test]
	public void Start_InvalidPath() {
		HeadlessChrome chrome;
		chrome = new HeadlessChrome("");
		Assert.Catch<InvalidOperationException>(() => {
			chrome.Start();
		});
	}

	[Test]
	public void Start_ProcessCount() {
		HeadlessChrome chrome = new HeadlessChrome(ChromePath);
		using (chrome) {
			chrome.Delay(1000);
			int processCount = TestHelper.GetChromeProcessCount();
			chrome.Start();
			Assert.AreEqual(
				processCount + 1,
				TestHelper.GetChromeProcessCount()
			);
			chrome.Close();
			Assert.AreEqual(
				processCount,
				TestHelper.GetChromeProcessCount()
			);
		}
	}

	[Test]
	public void Open_null() {
		HeadlessChrome chrome = new HeadlessChrome(ChromePath);
		using (chrome) {
			Assert.Catch<ArgumentNullException>(() => {
				chrome.Open(null);
			});
		}
	}

	[Test]
	public void Open_InvalidTarget() {
		HeadlessChrome chrome = new HeadlessChrome(ChromePath);

		TargetJson target = new TargetJson();
		Assert.Catch<NullReferenceException>(() => {
			chrome.Open(target);
		});

		using (chrome) {
			chrome.Start();
			Assert.Catch<NullReferenceException>(() => {
				chrome.Open(target);
			});

			target.Id = "";
			Assert.Catch<InvalidOperationException>(() => {
				chrome.Open(target);
			});

			target.WebSocketDebuggerUrl = "ws://localhost";
			Assert.Catch<InvalidOperationException>(() => {
				chrome.Open(target);
			});
		}
	}

	[Test]
	public void Open() {
		HeadlessChrome chrome = new HeadlessChrome(ChromePath);
		chrome.LogAdded += (sender, e) => {
			//Console.WriteLine("Log: {0}", e.Message);
		};
		chrome.Page.FrameNavigated += (sender, e) => {
			Console.WriteLine("** FrameNavigated: {0}", e);
		};
		using (chrome) {
			chrome.Start();
			chrome.Page.Enable();
			TargetJson target = chrome.OpenNewTab("http://google.com/");
			Assert.IsNotNull(target);
			HeadlessChrome chrome2 = chrome.Open(target);
			Assert.AreNotEqual(chrome.FrameId, chrome2.FrameId);

			//chrome2.LogAdded += (sender, e) => {
			//	Console.WriteLine("Log2: {0}", e.Message);
			//};
			chrome.Delay(1000);
			Assert.AreNotEqual(
				chrome.Page.GetFrameTree().FrameTree.Frame.Url,
				chrome2.Page.GetFrameTree().FrameTree.Frame.Url
			);

			chrome2.LogAdded += (sender, e) => {
				//Console.WriteLine("Log2: {0}", e.Message);
			};
			chrome2.Page.FrameNavigated += (sender, e) => {
				Console.WriteLine("** FrameNavigated2: {0}", e);
			};
			chrome2.Page.Enable();
			chrome.Page.Navigate("http://google.com/");
			chrome.Delay(1000);
			chrome2.Close();
		}
	}

	[Test]
	public void Close_Using() {
		HeadlessChrome chrome;
		int processCount = 0;
		using (chrome = new HeadlessChrome(ChromePath)) {
			chrome.Delay(1000);
			processCount = TestHelper.GetChromeProcessCount();
			chrome.Start();
			Assert.AreEqual(
				processCount + 1,
				TestHelper.GetChromeProcessCount()
			);
		}
		chrome.Delay(1000);
		Assert.AreEqual(
			processCount,
			TestHelper.GetChromeProcessCount()
		);
	}

	[Test]
	public void Send_NotOpen() {
		HeadlessChrome chrome;
		chrome = new HeadlessChrome(ChromePath);
		Assert.Catch<InvalidOperationException>(() => {
			chrome.Send("test");
		});
	}

	[Test]
	public void Send_BrowserClose() {
		HeadlessChrome chrome;
		using (chrome = new HeadlessChrome(ChromePath)) {
			chrome.Start();
			string result = chrome.Send("Browser.close");
			Assert.IsFalse(string.IsNullOrEmpty(result));
			JObject json = JObject.Parse(result);
			//Assert.AreEqual("3", json.SelectToken("id").ToString());
			Assert.AreEqual("{}", json.SelectToken("result").ToString());
		}
	}

	[Test]
	public void BeginSend_BrowserClose() {
		HeadlessChrome chrome;
		using (chrome = new HeadlessChrome(ChromePath)) {
			chrome.Start();
			IAsyncResult asyncResult = chrome.BeginSend(
				"Browser.close", null, null
			);
			Assert.IsNotNull(asyncResult);
			string result = chrome.EndSend(asyncResult);
			JObject json = JObject.Parse(result);
			//Assert.AreEqual("2", json.SelectToken("id").ToString());
			Assert.AreEqual("{}", json.SelectToken("result").ToString());
		}

		using (chrome = new HeadlessChrome(ChromePath)) {
			chrome.Start();
			bool success = false;
			AsyncCallback callback = (asyncResult2) => {
				success = true;
				Assert.AreSame(chrome, asyncResult2.AsyncState);
				string result = chrome.EndSend(asyncResult2);
				JObject json = JObject.Parse(result);
				try {
					//Assert.AreEqual("2", json.SelectToken("id").ToString());
					Assert.AreEqual("{}", json.SelectToken("result").ToString());
				} catch (Exception) {
					throw new Exception("Assertion failed.");
				}
			};
			IAsyncResult asyncResult = chrome.BeginSend(
				"Browser.close", callback, chrome
			);
			Assert.IsNotNull(asyncResult);
			Assert.IsFalse(asyncResult.IsCompleted);
			asyncResult.AsyncWaitHandle.WaitOne();
			Assert.IsTrue(asyncResult.IsCompleted);
			//chrome.EndSend(asyncResult);
			while (success == false) {
				chrome.Delay(100);
			}
			Assert.IsTrue(success);
		}
	}

	[Test]
	public void BeginSend_PageCrash() {
		HeadlessChrome chrome;
		using (chrome = new HeadlessChrome(ChromePath)) {
			//chrome.LogAdded += (sender, e) => {
			//	Console.WriteLine("Log: {0}", e.Message);
			//};
			chrome.SendTimeout = 1000;
			chrome.ThrowExceptionOnSend = false;
			chrome.Start();
			IAsyncResult asyncResult = chrome.BeginSend(
				"Page.crash", null, null
			);
			Assert.IsNotNull(asyncResult);
			string result = chrome.EndSend(asyncResult);
			Assert.IsNull(result);
		}
		using (chrome = new HeadlessChrome(ChromePath)) {
			chrome.SendTimeout = 1000;
			chrome.ThrowExceptionOnSend = true;
			chrome.Start();
			IAsyncResult asyncResult = chrome.BeginSend(
				"Page.crash", null, null
			);
			Assert.IsNotNull(asyncResult);
			Assert.Catch<TimeoutException>(() => {
				string result = chrome.EndSend(asyncResult);
			});
		}
	}

	[Test]
	public void EndSend_Null() {
		HeadlessChrome chrome;
		using (chrome = new HeadlessChrome(ChromePath)) {
			chrome.Start();
			Assert.Catch<ArgumentNullException>(() => {
				chrome.EndSend(null);
			});
		}
	}

#if NET40
	[Test]
	public void SendAsync() {
		HeadlessChrome chrome;
		using (chrome = new HeadlessChrome(ChromePath)) {
			chrome.Start();
			Task<string> task = chrome.SendAsync("Browser.close");
			string result = task.Result;
			//Console.WriteLine(result);
			Assert.IsFalse(string.IsNullOrEmpty(result));
			JObject json = JObject.Parse(result);
			Assert.AreEqual("{}", json.SelectToken("result").ToString());
		}
	}
#endif // NET40
}
