﻿using System;
using System.IO;
using CDP;
using CDP.Protocols;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

/// <summary>
/// Notice: All tests are running on mac.
/// </summary>
public class BrowserTest {
	TestResult testResult;
	TextWriter Console;

	[SetUp]
	public void SetUp() {
		testResult = new TestResult(nameof(BrowserTest));
		Console = TestContext.Out;
	}

	[Test]
	public void GetBrowserCommandLine() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start(args: new[] {
				"--enable-automation",
				"--headless",
				"--disable-gpu",
				"--remote-debugging-port=0"
			});
			var result = chrome.Browser.GetBrowserCommandLine();
			Assert.NotNull(result);
			Assert.IsNotEmpty(result.Arguments);
			testResult.WriteJson(nameof(GetBrowserCommandLine), result);
		}
	}

	[Test]
	public void GetHistograms() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var result = chrome.Browser.GetHistograms();
			Assert.NotNull(result);
			testResult.WriteJson(nameof(GetHistograms), result);
		}
	}

	[Test]
	public void GetHistogram() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			chrome.Delay(200);
			var result = chrome.Browser.GetHistogram(
				"CompositorLatency.TotalLatency"
			);
			Assert.NotNull(result);
			testResult.WriteJson(nameof(GetHistogram), result);
		}
	}

	[Test]
	public void GetWindowBounds() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var window = chrome.Browser.GetWindowForTarget(
				chrome.FrameId
			);
			Assert.NotNull(window);
			var result = chrome.Browser.GetWindowBounds(
				window.WindowId
			);
			Assert.NotNull(result);
			testResult.WriteJson(nameof(GetWindowBounds), result);
		}
	}

	[Test]
	public void GetWindowForTarget() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var result = chrome.Browser.GetWindowForTarget(
				chrome.FrameId
			);
			Assert.NotNull(result);
			testResult.WriteJson(nameof(GetWindowForTarget), result);
		}
	}

	[Test]
	public void SetWindowBounds() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var window = chrome.Browser.GetWindowForTarget(
				chrome.FrameId
			);
			Assert.NotNull(window);
			var bounds = new BrowserDomain.Bounds {
				Left = 20,
				Top = 10,
				Width = 1000,
				Height = 900
			};
			chrome.Browser.SetWindowBounds(
				window.WindowId, bounds
			);
			var result = chrome.Browser.GetWindowBounds(
				window.WindowId
			);
			Assert.NotNull(result);
			//Assert.AreEqual(bounds.Left, result.Bounds.Left);
			//Assert.AreEqual(bounds.Top, result.Bounds.Top);
			Assert.AreEqual(bounds.Width, result.Bounds.Width);
			Assert.AreEqual(bounds.Height, result.Bounds.Height);
			testResult.WriteJson(nameof(SetWindowBounds), result);
		}
	}

	[Test]
	public void GetVersion() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var result = chrome.Browser.GetVersion();
			Assert.NotNull(result);
			testResult.WriteJson(nameof(GetVersion), result);
		}
	}

	protected HeadlessChrome CreateHeadlessChrome() {
		HeadlessChrome chrome = new HeadlessChrome(
			HeadlessChromeTest.ChromePath
		);
		chrome.ThrowExceptionOnSend = true;
		//SetupLogAddedEventHandler(chrome);
		return chrome;
	}

	protected void SetupLogAddedEventHandler(HeadlessChrome chrome) {
		if (chrome == null) {
			return;
		}
		chrome.LogAdded += (sender, e) => {
			Console.WriteLine("Log: {0}", e.Message);
		};
	}
}
