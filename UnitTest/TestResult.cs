﻿using System;
using System.IO;

public class TestResult {
	protected string _path;

	public TestResult(string testName) {
		if (string.IsNullOrEmpty(testName)) {
			throw new InvalidOperationException();
		}

		if (Environment.OSVersion.Platform == PlatformID.Win32NT) {
			_path = Path.Combine(
				AppDomain.CurrentDomain.BaseDirectory,
				"Results"
			);
			_path = Path.Combine(_path, testName);
		} else {
			_path = Path.Combine("Results", testName);
		}
		if (Directory.Exists(_path) == false) {
			Directory.CreateDirectory(_path);
		}
	}

	public void Write(string fileName, byte[] contents) {
		string path = Path.Combine(_path, fileName);
		File.WriteAllBytes(path, contents);
	}

	public void Write(string fileName, object contents) {
		string path = Path.Combine(_path, fileName);
		File.WriteAllText(path, contents.ToString());
	}

	public void WriteJson(string fileName, object contents) {
		Write(fileName + ".json", contents);
	}
}
