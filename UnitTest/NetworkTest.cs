﻿using System;
using System.IO;
using CDP;
using CDP.Protocols;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

/// <summary>
/// Notice: All tests are running on mac.
/// </summary>
public class NetworkTest {
	TestResult testResult;
	TextWriter Console;

	[SetUp]
	public void SetUp() {
		testResult = new TestResult(nameof(NetworkTest));
		Console = TestContext.Out;
	}

	[Test]
	public void CanClearBrowserCache() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var result = chrome.Network.CanClearBrowserCache();
			Assert.NotNull(result);
			testResult.WriteJson(nameof(CanClearBrowserCache), result);
		}
	}

	[Test]
	public void CanClearBrowserCookies() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var result = chrome.Network.CanClearBrowserCookies();
			Assert.NotNull(result);
			testResult.WriteJson(nameof(CanClearBrowserCookies), result);
		}
	}

	[Test]
	public void CanEmulateNetworkConditions() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var result = chrome.Network.CanEmulateNetworkConditions();
			Assert.NotNull(result);
			testResult.WriteJson(nameof(CanEmulateNetworkConditions), result);
		}
	}

	/*
	[Test]
	public void ClearBrowserCache() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			chrome.SendTimeout = 10000;
			Assert.DoesNotThrow(() => {
				chrome.Network.ClearBrowserCache();
			});
		}
	}
	//*/
	
	[Test]
	public void GetAllCookies() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var result = chrome.Network.GetAllCookies();
			Assert.NotNull(result);
			testResult.WriteJson(nameof(GetAllCookies), result);
		}
	}

	protected HeadlessChrome CreateHeadlessChrome() {
		HeadlessChrome chrome = new HeadlessChrome(
			HeadlessChromeTest.ChromePath
		);
		chrome.ThrowExceptionOnSend = true;
		//SetupLogAddedEventHandler(chrome);
		return chrome;
	}

	protected void SetupLogAddedEventHandler(HeadlessChrome chrome) {
		if (chrome == null) {
			return;
		}
		chrome.LogAdded += (sender, e) => {
			Console.WriteLine("Log: {0}", e.Message);
		};
	}
}
