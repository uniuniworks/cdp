﻿using System;
using System.IO;
using CDP;
using CDP.Protocols;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

/// <summary>
/// Notice: All tests are running on mac.
/// </summary>
public class TargetTest {
	TestResult testResult;
	TextWriter Console;

	[SetUp]
	public void SetUp() {
		testResult = new TestResult(nameof(TargetTest));
		Console = TestContext.Out;
	}

	[Test]
	public void ActivateTarget() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			chrome.Target.ActivateTarget(chrome.FrameId);
		}
	}

	[Test]
	public void AttachToBrowserTarget() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();

			Assert.Catch<InvalidOperationException>(() => {
				var result = chrome.Target.AttachToBrowserTarget();
			});
			//Assert.NotNull(result);
			//testResult.WriteJson(nameof(AttachToBrowserTarget), result);
		}
	}

	/*
	[Test]
	public void CloseTarget() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var result = chrome.Target.CloseTarget(chrome.FrameId);
			Assert.NotNull(result);
			testResult.WriteJson(nameof(CloseTarget), result);
		}
	}
	*/

	[Test]
	public void ExposeDevToolsProtocol() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			Assert.Catch<InvalidOperationException>(() => {
				chrome.Target.ExposeDevToolsProtocol(
					chrome.FrameId
				);
			});
		}
	}

	protected HeadlessChrome CreateHeadlessChrome() {
		HeadlessChrome chrome = new HeadlessChrome(
			HeadlessChromeTest.ChromePath
		);
		chrome.ThrowExceptionOnSend = true;
		//SetupLogAddedEventHandler(chrome);
		return chrome;
	}

	protected void SetupLogAddedEventHandler(HeadlessChrome chrome) {
		if (chrome == null) {
			return;
		}
		chrome.LogAdded += (sender, e) => {
			Console.WriteLine("Log: {0}", e.Message);
		};
	}
}
