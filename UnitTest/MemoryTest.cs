﻿using System;
using System.IO;
using CDP;
using CDP.Protocols;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

/// <summary>
/// Notice: All tests are running on mac.
/// </summary>
public class MemoryTest {
	TestResult testResult;
	TextWriter Console;

	[SetUp]
	public void SetUp() {
		testResult = new TestResult(nameof(MemoryTest));
		Console = TestContext.Out;
	}

	[Test]
	public void GetDOMCounters() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var result = chrome.Memory.GetDOMCounters();
			Assert.NotNull(result);
			testResult.WriteJson(nameof(GetDOMCounters), result);
		}
	}

	[Test]
	public void GetAllTimeSamplingProfile() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var result = chrome.Memory.GetAllTimeSamplingProfile();
			Assert.NotNull(result);
			testResult.WriteJson(nameof(GetAllTimeSamplingProfile), result);
		}
	}

	[Test]
	public void GetBrowserSamplingProfile() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var result = chrome.Memory.GetBrowserSamplingProfile();
			Assert.NotNull(result);
			testResult.WriteJson(nameof(GetBrowserSamplingProfile), result);
		}
	}

	[Test]
	public void GetSamplingProfile() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var result = chrome.Memory.GetSamplingProfile();
			Assert.NotNull(result);
			testResult.WriteJson(nameof(GetSamplingProfile), result);
		}
	}

	protected HeadlessChrome CreateHeadlessChrome() {
		HeadlessChrome chrome = new HeadlessChrome(
			HeadlessChromeTest.ChromePath
		);
		chrome.ThrowExceptionOnSend = true;
		//SetupLogAddedEventHandler(chrome);
		return chrome;
	}

	protected void SetupLogAddedEventHandler(HeadlessChrome chrome) {
		if (chrome == null) {
			return;
		}
		chrome.LogAdded += (sender, e) => {
			Console.WriteLine("Log: {0}", e.Message);
		};
	}
}
