﻿using System;
using System.IO;
using CDP;
using CDP.Protocols;
using Newtonsoft.Json.Linq;
using NUnit.Framework;

/// <summary>
/// Notice: All tests are running on mac.
/// </summary>
[TestFixture]
public class DOMTest {
	TestResult testResult;
	TextWriter Console;

	[SetUp]
	public void SetUp() {
		testResult = new TestResult(nameof(DOMTest));
		Console = TestContext.Out;
	}

	[Test]
	public void GetDocument() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var result = chrome.DOM.GetDocument();
			Assert.NotNull(result);
			testResult.WriteJson(nameof(GetDocument), result);
		}
	}

	[Test]
	public void QuerySelector() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var doc = chrome.DOM.GetDocument();
			var result = chrome.DOM.QuerySelector(
				doc.Root.NodeId,
				"html"
			);
			Assert.NotNull(result);
			Assert.AreNotEqual(0, result.NodeId);
			testResult.WriteJson(nameof(QuerySelector), result);
		}
	}

	[Test]
	public void SetOuterHTML() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var doc = chrome.DOM.GetDocument();
			var body = chrome.DOM.QuerySelector(
				doc.Root.NodeId,
				"body"
			);
			chrome.DOM.SetOuterHTML(
				body.NodeId,
				"<body>" +
				"<h1>Title</h1>" +
				"<div id='content'>test test</div>" +
				"<button>Button</button>" +
				"</body>"
			);
			//chrome.Delay(500);

			var content = chrome.DOM.QuerySelector(
				doc.Root.NodeId,
				"#content"
			);
			chrome.DOM.SetOuterHTML(
				content.NodeId,
				"abcdef"
			);

			var png = chrome.Page.CaptureScreenshot();
			testResult.Write(nameof(SetOuterHTML) + ".png", png.Data);

			var html = chrome.DOM.GetOuterHTML(doc.Root.NodeId);
			testResult.WriteJson(nameof(SetOuterHTML), html);
		}
	}

	/*
	[Test]
	public void GetFrameOwner() {
		HeadlessChrome chrome = CreateHeadlessChrome();
		using (chrome) {
			chrome.Start();
			var result = chrome.DOM.GetFrameOwner(
				chrome.FrameId
			);
			Assert.NotNull(result);
			testResult.WriteJson(nameof(GetFrameOwner), result);
		}
	}
	//*/

	protected HeadlessChrome CreateHeadlessChrome() {
		HeadlessChrome chrome = new HeadlessChrome(
			HeadlessChromeTest.ChromePath
		);
		chrome.ThrowExceptionOnSend = true;
		//SetupLogAddedEventHandler(chrome);
		return chrome;
	}

	protected void SetupLogAddedEventHandler(HeadlessChrome chrome) {
		if (chrome == null) {
			return;
		}
		chrome.LogAdded += (sender, e) => {
			Console.WriteLine("Log: {0}", e.Message);
		};
	}
}
