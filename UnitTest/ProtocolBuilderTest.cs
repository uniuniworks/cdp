﻿using CDP;
using NUnit.Framework;

[TestFixture]
public class ProtocolBuilderTest {
	[Test]
	public void Build() {
		HeadlessChrome chrome = new HeadlessChrome(
			HeadlessChrome.DefaultAppPathMac
		);
		using (chrome) {
			chrome.Start();
			ProtocolJson protocol = chrome.GetProtocol();
			ProtocolBuilder.Build(protocol, "Protocols");
		}
	}
}
