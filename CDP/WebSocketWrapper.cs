﻿#if NET45
using System;
using System.IO;
using System.Net.WebSockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

public class WebSocketWrapper {
	public class MessageReceivedEventArgs : EventArgs {
		public string Message;

		public MessageReceivedEventArgs(string message) {
			Message = message;
		}
	}

	public event EventHandler Opened;
	public event EventHandler Closed;
	public event EventHandler<string> Error;
	public event EventHandler<MessageReceivedEventArgs> MessageReceived;

	protected readonly int ReceiveBufferSize = 1024;
	protected readonly ClientWebSocket _webSocket;
	protected readonly CancellationTokenSource _cancellationTokenSource;
	protected string _url;

	public WebSocketState State {
		get { return _webSocket.State; }
	}

	public WebSocketWrapper(string url) {
		_url = url;
		_webSocket = new ClientWebSocket();
		_webSocket.Options.KeepAliveInterval = TimeSpan.FromSeconds(20);
		_cancellationTokenSource = new CancellationTokenSource();
	}

	/// <summary>
	/// Connects to the WebSocket server.
	/// </summary>
	public void Open() {
		Task task = Task.Run(OpenAsync);
		task.Wait();
		task.Dispose();
	}

	public void Close() {
		switch (_webSocket.State) {
			case WebSocketState.Connecting:
			case WebSocketState.Open:
				_cancellationTokenSource.Cancel();
				break;
		}
	}

	/// <summary>
	/// Send a message to the WebSocket server.
	/// </summary>
	/// <param name="message">The message to send</param>
	public void Send(string message) {
		/*
		Task task = Task.Run(() => SendAsync(message));
		task.Wait();
		task.Dispose();
		//*/
		if (_webSocket.State != WebSocketState.Open) {
			throw new InvalidOperationException(
				"Connection is not open."
			);
		}
		if (string.IsNullOrEmpty(message)) {
			return;
		}
		byte[] bytes = Encoding.UTF8.GetBytes(message);
		try {
			_webSocket.SendAsync(
				new ArraySegment<byte>(bytes, 0, bytes.Length),
				WebSocketMessageType.Text,
				true,
				CancellationToken.None
			);
		} catch (Exception e) {
			EmitErrorEvent(e.Message);
			return;
		}
	}

	protected async Task OpenAsync() {
		if (_webSocket.State == WebSocketState.Open) {
			return;
		}
		try {
			await _webSocket.ConnectAsync(
				new Uri(_url),
				_cancellationTokenSource.Token
			);
		} catch (Exception e) {
			EmitErrorEvent(e.Message);
			return;
		}
		_ = StartListenAsync();
		EmitOpenedEvent();
	}

	protected async Task SendAsync(string message) {
		if (_webSocket.State != WebSocketState.Open) {
			throw new InvalidOperationException(
				"Connection is not open."
			);
		}
		if (string.IsNullOrEmpty(message)) {
			return;
		}
		byte[] bytes = Encoding.UTF8.GetBytes(message);
		try {
			await _webSocket.SendAsync(
				new ArraySegment<byte>(bytes, 0, bytes.Length),
				WebSocketMessageType.Text,
				true,
				CancellationToken.None
			);
		} catch (Exception e) {
			EmitErrorEvent(e.Message);
			return;
		}
	}

	protected async Task CloseSocketAsync() {
		if (_webSocket.State != WebSocketState.Open) {
			EmitClosedEvent();
			return;
		}
		await _webSocket.CloseAsync(
			WebSocketCloseStatus.NormalClosure,
			string.Empty,
			CancellationToken.None
		);
		EmitClosedEvent();
	}

	protected async Task StartListenAsync() {
		byte[] bytes = new byte[ReceiveBufferSize];
		ArraySegment<byte> array = new ArraySegment<byte>(bytes);
		MemoryStream stream = new MemoryStream();
		try {
			while (_webSocket.State == WebSocketState.Open) {
				//await Task.Delay(1);
				WebSocketReceiveResult result = null;
				do {
					try {
						result = await _webSocket.ReceiveAsync(
							array, _cancellationTokenSource.Token
						);
					} catch (Exception) {
						await CloseSocketAsync();
						return;
					}
					if (_cancellationTokenSource.IsCancellationRequested) {
						await CloseSocketAsync();
						return;
					}
					if (result.MessageType == WebSocketMessageType.Close) {
						await CloseSocketAsync();
						return;
					}
					stream.Write(bytes, 0, result.Count);
				} while (!result.EndOfMessage);

				string message = Encoding.UTF8.GetString(
					stream.GetBuffer(),
					0,
					(int)stream.Position
				);
				stream.Position = 0;
				EmitMessageReceivedEvent(message);
			}
		} catch (Exception e) {
			EmitErrorEvent(e.Message);
			EmitClosedEvent();
		} finally {
			_webSocket.Dispose();
			stream.Close();
			stream.Dispose();
		}
	}

	protected void EmitOpenedEvent() {
		if (Opened == null) {
			return;
		}
		//_ = Task.Run(() => {
			Opened(this, EventArgs.Empty);
		//});
	}

	protected void EmitClosedEvent() {
		if (Closed == null) {
			return;
		}
		//_ = Task.Run(() => {
			Closed(this, EventArgs.Empty);
		//});
	}

	protected void EmitErrorEvent(string message) {
		if (Error == null) {
			return;
		}
		//_ = Task.Run(() => {
			Error(this, message);
		//});
	}

	protected void EmitMessageReceivedEvent(string message) {
		if (MessageReceived == null || string.IsNullOrEmpty(message)) {
			return;
		}
		//_ = Task.Run(() => {
			MessageReceived(this, new MessageReceivedEventArgs(message));
		//});
	}
}
#endif // NET45
