﻿using Newtonsoft.Json;

namespace CDP {
	/// <summary>
	/// The CDP request.
	/// </summary>
	[JsonObject]
	public class RequestJson {
		/// <summary>
		/// The JSON serializer settings
		/// used when serialize a RequestJson object.
		/// </summary>
		[JsonIgnore]
		public static JsonSerializerSettings Settings;

		/// <summary>
		/// The session id.
		/// </summary>
		[JsonProperty("sessionId")]
		public string SessionId;

		/// <summary>
		/// The request id.
		/// </summary>
		[JsonProperty("id")]
		public int Id;

		/// <summary>
		/// The request method.
		/// </summary>
		[JsonProperty("method")]
		public string Method;

		/// <summary>
		/// The request parameters.
		/// </summary>
		[JsonProperty("params", NullValueHandling = NullValueHandling.Ignore)]
		public object Params;

		/// <summary>
		/// The static constructor.
		/// </summary>
		static RequestJson() {
			Settings = new JsonSerializerSettings();
			Settings.NullValueHandling = NullValueHandling.Ignore;
		}

		/// <summary>
		/// Initializes a new instance of the RequestJson class.
		/// </summary>
		/// <param name="id">The request id.</param>
		/// <param name="method">The request method.</param>
		public RequestJson(int id, string method) {
			Id = id;
			Method = method;
		}

		/// <summary>
		/// Initializes a new instance of the RequestJson class.
		/// </summary>
		/// <param name="id">The request id.</param>
		/// <param name="method">The request method.</param>
		/// <param name="params">The request parameters.</param>
		public RequestJson(int id, string method, object @params) {
			Id = id;
			Method = method;
			Params = @params;
		}

		/// <summary>
		/// Returns a JSON string that represents the current object.
		/// </summary>
		/// <returns>A JSON string.</returns>
		public override string ToString() {
			return JsonConvert.SerializeObject(this, Settings);
		}
	}
}
