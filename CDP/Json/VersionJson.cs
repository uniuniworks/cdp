﻿using Newtonsoft.Json;

namespace CDP {
	/// <summary>
	/// The browser version metadata.
	/// <para>
	/// HTTP Endpoint: /json/version
	/// </para>
	/// </summary>
	[JsonObject]
	public class VersionJson {
		/// <summary>
		/// Browser name/Version.
		/// </summary>
		[JsonProperty("Browser")]
		public string Browser;

		/// <summary>
		/// CDP protocol version.
		/// </summary>
		[JsonProperty("Protocol-Version")]
		public string ProtocolVersion;

		/// <summary>
		/// User agent.
		/// </summary>
		[JsonProperty("User-Agent")]
		public string UserAgent;

		/// <summary>
		/// V8 version.
		/// </summary>
		[JsonProperty("V8-Version")]
		public string V8Version;

		/// <summary>
		/// WebKit version.
		/// </summary>
		[JsonProperty("WebKit-Version")]
		public string WebKitVersion;

		/// <summary>
		/// WebSocket debugger url.
		/// </summary>
		[JsonProperty("webSocketDebuggerUrl")]
		public string WebSocketDebuggerUrl;

		/// <summary>
		/// Returns a JSON string that represents the current object.
		/// </summary>
		/// <returns>A JSON string.</returns>
		public override string ToString() {
			return JsonConvert.SerializeObject(
				this, Formatting.Indented
			);
		}
	}
}
