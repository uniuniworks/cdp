﻿using Newtonsoft.Json;

namespace CDP {
	/// <summary>
	/// Available websocket target.
	/// <para>
	/// HTTP Endpoint: /json or /json/list
	/// </para>
	/// </summary>
	[JsonObject]
	public class TargetJson {
		/// <summary>
		/// Description.
		/// </summary>
		[JsonProperty("description")]
		public string Description;

		/// <summary>
		/// DevTools frontend url.
		/// </summary>
		[JsonProperty("devtoolsFrontendUrl")]
		public string DevtoolsFrontendUrl;

		/// <summary>
		/// Frame id.
		/// </summary>
		[JsonProperty("id")]
		public string Id;

		/// <summary>
		/// Page title.
		/// </summary>
		[JsonProperty("title")]
		public string Title;

		/// <summary>
		/// Type.
		/// </summary>
		[JsonProperty("type")]
		public string Type;

		/// <summary>
		/// Url.
		/// </summary>
		[JsonProperty("url")]
		public string Url;

		/// <summary>
		/// WebSocket debugger url.
		/// </summary>
		[JsonProperty("webSocketDebuggerUrl")]
		public string WebSocketDebuggerUrl;

		/// <summary>
		/// Returns a JSON string that represents the current object.
		/// </summary>
		/// <returns>A JSON string.</returns>
		public override string ToString() {
			return JsonConvert.SerializeObject(
				this, Formatting.Indented
			);
		}
	}
}
