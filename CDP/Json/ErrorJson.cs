﻿using Newtonsoft.Json;

namespace CDP {
	/// <summary>
	/// The Error JSON.
	/// </summary>
	[JsonObject]
	public class ErrorJson {
		/// <summary>
		/// The error code.
		/// </summary>
		[JsonProperty("code")]
		public int Code;

		/// <summary>
		/// The error message.
		/// </summary>
		[JsonProperty("message")]
		public string Message;

		/// <summary>
		/// Returns a JSON string that represents the current object.
		/// </summary>
		/// <returns>A JSON string.</returns>
		public override string ToString() {
			return JsonConvert.SerializeObject(
				this, Formatting.Indented
			);
		}
	}
}
