﻿using System;
using System.Reflection;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP {
	/// <summary>
	/// The current devtools protocol.
	/// <para>
	/// HTTP Endpoint: /json/protocol/
	/// </para>
	/// </summary>
	[JsonObject]
	public class ProtocolJson {
		[JsonObject]
		public class DomainJson {
			[JsonProperty("domain")]
			public string Domain;

			[JsonProperty("description")]
			public string Description;

			[JsonProperty("deprecated")]
			public bool Deprecated;

			[JsonProperty("experimental")]
			public bool Experimental;

			[JsonProperty("dependencies")]
			public string[] Dependencies;

			[JsonProperty("types")]
			public TypeJson[] Types;

			[JsonProperty("commands")]
			public CommandJson[] Commands;

			[JsonProperty("events")]
			public CommandJson[] Events;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		[JsonObject]
		public class TypeJson {
			[JsonProperty("id")]
			public string ID;

			[JsonProperty("description")]
			public string Description;

			[JsonProperty("type")]
			public string Type;

			[JsonProperty("enum")]
			public string[] Enum;

			[JsonProperty("properties")]
			public PropertyJson[] Properties;

			[JsonProperty("items")]
			public PropertyItemsJson Items;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		[JsonObject]
		public class CommandJson {
			[JsonProperty("name")]
			public string Name;

			[JsonProperty("description")]
			public string Description;

			[JsonProperty("deprecated")]
			public bool Deprecated;

			[JsonProperty("experimental")]
			public bool Experimental;

			[JsonProperty("type")]
			public string Type;

			[JsonProperty("parameters")]
			public PropertyJson[] Parameters;

			[JsonProperty("returns")]
			public PropertyJson[] Returns;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		[JsonObject]
		public class PropertyJson {
			[JsonProperty("name")]
			public string Name;

			[JsonProperty("description")]
			public string Description;

			[JsonProperty("optional")]
			public bool Optional;

			[JsonProperty("type")]
			public string Type;

			[JsonProperty("$ref")]
			public string Ref;

			[JsonProperty("enum")]
			public string[] Enum;

			[JsonProperty("items")]
			public PropertyItemsJson Items;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		[JsonConverter(typeof(PropertyItemsJsonConverter))]
		[JsonObject]
		public class PropertyItemsJson {
			[JsonProperty("type")]
			public string Type;

			[JsonProperty("$ref")]
			public string Ref;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns></returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		class PropertyItemsJsonConverter : JsonConverter {
			public override object ReadJson(
				JsonReader reader, Type objectType, object existingValue,
				JsonSerializer serializer)
			{
				JObject jObject = JObject.Load(reader);
				object targetObj = Activator.CreateInstance(objectType);
				foreach (FieldInfo field in objectType.GetFields()) {
					object[] attributes = field.GetCustomAttributes(true);
					JsonPropertyAttribute attribute = null;
					foreach (object item in attributes) {
						if (item is JsonPropertyAttribute) {
							attribute = (JsonPropertyAttribute)item;
							break;
						}
					}
					string jsonPath = field.Name;
					if (attribute != null) {
						jsonPath = attribute.PropertyName;
					}
					JToken token = jObject.SelectToken(jsonPath);
					if (token != null && token.Type != JTokenType.Null) {
						object value = token.ToObject(field.FieldType, serializer);
						field.SetValue(targetObj, value);
					}
				}
				return targetObj;
			}

			public override bool CanConvert(Type objectType) {
				// CanConvert is not called when [JsonConverter] attribute is used
				return false;
			}

			public override bool CanWrite {
				get { return false; }
			}

			public override void WriteJson(JsonWriter writer, object value, JsonSerializer serializer) {
				throw new NotImplementedException();
			}
		}

		[JsonObject]
		public class VersionJson {
			[JsonProperty("major")]
			public string Major;

			[JsonProperty("minor")]
			public string Minor;

			/// <summary>
			/// Major.Minor
			/// </summary>
			/// <returns></returns>
			public override string ToString() {
				return Major + "." + Minor;
			}
		}

		[JsonProperty("domains")]
		public DomainJson[] Domains;

		[JsonProperty("version")]
		public VersionJson Version;

		/// <summary>
		/// Returns a JSON string that represents the current object.
		/// </summary>
		/// <returns>A JSON string.</returns>
		public override string ToString() {
			return JsonConvert.SerializeObject(
				this, Formatting.Indented
			);
		}
	}
}
