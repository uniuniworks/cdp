﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net;
using System.Threading;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using WebSocket4Net;

#if NET40
using System.Threading.Tasks;
#endif // NET40

namespace CDP {
	/// <summary>
	/// https://chromedevtools.github.io/devtools-protocol/
	/// </summary>
	public partial class HeadlessChrome : HeadlessChromeBase, IDisposable {
		/// <summary>
		/// /Applications/Google Chrome.app/Contents/MacOS/Google Chrome
		/// </summary>
		public static readonly string DefaultAppPathMac =
			"/Applications/Google Chrome.app" +
			"/Contents/MacOS/Google Chrome";

		/// <summary>
		/// /Applications/Firefox Nightly.app/Contents/MacOS/firefox
		/// </summary>
		/*
		public static readonly string DefaultFirefoxAppPathMac =
			"/Applications/Firefox Nightly.app" +
			"/Contents/MacOS/firefox";
		*/

		/// <summary>
		/// C:\Program Files (x86)\Google\Chrome\Application\chrome.exe
		/// </summary>
		public static readonly string DefaultAppPathWindows =
			@"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe";

		/// <summary>
		/// If this string is output from the chrome process,
		/// Start() method will be completed.
		/// </summary>
		public static string ProcessReadyString = "DevTools listening on";

		/// <summary>
		/// If this string is output from the chrome process,
		/// Start() method will be failed.
		/// </summary>
		public static string ProcessStartFailedString =
			"Cannot start http server for devtools";

		/// <summary>
		/// The request text and id.
		/// </summary>
		protected struct Request {
			/// <summary>
			/// The request id.
			/// </summary>
			public int Id;

			/// <summary>
			/// The request text.
			/// </summary>
			public string Text;
		}

		/// <summary>
		/// Occurs when the WebSocket is connected.
		/// </summary>
		public event EventHandler Connected;

		/// <summary>
		/// Occurs when the WebSocket receives an error.
		/// </summary>
		public event EventHandler<ErrorEventArgs> Error;

		/// <summary>
		/// Occurs when a log is added.
		/// </summary>
		public event EventHandler<LogAddedEventArgs> LogAdded;

		/// <summary>
		/// Timeout value for starting the process (milliseconds).
		/// </summary>
		public int StartTimeout = 5000;

		/// <summary>
		/// Timeout value for opening a WebSocket connection (milliseconds).
		/// </summary>
		public int OpenTimeout = 5000;

		/// <summary>
		/// Timeout value for sending a request (milliseconds).
		/// </summary>
		public int SendTimeout = 5000;

		/// <summary>
		/// Throw an exception when sending a request.
		/// </summary>
		public bool ThrowExceptionOnSend = false;

		/// <summary>
		/// The top frame id.
		/// </summary>
		public string FrameId {
			get { return _frameId; }
		}

		/// <summary>
		/// The delegate for the Send method.
		/// </summary>
		/// <param name="method"></param>
		/// <param name="params"></param>
		/// <returns></returns>
		protected delegate string SendFunc(string method, object @params = null);

		/// <summary>
		/// The callback used in Send method.
		/// </summary>
		/// <param name="response"></param>
		/// <param name="error"></param>
		protected delegate void SendCallback(string response, string error);

		/// <summary>
		/// The value of this constant is equivalent to 00:00:00.0000000 UTC,
		/// January 1, 1970, in the Gregorian calendar.
		/// UnixEpoch defines the point in time when Unix time is equal to 0.
		/// </summary>
		protected static readonly DateTime UnixEpoch
			= new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc);

		/// <summary>
		/// The chrome app path.
		/// </summary>
		protected readonly string _appPath;

		/// <summary>
		/// Used to send http requests to browsers.
		/// </summary>
		protected WebClient _webClient;

		/// <summary>
		/// Used to interact with the CDP protocol.
		/// </summary>
		protected WebSocket _webSocket;

		/// <summary>
		/// Used to launch a browser process.
		/// </summary>
		protected Process _process;

		/// <summary>
		/// Indicates whether the process has started.
		/// </summary>
		protected bool _started;

		/// <summary>
		/// Browser WebSocket port.
		/// </summary>
		protected int _port;

		/// <summary>
		/// To make the _nextRequestId thread-safe.
		/// </summary>
		protected static object _nextRequestIdLock;

		/// <summary>
		/// ID of the next request to send.
		/// </summary>
		protected static int _nextRequestId = 1;

		/// <summary>
		/// The session id.
		/// </summary>
		protected string _sessionId;

		/// <summary>
		/// The top frame id.
		/// </summary>
		protected string _frameId;

		/// <summary>
		/// Used to execute the Send() method asynchronously (APM).
		/// </summary>
		protected SendFunc _sendFunc;

		/// <summary>
		/// Indicates that the browser has been closed.
		/// </summary>
		protected AutoResetEvent _closeEvent;

		/// <summary>
		/// The URL displayed at the start of the process.
		/// </summary>
		protected string _webSocketDebuggerUrl;

		/// <summary>
		/// The list that associates the sent id with the method name.
		/// </summary>
		protected static Dictionary<int, string> _requestMethods;

		/// <summary>
		/// The list that associates the sent id with the callback.
		/// </summary>
		protected static Dictionary<int, SendCallback> _callbacks;

		protected static Dictionary<string, HeadlessChrome> _sessions;

		/// <summary>
		/// The static constructor.
		/// </summary>
		static HeadlessChrome() {
			_requestMethods = new Dictionary<int, string>();
			_callbacks = new Dictionary<int, SendCallback>();
			_nextRequestIdLock = new object();
			_sessions = new Dictionary<string, HeadlessChrome>();
		}

		/// <summary>
		/// Initializes a new instance of the HeadlessChrome class.
		/// </summary>
		/// <param name="appPath">The chrome app path.</param>
		public HeadlessChrome(string appPath) {
			if (appPath == null) {
				throw new ArgumentNullException(nameof(appPath));
			}
			_appPath = appPath;
			_webClient = new WebClient();
			_sendFunc = Send;
			_closeEvent = new AutoResetEvent(false);
#pragma warning disable RECS0021
			InitProtocols();
#pragma warning restore RECS0021
			// used for detect Visual Studio debugger stop
			AppDomain.CurrentDomain.ProcessExit += CurrentDomainProcessExit;
		}

		/// <summary>
		/// Converts a Unix time expressed as the number of seconds
		/// that have elapsed since 1970-01-01T00:00:00Z
		/// to a DateTime value.
		/// </summary>
		/// <param name="seconds">The elapsed seconds.</param>
		/// <returns>The Unix time in UTC.</returns>
		public static DateTime ConvertTime(double seconds) {
			return UnixEpoch.AddSeconds(seconds);
		}

		/// <summary>
		/// Releases the resources used by the HeadlessChrome.
		/// </summary>
		public void Dispose() {
			if (_process != null) {
				CloseProcess(_process);
				_process = null;
			}
			// used for detect Visual Studio debugger stop
			AppDomain.CurrentDomain.ProcessExit -= CurrentDomainProcessExit;
		}

		/// <summary>
		/// Starts the chrome process and opens a WebSocket.
		/// <para>
		/// The current thread blocks until the socket is opened.
		/// </para>
		/// </summary>
		/// <param name="port">The remote debugging port.</param>
		/// <param name="args">
		/// The command line args.
		/// <example>
		/// default value: 
		/// <code>
		/// new string[] {
		///		"--headless",
		///		"--disable-gpu",
		///		"--remote-debugging-port=" + port
		/// }
		/// </code>
		/// </example>
		/// </param>
		/// <returns>
		/// True if successful, otherwise false.
		/// </returns>
		public bool Start(int port = 0, params string[] args) {
			if (_started) {
				return false;
			}
			_started = true;
			_port = port;
			_webSocketDebuggerUrl = null;

			if (args == null || args.Length <= 0) {
				args = new string[] {
					"--headless",
					"--disable-gpu",
					"--remote-debugging-port=" + port
				};
			}
			_process = CreateProcess(_appPath, args);

			if (_process.Start() == false) {
				ProcessStartFailed();
				return false;
			}
			_process.BeginOutputReadLine();
			_process.BeginErrorReadLine();

			bool status = WaitForProcessReady(_process);
			if (status == false) {
				ProcessStartFailed();
				return false;
			}
			if (_webSocketDebuggerUrl == null) {
				AddLog("webSocketDebuggerUrl not found.");
				ProcessStartFailed();
				return false;
			}
			Delay(300);

			_webSocket = CreateWebSocket(_webSocketDebuggerUrl);
			OpenWebSocket(_webSocket);
			Send(
				"Target.setDiscoverTargets",
				new {
					discover = true
				}
			);
			string targetId = GetTarget();
			if (targetId == null) {
				AddLog("Target.getTargets failed.");
				ProcessStartFailed();
				return false;
			}
			_sessionId = OpenSession(targetId);
			_frameId = targetId;
			if (_sessions.ContainsKey(_sessionId) == false) {
				_sessions.Add(_sessionId, this);
			}
			return true;
		}

		/// <summary>
		/// Opens a new WebSocket connection.
		/// </summary>
		/// <param name="target">The TargetJson object.</param>
		/// <returns>The new HeadlessChrome instance.</returns>
		public HeadlessChrome Open(TargetJson target) {
			if (target == null) {
				throw new ArgumentNullException(nameof(target));
			}
			if (target.Id == null) {
				throw new NullReferenceException(nameof(target));
			}
			string sessionId = OpenSession(target.Id);
			if (string.IsNullOrEmpty(sessionId)) {
				throw new InvalidOperationException("Invalid target.");
			}

			HeadlessChrome chrome = new HeadlessChrome(_appPath);
			chrome._frameId = target.Id;
			//chrome._webSocket = chrome.CreateWebSocket(
				//_webSocketDebuggerUrl
				//target.WebSocketDebuggerUrl
			//);
			//chrome.OpenWebSocket(chrome._webSocket);
			chrome._webSocket = _webSocket;
			chrome._started = true;
			chrome._sessionId = sessionId;
			if (_sessions.ContainsKey(sessionId) == false) {
				_sessions.Add(sessionId, chrome);
			}
			return chrome;
		}

		/// <summary>
		/// Closes the WebSocket and the chrome process.
		/// </summary>
		public void Close() {
			if (_started == false) {
				return;
			}
			if (string.IsNullOrEmpty(_sessionId) == false) {
				if (_sessions.ContainsKey(_sessionId)) {
					_sessions.Remove(_sessionId);
				}
			}
			AddLog("{0}.Close()", nameof(HeadlessChrome));
			if (_webSocket == null) {
				CloseProcess(_process);
				_process = null;
				_started = false;
				_closeEvent.Set();
				return;
			}
			if (_webSocket.State == WebSocketState.Open) {
				Send("Browser.close");
				Delay(200);
				_webSocket.Close();
				_webSocket = null;
			}
			CloseProcess(_process);
			_process = null;
			_started = false;
			_closeEvent.Set();
		}

		/// <summary>
		/// Blocks the current thread until the WebSocket is closed.
		/// </summary>
		public void WaitForClose() {
			if (_started == false) {
				return;
			}
			_closeEvent.WaitOne();
			_closeEvent.Reset();
		}

		/// <summary>
		/// Delays execution for a specified time.
		/// </summary>
		/// <param name="milliseconds"></param>
		public void Delay(int milliseconds) {
			Thread.Sleep(milliseconds);
		}

		/// <summary>
		/// Adds a log message.
		/// The log message is sent to the LogAdded event.
		/// </summary>
		/// <param name="format"></param>
		/// <param name="args"></param>
		public void AddLog(string format, params object[] args) {
			if (LogAdded == null) {
				return;
			}
			if (args == null || args.Length <= 0) {
				LogAdded(this, new LogAddedEventArgs(format));
				return;
			}
			LogAdded(this, new LogAddedEventArgs(
				string.Format(format, args)
			));
		}

		/// <summary>
		/// Sends a CDP request via WebSocket.
		/// <example>
		/// usage:
		/// <code>
		/// chrome.Send("Domain.method", new {
		///		param1 = param1,
		///		param2 = param2
		///	});
		/// </code>
		/// </example>
		/// </summary>
		/// <exception cref="ArgumentNullException">
		/// The method parameter is null.
		/// </exception>
		/// <exception cref="TimeoutException">
		/// Only occurs when ThrowExceptionOnSend is true.
		/// </exception>
		/// <exception cref="InvalidOperationException">
		/// Only occurs when ThrowExceptionOnSend is true,
		/// when an error is returned from the browser.
		/// </exception>
		/// <param name="method">The CDP method name.</param>
		/// <param name="params">The parameters specified in the method.</param>
		/// <returns></returns>
		public string Send(string method, object @params = null) {
			if (method == null) {
				throw new ArgumentNullException(nameof(method));
			}
			CheckWebSocketIsOpen();
			Request r = CreateRequest(method, @params);
			AddLog("{0}.Send(): {1}", nameof(HeadlessChrome), r.Text);
			string result = null;
			string errorResult = null;
			AutoResetEvent autoResetEvent = new AutoResetEvent(false);
			using (autoResetEvent) {
				_callbacks.Add(r.Id, (response, error) => {
					result = response;
					errorResult = error;
					autoResetEvent.Set();
				});
				_webSocket.Send(r.Text);
				bool waitResult = autoResetEvent.WaitOne(SendTimeout);
				autoResetEvent.Close();
				//autoResetEvent.Dispose();
				if (waitResult == false) {
					if (ThrowExceptionOnSend) {
						throw CreateTimeoutException(
							"Send",
							"request = " + r.Text
						);
					}
					return null;
				}
				if (errorResult != null) {
					if (ThrowExceptionOnSend) {
						throw new InvalidOperationException(
							errorResult
						);
					}
					return null;
				}
			}
			return result;
		}

		/// <summary>
		/// Sends a CDP request asynchronously via WebSocket.
		/// <example>
		/// usage:
		/// <code>
		/// chrome.BeginSend("Domain.method");
		/// </code>
		/// </example>
		/// </summary>
		/// <exception cref="ArgumentNullException">
		/// The method parameter is null.
		/// </exception>
		/// <param name="method">The CDP method name.</param>
		/// <param name="callback">The AsyncCallback delegate.</param>
		/// <param name="state">
		/// An object that contains state information for this request.
		/// </param>
		/// <returns></returns>
		public IAsyncResult BeginSend(
			string method, AsyncCallback callback, object state) {
			if (method == null) {
				throw new ArgumentNullException(nameof(method));
			}
			return _sendFunc.BeginInvoke(
				method, null, callback, state
			);
		}

		/// <summary>
		/// Sends a CDP request asynchronously via WebSocket.
		/// <example>
		/// usage:
		/// <code>
		/// chrome.BeginSend("Domain.method", new {
		///		param1 = param1,
		///		param2 = param2
		///	});
		/// </code>
		/// </example>
		/// </summary>
		/// <exception cref="ArgumentNullException">
		/// The method parameter is null.
		/// </exception>
		/// <param name="method">The CDP method name.</param>
		/// <param name="params">The parameters specified in the method.</param>
		/// <param name="callback">The AsyncCallback delegate.</param>
		/// <param name="state">
		/// An object that contains state information for this request.
		/// </param>
		/// <returns></returns>
		public IAsyncResult BeginSend(
			string method, object @params,
			AsyncCallback callback, object state)
		{
			if (method == null) {
				throw new ArgumentNullException(nameof(method));
			}
			return _sendFunc.BeginInvoke(
				method, @params, callback, state
			);
		}

		/// <summary>
		/// Ends a pending asynchronous send request.
		/// </summary>
		/// <exception cref="ArgumentNullException">
		/// The result parameter is null.
		/// </exception>
		/// <param name="result">
		/// An IAsyncResult that stores state information and any user defined
		/// data for this asynchronous operation.
		/// </param>
		/// <returns>The string returned via WebSocket.</returns>
		public string EndSend(IAsyncResult result) {
			if (result == null) {
				throw new ArgumentNullException(nameof(result));
			}
			return _sendFunc.EndInvoke(result);
		}

#if NET40
		/// <summary>
		/// Sends a CDP request asynchronously via WebSocket.
		/// <example>
		/// usage:
		/// <code>
		/// var task = chrome.SendAsync("Domain.method", new {
		///		param1 = param1,
		///		param2 = param2
		///	});
		///	string result = task.Result;
		/// </code>
		/// </example>
		/// </summary>
		/// <exception cref="ArgumentNullException">
		/// The method parameter is null.
		/// </exception>
		/// <param name="method">The CDP method name.</param>
		/// <param name="params">The parameters specified in the method.</param>
		/// <returns>
		/// The task object representing the asynchronous operation.
		/// </returns>
		public Task<string> SendAsync(
			string method, object @params = null)
		{
			if (method == null) {
				throw new ArgumentNullException(nameof(method));
			}
			Task<string> task = new Task<string>(() => {
				return Send(method, @params);
			});
			task.Start();
			return task;
		}
#endif // NET40

		/// <summary>
		/// Returns a browser version metadata.
		/// <para>
		/// This method sends a http request to
		/// http://localhost:{port}/json/version
		/// </para>
		/// </summary>
		/// <returns>VersionJson</returns>
		public VersionJson GetVersion() {
			string s = DownloadString(
				string.Format(
					"http://localhost:{0}/json/version",
					_port
				)
			);
			//AddLog("GetVersion: " + s);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			return JsonConvert.DeserializeObject<VersionJson>(s);
		}

		/// <summary>
		/// Returns a list of all available websocket targets.
		/// <para>
		/// This method sends a http request to
		/// http://localhost:{port}/json
		/// </para>
		/// </summary>
		/// <returns></returns>
		public TargetJson[] GetTargets() {
			string s = DownloadString(
				string.Format(
					"http://localhost:{0}/json",
					_port
				)
			);
			//AddLog("GetTargets: " + s);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			return JsonConvert.DeserializeObject<TargetJson[]>(s);
		}

		/// <summary>
		/// Returns the current devtools protocol.
		/// <para>
		/// This method sends a http request to
		/// http://localhost:{port}/json/protocol/
		/// </para>
		/// </summary>
		/// <returns></returns>
		public ProtocolJson GetProtocol() {
			string s = DownloadString(
				string.Format(
					"http://localhost:{0}/json/protocol/",
					_port
				)
			);
			//AddLog("GetProtocol: " + s);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JsonSerializerSettings settings = new JsonSerializerSettings();
			//settings.MaxDepth = 100;
			settings.ObjectCreationHandling = ObjectCreationHandling.Replace;
			return JsonConvert.DeserializeObject<ProtocolJson>(s, settings);
		}

		/// <summary>
		/// Opens a new tab.
		/// Responds with the websocket target data for the new tab.
		/// <para>
		/// This method sends a http request to
		/// http://localhost:{port}/json/new?{url}
		/// </para>
		/// </summary>
		/// <param name="url"></param>
		/// <returns>
		/// The websocket target data for the new tab.
		/// </returns>
		public TargetJson OpenNewTab(string url) {
			string s = DownloadString(
				string.Format(
					"http://localhost:{0}/json/new?{1}",
					_port,
					Uri.EscapeDataString(url)
				)
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			return JsonConvert.DeserializeObject<TargetJson>(s);
		}

		/// <summary>
		/// Brings a page into the foreground (activate a tab).
		/// <para>
		/// This method sends a http request to
		/// http://localhost:{port}/json/activate/{targetId}
		/// </para>
		/// </summary>
		/// <param name="targetId"></param>
		/// <returns>
		/// For valid targets, the response is 200: "Target activated".
		/// If the target is invalid,
		/// the response is 404: "No such target id: {targetId}"
		/// </returns>
		public string ActivateTab(string targetId) {
			try {
				return _webClient.DownloadString(
					string.Format(
						"http://localhost:{0}/json/activate/{1}",
						_port,
						targetId
					)
				);
			} catch (Exception e) {
				return e.Message;
			}
		}

		/// <summary>
		/// Closes the target page identified by targetId.
		/// <para>
		/// This method sends a http request to
		/// http://localhost:{port}/json/close/{targetId}
		/// </para>
		/// </summary>
		/// <param name="targetId"></param>
		/// <returns>
		/// For valid targets, the response is 200: "Target is closing".
		/// If the target is invalid,
		/// the response is 404: "No such target id: {targetId}"
		/// </returns>
		public string ClosePage(string targetId) {
			try {
				return _webClient.DownloadString(
					string.Format(
						"http://localhost:{0}/json/close/{1}",
						_port,
						targetId
					)
				);
			} catch (Exception e) {
				return e.Message;
			}
		}

		/// <summary>
		/// Returns a inspector url.
		/// </summary>
		/// <returns>
		/// A copy of the DevTools frontend that ship with Chrome.
		/// </returns>
		public string GetInspectorUrl() {
			return string.Format(
				"http://localhost:{0}/devtools/inspector.html",
				_port
			);
		}

		protected Process CreateProcess(string fileName, string[] args) {
			Process process = new Process();
			ProcessStartInfo startInfo = process.StartInfo;
			startInfo.UseShellExecute = false;
			startInfo.RedirectStandardInput = false;
			startInfo.RedirectStandardOutput = true;
			startInfo.RedirectStandardError = true;
			startInfo.CreateNoWindow = true;
			//_startInfo.WorkingDirectory = "..";

			startInfo.FileName = fileName;
			startInfo.Arguments = string.Join(" ", args);

			process.Exited += ProcessExited;
			process.OutputDataReceived += ProcessDataReceived;
			process.ErrorDataReceived += ProcessDataReceived;
			return process;
		}

		protected WebSocket CreateWebSocket(string url) {
			WebSocket webSocket = new WebSocket(url);
			webSocket.Opened += WebSocketOpened;
			webSocket.Closed += WebSocketClosed;
			webSocket.MessageReceived += WebSocketMessageReceived;
			//webSocket.Error += (sender, e) => {
			//	AddLog(e.Exception.ToString());
			//};
			return webSocket;
		}

		protected int GetNextRequestId() {
			int id;
			lock (_nextRequestIdLock) {
				id = _nextRequestId;
				_nextRequestId++;
				if (_nextRequestId >= int.MaxValue) {
					_nextRequestId = 1;
				}
			}
			return id;
		}

		protected Request CreateRequest(string method, object @params = null) {
			Request r = new Request();
			int id = GetNextRequestId();
			r.Id = id;
			RequestJson request = new RequestJson(
				id, method, @params
			);
			request.SessionId = _sessionId;
			_requestMethods.Add(id, method);
			r.Text = request.ToString();
			return r;
		}

		protected TimeoutException CreateTimeoutException(
			string method, string message = null) {
			if (string.IsNullOrEmpty(message)) {
				return new TimeoutException(string.Format(
					"{0}.{1}() timeout.",
					nameof(HeadlessChrome),
					method
				));
			}
			return new TimeoutException(string.Format(
				"{0}.{1}() timeout. {2}",
				nameof(HeadlessChrome),
				method,
				message
			));
		}

		protected void CloseProcess(Process process) {
#pragma warning disable RECS0022
			if (process == null) {
				return;
			}
			try {
				if (process.HasExited == false) {
					try {
						process.CloseMainWindow();
					} catch (Exception) {
						/*
						AddLog(
							"{0}.CloseProcess(): {1}",
							nameof(HeadlessChrome),
							e.Message
						);
						*/
					}
				}
				process.Exited -= ProcessExited;
				process.OutputDataReceived -= ProcessDataReceived;
				process.ErrorDataReceived -= ProcessDataReceived;
				process.Close();
				process.Dispose();
			} catch (Exception) {
			}
#pragma warning restore RECS0022
		}

		protected void OpenWebSocket(WebSocket webSocket) {
			AutoResetEvent autoResetEvent = new AutoResetEvent(false);
			EventHandler opened = null;
			opened = (sender, e) => {
				if (webSocket != null) {
					webSocket.Opened -= opened;
				}
				autoResetEvent.Set();
			};
			webSocket.Opened += opened;
			webSocket.Open();
			bool waitResult = autoResetEvent.WaitOne(OpenTimeout);
			autoResetEvent.Close();
			if (waitResult == false) {
				throw CreateTimeoutException("OpenWebSocket");
			}
			Delay(100);
		}

		protected void CheckWebSocketIsOpen() {
			if (_started &&
				_webSocket != null &&
				_webSocket.State == WebSocketState.Open) {
				return;
			}
			throw new InvalidOperationException(
				"WebSocket is not open."
			);
		}

		protected string GetTarget() {
			string json = Send("Target.getTargets");
			if (json == null) {
				return null;
			}
			JObject targets = JObject.Parse(json);
			JToken infos = targets.SelectToken(
				"result.targetInfos"
			);
			if (infos == null) {
				return null;
			}
			foreach (JToken info in infos.AsJEnumerable()) {
				string type = info.Value<string>("type");
				if (type == "page") {
					return info.Value<string>("targetId");
				}
			}
			return null;
		}

		protected string OpenSession(string targetId) {
			if (string.IsNullOrEmpty(targetId)) {
				return null;
			}
			string json = Send(
				"Target.attachToTarget",
				new {
					targetId,
					flatten = true
				}
			);
			if (json == null) {
				return null;
			}
			JObject jObject = JObject.Parse(json);
			if (jObject == null) {
				return null;
			}
			JToken token = jObject.SelectToken(
				"result.sessionId"
			);
			if (token == null) {
				return null;
			}
			return token.ToString();
		}

		protected void ProcessStartFailed() {
			AddLog("Process.Start() failed.");
			CloseProcess(_process);
			_process = null;
			_started = false;
			_closeEvent.Set();
		}

		protected bool WaitForProcessReady(Process process) {
			AutoResetEvent autoResetEvent = new AutoResetEvent(false);
			bool status = false;
			DataReceivedEventHandler handler = (sender, e) => {
				if (string.IsNullOrEmpty(e.Data)) {
					return;
				}
				if (e.Data.Contains(ProcessReadyString)) {
					status = true;
					int index = e.Data.IndexOf(
						"ws:", StringComparison.CurrentCulture
					);
					if (index >= 0) {
						_webSocketDebuggerUrl = e.Data.Substring(index);
						_port = new Uri(_webSocketDebuggerUrl).Port;
					}
					autoResetEvent.Set();
					return;
				}
				if (e.Data.Contains(ProcessStartFailedString)) {
					status = false;
					autoResetEvent.Set();
					return;
				}
			};
			process.OutputDataReceived += handler;
			process.ErrorDataReceived += handler;

			using (autoResetEvent) {
				bool waitResult = autoResetEvent.WaitOne(StartTimeout);
				process.OutputDataReceived -= handler;
				process.ErrorDataReceived -= handler;
				autoResetEvent.Close();
				if (waitResult == false) {
					throw CreateTimeoutException("Start");
				}
			}
			return status;
		}

		protected string DownloadString(string url) {
			try {
				return _webClient.DownloadString(url);
			} catch (Exception e) {
				AddLog(
					"{0}.DownloadString: {1}",
					nameof(HeadlessChrome),
					e.Message
				);
				return null;
			}
		}

		protected void ThrowWebSocketIdError(string message) {
			message = "WebSocket.MessageReceived: id error: " + message;
			AddLog(message);
			throw new InvalidOperationException(message);
		}

		protected int GetId(JObject json) {
			JToken idToken = json.SelectToken("id");
			if (idToken == null) {
				return -1;
			}
			//AddLog("WebSocket.MessageReceived: id = {0}", idToken.ToString());
			if (int.TryParse(idToken.ToString(), out int id) == false) {
				return -1;
			}
			return id;
		}

		protected void CurrentDomainProcessExit(object sender, EventArgs e) {
			Dispose();
		}

		protected void ProcessExited(object sender, EventArgs e) {
			AddLog("Process.Exited");
			Close();
		}

		protected void ProcessDataReceived(
			object sender, System.Diagnostics.DataReceivedEventArgs e) {
			if (string.IsNullOrEmpty(e.Data)) {
				return;
			}
			AddLog("Process.DataReceived: " + e.Data);
		}

		protected void WebSocketOpened(object sender, EventArgs e) {
			AddLog("WebSocket.Opened");
			if (Connected != null) {
				Connected(this, EventArgs.Empty);
			}
		}

		protected void WebSocketClosed(object sender, EventArgs e) {
			AddLog("WebSocket.Closed");
		}

		protected void WebSocketMessageReceived(
			object sender, MessageReceivedEventArgs e)
		{
			AddLog("WebSocket.MessageReceived: {0}", e.Message);
			JObject json = JObject.Parse(e.Message);
			JToken methodToken = json.SelectToken("method");
			if (methodToken != null) {
				JToken sessionIdToken = json.SelectToken("sessionId");
				JToken paramsToken = json.SelectToken("params");
				if (sessionIdToken == null) {
					ThreadPool.QueueUserWorkItem((state) => {
						OnEventReceived(methodToken, paramsToken);
					});
					return;
				}
				string sessionId = sessionIdToken.ToString();
				if (_sessions.ContainsKey(sessionId) == false) {
					return;
				}
				HeadlessChrome chrome = _sessions[sessionId];
				ThreadPool.QueueUserWorkItem((state) => {
					chrome.OnEventReceived(methodToken, paramsToken);
				});
				return;
			}

			int id = GetId(json);
			if (_callbacks.ContainsKey(id) == false) {
				ThrowWebSocketIdError(e.Message);
				return;
			}
			SendCallback callback = _callbacks[id];
			_callbacks.Remove(id);

			string method = string.Empty;
			if (_requestMethods.ContainsKey(id)) {
				method = _requestMethods[id];
				_requestMethods.Remove(id);
			}

			JToken errorToken = json.SelectToken("error");
			if (errorToken != null) {
				AddLog("WebSocket.MessageReceived: Error {0}", e.Message);
				ErrorJson errorJson = JsonConvert.DeserializeObject
					<ErrorJson>(errorToken.ToString());
				if (Error != null) {
					Error(this, new ErrorEventArgs(method, errorJson));
				}
				callback(null, errorJson.ToString());
				//throw new InvalidOperationException(message);
				return;
			}
			callback(e.Message, null);
		}
	}
}
