// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// 
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: false</description></item>
	/// <item><description>dependencies: Runtime, Debugger</description></item>
	/// </list>
	/// </summary>
	public class ProfilerDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the ProfilerDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public ProfilerDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ConsoleProfileFinishedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("id")]
			public string Id;

			/// <summary>
			/// Location of console.profileEnd().
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Debugger.Location</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("location")]
			public DebuggerDomain.Location Location;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Profile</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("profile")]
			public Profile Profile;

			/// <summary>
			/// Profile title passed as an argument to console.profile().
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("title")]
			public string Title;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ConsoleProfileFinishedEventArgs> ConsoleProfileFinished;

		/// <summary>
		/// Sent when new profile recording is started using console.profile() call.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ConsoleProfileStartedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("id")]
			public string Id;

			/// <summary>
			/// Location of console.profile().
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Debugger.Location</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("location")]
			public DebuggerDomain.Location Location;

			/// <summary>
			/// Profile title passed as an argument to console.profile().
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("title")]
			public string Title;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Sent when new profile recording is started using console.profile() call.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ConsoleProfileStartedEventArgs> ConsoleProfileStarted;

		/// <summary>
		/// Profile node. Holds callsite information, execution statistics and child nodes.
		/// </summary>
		[JsonObject]
		public class ProfileNode {
			/// <summary>
			/// Unique id of the node.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("id")]
			public int Id;

			/// <summary>
			/// Function location.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.CallFrame</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("callFrame")]
			public RuntimeDomain.CallFrame CallFrame;

			/// <summary>
			/// Number of samples where this node was on top of the call stack.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("hitCount")]
			public int HitCount;

			/// <summary>
			/// Child node ids.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("children")]
			public int[] Children;

			/// <summary>
			/// The reason of being not optimized. The function may be deoptimized or marked as don't optimize.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("deoptReason")]
			public string DeoptReason;

			/// <summary>
			/// An array of source position ticks.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("positionTicks")]
			public PositionTickInfo[] PositionTicks;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Profile.
		/// </summary>
		[JsonObject]
		public class Profile {
			/// <summary>
			/// The list of profile nodes. First item is the root node.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodes")]
			public ProfileNode[] Nodes;

			/// <summary>
			/// Profiling start timestamp in microseconds.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("startTime")]
			public double StartTime;

			/// <summary>
			/// Profiling end timestamp in microseconds.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("endTime")]
			public double EndTime;

			/// <summary>
			/// Ids of samples top nodes.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("samples")]
			public int[] Samples;

			/// <summary>
			/// Time intervals between adjacent samples in microseconds. The first delta is relative to the profile startTime.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timeDeltas")]
			public int[] TimeDeltas;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Specifies a number of samples attributed to a certain source position.
		/// </summary>
		[JsonObject]
		public class PositionTickInfo {
			/// <summary>
			/// Source line number (1-based).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("line")]
			public int Line;

			/// <summary>
			/// Number of samples attributed to the source line.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("ticks")]
			public int Ticks;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Coverage data for a source range.
		/// </summary>
		[JsonObject]
		public class CoverageRange {
			/// <summary>
			/// JavaScript script source offset for the range start.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("startOffset")]
			public int StartOffset;

			/// <summary>
			/// JavaScript script source offset for the range end.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("endOffset")]
			public int EndOffset;

			/// <summary>
			/// Collected execution count of the source range.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("count")]
			public int Count;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Coverage data for a JavaScript function.
		/// </summary>
		[JsonObject]
		public class FunctionCoverage {
			/// <summary>
			/// JavaScript function name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("functionName")]
			public string FunctionName;

			/// <summary>
			/// Source ranges inside the function with coverage data.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("ranges")]
			public CoverageRange[] Ranges;

			/// <summary>
			/// Whether coverage data for this function has block granularity.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("isBlockCoverage")]
			public bool IsBlockCoverage;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Coverage data for a JavaScript script.
		/// </summary>
		[JsonObject]
		public class ScriptCoverage {
			/// <summary>
			/// JavaScript script id.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.ScriptId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scriptId")]
			public string ScriptId;

			/// <summary>
			/// JavaScript script name or url.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Functions contained in the script that has coverage data.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("functions")]
			public FunctionCoverage[] Functions;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Describes a type collected during runtime.
		/// </summary>
		[JsonObject]
		public class TypeObject {
			/// <summary>
			/// Name of a type collected with type profiling.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Source offset and types for a parameter or return value.
		/// </summary>
		[JsonObject]
		public class TypeProfileEntry {
			/// <summary>
			/// Source offset of the parameter or end of function for return values.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("offset")]
			public int Offset;

			/// <summary>
			/// The types for this parameter or return value.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("types")]
			public TypeObject[] Types;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Type profile data collected during runtime for a JavaScript script.
		/// </summary>
		[JsonObject]
		public class ScriptTypeProfile {
			/// <summary>
			/// JavaScript script id.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.ScriptId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scriptId")]
			public string ScriptId;

			/// <summary>
			/// JavaScript script name or url.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Type profile entries for parameters and return values of the functions in the script.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("entries")]
			public TypeProfileEntry[] Entries;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"Profiler.disable"
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"Profiler.enable"
			);
		}

		/// <summary>
		/// Collect coverage data for the current isolate. The coverage data may be incomplete due to garbage collection.
		/// </summary>
		[JsonObject]
		public class GetBestEffortCoverageResult {
			/// <summary>
			/// Coverage data for the current isolate.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("result")]
			public ScriptCoverage[] Result;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Collect coverage data for the current isolate. The coverage data may be incomplete due to garbage collection.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public GetBestEffortCoverageResult GetBestEffortCoverage() {
			string s = _chrome.Send(
				"Profiler.getBestEffortCoverage"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetBestEffortCoverageResult>();
		}

		/// <summary>
		/// Changes CPU profiler sampling interval. Must be called before CPU profiles recording started.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="interval">New sampling interval in microseconds.</param>
		public void SetSamplingInterval(int interval) {
			_chrome.Send(
				"Profiler.setSamplingInterval",
				new {
					interval
				}
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Start() {
			_chrome.Send(
				"Profiler.start"
			);
		}

		/// <summary>
		/// Enable precise code coverage. Coverage data for JavaScript executed before enabling precise code coverage may be incomplete. Enabling prevents running optimized code and resets execution counters.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="callCount">Collect accurate call counts beyond simple 'covered' or 'not covered'.</param>
		/// <param name="detailed">Collect block-based coverage.</param>
		public void StartPreciseCoverage(bool? callCount = null, bool? detailed = null) {
			_chrome.Send(
				"Profiler.startPreciseCoverage",
				new {
					callCount,
					detailed
				}
			);
		}

		/// <summary>
		/// Enable type profile.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public void StartTypeProfile() {
			_chrome.Send(
				"Profiler.startTypeProfile"
			);
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class StopResult {
			/// <summary>
			/// Recorded profile.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Profile</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("profile")]
			public Profile Profile;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public StopResult Stop() {
			string s = _chrome.Send(
				"Profiler.stop"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<StopResult>();
		}

		/// <summary>
		/// Disable precise code coverage. Disabling releases unnecessary execution count records and allows executing optimized code.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void StopPreciseCoverage() {
			_chrome.Send(
				"Profiler.stopPreciseCoverage"
			);
		}

		/// <summary>
		/// Disable type profile. Disabling releases type profile data collected so far.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public void StopTypeProfile() {
			_chrome.Send(
				"Profiler.stopTypeProfile"
			);
		}

		/// <summary>
		/// Collect coverage data for the current isolate, and resets execution counters. Precise code coverage needs to have started.
		/// </summary>
		[JsonObject]
		public class TakePreciseCoverageResult {
			/// <summary>
			/// Coverage data for the current isolate.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("result")]
			public ScriptCoverage[] Result;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Collect coverage data for the current isolate, and resets execution counters. Precise code coverage needs to have started.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public TakePreciseCoverageResult TakePreciseCoverage() {
			string s = _chrome.Send(
				"Profiler.takePreciseCoverage"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<TakePreciseCoverageResult>();
		}

		/// <summary>
		/// Collect type profile.
		/// </summary>
		[JsonObject]
		public class TakeTypeProfileResult {
			/// <summary>
			/// Type profile for all scripts since startTypeProfile() was turned on.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("result")]
			public ScriptTypeProfile[] Result;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Collect type profile.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public TakeTypeProfileResult TakeTypeProfile() {
			string s = _chrome.Send(
				"Profiler.takeTypeProfile"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<TakeTypeProfileResult>();
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("Profiler.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "consoleProfileFinished":
					if (ConsoleProfileFinished == null) {
						return;
					}
					ConsoleProfileFinished(
						_chrome,
						JsonConvert.DeserializeObject
							<ConsoleProfileFinishedEventArgs>(@params.ToString())
					);
					break;
				case "consoleProfileStarted":
					if (ConsoleProfileStarted == null) {
						return;
					}
					ConsoleProfileStarted(
						_chrome,
						JsonConvert.DeserializeObject
							<ConsoleProfileStartedEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
