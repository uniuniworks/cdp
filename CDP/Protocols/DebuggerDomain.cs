// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// Debugger domain exposes JavaScript debugging capabilities. It allows setting and removing breakpoints, stepping through execution, exploring stack traces, etc.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: false</description></item>
	/// <item><description>dependencies: Runtime</description></item>
	/// </list>
	/// </summary>
	public class DebuggerDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the DebuggerDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public DebuggerDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Fired when breakpoint is resolved to an actual script and location.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class BreakpointResolvedEventArgs : EventArgs {
			/// <summary>
			/// Breakpoint unique identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: BreakpointId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("breakpointId")]
			public string BreakpointId;

			/// <summary>
			/// Actual breakpoint location.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Location</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("location")]
			public Location Location;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when breakpoint is resolved to an actual script and location.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<BreakpointResolvedEventArgs> BreakpointResolved;

		/// <summary>
		/// Fired when the virtual machine stopped on breakpoint or exception or any other stop criteria.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class PausedEventArgs : EventArgs {
			/// <summary>
			/// Call stack the virtual machine stopped on.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("callFrames")]
			public CallFrame[] CallFrames;

			/// <summary>
			/// Pause reason.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: ambiguous, assert, debugCommand, DOM, EventListener, exception, instrumentation, OOM, other, promiseRejection, XHR
			/// </summary>
			[JsonProperty("reason")]
			public string Reason;

			/// <summary>
			/// Object containing break-specific auxiliary properties.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: object</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("data")]
			public object Data;

			/// <summary>
			/// Hit breakpoints IDs
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("hitBreakpoints")]
			public string[] HitBreakpoints;

			/// <summary>
			/// Async stack trace, if any.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Runtime.StackTrace</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("asyncStackTrace")]
			public RuntimeDomain.StackTrace AsyncStackTrace;

			/// <summary>
			/// Async stack trace, if any.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Runtime.StackTraceId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("asyncStackTraceId")]
			public RuntimeDomain.StackTraceId AsyncStackTraceId;

			/// <summary>
			/// Never present, will be removed.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Runtime.StackTraceId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("asyncCallStackTraceId")]
			public RuntimeDomain.StackTraceId AsyncCallStackTraceId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when the virtual machine stopped on breakpoint or exception or any other stop criteria.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<PausedEventArgs> Paused;

		/// <summary>
		/// Fired when the virtual machine resumed execution.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler Resumed;

		/// <summary>
		/// Fired when virtual machine fails to parse the script.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ScriptFailedToParseEventArgs : EventArgs {
			/// <summary>
			/// Identifier of the script parsed.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.ScriptId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scriptId")]
			public string ScriptId;

			/// <summary>
			/// URL or name of the script parsed (if any).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Line offset of the script within the resource with given URL (for script tags).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("startLine")]
			public int StartLine;

			/// <summary>
			/// Column offset of the script within the resource with given URL.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("startColumn")]
			public int StartColumn;

			/// <summary>
			/// Last line of the script.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("endLine")]
			public int EndLine;

			/// <summary>
			/// Length of the last line of the script.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("endColumn")]
			public int EndColumn;

			/// <summary>
			/// Specifies script creation context.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.ExecutionContextId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("executionContextId")]
			public int ExecutionContextId;

			/// <summary>
			/// Content hash of the script.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("hash")]
			public string Hash;

			/// <summary>
			/// Embedder-specific auxiliary data.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: object</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("executionContextAuxData")]
			public object ExecutionContextAuxData;

			/// <summary>
			/// URL of source map associated with script (if any).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sourceMapURL")]
			public string SourceMapURL;

			/// <summary>
			/// True, if this script has sourceURL.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("hasSourceURL")]
			public bool HasSourceURL;

			/// <summary>
			/// True, if this script is ES6 module.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("isModule")]
			public bool IsModule;

			/// <summary>
			/// This script length.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("length")]
			public int Length;

			/// <summary>
			/// JavaScript top stack frame of where the script parsed event was triggered if available.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Runtime.StackTrace</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("stackTrace")]
			public RuntimeDomain.StackTrace StackTrace;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when virtual machine fails to parse the script.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ScriptFailedToParseEventArgs> ScriptFailedToParse;

		/// <summary>
		/// Fired when virtual machine parses script. This event is also fired for all known and uncollected scripts upon enabling debugger.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ScriptParsedEventArgs : EventArgs {
			/// <summary>
			/// Identifier of the script parsed.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.ScriptId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scriptId")]
			public string ScriptId;

			/// <summary>
			/// URL or name of the script parsed (if any).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Line offset of the script within the resource with given URL (for script tags).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("startLine")]
			public int StartLine;

			/// <summary>
			/// Column offset of the script within the resource with given URL.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("startColumn")]
			public int StartColumn;

			/// <summary>
			/// Last line of the script.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("endLine")]
			public int EndLine;

			/// <summary>
			/// Length of the last line of the script.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("endColumn")]
			public int EndColumn;

			/// <summary>
			/// Specifies script creation context.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.ExecutionContextId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("executionContextId")]
			public int ExecutionContextId;

			/// <summary>
			/// Content hash of the script.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("hash")]
			public string Hash;

			/// <summary>
			/// Embedder-specific auxiliary data.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: object</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("executionContextAuxData")]
			public object ExecutionContextAuxData;

			/// <summary>
			/// True, if this script is generated as a result of the live edit operation.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("isLiveEdit")]
			public bool IsLiveEdit;

			/// <summary>
			/// URL of source map associated with script (if any).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sourceMapURL")]
			public string SourceMapURL;

			/// <summary>
			/// True, if this script has sourceURL.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("hasSourceURL")]
			public bool HasSourceURL;

			/// <summary>
			/// True, if this script is ES6 module.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("isModule")]
			public bool IsModule;

			/// <summary>
			/// This script length.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("length")]
			public int Length;

			/// <summary>
			/// JavaScript top stack frame of where the script parsed event was triggered if available.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Runtime.StackTrace</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("stackTrace")]
			public RuntimeDomain.StackTrace StackTrace;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when virtual machine parses script. This event is also fired for all known and uncollected scripts upon enabling debugger.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ScriptParsedEventArgs> ScriptParsed;

		/// <summary>
		/// Location in the source code.
		/// </summary>
		[JsonObject]
		public class Location {
			/// <summary>
			/// Script identifier as reported in the `Debugger.scriptParsed`.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.ScriptId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scriptId")]
			public string ScriptId;

			/// <summary>
			/// Line number in the script (0-based).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("lineNumber")]
			public int LineNumber;

			/// <summary>
			/// Column number in the script (0-based).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("columnNumber")]
			public int ColumnNumber;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Location in the source code.
		/// </summary>
		[JsonObject]
		public class ScriptPosition {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("lineNumber")]
			public int LineNumber;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("columnNumber")]
			public int ColumnNumber;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// JavaScript call frame. Array of call frames form the call stack.
		/// </summary>
		[JsonObject]
		public class CallFrame {
			/// <summary>
			/// Call frame identifier. This identifier is only valid while the virtual machine is paused.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: CallFrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("callFrameId")]
			public string CallFrameId;

			/// <summary>
			/// Name of the JavaScript function called on this call frame.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("functionName")]
			public string FunctionName;

			/// <summary>
			/// Location in the source code.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Location</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("functionLocation")]
			public Location FunctionLocation;

			/// <summary>
			/// Location in the source code.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Location</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("location")]
			public Location Location;

			/// <summary>
			/// JavaScript script name or url.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Scope chain for this call frame.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scopeChain")]
			public Scope[] ScopeChain;

			/// <summary>
			/// `this` object for this call frame.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("this")]
			public RuntimeDomain.RemoteObject This;

			/// <summary>
			/// The value being returned, if the function is at return point.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Runtime.RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("returnValue")]
			public RuntimeDomain.RemoteObject ReturnValue;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Scope description.
		/// </summary>
		[JsonObject]
		public class Scope {
			/// <summary>
			/// Scope type.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: global, local, with, closure, catch, block, script, eval, module
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// Object representing the scope. For `global` and `with` scopes it represents the actual object; for the rest of the scopes, it is artificial transient object enumerating scope variables as its properties.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("object")]
			public RuntimeDomain.RemoteObject Object;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// Location in the source code where scope starts
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Location</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("startLocation")]
			public Location StartLocation;

			/// <summary>
			/// Location in the source code where scope ends
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Location</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("endLocation")]
			public Location EndLocation;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Search match for resource.
		/// </summary>
		[JsonObject]
		public class SearchMatch {
			/// <summary>
			/// Line number in resource content.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("lineNumber")]
			public double LineNumber;

			/// <summary>
			/// Line with match content.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("lineContent")]
			public string LineContent;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class BreakLocation {
			/// <summary>
			/// Script identifier as reported in the `Debugger.scriptParsed`.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.ScriptId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scriptId")]
			public string ScriptId;

			/// <summary>
			/// Line number in the script (0-based).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("lineNumber")]
			public int LineNumber;

			/// <summary>
			/// Column number in the script (0-based).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("columnNumber")]
			public int ColumnNumber;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: debuggerStatement, call, return
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Continues execution until specific location is reached.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="location">Location to continue to.</param>
		/// <param name="targetCallFrames"></param>
		public void ContinueToLocation(Location location, string targetCallFrames = null) {
			_chrome.Send(
				"Debugger.continueToLocation",
				new {
					location,
					targetCallFrames
				}
			);
		}

		/// <summary>
		/// Disables debugger for given page.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"Debugger.disable"
			);
		}

		/// <summary>
		/// Enables debugger for the given page. Clients should not assume that the debugging has been enabled until the result for this command is received.
		/// </summary>
		[JsonObject]
		public class EnableResult {
			/// <summary>
			/// Unique identifier of the debugger.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.UniqueDebuggerId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("debuggerId")]
			public string DebuggerId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Enables debugger for the given page. Clients should not assume that the debugging has been enabled until the result for this command is received.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="maxScriptsCacheSize">The maximum size in bytes of collected scripts (not referenced by other heap objects) the debugger can hold. Puts no limit if paramter is omitted.</param>
		public EnableResult Enable(double? maxScriptsCacheSize = null) {
			string s = _chrome.Send(
				"Debugger.enable",
				new {
					maxScriptsCacheSize
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<EnableResult>();
		}

		/// <summary>
		/// Evaluates expression on a given call frame.
		/// </summary>
		[JsonObject]
		public class EvaluateOnCallFrameResult {
			/// <summary>
			/// Object wrapper for the evaluation result.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("result")]
			public RuntimeDomain.RemoteObject Result;

			/// <summary>
			/// Exception details.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Runtime.ExceptionDetails</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("exceptionDetails")]
			public RuntimeDomain.ExceptionDetails ExceptionDetails;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Evaluates expression on a given call frame.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="callFrameId">Call frame identifier to evaluate on.</param>
		/// <param name="expression">Expression to evaluate.</param>
		/// <param name="objectGroup">String object group name to put result into (allows rapid releasing resulting object handles using `releaseObjectGroup`).</param>
		/// <param name="includeCommandLineAPI">Specifies whether command line API should be available to the evaluated expression, defaults to false.</param>
		/// <param name="silent">In silent mode exceptions thrown during evaluation are not reported and do not pause execution. Overrides `setPauseOnException` state.</param>
		/// <param name="returnByValue">Whether the result is expected to be a JSON object that should be sent by value.</param>
		/// <param name="generatePreview">Whether preview should be generated for the result.</param>
		/// <param name="throwOnSideEffect">Whether to throw an exception if side effect cannot be ruled out during evaluation.</param>
		/// <param name="timeout">Terminate execution after timing out (number of milliseconds).</param>
		public EvaluateOnCallFrameResult EvaluateOnCallFrame(string callFrameId, string expression, string objectGroup = null, bool? includeCommandLineAPI = null, bool? silent = null, bool? returnByValue = null, bool? generatePreview = null, bool? throwOnSideEffect = null, double? timeout = null) {
			string s = _chrome.Send(
				"Debugger.evaluateOnCallFrame",
				new {
					callFrameId,
					expression,
					objectGroup,
					includeCommandLineAPI,
					silent,
					returnByValue,
					generatePreview,
					throwOnSideEffect,
					timeout
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<EvaluateOnCallFrameResult>();
		}

		/// <summary>
		/// Returns possible locations for breakpoint. scriptId in start and end range locations should be the same.
		/// </summary>
		[JsonObject]
		public class GetPossibleBreakpointsResult {
			/// <summary>
			/// List of the possible breakpoint locations.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("locations")]
			public BreakLocation[] Locations;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns possible locations for breakpoint. scriptId in start and end range locations should be the same.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="start">Start of range to search possible breakpoint locations in.</param>
		/// <param name="end">End of range to search possible breakpoint locations in (excluding). When not specified, end of scripts is used as end of range.</param>
		/// <param name="restrictToFunction">Only consider locations which are in the same (non-nested) function as start.</param>
		public GetPossibleBreakpointsResult GetPossibleBreakpoints(Location start, Location end = null, bool? restrictToFunction = null) {
			string s = _chrome.Send(
				"Debugger.getPossibleBreakpoints",
				new {
					start,
					end,
					restrictToFunction
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetPossibleBreakpointsResult>();
		}

		/// <summary>
		/// Returns source for the script with given id.
		/// </summary>
		[JsonObject]
		public class GetScriptSourceResult {
			/// <summary>
			/// Script source.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scriptSource")]
			public string ScriptSource;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns source for the script with given id.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="scriptId">Id of the script to get source for.</param>
		public GetScriptSourceResult GetScriptSource(string scriptId) {
			string s = _chrome.Send(
				"Debugger.getScriptSource",
				new {
					scriptId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetScriptSourceResult>();
		}

		/// <summary>
		/// Returns bytecode for the WebAssembly script with given id.
		/// </summary>
		[JsonObject]
		public class GetWasmBytecodeResult {
			/// <summary>
			/// Script source.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: binary</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("bytecode")]
			public byte[] Bytecode;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns bytecode for the WebAssembly script with given id.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="scriptId">Id of the Wasm script to get source for.</param>
		public GetWasmBytecodeResult GetWasmBytecode(string scriptId) {
			string s = _chrome.Send(
				"Debugger.getWasmBytecode",
				new {
					scriptId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetWasmBytecodeResult>();
		}

		/// <summary>
		/// Returns stack trace with given `stackTraceId`.
		/// </summary>
		[JsonObject]
		public class GetStackTraceResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.StackTrace</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("stackTrace")]
			public RuntimeDomain.StackTrace StackTrace;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns stack trace with given `stackTraceId`.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="stackTraceId"></param>
		public GetStackTraceResult GetStackTrace(RuntimeDomain.StackTraceId stackTraceId) {
			string s = _chrome.Send(
				"Debugger.getStackTrace",
				new {
					stackTraceId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetStackTraceResult>();
		}

		/// <summary>
		/// Stops on the next JavaScript statement.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Pause() {
			_chrome.Send(
				"Debugger.pause"
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="parentStackTraceId">Debugger will pause when async call with given stack trace is started.</param>
		[Obsolete]
		public void PauseOnAsyncCall(RuntimeDomain.StackTraceId parentStackTraceId) {
			_chrome.Send(
				"Debugger.pauseOnAsyncCall",
				new {
					parentStackTraceId
				}
			);
		}

		/// <summary>
		/// Removes JavaScript breakpoint.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="breakpointId"></param>
		public void RemoveBreakpoint(string breakpointId) {
			_chrome.Send(
				"Debugger.removeBreakpoint",
				new {
					breakpointId
				}
			);
		}

		/// <summary>
		/// Restarts particular call frame from the beginning.
		/// </summary>
		[JsonObject]
		public class RestartFrameResult {
			/// <summary>
			/// New stack trace.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("callFrames")]
			public CallFrame[] CallFrames;

			/// <summary>
			/// Async stack trace, if any.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Runtime.StackTrace</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("asyncStackTrace")]
			public RuntimeDomain.StackTrace AsyncStackTrace;

			/// <summary>
			/// Async stack trace, if any.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Runtime.StackTraceId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("asyncStackTraceId")]
			public RuntimeDomain.StackTraceId AsyncStackTraceId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Restarts particular call frame from the beginning.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="callFrameId">Call frame identifier to evaluate on.</param>
		public RestartFrameResult RestartFrame(string callFrameId) {
			string s = _chrome.Send(
				"Debugger.restartFrame",
				new {
					callFrameId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<RestartFrameResult>();
		}

		/// <summary>
		/// Resumes JavaScript execution.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Resume() {
			_chrome.Send(
				"Debugger.resume"
			);
		}

		/// <summary>
		/// Searches for given string in script content.
		/// </summary>
		[JsonObject]
		public class SearchInContentResult {
			/// <summary>
			/// List of search matches.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("result")]
			public SearchMatch[] Result;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Searches for given string in script content.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="scriptId">Id of the script to search in.</param>
		/// <param name="query">String to search for.</param>
		/// <param name="caseSensitive">If true, search is case sensitive.</param>
		/// <param name="isRegex">If true, treats string parameter as regex.</param>
		public SearchInContentResult SearchInContent(string scriptId, string query, bool? caseSensitive = null, bool? isRegex = null) {
			string s = _chrome.Send(
				"Debugger.searchInContent",
				new {
					scriptId,
					query,
					caseSensitive,
					isRegex
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<SearchInContentResult>();
		}

		/// <summary>
		/// Enables or disables async call stacks tracking.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="maxDepth">Maximum depth of async call stacks. Setting to `0` will effectively disable collecting async call stacks (default).</param>
		public void SetAsyncCallStackDepth(int maxDepth) {
			_chrome.Send(
				"Debugger.setAsyncCallStackDepth",
				new {
					maxDepth
				}
			);
		}

		/// <summary>
		/// Replace previous blackbox patterns with passed ones. Forces backend to skip stepping/pausing in scripts with url matching one of the patterns. VM will try to leave blackboxed script by performing 'step in' several times, finally resorting to 'step out' if unsuccessful.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="patterns">Array of regexps that will be used to check script url for blackbox state.</param>
		public void SetBlackboxPatterns(string[] patterns) {
			_chrome.Send(
				"Debugger.setBlackboxPatterns",
				new {
					patterns
				}
			);
		}

		/// <summary>
		/// Makes backend skip steps in the script in blackboxed ranges. VM will try leave blacklisted scripts by performing 'step in' several times, finally resorting to 'step out' if unsuccessful. Positions array contains positions where blackbox state is changed. First interval isn't blackboxed. Array should be sorted.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="scriptId">Id of the script.</param>
		/// <param name="positions"></param>
		public void SetBlackboxedRanges(string scriptId, ScriptPosition[] positions) {
			_chrome.Send(
				"Debugger.setBlackboxedRanges",
				new {
					scriptId,
					positions
				}
			);
		}

		/// <summary>
		/// Sets JavaScript breakpoint at a given location.
		/// </summary>
		[JsonObject]
		public class SetBreakpointResult {
			/// <summary>
			/// Id of the created breakpoint for further reference.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: BreakpointId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("breakpointId")]
			public string BreakpointId;

			/// <summary>
			/// Location this breakpoint resolved into.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Location</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("actualLocation")]
			public Location ActualLocation;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Sets JavaScript breakpoint at a given location.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="location">Location to set breakpoint in.</param>
		/// <param name="condition">Expression to use as a breakpoint condition. When specified, debugger will only stop on the breakpoint if this expression evaluates to true.</param>
		public SetBreakpointResult SetBreakpoint(Location location, string condition = null) {
			string s = _chrome.Send(
				"Debugger.setBreakpoint",
				new {
					location,
					condition
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<SetBreakpointResult>();
		}

		/// <summary>
		/// Sets instrumentation breakpoint.
		/// </summary>
		[JsonObject]
		public class SetInstrumentationBreakpointResult {
			/// <summary>
			/// Id of the created breakpoint for further reference.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: BreakpointId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("breakpointId")]
			public string BreakpointId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Sets instrumentation breakpoint.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="instrumentation">Instrumentation name.</param>
		public SetInstrumentationBreakpointResult SetInstrumentationBreakpoint(string instrumentation) {
			string s = _chrome.Send(
				"Debugger.setInstrumentationBreakpoint",
				new {
					instrumentation
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<SetInstrumentationBreakpointResult>();
		}

		/// <summary>
		/// Sets JavaScript breakpoint at given location specified either by URL or URL regex. Once this command is issued, all existing parsed scripts will have breakpoints resolved and returned in `locations` property. Further matching script parsing will result in subsequent `breakpointResolved` events issued. This logical breakpoint will survive page reloads.
		/// </summary>
		[JsonObject]
		public class SetBreakpointByUrlResult {
			/// <summary>
			/// Id of the created breakpoint for further reference.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: BreakpointId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("breakpointId")]
			public string BreakpointId;

			/// <summary>
			/// List of the locations this breakpoint resolved into upon addition.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("locations")]
			public Location[] Locations;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Sets JavaScript breakpoint at given location specified either by URL or URL regex. Once this command is issued, all existing parsed scripts will have breakpoints resolved and returned in `locations` property. Further matching script parsing will result in subsequent `breakpointResolved` events issued. This logical breakpoint will survive page reloads.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="lineNumber">Line number to set breakpoint at.</param>
		/// <param name="url">URL of the resources to set breakpoint on.</param>
		/// <param name="urlRegex">Regex pattern for the URLs of the resources to set breakpoints on. Either `url` or `urlRegex` must be specified.</param>
		/// <param name="scriptHash">Script hash of the resources to set breakpoint on.</param>
		/// <param name="columnNumber">Offset in the line to set breakpoint at.</param>
		/// <param name="condition">Expression to use as a breakpoint condition. When specified, debugger will only stop on the breakpoint if this expression evaluates to true.</param>
		public SetBreakpointByUrlResult SetBreakpointByUrl(int lineNumber, string url = null, string urlRegex = null, string scriptHash = null, int? columnNumber = null, string condition = null) {
			string s = _chrome.Send(
				"Debugger.setBreakpointByUrl",
				new {
					lineNumber,
					url,
					urlRegex,
					scriptHash,
					columnNumber,
					condition
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<SetBreakpointByUrlResult>();
		}

		/// <summary>
		/// Sets JavaScript breakpoint before each call to the given function. If another function was created from the same source as a given one, calling it will also trigger the breakpoint.
		/// </summary>
		[JsonObject]
		public class SetBreakpointOnFunctionCallResult {
			/// <summary>
			/// Id of the created breakpoint for further reference.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: BreakpointId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("breakpointId")]
			public string BreakpointId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Sets JavaScript breakpoint before each call to the given function. If another function was created from the same source as a given one, calling it will also trigger the breakpoint.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="objectId">Function object id.</param>
		/// <param name="condition">Expression to use as a breakpoint condition. When specified, debugger will stop on the breakpoint if this expression evaluates to true.</param>
		public SetBreakpointOnFunctionCallResult SetBreakpointOnFunctionCall(string objectId, string condition = null) {
			string s = _chrome.Send(
				"Debugger.setBreakpointOnFunctionCall",
				new {
					objectId,
					condition
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<SetBreakpointOnFunctionCallResult>();
		}

		/// <summary>
		/// Activates / deactivates all breakpoints on the page.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="active">New value for breakpoints active state.</param>
		public void SetBreakpointsActive(bool active) {
			_chrome.Send(
				"Debugger.setBreakpointsActive",
				new {
					active
				}
			);
		}

		/// <summary>
		/// Defines pause on exceptions state. Can be set to stop on all exceptions, uncaught exceptions or no exceptions. Initial pause on exceptions state is `none`.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="state">Pause on exceptions mode.</param>
		public void SetPauseOnExceptions(string state) {
			_chrome.Send(
				"Debugger.setPauseOnExceptions",
				new {
					state
				}
			);
		}

		/// <summary>
		/// Changes return value in top frame. Available only at return break position.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="newValue">New return value.</param>
		public void SetReturnValue(RuntimeDomain.CallArgument newValue) {
			_chrome.Send(
				"Debugger.setReturnValue",
				new {
					newValue
				}
			);
		}

		/// <summary>
		/// Edits JavaScript source live.
		/// </summary>
		[JsonObject]
		public class SetScriptSourceResult {
			/// <summary>
			/// New stack trace in case editing has happened while VM was stopped.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("callFrames")]
			public CallFrame[] CallFrames;

			/// <summary>
			/// Whether current call stack  was modified after applying the changes.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("stackChanged")]
			public bool StackChanged;

			/// <summary>
			/// Async stack trace, if any.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Runtime.StackTrace</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("asyncStackTrace")]
			public RuntimeDomain.StackTrace AsyncStackTrace;

			/// <summary>
			/// Async stack trace, if any.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Runtime.StackTraceId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("asyncStackTraceId")]
			public RuntimeDomain.StackTraceId AsyncStackTraceId;

			/// <summary>
			/// Exception details if any.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Runtime.ExceptionDetails</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("exceptionDetails")]
			public RuntimeDomain.ExceptionDetails ExceptionDetails;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Edits JavaScript source live.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="scriptId">Id of the script to edit.</param>
		/// <param name="scriptSource">New content of the script.</param>
		/// <param name="dryRun">If true the change will not actually be applied. Dry run may be used to get result description without actually modifying the code.</param>
		public SetScriptSourceResult SetScriptSource(string scriptId, string scriptSource, bool? dryRun = null) {
			string s = _chrome.Send(
				"Debugger.setScriptSource",
				new {
					scriptId,
					scriptSource,
					dryRun
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<SetScriptSourceResult>();
		}

		/// <summary>
		/// Makes page not interrupt on any pauses (breakpoint, exception, dom exception etc).
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="skip">New value for skip pauses state.</param>
		public void SetSkipAllPauses(bool skip) {
			_chrome.Send(
				"Debugger.setSkipAllPauses",
				new {
					skip
				}
			);
		}

		/// <summary>
		/// Changes value of variable in a callframe. Object-based scopes are not supported and must be mutated manually.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="scopeNumber">0-based number of scope as was listed in scope chain. Only 'local', 'closure' and 'catch' scope types are allowed. Other scopes could be manipulated manually.</param>
		/// <param name="variableName">Variable name.</param>
		/// <param name="newValue">New variable value.</param>
		/// <param name="callFrameId">Id of callframe that holds variable.</param>
		public void SetVariableValue(int scopeNumber, string variableName, RuntimeDomain.CallArgument newValue, string callFrameId) {
			_chrome.Send(
				"Debugger.setVariableValue",
				new {
					scopeNumber,
					variableName,
					newValue,
					callFrameId
				}
			);
		}

		/// <summary>
		/// Steps into the function call.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="breakOnAsyncCall">Debugger will pause on the execution of the first async task which was scheduled before next pause.</param>
		public void StepInto(bool? breakOnAsyncCall = null) {
			_chrome.Send(
				"Debugger.stepInto",
				new {
					breakOnAsyncCall
				}
			);
		}

		/// <summary>
		/// Steps out of the function call.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void StepOut() {
			_chrome.Send(
				"Debugger.stepOut"
			);
		}

		/// <summary>
		/// Steps over the statement.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void StepOver() {
			_chrome.Send(
				"Debugger.stepOver"
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("Debugger.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "breakpointResolved":
					if (BreakpointResolved == null) {
						return;
					}
					BreakpointResolved(
						_chrome,
						JsonConvert.DeserializeObject
							<BreakpointResolvedEventArgs>(@params.ToString())
					);
					break;
				case "paused":
					if (Paused == null) {
						return;
					}
					Paused(
						_chrome,
						JsonConvert.DeserializeObject
							<PausedEventArgs>(@params.ToString())
					);
					break;
				case "resumed":
					if (Resumed == null) {
						return;
					}
					Resumed(_chrome, EventArgs.Empty);
					break;
				case "scriptFailedToParse":
					if (ScriptFailedToParse == null) {
						return;
					}
					ScriptFailedToParse(
						_chrome,
						JsonConvert.DeserializeObject
							<ScriptFailedToParseEventArgs>(@params.ToString())
					);
					break;
				case "scriptParsed":
					if (ScriptParsed == null) {
						return;
					}
					ScriptParsed(
						_chrome,
						JsonConvert.DeserializeObject
							<ScriptParsedEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
