// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// Actions and events related to the inspected page belong to the page domain.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: false</description></item>
	/// <item><description>dependencies: Debugger, DOM, IO, Network, Runtime</description></item>
	/// </list>
	/// </summary>
	public class PageDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the PageDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public PageDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class DomContentEventFiredEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Network.MonotonicTime</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<DomContentEventFiredEventArgs> DomContentEventFired;

		/// <summary>
		/// Emitted only when `page.interceptFileChooser` is enabled.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class FileChooserOpenedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: selectSingle, selectMultiple
			/// </summary>
			[JsonProperty("mode")]
			public string Mode;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Emitted only when `page.interceptFileChooser` is enabled.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<FileChooserOpenedEventArgs> FileChooserOpened;

		/// <summary>
		/// Fired when frame has been attached to its parent.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class FrameAttachedEventArgs : EventArgs {
			/// <summary>
			/// Id of the frame that has been attached.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// Parent frame identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("parentFrameId")]
			public string ParentFrameId;

			/// <summary>
			/// JavaScript stack trace of when frame was attached, only set if frame initiated from script.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Runtime.StackTrace</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("stack")]
			public RuntimeDomain.StackTrace Stack;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when frame has been attached to its parent.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<FrameAttachedEventArgs> FrameAttached;

		/// <summary>
		/// Fired when frame no longer has a scheduled navigation.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class FrameClearedScheduledNavigationEventArgs : EventArgs {
			/// <summary>
			/// Id of the frame that has cleared its scheduled navigation.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when frame no longer has a scheduled navigation.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<FrameClearedScheduledNavigationEventArgs> FrameClearedScheduledNavigation;

		/// <summary>
		/// Fired when frame has been detached from its parent.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class FrameDetachedEventArgs : EventArgs {
			/// <summary>
			/// Id of the frame that has been detached.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when frame has been detached from its parent.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<FrameDetachedEventArgs> FrameDetached;

		/// <summary>
		/// Fired once navigation of the frame has completed. Frame is now associated with the new loader.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class FrameNavigatedEventArgs : EventArgs {
			/// <summary>
			/// Frame object.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Frame</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frame")]
			public Frame Frame;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired once navigation of the frame has completed. Frame is now associated with the new loader.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<FrameNavigatedEventArgs> FrameNavigated;

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler FrameResized;

		/// <summary>
		/// Fired when a renderer-initiated navigation is requested. Navigation may still be cancelled after the event is issued.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class FrameRequestedNavigationEventArgs : EventArgs {
			/// <summary>
			/// Id of the frame that is being navigated.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// The reason for the navigation.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ClientNavigationReason</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("reason")]
			public string Reason;

			/// <summary>
			/// The destination URL for the requested navigation.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when a renderer-initiated navigation is requested. Navigation may still be cancelled after the event is issued.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<FrameRequestedNavigationEventArgs> FrameRequestedNavigation;

		/// <summary>
		/// Fired when frame schedules a potential navigation.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class FrameScheduledNavigationEventArgs : EventArgs {
			/// <summary>
			/// Id of the frame that has scheduled a navigation.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// Delay (in seconds) until the navigation is scheduled to begin. The navigation is not guaranteed to start.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("delay")]
			public double Delay;

			/// <summary>
			/// The reason for the navigation.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: formSubmissionGet, formSubmissionPost, httpHeaderRefresh, scriptInitiated, metaTagRefresh, pageBlockInterstitial, reload
			/// </summary>
			[JsonProperty("reason")]
			public string Reason;

			/// <summary>
			/// The destination URL for the scheduled navigation.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when frame schedules a potential navigation.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<FrameScheduledNavigationEventArgs> FrameScheduledNavigation;

		/// <summary>
		/// Fired when frame has started loading.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class FrameStartedLoadingEventArgs : EventArgs {
			/// <summary>
			/// Id of the frame that has started loading.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when frame has started loading.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<FrameStartedLoadingEventArgs> FrameStartedLoading;

		/// <summary>
		/// Fired when frame has stopped loading.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class FrameStoppedLoadingEventArgs : EventArgs {
			/// <summary>
			/// Id of the frame that has stopped loading.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when frame has stopped loading.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<FrameStoppedLoadingEventArgs> FrameStoppedLoading;

		/// <summary>
		/// Fired when page is about to start a download.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class DownloadWillBeginEventArgs : EventArgs {
			/// <summary>
			/// Id of the frame that caused download to begin.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// URL of the resource being downloaded.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when page is about to start a download.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<DownloadWillBeginEventArgs> DownloadWillBegin;

		/// <summary>
		/// Fired when interstitial page was hidden
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler InterstitialHidden;

		/// <summary>
		/// Fired when interstitial page was shown
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler InterstitialShown;

		/// <summary>
		/// Fired when a JavaScript initiated dialog (alert, confirm, prompt, or onbeforeunload) has been closed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class JavascriptDialogClosedEventArgs : EventArgs {
			/// <summary>
			/// Whether dialog was confirmed.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("result")]
			public bool Result;

			/// <summary>
			/// User input in case of prompt.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("userInput")]
			public string UserInput;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when a JavaScript initiated dialog (alert, confirm, prompt, or onbeforeunload) has been closed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<JavascriptDialogClosedEventArgs> JavascriptDialogClosed;

		/// <summary>
		/// Fired when a JavaScript initiated dialog (alert, confirm, prompt, or onbeforeunload) is about to open.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class JavascriptDialogOpeningEventArgs : EventArgs {
			/// <summary>
			/// Frame url.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Message that will be displayed by the dialog.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("message")]
			public string Message;

			/// <summary>
			/// Dialog type.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: DialogType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// True iff browser is capable showing or acting on the given dialog. When browser has no dialog handler for given target, calling alert while Page domain is engaged will stall the page execution. Execution can be resumed via calling Page.handleJavaScriptDialog.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("hasBrowserHandler")]
			public bool HasBrowserHandler;

			/// <summary>
			/// Default dialog prompt.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("defaultPrompt")]
			public string DefaultPrompt;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when a JavaScript initiated dialog (alert, confirm, prompt, or onbeforeunload) is about to open.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<JavascriptDialogOpeningEventArgs> JavascriptDialogOpening;

		/// <summary>
		/// Fired for top level page lifecycle events such as navigation, load, paint, etc.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class LifecycleEventEventArgs : EventArgs {
			/// <summary>
			/// Id of the frame.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// Loader identifier. Empty string if the request is fetched from worker.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Network.LoaderId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("loaderId")]
			public string LoaderId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Network.MonotonicTime</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired for top level page lifecycle events such as navigation, load, paint, etc.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<LifecycleEventEventArgs> LifecycleEvent;

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class LoadEventFiredEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Network.MonotonicTime</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<LoadEventFiredEventArgs> LoadEventFired;

		/// <summary>
		/// Fired when same-document navigation happens, e.g. due to history API usage or anchor navigation.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class NavigatedWithinDocumentEventArgs : EventArgs {
			/// <summary>
			/// Id of the frame.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// Frame's new url.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when same-document navigation happens, e.g. due to history API usage or anchor navigation.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<NavigatedWithinDocumentEventArgs> NavigatedWithinDocument;

		/// <summary>
		/// Compressed image data requested by the `startScreencast`.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ScreencastFrameEventArgs : EventArgs {
			/// <summary>
			/// Base64-encoded compressed image.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: binary</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("data")]
			public byte[] Data;

			/// <summary>
			/// Screencast frame metadata.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ScreencastFrameMetadata</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("metadata")]
			public ScreencastFrameMetadata Metadata;

			/// <summary>
			/// Frame number.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sessionId")]
			public int SessionId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Compressed image data requested by the `startScreencast`.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ScreencastFrameEventArgs> ScreencastFrame;

		/// <summary>
		/// Fired when the page with currently enabled screencast was shown or hidden `.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ScreencastVisibilityChangedEventArgs : EventArgs {
			/// <summary>
			/// True if the page is visible.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("visible")]
			public bool Visible;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when the page with currently enabled screencast was shown or hidden `.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ScreencastVisibilityChangedEventArgs> ScreencastVisibilityChanged;

		/// <summary>
		/// Fired when a new window is going to be opened, via window.open(), link click, form submission, etc.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class WindowOpenEventArgs : EventArgs {
			/// <summary>
			/// The URL for the new window.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Window name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("windowName")]
			public string WindowName;

			/// <summary>
			/// An array of enabled window features.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("windowFeatures")]
			public string[] WindowFeatures;

			/// <summary>
			/// Whether or not it was triggered by user gesture.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("userGesture")]
			public bool UserGesture;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when a new window is going to be opened, via window.open(), link click, form submission, etc.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<WindowOpenEventArgs> WindowOpen;

		/// <summary>
		/// Issued for every compilation cache generated. Is only available if Page.setGenerateCompilationCache is enabled.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class CompilationCacheProducedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Base64-encoded data
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: binary</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("data")]
			public byte[] Data;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Issued for every compilation cache generated. Is only available if Page.setGenerateCompilationCache is enabled.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<CompilationCacheProducedEventArgs> CompilationCacheProduced;

		/// <summary>
		/// Information about the Frame on the page.
		/// </summary>
		[JsonObject]
		public class Frame {
			/// <summary>
			/// Frame unique identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("id")]
			public string Id;

			/// <summary>
			/// Parent frame identifier.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("parentId")]
			public string ParentId;

			/// <summary>
			/// Identifier of the loader associated with this frame.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Network.LoaderId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("loaderId")]
			public string LoaderId;

			/// <summary>
			/// Frame's name as specified in the tag.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// Frame document's URL without fragment.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Frame document's URL fragment including the '#'.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("urlFragment")]
			public string UrlFragment;

			/// <summary>
			/// Frame document's security origin.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("securityOrigin")]
			public string SecurityOrigin;

			/// <summary>
			/// Frame document's mimeType as determined by the browser.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("mimeType")]
			public string MimeType;

			/// <summary>
			/// If the frame failed to load, this contains the URL that could not be loaded. Note that unlike url above, this URL may contain a fragment.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("unreachableUrl")]
			public string UnreachableUrl;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Information about the Resource on the page.
		/// </summary>
		[JsonObject]
		public class FrameResource {
			/// <summary>
			/// Resource URL.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Type of this resource.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Network.ResourceType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// Resource mimeType as determined by the browser.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("mimeType")]
			public string MimeType;

			/// <summary>
			/// last-modified timestamp as reported by server.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Network.TimeSinceEpoch</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("lastModified")]
			public double LastModified;

			/// <summary>
			/// Resource content size.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contentSize")]
			public double ContentSize;

			/// <summary>
			/// True if the resource failed to load.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("failed")]
			public bool Failed;

			/// <summary>
			/// True if the resource was canceled during loading.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("canceled")]
			public bool Canceled;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Information about the Frame hierarchy along with their cached resources.
		/// </summary>
		[JsonObject]
		public class FrameResourceTree {
			/// <summary>
			/// Frame information for this tree item.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Frame</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frame")]
			public Frame Frame;

			/// <summary>
			/// Child frames.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("childFrames")]
			public FrameResourceTree[] ChildFrames;

			/// <summary>
			/// Information about frame resources.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("resources")]
			public FrameResource[] Resources;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Information about the Frame hierarchy.
		/// </summary>
		[JsonObject]
		public class FrameTree {
			/// <summary>
			/// Frame information for this tree item.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Frame</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frame")]
			public Frame Frame;

			/// <summary>
			/// Child frames.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("childFrames")]
			public FrameTree[] ChildFrames;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Navigation history entry.
		/// </summary>
		[JsonObject]
		public class NavigationEntry {
			/// <summary>
			/// Unique id of the navigation history entry.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("id")]
			public int Id;

			/// <summary>
			/// URL of the navigation history entry.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// URL that the user typed in the url bar.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("userTypedURL")]
			public string UserTypedURL;

			/// <summary>
			/// Title of the navigation history entry.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("title")]
			public string Title;

			/// <summary>
			/// Transition type.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: TransitionType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("transitionType")]
			public string TransitionType;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Screencast frame metadata.
		/// </summary>
		[JsonObject]
		public class ScreencastFrameMetadata {
			/// <summary>
			/// Top offset in DIP.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("offsetTop")]
			public double OffsetTop;

			/// <summary>
			/// Page scale factor.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("pageScaleFactor")]
			public double PageScaleFactor;

			/// <summary>
			/// Device screen width in DIP.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("deviceWidth")]
			public double DeviceWidth;

			/// <summary>
			/// Device screen height in DIP.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("deviceHeight")]
			public double DeviceHeight;

			/// <summary>
			/// Position of horizontal scroll in CSS pixels.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scrollOffsetX")]
			public double ScrollOffsetX;

			/// <summary>
			/// Position of vertical scroll in CSS pixels.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scrollOffsetY")]
			public double ScrollOffsetY;

			/// <summary>
			/// Frame swap timestamp.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Network.TimeSinceEpoch</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Error while paring app manifest.
		/// </summary>
		[JsonObject]
		public class AppManifestError {
			/// <summary>
			/// Error message.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("message")]
			public string Message;

			/// <summary>
			/// If criticial, this is a non-recoverable parse error.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("critical")]
			public int Critical;

			/// <summary>
			/// Error line.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("line")]
			public int Line;

			/// <summary>
			/// Error column.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("column")]
			public int Column;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Layout viewport position and dimensions.
		/// </summary>
		[JsonObject]
		public class LayoutViewport {
			/// <summary>
			/// Horizontal offset relative to the document (CSS pixels).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("pageX")]
			public int PageX;

			/// <summary>
			/// Vertical offset relative to the document (CSS pixels).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("pageY")]
			public int PageY;

			/// <summary>
			/// Width (CSS pixels), excludes scrollbar if present.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("clientWidth")]
			public int ClientWidth;

			/// <summary>
			/// Height (CSS pixels), excludes scrollbar if present.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("clientHeight")]
			public int ClientHeight;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Visual viewport position, dimensions, and scale.
		/// </summary>
		[JsonObject]
		public class VisualViewport {
			/// <summary>
			/// Horizontal offset relative to the layout viewport (CSS pixels).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("offsetX")]
			public double OffsetX;

			/// <summary>
			/// Vertical offset relative to the layout viewport (CSS pixels).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("offsetY")]
			public double OffsetY;

			/// <summary>
			/// Horizontal offset relative to the document (CSS pixels).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("pageX")]
			public double PageX;

			/// <summary>
			/// Vertical offset relative to the document (CSS pixels).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("pageY")]
			public double PageY;

			/// <summary>
			/// Width (CSS pixels), excludes scrollbar if present.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("clientWidth")]
			public double ClientWidth;

			/// <summary>
			/// Height (CSS pixels), excludes scrollbar if present.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("clientHeight")]
			public double ClientHeight;

			/// <summary>
			/// Scale relative to the ideal viewport (size at width=device-width).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scale")]
			public double Scale;

			/// <summary>
			/// Page zoom factor (CSS to device independent pixels ratio).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("zoom")]
			public double Zoom;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Viewport for capturing screenshot.
		/// </summary>
		[JsonObject]
		public class Viewport {
			/// <summary>
			/// X offset in device independent pixels (dip).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("x")]
			public double X;

			/// <summary>
			/// Y offset in device independent pixels (dip).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("y")]
			public double Y;

			/// <summary>
			/// Rectangle width in device independent pixels (dip).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("width")]
			public double Width;

			/// <summary>
			/// Rectangle height in device independent pixels (dip).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("height")]
			public double Height;

			/// <summary>
			/// Page scale factor.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scale")]
			public double Scale;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Generic font families collection.
		/// </summary>
		[JsonObject]
		public class FontFamilies {
			/// <summary>
			/// The standard font-family.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("standard")]
			public string Standard;

			/// <summary>
			/// The fixed font-family.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("fixed")]
			public string Fixed;

			/// <summary>
			/// The serif font-family.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("serif")]
			public string Serif;

			/// <summary>
			/// The sansSerif font-family.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sansSerif")]
			public string SansSerif;

			/// <summary>
			/// The cursive font-family.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("cursive")]
			public string Cursive;

			/// <summary>
			/// The fantasy font-family.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("fantasy")]
			public string Fantasy;

			/// <summary>
			/// The pictograph font-family.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("pictograph")]
			public string Pictograph;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Default font sizes.
		/// </summary>
		[JsonObject]
		public class FontSizes {
			/// <summary>
			/// Default standard font size.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("standard")]
			public int Standard;

			/// <summary>
			/// Default fixed font size.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("fixed")]
			public int Fixed;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Deprecated, please use addScriptToEvaluateOnNewDocument instead.
		/// </summary>
		[JsonObject]
		public class AddScriptToEvaluateOnLoadResult {
			/// <summary>
			/// Identifier of the added script.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ScriptIdentifier</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("identifier")]
			public string Identifier;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Deprecated, please use addScriptToEvaluateOnNewDocument instead.
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="scriptSource"></param>
		[Obsolete]
		public AddScriptToEvaluateOnLoadResult AddScriptToEvaluateOnLoad(string scriptSource) {
			string s = _chrome.Send(
				"Page.addScriptToEvaluateOnLoad",
				new {
					scriptSource
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<AddScriptToEvaluateOnLoadResult>();
		}

		/// <summary>
		/// Evaluates given script in every frame upon creation (before loading frame's scripts).
		/// </summary>
		[JsonObject]
		public class AddScriptToEvaluateOnNewDocumentResult {
			/// <summary>
			/// Identifier of the added script.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ScriptIdentifier</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("identifier")]
			public string Identifier;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Evaluates given script in every frame upon creation (before loading frame's scripts).
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="source"></param>
		/// <param name="worldName">If specified, creates an isolated world with the given name and evaluates given script in it. This world name will be used as the ExecutionContextDescription::name when the corresponding event is emitted.</param>
		public AddScriptToEvaluateOnNewDocumentResult AddScriptToEvaluateOnNewDocument(string source, string worldName = null) {
			string s = _chrome.Send(
				"Page.addScriptToEvaluateOnNewDocument",
				new {
					source,
					worldName
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<AddScriptToEvaluateOnNewDocumentResult>();
		}

		/// <summary>
		/// Brings page to front (activates tab).
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void BringToFront() {
			_chrome.Send(
				"Page.bringToFront"
			);
		}

		/// <summary>
		/// Capture page screenshot.
		/// </summary>
		[JsonObject]
		public class CaptureScreenshotResult {
			/// <summary>
			/// Base64-encoded image data.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: binary</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("data")]
			public byte[] Data;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Capture page screenshot.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="format">Image compression format (defaults to png).</param>
		/// <param name="quality">Compression quality from range [0..100] (jpeg only).</param>
		/// <param name="clip">Capture the screenshot of a given region only.</param>
		/// <param name="fromSurface">Capture the screenshot from the surface, rather than the view. Defaults to true.</param>
		public CaptureScreenshotResult CaptureScreenshot(string format = null, int? quality = null, Viewport clip = null, bool? fromSurface = null) {
			string s = _chrome.Send(
				"Page.captureScreenshot",
				new {
					format,
					quality,
					clip,
					fromSurface
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<CaptureScreenshotResult>();
		}

		/// <summary>
		/// Returns a snapshot of the page as a string. For MHTML format, the serialization includes iframes, shadow DOM, external resources, and element-inline styles.
		/// </summary>
		[JsonObject]
		public class CaptureSnapshotResult {
			/// <summary>
			/// Serialized page data.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("data")]
			public string Data;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns a snapshot of the page as a string. For MHTML format, the serialization includes iframes, shadow DOM, external resources, and element-inline styles.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="format">Format (defaults to mhtml).</param>
		public CaptureSnapshotResult CaptureSnapshot(string format = null) {
			string s = _chrome.Send(
				"Page.captureSnapshot",
				new {
					format
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<CaptureSnapshotResult>();
		}

		/// <summary>
		/// Clears the overriden device metrics.
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[Obsolete]
		public void ClearDeviceMetricsOverride() {
			_chrome.Send(
				"Page.clearDeviceMetricsOverride"
			);
		}

		/// <summary>
		/// Clears the overridden Device Orientation.
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[Obsolete]
		public void ClearDeviceOrientationOverride() {
			_chrome.Send(
				"Page.clearDeviceOrientationOverride"
			);
		}

		/// <summary>
		/// Clears the overriden Geolocation Position and Error.
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[Obsolete]
		public void ClearGeolocationOverride() {
			_chrome.Send(
				"Page.clearGeolocationOverride"
			);
		}

		/// <summary>
		/// Creates an isolated world for the given frame.
		/// </summary>
		[JsonObject]
		public class CreateIsolatedWorldResult {
			/// <summary>
			/// Execution context of the isolated world.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.ExecutionContextId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("executionContextId")]
			public int ExecutionContextId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Creates an isolated world for the given frame.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="frameId">Id of the frame in which the isolated world should be created.</param>
		/// <param name="worldName">An optional name which is reported in the Execution Context.</param>
		/// <param name="grantUniveralAccess">Whether or not universal access should be granted to the isolated world. This is a powerful option, use with caution.</param>
		public CreateIsolatedWorldResult CreateIsolatedWorld(string frameId, string worldName = null, bool? grantUniveralAccess = null) {
			string s = _chrome.Send(
				"Page.createIsolatedWorld",
				new {
					frameId,
					worldName,
					grantUniveralAccess
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<CreateIsolatedWorldResult>();
		}

		/// <summary>
		/// Deletes browser cookie with given name, domain and path.
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="cookieName">Name of the cookie to remove.</param>
		/// <param name="url">URL to match cooke domain and path.</param>
		[Obsolete]
		public void DeleteCookie(string cookieName, string url) {
			_chrome.Send(
				"Page.deleteCookie",
				new {
					cookieName,
					url
				}
			);
		}

		/// <summary>
		/// Disables page domain notifications.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"Page.disable"
			);
		}

		/// <summary>
		/// Enables page domain notifications.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"Page.enable"
			);
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class GetAppManifestResult {
			/// <summary>
			/// Manifest location.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("errors")]
			public AppManifestError[] Errors;

			/// <summary>
			/// Manifest content.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("data")]
			public string Data;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public GetAppManifestResult GetAppManifest() {
			string s = _chrome.Send(
				"Page.getAppManifest"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetAppManifestResult>();
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class GetInstallabilityErrorsResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("errors")]
			public string[] Errors;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public GetInstallabilityErrorsResult GetInstallabilityErrors() {
			string s = _chrome.Send(
				"Page.getInstallabilityErrors"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetInstallabilityErrorsResult>();
		}

		/// <summary>
		/// Returns all browser cookies. Depending on the backend support, will return detailed cookie information in the `cookies` field.
		/// </summary>
		[JsonObject]
		public class GetCookiesResult {
			/// <summary>
			/// Array of cookie objects.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("cookies")]
			public NetworkDomain.Cookie[] Cookies;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns all browser cookies. Depending on the backend support, will return detailed cookie information in the `cookies` field.
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[Obsolete]
		public GetCookiesResult GetCookies() {
			string s = _chrome.Send(
				"Page.getCookies"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetCookiesResult>();
		}

		/// <summary>
		/// Returns present frame tree structure.
		/// </summary>
		[JsonObject]
		public class GetFrameTreeResult {
			/// <summary>
			/// Present frame tree structure.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: FrameTree</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameTree")]
			public FrameTree FrameTree;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns present frame tree structure.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public GetFrameTreeResult GetFrameTree() {
			string s = _chrome.Send(
				"Page.getFrameTree"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetFrameTreeResult>();
		}

		/// <summary>
		/// Returns metrics relating to the layouting of the page, such as viewport bounds/scale.
		/// </summary>
		[JsonObject]
		public class GetLayoutMetricsResult {
			/// <summary>
			/// Metrics relating to the layout viewport.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: LayoutViewport</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("layoutViewport")]
			public LayoutViewport LayoutViewport;

			/// <summary>
			/// Metrics relating to the visual viewport.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: VisualViewport</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("visualViewport")]
			public VisualViewport VisualViewport;

			/// <summary>
			/// Size of scrollable area.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: DOM.Rect</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contentSize")]
			public DOMDomain.Rect ContentSize;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns metrics relating to the layouting of the page, such as viewport bounds/scale.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public GetLayoutMetricsResult GetLayoutMetrics() {
			string s = _chrome.Send(
				"Page.getLayoutMetrics"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetLayoutMetricsResult>();
		}

		/// <summary>
		/// Returns navigation history for the current page.
		/// </summary>
		[JsonObject]
		public class GetNavigationHistoryResult {
			/// <summary>
			/// Index of the current navigation history entry.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("currentIndex")]
			public int CurrentIndex;

			/// <summary>
			/// Array of navigation history entries.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("entries")]
			public NavigationEntry[] Entries;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns navigation history for the current page.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public GetNavigationHistoryResult GetNavigationHistory() {
			string s = _chrome.Send(
				"Page.getNavigationHistory"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetNavigationHistoryResult>();
		}

		/// <summary>
		/// Resets navigation history for the current page.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void ResetNavigationHistory() {
			_chrome.Send(
				"Page.resetNavigationHistory"
			);
		}

		/// <summary>
		/// Returns content of the given resource.
		/// </summary>
		[JsonObject]
		public class GetResourceContentResult {
			/// <summary>
			/// Resource content.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("content")]
			public string Content;

			/// <summary>
			/// True, if content was served as base64.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("base64Encoded")]
			public bool Base64Encoded;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns content of the given resource.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="frameId">Frame id to get resource for.</param>
		/// <param name="url">URL of the resource to get content for.</param>
		public GetResourceContentResult GetResourceContent(string frameId, string url) {
			string s = _chrome.Send(
				"Page.getResourceContent",
				new {
					frameId,
					url
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetResourceContentResult>();
		}

		/// <summary>
		/// Returns present frame / resource tree structure.
		/// </summary>
		[JsonObject]
		public class GetResourceTreeResult {
			/// <summary>
			/// Present frame / resource tree structure.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: FrameResourceTree</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameTree")]
			public FrameResourceTree FrameTree;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns present frame / resource tree structure.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public GetResourceTreeResult GetResourceTree() {
			string s = _chrome.Send(
				"Page.getResourceTree"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetResourceTreeResult>();
		}

		/// <summary>
		/// Accepts or dismisses a JavaScript initiated dialog (alert, confirm, prompt, or onbeforeunload).
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="accept">Whether to accept or dismiss the dialog.</param>
		/// <param name="promptText">The text to enter into the dialog prompt before accepting. Used only if this is a prompt dialog.</param>
		public void HandleJavaScriptDialog(bool accept, string promptText = null) {
			_chrome.Send(
				"Page.handleJavaScriptDialog",
				new {
					accept,
					promptText
				}
			);
		}

		/// <summary>
		/// Navigates current page to the given URL.
		/// </summary>
		[JsonObject]
		public class NavigateResult {
			/// <summary>
			/// Frame id that has navigated (or failed to navigate)
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// Loader identifier.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Network.LoaderId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("loaderId")]
			public string LoaderId;

			/// <summary>
			/// User friendly error message, present if and only if navigation has failed.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("errorText")]
			public string ErrorText;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Navigates current page to the given URL.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="url">URL to navigate the page to.</param>
		/// <param name="referrer">Referrer URL.</param>
		/// <param name="transitionType">Intended transition type.</param>
		/// <param name="frameId">Frame id to navigate, if not specified navigates the top frame.</param>
		public NavigateResult Navigate(string url, string referrer = null, string transitionType = null, string frameId = null) {
			string s = _chrome.Send(
				"Page.navigate",
				new {
					url,
					referrer,
					transitionType,
					frameId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<NavigateResult>();
		}

		/// <summary>
		/// Navigates current page to the given history entry.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="entryId">Unique id of the entry to navigate to.</param>
		public void NavigateToHistoryEntry(int entryId) {
			_chrome.Send(
				"Page.navigateToHistoryEntry",
				new {
					entryId
				}
			);
		}

		/// <summary>
		/// Print page as PDF.
		/// </summary>
		[JsonObject]
		public class PrintToPDFResult {
			/// <summary>
			/// Base64-encoded pdf data. Empty if |returnAsStream| is specified.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: binary</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("data")]
			public byte[] Data;

			/// <summary>
			/// A handle of the stream that holds resulting PDF data.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: IO.StreamHandle</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("stream")]
			public string Stream;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Print page as PDF.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="landscape">Paper orientation. Defaults to false.</param>
		/// <param name="displayHeaderFooter">Display header and footer. Defaults to false.</param>
		/// <param name="printBackground">Print background graphics. Defaults to false.</param>
		/// <param name="scale">Scale of the webpage rendering. Defaults to 1.</param>
		/// <param name="paperWidth">Paper width in inches. Defaults to 8.5 inches.</param>
		/// <param name="paperHeight">Paper height in inches. Defaults to 11 inches.</param>
		/// <param name="marginTop">Top margin in inches. Defaults to 1cm (~0.4 inches).</param>
		/// <param name="marginBottom">Bottom margin in inches. Defaults to 1cm (~0.4 inches).</param>
		/// <param name="marginLeft">Left margin in inches. Defaults to 1cm (~0.4 inches).</param>
		/// <param name="marginRight">Right margin in inches. Defaults to 1cm (~0.4 inches).</param>
		/// <param name="pageRanges">Paper ranges to print, e.g., '1-5, 8, 11-13'. Defaults to the empty string, which means print all pages.</param>
		/// <param name="ignoreInvalidPageRanges">Whether to silently ignore invalid but successfully parsed page ranges, such as '3-2'. Defaults to false.</param>
		/// <param name="headerTemplate">HTML template for the print header. Should be valid HTML markup with following classes used to inject printing values into them: - `date`: formatted print date - `title`: document title - `url`: document location - `pageNumber`: current page number - `totalPages`: total pages in the document  For example, `&lt;span class=title&gt;&lt;/span&gt;` would generate span containing the title.</param>
		/// <param name="footerTemplate">HTML template for the print footer. Should use the same format as the `headerTemplate`.</param>
		/// <param name="preferCSSPageSize">Whether or not to prefer page size as defined by css. Defaults to false, in which case the content will be scaled to fit the paper size.</param>
		/// <param name="transferMode">return as stream</param>
		public PrintToPDFResult PrintToPDF(bool? landscape = null, bool? displayHeaderFooter = null, bool? printBackground = null, double? scale = null, double? paperWidth = null, double? paperHeight = null, double? marginTop = null, double? marginBottom = null, double? marginLeft = null, double? marginRight = null, string pageRanges = null, bool? ignoreInvalidPageRanges = null, string headerTemplate = null, string footerTemplate = null, bool? preferCSSPageSize = null, string transferMode = null) {
			string s = _chrome.Send(
				"Page.printToPDF",
				new {
					landscape,
					displayHeaderFooter,
					printBackground,
					scale,
					paperWidth,
					paperHeight,
					marginTop,
					marginBottom,
					marginLeft,
					marginRight,
					pageRanges,
					ignoreInvalidPageRanges,
					headerTemplate,
					footerTemplate,
					preferCSSPageSize,
					transferMode
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<PrintToPDFResult>();
		}

		/// <summary>
		/// Reloads given page optionally ignoring the cache.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="ignoreCache">If true, browser cache is ignored (as if the user pressed Shift+refresh).</param>
		/// <param name="scriptToEvaluateOnLoad">If set, the script will be injected into all frames of the inspected page after reload. Argument will be ignored if reloading dataURL origin.</param>
		public void Reload(bool? ignoreCache = null, string scriptToEvaluateOnLoad = null) {
			_chrome.Send(
				"Page.reload",
				new {
					ignoreCache,
					scriptToEvaluateOnLoad
				}
			);
		}

		/// <summary>
		/// Deprecated, please use removeScriptToEvaluateOnNewDocument instead.
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="identifier"></param>
		[Obsolete]
		public void RemoveScriptToEvaluateOnLoad(string identifier) {
			_chrome.Send(
				"Page.removeScriptToEvaluateOnLoad",
				new {
					identifier
				}
			);
		}

		/// <summary>
		/// Removes given script from the list.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="identifier"></param>
		public void RemoveScriptToEvaluateOnNewDocument(string identifier) {
			_chrome.Send(
				"Page.removeScriptToEvaluateOnNewDocument",
				new {
					identifier
				}
			);
		}

		/// <summary>
		/// Acknowledges that a screencast frame has been received by the frontend.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="sessionId">Frame number.</param>
		public void ScreencastFrameAck(int sessionId) {
			_chrome.Send(
				"Page.screencastFrameAck",
				new {
					sessionId
				}
			);
		}

		/// <summary>
		/// Searches for given string in resource content.
		/// </summary>
		[JsonObject]
		public class SearchInResourceResult {
			/// <summary>
			/// List of search matches.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("result")]
			public DebuggerDomain.SearchMatch[] Result;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Searches for given string in resource content.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="frameId">Frame id for resource to search in.</param>
		/// <param name="url">URL of the resource to search in.</param>
		/// <param name="query">String to search for.</param>
		/// <param name="caseSensitive">If true, search is case sensitive.</param>
		/// <param name="isRegex">If true, treats string parameter as regex.</param>
		public SearchInResourceResult SearchInResource(string frameId, string url, string query, bool? caseSensitive = null, bool? isRegex = null) {
			string s = _chrome.Send(
				"Page.searchInResource",
				new {
					frameId,
					url,
					query,
					caseSensitive,
					isRegex
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<SearchInResourceResult>();
		}

		/// <summary>
		/// Enable Chrome's experimental ad filter on all sites.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="enabled">Whether to block ads.</param>
		public void SetAdBlockingEnabled(bool enabled) {
			_chrome.Send(
				"Page.setAdBlockingEnabled",
				new {
					enabled
				}
			);
		}

		/// <summary>
		/// Enable page Content Security Policy by-passing.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="enabled">Whether to bypass page CSP.</param>
		public void SetBypassCSP(bool enabled) {
			_chrome.Send(
				"Page.setBypassCSP",
				new {
					enabled
				}
			);
		}

		/// <summary>
		/// Overrides the values of device screen dimensions (window.screen.width, window.screen.height, window.innerWidth, window.innerHeight, and "device-width"/"device-height"-related CSS media query results).
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="width">Overriding width value in pixels (minimum 0, maximum 10000000). 0 disables the override.</param>
		/// <param name="height">Overriding height value in pixels (minimum 0, maximum 10000000). 0 disables the override.</param>
		/// <param name="deviceScaleFactor">Overriding device scale factor value. 0 disables the override.</param>
		/// <param name="mobile">Whether to emulate mobile device. This includes viewport meta tag, overlay scrollbars, text autosizing and more.</param>
		/// <param name="scale">Scale to apply to resulting view image.</param>
		/// <param name="screenWidth">Overriding screen width value in pixels (minimum 0, maximum 10000000).</param>
		/// <param name="screenHeight">Overriding screen height value in pixels (minimum 0, maximum 10000000).</param>
		/// <param name="positionX">Overriding view X position on screen in pixels (minimum 0, maximum 10000000).</param>
		/// <param name="positionY">Overriding view Y position on screen in pixels (minimum 0, maximum 10000000).</param>
		/// <param name="dontSetVisibleSize">Do not set visible view size, rely upon explicit setVisibleSize call.</param>
		/// <param name="screenOrientation">Screen orientation override.</param>
		/// <param name="viewport">The viewport dimensions and scale. If not set, the override is cleared.</param>
		[Obsolete]
		public void SetDeviceMetricsOverride(int width, int height, double deviceScaleFactor, bool mobile, double? scale = null, int? screenWidth = null, int? screenHeight = null, int? positionX = null, int? positionY = null, bool? dontSetVisibleSize = null, EmulationDomain.ScreenOrientation screenOrientation = null, Viewport viewport = null) {
			_chrome.Send(
				"Page.setDeviceMetricsOverride",
				new {
					width,
					height,
					deviceScaleFactor,
					mobile,
					scale,
					screenWidth,
					screenHeight,
					positionX,
					positionY,
					dontSetVisibleSize,
					screenOrientation,
					viewport
				}
			);
		}

		/// <summary>
		/// Overrides the Device Orientation.
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="alpha">Mock alpha</param>
		/// <param name="beta">Mock beta</param>
		/// <param name="gamma">Mock gamma</param>
		[Obsolete]
		public void SetDeviceOrientationOverride(double alpha, double beta, double gamma) {
			_chrome.Send(
				"Page.setDeviceOrientationOverride",
				new {
					alpha,
					beta,
					gamma
				}
			);
		}

		/// <summary>
		/// Set generic font families.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="fontFamilies">Specifies font families to set. If a font family is not specified, it won't be changed.</param>
		public void SetFontFamilies(FontFamilies fontFamilies) {
			_chrome.Send(
				"Page.setFontFamilies",
				new {
					fontFamilies
				}
			);
		}

		/// <summary>
		/// Set default font sizes.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="fontSizes">Specifies font sizes to set. If a font size is not specified, it won't be changed.</param>
		public void SetFontSizes(FontSizes fontSizes) {
			_chrome.Send(
				"Page.setFontSizes",
				new {
					fontSizes
				}
			);
		}

		/// <summary>
		/// Sets given markup as the document's HTML.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="frameId">Frame id to set HTML for.</param>
		/// <param name="html">HTML content to set.</param>
		public void SetDocumentContent(string frameId, string html) {
			_chrome.Send(
				"Page.setDocumentContent",
				new {
					frameId,
					html
				}
			);
		}

		/// <summary>
		/// Set the behavior when downloading a file.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="behavior">Whether to allow all or deny all download requests, or use default Chrome behavior if available (otherwise deny).</param>
		/// <param name="downloadPath">The default path to save downloaded files to. This is requred if behavior is set to 'allow'</param>
		public void SetDownloadBehavior(string behavior, string downloadPath = null) {
			_chrome.Send(
				"Page.setDownloadBehavior",
				new {
					behavior,
					downloadPath
				}
			);
		}

		/// <summary>
		/// Overrides the Geolocation Position or Error. Omitting any of the parameters emulates position unavailable.
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="latitude">Mock latitude</param>
		/// <param name="longitude">Mock longitude</param>
		/// <param name="accuracy">Mock accuracy</param>
		[Obsolete]
		public void SetGeolocationOverride(double? latitude = null, double? longitude = null, double? accuracy = null) {
			_chrome.Send(
				"Page.setGeolocationOverride",
				new {
					latitude,
					longitude,
					accuracy
				}
			);
		}

		/// <summary>
		/// Controls whether page will emit lifecycle events.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="enabled">If true, starts emitting lifecycle events.</param>
		public void SetLifecycleEventsEnabled(bool enabled) {
			_chrome.Send(
				"Page.setLifecycleEventsEnabled",
				new {
					enabled
				}
			);
		}

		/// <summary>
		/// Toggles mouse event-based touch event emulation.
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="enabled">Whether the touch event emulation should be enabled.</param>
		/// <param name="configuration">Touch/gesture events configuration. Default: current platform.</param>
		[Obsolete]
		public void SetTouchEmulationEnabled(bool enabled, string configuration = null) {
			_chrome.Send(
				"Page.setTouchEmulationEnabled",
				new {
					enabled,
					configuration
				}
			);
		}

		/// <summary>
		/// Starts sending each frame using the `screencastFrame` event.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="format">Image compression format.</param>
		/// <param name="quality">Compression quality from range [0..100].</param>
		/// <param name="maxWidth">Maximum screenshot width.</param>
		/// <param name="maxHeight">Maximum screenshot height.</param>
		/// <param name="everyNthFrame">Send every n-th frame.</param>
		public void StartScreencast(string format = null, int? quality = null, int? maxWidth = null, int? maxHeight = null, int? everyNthFrame = null) {
			_chrome.Send(
				"Page.startScreencast",
				new {
					format,
					quality,
					maxWidth,
					maxHeight,
					everyNthFrame
				}
			);
		}

		/// <summary>
		/// Force the page stop all navigations and pending resource fetches.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void StopLoading() {
			_chrome.Send(
				"Page.stopLoading"
			);
		}

		/// <summary>
		/// Crashes renderer on the IO thread, generates minidumps.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public void Crash() {
			_chrome.Send(
				"Page.crash"
			);
		}

		/// <summary>
		/// Tries to close page, running its beforeunload hooks, if any.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public void Close() {
			_chrome.Send(
				"Page.close"
			);
		}

		/// <summary>
		/// Tries to update the web lifecycle state of the page. It will transition the page to the given state according to: https://github.com/WICG/web-lifecycle/
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="state">Target lifecycle state</param>
		public void SetWebLifecycleState(string state) {
			_chrome.Send(
				"Page.setWebLifecycleState",
				new {
					state
				}
			);
		}

		/// <summary>
		/// Stops sending each frame in the `screencastFrame`.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public void StopScreencast() {
			_chrome.Send(
				"Page.stopScreencast"
			);
		}

		/// <summary>
		/// Forces compilation cache to be generated for every subresource script.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="enabled"></param>
		public void SetProduceCompilationCache(bool enabled) {
			_chrome.Send(
				"Page.setProduceCompilationCache",
				new {
					enabled
				}
			);
		}

		/// <summary>
		/// Seeds compilation cache for given url. Compilation cache does not survive cross-process navigation.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="url"></param>
		/// <param name="data">Base64-encoded data</param>
		public void AddCompilationCache(string url, byte[] data) {
			_chrome.Send(
				"Page.addCompilationCache",
				new {
					url,
					data
				}
			);
		}

		/// <summary>
		/// Clears seeded compilation cache.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public void ClearCompilationCache() {
			_chrome.Send(
				"Page.clearCompilationCache"
			);
		}

		/// <summary>
		/// Generates a report for testing.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="message">Message to be displayed in the report.</param>
		/// <param name="group">Specifies the endpoint group to deliver the report to.</param>
		public void GenerateTestReport(string message, string group = null) {
			_chrome.Send(
				"Page.generateTestReport",
				new {
					message,
					group
				}
			);
		}

		/// <summary>
		/// Pauses page execution. Can be resumed using generic Runtime.runIfWaitingForDebugger.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public void WaitForDebugger() {
			_chrome.Send(
				"Page.waitForDebugger"
			);
		}

		/// <summary>
		/// Intercept file chooser requests and transfer control to protocol clients. When file chooser interception is enabled, native file chooser dialog is not shown. Instead, a protocol event `Page.fileChooserOpened` is emitted. File chooser can be handled with `page.handleFileChooser` command.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="enabled"></param>
		public void SetInterceptFileChooserDialog(bool enabled) {
			_chrome.Send(
				"Page.setInterceptFileChooserDialog",
				new {
					enabled
				}
			);
		}

		/// <summary>
		/// Accepts or cancels an intercepted file chooser dialog.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="action"></param>
		/// <param name="files">Array of absolute file paths to set, only respected with `accept` action.</param>
		public void HandleFileChooser(string action, string[] files = null) {
			_chrome.Send(
				"Page.handleFileChooser",
				new {
					action,
					files
				}
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("Page.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "domContentEventFired":
					if (DomContentEventFired == null) {
						return;
					}
					DomContentEventFired(
						_chrome,
						JsonConvert.DeserializeObject
							<DomContentEventFiredEventArgs>(@params.ToString())
					);
					break;
				case "fileChooserOpened":
					if (FileChooserOpened == null) {
						return;
					}
					FileChooserOpened(
						_chrome,
						JsonConvert.DeserializeObject
							<FileChooserOpenedEventArgs>(@params.ToString())
					);
					break;
				case "frameAttached":
					if (FrameAttached == null) {
						return;
					}
					FrameAttached(
						_chrome,
						JsonConvert.DeserializeObject
							<FrameAttachedEventArgs>(@params.ToString())
					);
					break;
				case "frameClearedScheduledNavigation":
					if (FrameClearedScheduledNavigation == null) {
						return;
					}
					FrameClearedScheduledNavigation(
						_chrome,
						JsonConvert.DeserializeObject
							<FrameClearedScheduledNavigationEventArgs>(@params.ToString())
					);
					break;
				case "frameDetached":
					if (FrameDetached == null) {
						return;
					}
					FrameDetached(
						_chrome,
						JsonConvert.DeserializeObject
							<FrameDetachedEventArgs>(@params.ToString())
					);
					break;
				case "frameNavigated":
					if (FrameNavigated == null) {
						return;
					}
					FrameNavigated(
						_chrome,
						JsonConvert.DeserializeObject
							<FrameNavigatedEventArgs>(@params.ToString())
					);
					break;
				case "frameResized":
					if (FrameResized == null) {
						return;
					}
					FrameResized(_chrome, EventArgs.Empty);
					break;
				case "frameRequestedNavigation":
					if (FrameRequestedNavigation == null) {
						return;
					}
					FrameRequestedNavigation(
						_chrome,
						JsonConvert.DeserializeObject
							<FrameRequestedNavigationEventArgs>(@params.ToString())
					);
					break;
				case "frameScheduledNavigation":
					if (FrameScheduledNavigation == null) {
						return;
					}
					FrameScheduledNavigation(
						_chrome,
						JsonConvert.DeserializeObject
							<FrameScheduledNavigationEventArgs>(@params.ToString())
					);
					break;
				case "frameStartedLoading":
					if (FrameStartedLoading == null) {
						return;
					}
					FrameStartedLoading(
						_chrome,
						JsonConvert.DeserializeObject
							<FrameStartedLoadingEventArgs>(@params.ToString())
					);
					break;
				case "frameStoppedLoading":
					if (FrameStoppedLoading == null) {
						return;
					}
					FrameStoppedLoading(
						_chrome,
						JsonConvert.DeserializeObject
							<FrameStoppedLoadingEventArgs>(@params.ToString())
					);
					break;
				case "downloadWillBegin":
					if (DownloadWillBegin == null) {
						return;
					}
					DownloadWillBegin(
						_chrome,
						JsonConvert.DeserializeObject
							<DownloadWillBeginEventArgs>(@params.ToString())
					);
					break;
				case "interstitialHidden":
					if (InterstitialHidden == null) {
						return;
					}
					InterstitialHidden(_chrome, EventArgs.Empty);
					break;
				case "interstitialShown":
					if (InterstitialShown == null) {
						return;
					}
					InterstitialShown(_chrome, EventArgs.Empty);
					break;
				case "javascriptDialogClosed":
					if (JavascriptDialogClosed == null) {
						return;
					}
					JavascriptDialogClosed(
						_chrome,
						JsonConvert.DeserializeObject
							<JavascriptDialogClosedEventArgs>(@params.ToString())
					);
					break;
				case "javascriptDialogOpening":
					if (JavascriptDialogOpening == null) {
						return;
					}
					JavascriptDialogOpening(
						_chrome,
						JsonConvert.DeserializeObject
							<JavascriptDialogOpeningEventArgs>(@params.ToString())
					);
					break;
				case "lifecycleEvent":
					if (LifecycleEvent == null) {
						return;
					}
					LifecycleEvent(
						_chrome,
						JsonConvert.DeserializeObject
							<LifecycleEventEventArgs>(@params.ToString())
					);
					break;
				case "loadEventFired":
					if (LoadEventFired == null) {
						return;
					}
					LoadEventFired(
						_chrome,
						JsonConvert.DeserializeObject
							<LoadEventFiredEventArgs>(@params.ToString())
					);
					break;
				case "navigatedWithinDocument":
					if (NavigatedWithinDocument == null) {
						return;
					}
					NavigatedWithinDocument(
						_chrome,
						JsonConvert.DeserializeObject
							<NavigatedWithinDocumentEventArgs>(@params.ToString())
					);
					break;
				case "screencastFrame":
					if (ScreencastFrame == null) {
						return;
					}
					ScreencastFrame(
						_chrome,
						JsonConvert.DeserializeObject
							<ScreencastFrameEventArgs>(@params.ToString())
					);
					break;
				case "screencastVisibilityChanged":
					if (ScreencastVisibilityChanged == null) {
						return;
					}
					ScreencastVisibilityChanged(
						_chrome,
						JsonConvert.DeserializeObject
							<ScreencastVisibilityChangedEventArgs>(@params.ToString())
					);
					break;
				case "windowOpen":
					if (WindowOpen == null) {
						return;
					}
					WindowOpen(
						_chrome,
						JsonConvert.DeserializeObject
							<WindowOpenEventArgs>(@params.ToString())
					);
					break;
				case "compilationCacheProduced":
					if (CompilationCacheProduced == null) {
						return;
					}
					CompilationCacheProduced(
						_chrome,
						JsonConvert.DeserializeObject
							<CompilationCacheProducedEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
