// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// This domain allows configuring virtual authenticators to test the WebAuthn API.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class WebAuthnDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the WebAuthnDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public WebAuthnDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class VirtualAuthenticatorOptions {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: AuthenticatorProtocol</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("protocol")]
			public string Protocol;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: AuthenticatorTransport</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("transport")]
			public string Transport;

			/// <summary>
			/// Defaults to false.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("hasResidentKey")]
			public bool HasResidentKey;

			/// <summary>
			/// Defaults to false.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("hasUserVerification")]
			public bool HasUserVerification;

			/// <summary>
			/// If set to true, tests of user presence will succeed immediately. Otherwise, they will not be resolved. Defaults to true.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("automaticPresenceSimulation")]
			public bool AutomaticPresenceSimulation;

			/// <summary>
			/// Sets whether User Verification succeeds or fails for an authenticator. Defaults to false.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("isUserVerified")]
			public bool IsUserVerified;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class Credential {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: binary</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("credentialId")]
			public byte[] CredentialId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("isResidentCredential")]
			public bool IsResidentCredential;

			/// <summary>
			/// Relying Party ID the credential is scoped to. Must be set when adding a credential.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("rpId")]
			public string RpId;

			/// <summary>
			/// The ECDSA P-256 private key in PKCS#8 format.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: binary</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("privateKey")]
			public byte[] PrivateKey;

			/// <summary>
			/// An opaque byte sequence with a maximum size of 64 bytes mapping the credential to a specific user.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: binary</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("userHandle")]
			public byte[] UserHandle;

			/// <summary>
			/// Signature counter. This is incremented by one for each successful assertion. See https://w3c.github.io/webauthn/#signature-counter
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("signCount")]
			public int SignCount;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Enable the WebAuthn domain and start intercepting credential storage and retrieval with a virtual authenticator.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"WebAuthn.enable"
			);
		}

		/// <summary>
		/// Disable the WebAuthn domain.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"WebAuthn.disable"
			);
		}

		/// <summary>
		/// Creates and adds a virtual authenticator.
		/// </summary>
		[JsonObject]
		public class AddVirtualAuthenticatorResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: AuthenticatorId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("authenticatorId")]
			public string AuthenticatorId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Creates and adds a virtual authenticator.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="options"></param>
		public AddVirtualAuthenticatorResult AddVirtualAuthenticator(VirtualAuthenticatorOptions options) {
			string s = _chrome.Send(
				"WebAuthn.addVirtualAuthenticator",
				new {
					options
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<AddVirtualAuthenticatorResult>();
		}

		/// <summary>
		/// Removes the given authenticator.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="authenticatorId"></param>
		public void RemoveVirtualAuthenticator(string authenticatorId) {
			_chrome.Send(
				"WebAuthn.removeVirtualAuthenticator",
				new {
					authenticatorId
				}
			);
		}

		/// <summary>
		/// Adds the credential to the specified authenticator.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="authenticatorId"></param>
		/// <param name="credential"></param>
		public void AddCredential(string authenticatorId, Credential credential) {
			_chrome.Send(
				"WebAuthn.addCredential",
				new {
					authenticatorId,
					credential
				}
			);
		}

		/// <summary>
		/// Returns a single credential stored in the given virtual authenticator that matches the credential ID.
		/// </summary>
		[JsonObject]
		public class GetCredentialResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Credential</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("credential")]
			public Credential Credential;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns a single credential stored in the given virtual authenticator that matches the credential ID.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="authenticatorId"></param>
		/// <param name="credentialId"></param>
		public GetCredentialResult GetCredential(string authenticatorId, byte[] credentialId) {
			string s = _chrome.Send(
				"WebAuthn.getCredential",
				new {
					authenticatorId,
					credentialId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetCredentialResult>();
		}

		/// <summary>
		/// Returns all the credentials stored in the given virtual authenticator.
		/// </summary>
		[JsonObject]
		public class GetCredentialsResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("credentials")]
			public Credential[] Credentials;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns all the credentials stored in the given virtual authenticator.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="authenticatorId"></param>
		public GetCredentialsResult GetCredentials(string authenticatorId) {
			string s = _chrome.Send(
				"WebAuthn.getCredentials",
				new {
					authenticatorId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetCredentialsResult>();
		}

		/// <summary>
		/// Removes a credential from the authenticator.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="authenticatorId"></param>
		/// <param name="credentialId"></param>
		public void RemoveCredential(string authenticatorId, byte[] credentialId) {
			_chrome.Send(
				"WebAuthn.removeCredential",
				new {
					authenticatorId,
					credentialId
				}
			);
		}

		/// <summary>
		/// Clears all the credentials from the specified device.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="authenticatorId"></param>
		public void ClearCredentials(string authenticatorId) {
			_chrome.Send(
				"WebAuthn.clearCredentials",
				new {
					authenticatorId
				}
			);
		}

		/// <summary>
		/// Sets whether User Verification succeeds or fails for an authenticator. The default is true.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="authenticatorId"></param>
		/// <param name="isUserVerified"></param>
		public void SetUserVerified(string authenticatorId, bool isUserVerified) {
			_chrome.Send(
				"WebAuthn.setUserVerified",
				new {
					authenticatorId,
					isUserVerified
				}
			);
		}



	}
}
