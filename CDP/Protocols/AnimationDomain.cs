// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// 
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: Runtime, DOM</description></item>
	/// </list>
	/// </summary>
	public class AnimationDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the AnimationDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public AnimationDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Event for when an animation has been cancelled.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class AnimationCanceledEventArgs : EventArgs {
			/// <summary>
			/// Id of the animation that was cancelled.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("id")]
			public string Id;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Event for when an animation has been cancelled.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<AnimationCanceledEventArgs> AnimationCanceled;

		/// <summary>
		/// Event for each animation that has been created.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class AnimationCreatedEventArgs : EventArgs {
			/// <summary>
			/// Id of the animation that was created.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("id")]
			public string Id;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Event for each animation that has been created.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<AnimationCreatedEventArgs> AnimationCreated;

		/// <summary>
		/// Event for animation that has been started.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class AnimationStartedEventArgs : EventArgs {
			/// <summary>
			/// Animation that was started.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Animation</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("animation")]
			public Animation Animation;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Event for animation that has been started.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<AnimationStartedEventArgs> AnimationStarted;

		/// <summary>
		/// Animation instance.
		/// </summary>
		[JsonObject]
		public class Animation {
			/// <summary>
			/// `Animation`'s id.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("id")]
			public string Id;

			/// <summary>
			/// `Animation`'s name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// `Animation`'s internal paused state.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("pausedState")]
			public bool PausedState;

			/// <summary>
			/// `Animation`'s play state.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("playState")]
			public string PlayState;

			/// <summary>
			/// `Animation`'s playback rate.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("playbackRate")]
			public double PlaybackRate;

			/// <summary>
			/// `Animation`'s start time.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("startTime")]
			public double StartTime;

			/// <summary>
			/// `Animation`'s current time.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("currentTime")]
			public double CurrentTime;

			/// <summary>
			/// Animation type of `Animation`.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: CSSTransition, CSSAnimation, WebAnimation
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// `Animation`'s source animation node.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: AnimationEffect</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("source")]
			public AnimationEffect Source;

			/// <summary>
			/// A unique ID for `Animation` representing the sources that triggered this CSS animation/transition.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("cssId")]
			public string CssId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// AnimationEffect instance
		/// </summary>
		[JsonObject]
		public class AnimationEffect {
			/// <summary>
			/// `AnimationEffect`'s delay.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("delay")]
			public double Delay;

			/// <summary>
			/// `AnimationEffect`'s end delay.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("endDelay")]
			public double EndDelay;

			/// <summary>
			/// `AnimationEffect`'s iteration start.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("iterationStart")]
			public double IterationStart;

			/// <summary>
			/// `AnimationEffect`'s iterations.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("iterations")]
			public double Iterations;

			/// <summary>
			/// `AnimationEffect`'s iteration duration.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("duration")]
			public double Duration;

			/// <summary>
			/// `AnimationEffect`'s playback direction.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("direction")]
			public string Direction;

			/// <summary>
			/// `AnimationEffect`'s fill mode.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("fill")]
			public string Fill;

			/// <summary>
			/// `AnimationEffect`'s target node.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: DOM.BackendNodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("backendNodeId")]
			public int BackendNodeId;

			/// <summary>
			/// `AnimationEffect`'s keyframes.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: KeyframesRule</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("keyframesRule")]
			public KeyframesRule KeyframesRule;

			/// <summary>
			/// `AnimationEffect`'s timing function.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("easing")]
			public string Easing;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Keyframes Rule
		/// </summary>
		[JsonObject]
		public class KeyframesRule {
			/// <summary>
			/// CSS keyframed animation's name.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// List of animation keyframes.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("keyframes")]
			public KeyframeStyle[] Keyframes;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Keyframe Style
		/// </summary>
		[JsonObject]
		public class KeyframeStyle {
			/// <summary>
			/// Keyframe's time offset.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("offset")]
			public string Offset;

			/// <summary>
			/// `AnimationEffect`'s timing function.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("easing")]
			public string Easing;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Disables animation domain notifications.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"Animation.disable"
			);
		}

		/// <summary>
		/// Enables animation domain notifications.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"Animation.enable"
			);
		}

		/// <summary>
		/// Returns the current time of the an animation.
		/// </summary>
		[JsonObject]
		public class GetCurrentTimeResult {
			/// <summary>
			/// Current time of the page.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("currentTime")]
			public double CurrentTime;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns the current time of the an animation.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="id">Id of animation.</param>
		public GetCurrentTimeResult GetCurrentTime(string id) {
			string s = _chrome.Send(
				"Animation.getCurrentTime",
				new {
					id
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetCurrentTimeResult>();
		}

		/// <summary>
		/// Gets the playback rate of the document timeline.
		/// </summary>
		[JsonObject]
		public class GetPlaybackRateResult {
			/// <summary>
			/// Playback rate for animations on page.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("playbackRate")]
			public double PlaybackRate;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Gets the playback rate of the document timeline.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public GetPlaybackRateResult GetPlaybackRate() {
			string s = _chrome.Send(
				"Animation.getPlaybackRate"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetPlaybackRateResult>();
		}

		/// <summary>
		/// Releases a set of animations to no longer be manipulated.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="animations">List of animation ids to seek.</param>
		public void ReleaseAnimations(string[] animations) {
			_chrome.Send(
				"Animation.releaseAnimations",
				new {
					animations
				}
			);
		}

		/// <summary>
		/// Gets the remote object of the Animation.
		/// </summary>
		[JsonObject]
		public class ResolveAnimationResult {
			/// <summary>
			/// Corresponding remote object.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("remoteObject")]
			public RuntimeDomain.RemoteObject RemoteObject;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Gets the remote object of the Animation.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="animationId">Animation id.</param>
		public ResolveAnimationResult ResolveAnimation(string animationId) {
			string s = _chrome.Send(
				"Animation.resolveAnimation",
				new {
					animationId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<ResolveAnimationResult>();
		}

		/// <summary>
		/// Seek a set of animations to a particular time within each animation.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="animations">List of animation ids to seek.</param>
		/// <param name="currentTime">Set the current time of each animation.</param>
		public void SeekAnimations(string[] animations, double currentTime) {
			_chrome.Send(
				"Animation.seekAnimations",
				new {
					animations,
					currentTime
				}
			);
		}

		/// <summary>
		/// Sets the paused state of a set of animations.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="animations">Animations to set the pause state of.</param>
		/// <param name="paused">Paused state to set to.</param>
		public void SetPaused(string[] animations, bool paused) {
			_chrome.Send(
				"Animation.setPaused",
				new {
					animations,
					paused
				}
			);
		}

		/// <summary>
		/// Sets the playback rate of the document timeline.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="playbackRate">Playback rate for animations on page</param>
		public void SetPlaybackRate(double playbackRate) {
			_chrome.Send(
				"Animation.setPlaybackRate",
				new {
					playbackRate
				}
			);
		}

		/// <summary>
		/// Sets the timing of an animation node.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="animationId">Animation id.</param>
		/// <param name="duration">Duration of the animation.</param>
		/// <param name="delay">Delay of the animation.</param>
		public void SetTiming(string animationId, double duration, double delay) {
			_chrome.Send(
				"Animation.setTiming",
				new {
					animationId,
					duration,
					delay
				}
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("Animation.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "animationCanceled":
					if (AnimationCanceled == null) {
						return;
					}
					AnimationCanceled(
						_chrome,
						JsonConvert.DeserializeObject
							<AnimationCanceledEventArgs>(@params.ToString())
					);
					break;
				case "animationCreated":
					if (AnimationCreated == null) {
						return;
					}
					AnimationCreated(
						_chrome,
						JsonConvert.DeserializeObject
							<AnimationCreatedEventArgs>(@params.ToString())
					);
					break;
				case "animationStarted":
					if (AnimationStarted == null) {
						return;
					}
					AnimationStarted(
						_chrome,
						JsonConvert.DeserializeObject
							<AnimationStartedEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
