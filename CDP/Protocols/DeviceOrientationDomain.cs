// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// 
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class DeviceOrientationDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the DeviceOrientationDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public DeviceOrientationDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}


		/// <summary>
		/// Clears the overridden Device Orientation.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void ClearDeviceOrientationOverride() {
			_chrome.Send(
				"DeviceOrientation.clearDeviceOrientationOverride"
			);
		}

		/// <summary>
		/// Overrides the Device Orientation.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="alpha">Mock alpha</param>
		/// <param name="beta">Mock beta</param>
		/// <param name="gamma">Mock gamma</param>
		public void SetDeviceOrientationOverride(double alpha, double beta, double gamma) {
			_chrome.Send(
				"DeviceOrientation.setDeviceOrientationOverride",
				new {
					alpha,
					beta,
					gamma
				}
			);
		}



	}
}
