// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// 
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class StorageDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the StorageDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public StorageDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// A cache's contents have been modified.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class CacheStorageContentUpdatedEventArgs : EventArgs {
			/// <summary>
			/// Origin to update.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("origin")]
			public string Origin;

			/// <summary>
			/// Name of cache in origin.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("cacheName")]
			public string CacheName;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// A cache's contents have been modified.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<CacheStorageContentUpdatedEventArgs> CacheStorageContentUpdated;

		/// <summary>
		/// A cache has been added/deleted.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class CacheStorageListUpdatedEventArgs : EventArgs {
			/// <summary>
			/// Origin to update.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("origin")]
			public string Origin;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// A cache has been added/deleted.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<CacheStorageListUpdatedEventArgs> CacheStorageListUpdated;

		/// <summary>
		/// The origin's IndexedDB object store has been modified.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class IndexedDBContentUpdatedEventArgs : EventArgs {
			/// <summary>
			/// Origin to update.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("origin")]
			public string Origin;

			/// <summary>
			/// Database to update.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("databaseName")]
			public string DatabaseName;

			/// <summary>
			/// ObjectStore to update.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("objectStoreName")]
			public string ObjectStoreName;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// The origin's IndexedDB object store has been modified.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<IndexedDBContentUpdatedEventArgs> IndexedDBContentUpdated;

		/// <summary>
		/// The origin's IndexedDB database list has been modified.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class IndexedDBListUpdatedEventArgs : EventArgs {
			/// <summary>
			/// Origin to update.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("origin")]
			public string Origin;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// The origin's IndexedDB database list has been modified.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<IndexedDBListUpdatedEventArgs> IndexedDBListUpdated;

		/// <summary>
		/// Usage for a storage type.
		/// </summary>
		[JsonObject]
		public class UsageForType {
			/// <summary>
			/// Name of storage type.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StorageType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("storageType")]
			public string StorageType;

			/// <summary>
			/// Storage usage (bytes).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("usage")]
			public double Usage;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Clears storage for origin.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="origin">Security origin.</param>
		/// <param name="storageTypes">Comma separated list of StorageType to clear.</param>
		public void ClearDataForOrigin(string origin, string storageTypes) {
			_chrome.Send(
				"Storage.clearDataForOrigin",
				new {
					origin,
					storageTypes
				}
			);
		}

		/// <summary>
		/// Returns usage and quota in bytes.
		/// </summary>
		[JsonObject]
		public class GetUsageAndQuotaResult {
			/// <summary>
			/// Storage usage (bytes).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("usage")]
			public double Usage;

			/// <summary>
			/// Storage quota (bytes).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("quota")]
			public double Quota;

			/// <summary>
			/// Storage usage per type (bytes).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("usageBreakdown")]
			public UsageForType[] UsageBreakdown;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns usage and quota in bytes.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="origin">Security origin.</param>
		public GetUsageAndQuotaResult GetUsageAndQuota(string origin) {
			string s = _chrome.Send(
				"Storage.getUsageAndQuota",
				new {
					origin
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetUsageAndQuotaResult>();
		}

		/// <summary>
		/// Registers origin to be notified when an update occurs to its cache storage list.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="origin">Security origin.</param>
		public void TrackCacheStorageForOrigin(string origin) {
			_chrome.Send(
				"Storage.trackCacheStorageForOrigin",
				new {
					origin
				}
			);
		}

		/// <summary>
		/// Registers origin to be notified when an update occurs to its IndexedDB.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="origin">Security origin.</param>
		public void TrackIndexedDBForOrigin(string origin) {
			_chrome.Send(
				"Storage.trackIndexedDBForOrigin",
				new {
					origin
				}
			);
		}

		/// <summary>
		/// Unregisters origin from receiving notifications for cache storage.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="origin">Security origin.</param>
		public void UntrackCacheStorageForOrigin(string origin) {
			_chrome.Send(
				"Storage.untrackCacheStorageForOrigin",
				new {
					origin
				}
			);
		}

		/// <summary>
		/// Unregisters origin from receiving notifications for IndexedDB.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="origin">Security origin.</param>
		public void UntrackIndexedDBForOrigin(string origin) {
			_chrome.Send(
				"Storage.untrackIndexedDBForOrigin",
				new {
					origin
				}
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("Storage.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "cacheStorageContentUpdated":
					if (CacheStorageContentUpdated == null) {
						return;
					}
					CacheStorageContentUpdated(
						_chrome,
						JsonConvert.DeserializeObject
							<CacheStorageContentUpdatedEventArgs>(@params.ToString())
					);
					break;
				case "cacheStorageListUpdated":
					if (CacheStorageListUpdated == null) {
						return;
					}
					CacheStorageListUpdated(
						_chrome,
						JsonConvert.DeserializeObject
							<CacheStorageListUpdatedEventArgs>(@params.ToString())
					);
					break;
				case "indexedDBContentUpdated":
					if (IndexedDBContentUpdated == null) {
						return;
					}
					IndexedDBContentUpdated(
						_chrome,
						JsonConvert.DeserializeObject
							<IndexedDBContentUpdatedEventArgs>(@params.ToString())
					);
					break;
				case "indexedDBListUpdated":
					if (IndexedDBListUpdated == null) {
						return;
					}
					IndexedDBListUpdated(
						_chrome,
						JsonConvert.DeserializeObject
							<IndexedDBListUpdatedEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
