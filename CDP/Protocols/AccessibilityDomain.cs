// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// 
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: DOM</description></item>
	/// </list>
	/// </summary>
	public class AccessibilityDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the AccessibilityDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public AccessibilityDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// A single source for a computed AX property.
		/// </summary>
		[JsonObject]
		public class AXValueSource {
			/// <summary>
			/// What type of source this is.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: AXValueSourceType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// The value of this property source.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: AXValue</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public AXValue Value;

			/// <summary>
			/// The name of the relevant attribute, if any.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("attribute")]
			public string Attribute;

			/// <summary>
			/// The value of the relevant attribute, if any.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: AXValue</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("attributeValue")]
			public AXValue AttributeValue;

			/// <summary>
			/// Whether this source is superseded by a higher priority source.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("superseded")]
			public bool Superseded;

			/// <summary>
			/// The native markup source for this value, e.g. a &lt;label&gt; element.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: AXValueNativeSourceType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nativeSource")]
			public string NativeSource;

			/// <summary>
			/// The value, such as a node or node list, of the native source.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: AXValue</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nativeSourceValue")]
			public AXValue NativeSourceValue;

			/// <summary>
			/// Whether the value for this property is invalid.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("invalid")]
			public bool Invalid;

			/// <summary>
			/// Reason for the value being invalid, if it is.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("invalidReason")]
			public string InvalidReason;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class AXRelatedNode {
			/// <summary>
			/// The BackendNodeId of the related DOM node.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: DOM.BackendNodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("backendDOMNodeId")]
			public int BackendDOMNodeId;

			/// <summary>
			/// The IDRef value provided, if any.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("idref")]
			public string Idref;

			/// <summary>
			/// The text alternative of this node in the current context.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("text")]
			public string Text;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class AXProperty {
			/// <summary>
			/// The name of this property.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: AXPropertyName</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// The value of this property.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: AXValue</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public AXValue Value;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// A single computed AX property.
		/// </summary>
		[JsonObject]
		public class AXValue {
			/// <summary>
			/// The type of this value.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: AXValueType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// The computed value of this property.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: any</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public object Value;

			/// <summary>
			/// One or more related nodes, if applicable.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("relatedNodes")]
			public AXRelatedNode[] RelatedNodes;

			/// <summary>
			/// The sources which contributed to the computation of this property.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sources")]
			public AXValueSource[] Sources;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// A node in the accessibility tree.
		/// </summary>
		[JsonObject]
		public class AXNode {
			/// <summary>
			/// Unique identifier for this node.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: AXNodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public string NodeId;

			/// <summary>
			/// Whether this node is ignored for accessibility
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("ignored")]
			public bool Ignored;

			/// <summary>
			/// Collection of reasons why this node is hidden.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("ignoredReasons")]
			public AXProperty[] IgnoredReasons;

			/// <summary>
			/// This `Node`'s role, whether explicit or implicit.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: AXValue</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("role")]
			public AXValue Role;

			/// <summary>
			/// The accessible name for this `Node`.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: AXValue</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public AXValue Name;

			/// <summary>
			/// The accessible description for this `Node`.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: AXValue</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("description")]
			public AXValue Description;

			/// <summary>
			/// The value for this `Node`.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: AXValue</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public AXValue Value;

			/// <summary>
			/// All other properties
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("properties")]
			public AXProperty[] Properties;

			/// <summary>
			/// IDs for each of this node's child nodes.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("childIds")]
			public string[] ChildIds;

			/// <summary>
			/// The backend ID for the associated DOM node, if any.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: DOM.BackendNodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("backendDOMNodeId")]
			public int BackendDOMNodeId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Disables the accessibility domain.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"Accessibility.disable"
			);
		}

		/// <summary>
		/// Enables the accessibility domain which causes `AXNodeId`s to remain consistent between method calls. This turns on accessibility for the page, which can impact performance until accessibility is disabled.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"Accessibility.enable"
			);
		}

		/// <summary>
		/// Fetches the accessibility node and partial accessibility tree for this DOM node, if it exists.
		/// </summary>
		[JsonObject]
		public class GetPartialAXTreeResult {
			/// <summary>
			/// The `Accessibility.AXNode` for this DOM node, if it exists, plus its ancestors, siblings and children, if requested.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodes")]
			public AXNode[] Nodes;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fetches the accessibility node and partial accessibility tree for this DOM node, if it exists.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Identifier of the node to get the partial accessibility tree for.</param>
		/// <param name="backendNodeId">Identifier of the backend node to get the partial accessibility tree for.</param>
		/// <param name="objectId">JavaScript object id of the node wrapper to get the partial accessibility tree for.</param>
		/// <param name="fetchRelatives">Whether to fetch this nodes ancestors, siblings and children. Defaults to true.</param>
		public GetPartialAXTreeResult GetPartialAXTree(int? nodeId = null, int? backendNodeId = null, string objectId = null, bool? fetchRelatives = null) {
			string s = _chrome.Send(
				"Accessibility.getPartialAXTree",
				new {
					nodeId,
					backendNodeId,
					objectId,
					fetchRelatives
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetPartialAXTreeResult>();
		}

		/// <summary>
		/// Fetches the entire accessibility tree
		/// </summary>
		[JsonObject]
		public class GetFullAXTreeResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodes")]
			public AXNode[] Nodes;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fetches the entire accessibility tree
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public GetFullAXTreeResult GetFullAXTree() {
			string s = _chrome.Send(
				"Accessibility.getFullAXTree"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetFullAXTreeResult>();
		}



	}
}
