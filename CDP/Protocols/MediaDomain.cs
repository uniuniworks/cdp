// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// This domain allows detailed inspection of media elements
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class MediaDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the MediaDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public MediaDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// This can be called multiple times, and can be used to set / override / remove player properties. A null propValue indicates removal.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class PlayerPropertiesChangedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: PlayerId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("playerId")]
			public string PlayerId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("properties")]
			public PlayerProperty[] Properties;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// This can be called multiple times, and can be used to set / override / remove player properties. A null propValue indicates removal.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<PlayerPropertiesChangedEventArgs> PlayerPropertiesChanged;

		/// <summary>
		/// Send events as a list, allowing them to be batched on the browser for less congestion. If batched, events must ALWAYS be in chronological order.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class PlayerEventsAddedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: PlayerId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("playerId")]
			public string PlayerId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("events")]
			public PlayerEvent[] Events;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Send events as a list, allowing them to be batched on the browser for less congestion. If batched, events must ALWAYS be in chronological order.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<PlayerEventsAddedEventArgs> PlayerEventsAdded;

		/// <summary>
		/// Called whenever a player is created, or when a new agent joins and recieves a list of active players. If an agent is restored, it will recieve the full list of player ids and all events again.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class PlayersCreatedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("players")]
			public string[] Players;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Called whenever a player is created, or when a new agent joins and recieves a list of active players. If an agent is restored, it will recieve the full list of player ids and all events again.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<PlayersCreatedEventArgs> PlayersCreated;

		/// <summary>
		/// Player Property type
		/// </summary>
		[JsonObject]
		public class PlayerProperty {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public string Value;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class PlayerEvent {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: PlayerEventType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// Events are timestamped relative to the start of the player creation not relative to the start of playback.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Timestamp</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public string Value;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Enables the Media domain
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"Media.enable"
			);
		}

		/// <summary>
		/// Disables the Media domain.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"Media.disable"
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("Media.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "playerPropertiesChanged":
					if (PlayerPropertiesChanged == null) {
						return;
					}
					PlayerPropertiesChanged(
						_chrome,
						JsonConvert.DeserializeObject
							<PlayerPropertiesChangedEventArgs>(@params.ToString())
					);
					break;
				case "playerEventsAdded":
					if (PlayerEventsAdded == null) {
						return;
					}
					PlayerEventsAdded(
						_chrome,
						JsonConvert.DeserializeObject
							<PlayerEventsAddedEventArgs>(@params.ToString())
					);
					break;
				case "playersCreated":
					if (PlayersCreated == null) {
						return;
					}
					PlayersCreated(
						_chrome,
						JsonConvert.DeserializeObject
							<PlayersCreatedEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
