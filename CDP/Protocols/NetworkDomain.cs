// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// Network domain allows tracking network activities of the page. It exposes information about http, file, data and other requests and responses, their headers, bodies, timing, etc.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: false</description></item>
	/// <item><description>dependencies: Debugger, Runtime, Security</description></item>
	/// </list>
	/// </summary>
	public class NetworkDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the NetworkDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public NetworkDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Fired when data chunk was received over the network.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class DataReceivedEventArgs : EventArgs {
			/// <summary>
			/// Request identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestId")]
			public string RequestId;

			/// <summary>
			/// Timestamp.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: MonotonicTime</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// Data chunk length.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("dataLength")]
			public int DataLength;

			/// <summary>
			/// Actual bytes received (might be less than dataLength for compressed encodings).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("encodedDataLength")]
			public int EncodedDataLength;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when data chunk was received over the network.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<DataReceivedEventArgs> DataReceived;

		/// <summary>
		/// Fired when EventSource message is received.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class EventSourceMessageReceivedEventArgs : EventArgs {
			/// <summary>
			/// Request identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestId")]
			public string RequestId;

			/// <summary>
			/// Timestamp.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: MonotonicTime</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// Message type.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("eventName")]
			public string EventName;

			/// <summary>
			/// Message identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("eventId")]
			public string EventId;

			/// <summary>
			/// Message content.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("data")]
			public string Data;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when EventSource message is received.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<EventSourceMessageReceivedEventArgs> EventSourceMessageReceived;

		/// <summary>
		/// Fired when HTTP request has failed to load.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class LoadingFailedEventArgs : EventArgs {
			/// <summary>
			/// Request identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestId")]
			public string RequestId;

			/// <summary>
			/// Timestamp.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: MonotonicTime</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// Resource type.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ResourceType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// User friendly error message.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("errorText")]
			public string ErrorText;

			/// <summary>
			/// True if loading was canceled.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("canceled")]
			public bool Canceled;

			/// <summary>
			/// The reason why loading was blocked, if any.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: BlockedReason</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("blockedReason")]
			public string BlockedReason;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when HTTP request has failed to load.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<LoadingFailedEventArgs> LoadingFailed;

		/// <summary>
		/// Fired when HTTP request has finished loading.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class LoadingFinishedEventArgs : EventArgs {
			/// <summary>
			/// Request identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestId")]
			public string RequestId;

			/// <summary>
			/// Timestamp.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: MonotonicTime</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// Total number of bytes received for this request.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("encodedDataLength")]
			public double EncodedDataLength;

			/// <summary>
			/// Set when 1) response was blocked by Cross-Origin Read Blocking and also 2) this needs to be reported to the DevTools console.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("shouldReportCorbBlocking")]
			public bool ShouldReportCorbBlocking;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when HTTP request has finished loading.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<LoadingFinishedEventArgs> LoadingFinished;

		/// <summary>
		/// Details of an intercepted HTTP request, which must be either allowed, blocked, modified or mocked. Deprecated, use Fetch.requestPaused instead.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class RequestInterceptedEventArgs : EventArgs {
			/// <summary>
			/// Each request the page makes will have a unique id, however if any redirects are encountered while processing that fetch, they will be reported with the same id as the original fetch. Likewise if HTTP authentication is needed then the same fetch id will be used.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: InterceptionId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("interceptionId")]
			public string InterceptionId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Request</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("request")]
			public Request Request;

			/// <summary>
			/// The id of the frame that initiated the request.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Page.FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// How the requested resource will be used.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ResourceType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("resourceType")]
			public string ResourceType;

			/// <summary>
			/// Whether this is a navigation request, which can abort the navigation completely.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("isNavigationRequest")]
			public bool IsNavigationRequest;

			/// <summary>
			/// Set if the request is a navigation that will result in a download. Only present after response is received from the server (i.e. HeadersReceived stage).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("isDownload")]
			public bool IsDownload;

			/// <summary>
			/// Redirect location, only sent if a redirect was intercepted.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("redirectUrl")]
			public string RedirectUrl;

			/// <summary>
			/// Details of the Authorization Challenge encountered. If this is set then continueInterceptedRequest must contain an authChallengeResponse.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: AuthChallenge</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("authChallenge")]
			public AuthChallenge AuthChallenge;

			/// <summary>
			/// Response error if intercepted at response stage or if redirect occurred while intercepting request.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: ErrorReason</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("responseErrorReason")]
			public string ResponseErrorReason;

			/// <summary>
			/// Response code if intercepted at response stage or if redirect occurred while intercepting request or auth retry occurred.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("responseStatusCode")]
			public int ResponseStatusCode;

			/// <summary>
			/// Response headers if intercepted at the response stage or if redirect occurred while intercepting request or auth retry occurred.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Headers</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("responseHeaders")]
			public Headers ResponseHeaders;

			/// <summary>
			/// If the intercepted request had a corresponding requestWillBeSent event fired for it, then this requestId will be the same as the requestId present in the requestWillBeSent event.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestId")]
			public string RequestId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Details of an intercepted HTTP request, which must be either allowed, blocked, modified or mocked. Deprecated, use Fetch.requestPaused instead.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<RequestInterceptedEventArgs> RequestIntercepted;

		/// <summary>
		/// Fired if request ended up loading from cache.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class RequestServedFromCacheEventArgs : EventArgs {
			/// <summary>
			/// Request identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestId")]
			public string RequestId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired if request ended up loading from cache.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<RequestServedFromCacheEventArgs> RequestServedFromCache;

		/// <summary>
		/// Fired when page is about to send HTTP request.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class RequestWillBeSentEventArgs : EventArgs {
			/// <summary>
			/// Request identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestId")]
			public string RequestId;

			/// <summary>
			/// Loader identifier. Empty string if the request is fetched from worker.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: LoaderId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("loaderId")]
			public string LoaderId;

			/// <summary>
			/// URL of the document this request is loaded for.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("documentURL")]
			public string DocumentURL;

			/// <summary>
			/// Request data.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Request</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("request")]
			public Request Request;

			/// <summary>
			/// Timestamp.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: MonotonicTime</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// Timestamp.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: TimeSinceEpoch</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("wallTime")]
			public double WallTime;

			/// <summary>
			/// Request initiator.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Initiator</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("initiator")]
			public Initiator Initiator;

			/// <summary>
			/// Redirect response data.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Response</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("redirectResponse")]
			public Response RedirectResponse;

			/// <summary>
			/// Type of this resource.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: ResourceType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// Frame identifier.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Page.FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// Whether the request is initiated by a user gesture. Defaults to false.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("hasUserGesture")]
			public bool HasUserGesture;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when page is about to send HTTP request.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<RequestWillBeSentEventArgs> RequestWillBeSent;

		/// <summary>
		/// Fired when resource loading priority is changed
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ResourceChangedPriorityEventArgs : EventArgs {
			/// <summary>
			/// Request identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestId")]
			public string RequestId;

			/// <summary>
			/// New priority
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ResourcePriority</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("newPriority")]
			public string NewPriority;

			/// <summary>
			/// Timestamp.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: MonotonicTime</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when resource loading priority is changed
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ResourceChangedPriorityEventArgs> ResourceChangedPriority;

		/// <summary>
		/// Fired when a signed exchange was received over the network
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class SignedExchangeReceivedEventArgs : EventArgs {
			/// <summary>
			/// Request identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestId")]
			public string RequestId;

			/// <summary>
			/// Information about the signed exchange response.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SignedExchangeInfo</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("info")]
			public SignedExchangeInfo Info;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when a signed exchange was received over the network
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<SignedExchangeReceivedEventArgs> SignedExchangeReceived;

		/// <summary>
		/// Fired when HTTP response is available.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ResponseReceivedEventArgs : EventArgs {
			/// <summary>
			/// Request identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestId")]
			public string RequestId;

			/// <summary>
			/// Loader identifier. Empty string if the request is fetched from worker.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: LoaderId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("loaderId")]
			public string LoaderId;

			/// <summary>
			/// Timestamp.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: MonotonicTime</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// Resource type.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ResourceType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// Response data.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Response</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("response")]
			public Response Response;

			/// <summary>
			/// Frame identifier.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Page.FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when HTTP response is available.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ResponseReceivedEventArgs> ResponseReceived;

		/// <summary>
		/// Fired when WebSocket is closed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class WebSocketClosedEventArgs : EventArgs {
			/// <summary>
			/// Request identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestId")]
			public string RequestId;

			/// <summary>
			/// Timestamp.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: MonotonicTime</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when WebSocket is closed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<WebSocketClosedEventArgs> WebSocketClosed;

		/// <summary>
		/// Fired upon WebSocket creation.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class WebSocketCreatedEventArgs : EventArgs {
			/// <summary>
			/// Request identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestId")]
			public string RequestId;

			/// <summary>
			/// WebSocket request URL.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Request initiator.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Initiator</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("initiator")]
			public Initiator Initiator;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired upon WebSocket creation.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<WebSocketCreatedEventArgs> WebSocketCreated;

		/// <summary>
		/// Fired when WebSocket message error occurs.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class WebSocketFrameErrorEventArgs : EventArgs {
			/// <summary>
			/// Request identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestId")]
			public string RequestId;

			/// <summary>
			/// Timestamp.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: MonotonicTime</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// WebSocket error message.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("errorMessage")]
			public string ErrorMessage;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when WebSocket message error occurs.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<WebSocketFrameErrorEventArgs> WebSocketFrameError;

		/// <summary>
		/// Fired when WebSocket message is received.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class WebSocketFrameReceivedEventArgs : EventArgs {
			/// <summary>
			/// Request identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestId")]
			public string RequestId;

			/// <summary>
			/// Timestamp.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: MonotonicTime</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// WebSocket response data.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: WebSocketFrame</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("response")]
			public WebSocketFrame Response;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when WebSocket message is received.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<WebSocketFrameReceivedEventArgs> WebSocketFrameReceived;

		/// <summary>
		/// Fired when WebSocket message is sent.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class WebSocketFrameSentEventArgs : EventArgs {
			/// <summary>
			/// Request identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestId")]
			public string RequestId;

			/// <summary>
			/// Timestamp.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: MonotonicTime</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// WebSocket response data.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: WebSocketFrame</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("response")]
			public WebSocketFrame Response;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when WebSocket message is sent.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<WebSocketFrameSentEventArgs> WebSocketFrameSent;

		/// <summary>
		/// Fired when WebSocket handshake response becomes available.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class WebSocketHandshakeResponseReceivedEventArgs : EventArgs {
			/// <summary>
			/// Request identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestId")]
			public string RequestId;

			/// <summary>
			/// Timestamp.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: MonotonicTime</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// WebSocket response data.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: WebSocketResponse</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("response")]
			public WebSocketResponse Response;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when WebSocket handshake response becomes available.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<WebSocketHandshakeResponseReceivedEventArgs> WebSocketHandshakeResponseReceived;

		/// <summary>
		/// Fired when WebSocket is about to initiate handshake.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class WebSocketWillSendHandshakeRequestEventArgs : EventArgs {
			/// <summary>
			/// Request identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestId")]
			public string RequestId;

			/// <summary>
			/// Timestamp.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: MonotonicTime</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// UTC Timestamp.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: TimeSinceEpoch</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("wallTime")]
			public double WallTime;

			/// <summary>
			/// WebSocket request data.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: WebSocketRequest</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("request")]
			public WebSocketRequest Request;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when WebSocket is about to initiate handshake.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<WebSocketWillSendHandshakeRequestEventArgs> WebSocketWillSendHandshakeRequest;

		/// <summary>
		/// Fired when additional information about a requestWillBeSent event is available from the network stack. Not every requestWillBeSent event will have an additional requestWillBeSentExtraInfo fired for it, and there is no guarantee whether requestWillBeSent or requestWillBeSentExtraInfo will be fired first for the same request.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class RequestWillBeSentExtraInfoEventArgs : EventArgs {
			/// <summary>
			/// Request identifier. Used to match this information to an existing requestWillBeSent event.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestId")]
			public string RequestId;

			/// <summary>
			/// A list of cookies which will not be sent with this request along with corresponding reasons for blocking.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("blockedCookies")]
			public BlockedCookieWithReason[] BlockedCookies;

			/// <summary>
			/// Raw request headers as they will be sent over the wire.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Headers</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("headers")]
			public Headers Headers;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when additional information about a requestWillBeSent event is available from the network stack. Not every requestWillBeSent event will have an additional requestWillBeSentExtraInfo fired for it, and there is no guarantee whether requestWillBeSent or requestWillBeSentExtraInfo will be fired first for the same request.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<RequestWillBeSentExtraInfoEventArgs> RequestWillBeSentExtraInfo;

		/// <summary>
		/// Fired when additional information about a responseReceived event is available from the network stack. Not every responseReceived event will have an additional responseReceivedExtraInfo for it, and responseReceivedExtraInfo may be fired before or after responseReceived.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ResponseReceivedExtraInfoEventArgs : EventArgs {
			/// <summary>
			/// Request identifier. Used to match this information to another responseReceived event.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestId")]
			public string RequestId;

			/// <summary>
			/// A list of cookies which were not stored from the response along with the corresponding reasons for blocking. The cookies here may not be valid due to syntax errors, which are represented by the invalid cookie line string instead of a proper cookie.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("blockedCookies")]
			public BlockedSetCookieWithReason[] BlockedCookies;

			/// <summary>
			/// Raw response headers as they were received over the wire.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Headers</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("headers")]
			public Headers Headers;

			/// <summary>
			/// Raw response header text as it was received over the wire. The raw text may not always be available, such as in the case of HTTP/2 or QUIC.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("headersText")]
			public string HeadersText;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when additional information about a responseReceived event is available from the network stack. Not every responseReceived event will have an additional responseReceivedExtraInfo for it, and responseReceivedExtraInfo may be fired before or after responseReceived.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ResponseReceivedExtraInfoEventArgs> ResponseReceivedExtraInfo;

		/// <summary>
		/// Request / response headers as keys / values of JSON object.
		/// </summary>
		[JsonObject]
		public class Headers {


			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Timing information for the request.
		/// </summary>
		[JsonObject]
		public class ResourceTiming {
			/// <summary>
			/// Timing's requestTime is a baseline in seconds, while the other numbers are ticks in milliseconds relatively to this requestTime.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestTime")]
			public double RequestTime;

			/// <summary>
			/// Started resolving proxy.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("proxyStart")]
			public double ProxyStart;

			/// <summary>
			/// Finished resolving proxy.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("proxyEnd")]
			public double ProxyEnd;

			/// <summary>
			/// Started DNS address resolve.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("dnsStart")]
			public double DnsStart;

			/// <summary>
			/// Finished DNS address resolve.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("dnsEnd")]
			public double DnsEnd;

			/// <summary>
			/// Started connecting to the remote host.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("connectStart")]
			public double ConnectStart;

			/// <summary>
			/// Connected to the remote host.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("connectEnd")]
			public double ConnectEnd;

			/// <summary>
			/// Started SSL handshake.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sslStart")]
			public double SslStart;

			/// <summary>
			/// Finished SSL handshake.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sslEnd")]
			public double SslEnd;

			/// <summary>
			/// Started running ServiceWorker.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("workerStart")]
			public double WorkerStart;

			/// <summary>
			/// Finished Starting ServiceWorker.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("workerReady")]
			public double WorkerReady;

			/// <summary>
			/// Started sending request.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sendStart")]
			public double SendStart;

			/// <summary>
			/// Finished sending request.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sendEnd")]
			public double SendEnd;

			/// <summary>
			/// Time the server started pushing request.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("pushStart")]
			public double PushStart;

			/// <summary>
			/// Time the server finished pushing request.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("pushEnd")]
			public double PushEnd;

			/// <summary>
			/// Finished receiving response headers.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("receiveHeadersEnd")]
			public double ReceiveHeadersEnd;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// HTTP request data.
		/// </summary>
		[JsonObject]
		public class Request {
			/// <summary>
			/// Request URL (without fragment).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Fragment of the requested URL starting with hash, if present.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("urlFragment")]
			public string UrlFragment;

			/// <summary>
			/// HTTP request method.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("method")]
			public string Method;

			/// <summary>
			/// HTTP request headers.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Headers</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("headers")]
			public Headers Headers;

			/// <summary>
			/// HTTP POST request data.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("postData")]
			public string PostData;

			/// <summary>
			/// True when the request has POST data. Note that postData might still be omitted when this flag is true when the data is too long.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("hasPostData")]
			public bool HasPostData;

			/// <summary>
			/// The mixed content type of the request.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Security.MixedContentType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("mixedContentType")]
			public string MixedContentType;

			/// <summary>
			/// Priority of the resource request at the time request is sent.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ResourcePriority</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("initialPriority")]
			public string InitialPriority;

			/// <summary>
			/// The referrer policy of the request, as defined in https://www.w3.org/TR/referrer-policy/
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: unsafe-url, no-referrer-when-downgrade, no-referrer, origin, origin-when-cross-origin, same-origin, strict-origin, strict-origin-when-cross-origin
			/// </summary>
			[JsonProperty("referrerPolicy")]
			public string ReferrerPolicy;

			/// <summary>
			/// Whether is loaded via link preload.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("isLinkPreload")]
			public bool IsLinkPreload;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Details of a signed certificate timestamp (SCT).
		/// </summary>
		[JsonObject]
		public class SignedCertificateTimestamp {
			/// <summary>
			/// Validation status.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("status")]
			public string Status;

			/// <summary>
			/// Origin.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("origin")]
			public string Origin;

			/// <summary>
			/// Log name / description.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("logDescription")]
			public string LogDescription;

			/// <summary>
			/// Log ID.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("logId")]
			public string LogId;

			/// <summary>
			/// Issuance date.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: TimeSinceEpoch</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// Hash algorithm.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("hashAlgorithm")]
			public string HashAlgorithm;

			/// <summary>
			/// Signature algorithm.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("signatureAlgorithm")]
			public string SignatureAlgorithm;

			/// <summary>
			/// Signature data.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("signatureData")]
			public string SignatureData;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Security details about a request.
		/// </summary>
		[JsonObject]
		public class SecurityDetails {
			/// <summary>
			/// Protocol name (e.g. "TLS 1.2" or "QUIC").
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("protocol")]
			public string Protocol;

			/// <summary>
			/// Key Exchange used by the connection, or the empty string if not applicable.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("keyExchange")]
			public string KeyExchange;

			/// <summary>
			/// (EC)DH group used by the connection, if applicable.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("keyExchangeGroup")]
			public string KeyExchangeGroup;

			/// <summary>
			/// Cipher name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("cipher")]
			public string Cipher;

			/// <summary>
			/// TLS MAC. Note that AEAD ciphers do not have separate MACs.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("mac")]
			public string Mac;

			/// <summary>
			/// Certificate ID value.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Security.CertificateId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("certificateId")]
			public int CertificateId;

			/// <summary>
			/// Certificate subject name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("subjectName")]
			public string SubjectName;

			/// <summary>
			/// Subject Alternative Name (SAN) DNS names and IP addresses.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sanList")]
			public string[] SanList;

			/// <summary>
			/// Name of the issuing CA.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("issuer")]
			public string Issuer;

			/// <summary>
			/// Certificate valid from date.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: TimeSinceEpoch</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("validFrom")]
			public double ValidFrom;

			/// <summary>
			/// Certificate valid to (expiration) date
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: TimeSinceEpoch</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("validTo")]
			public double ValidTo;

			/// <summary>
			/// List of signed certificate timestamps (SCTs).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("signedCertificateTimestampList")]
			public SignedCertificateTimestamp[] SignedCertificateTimestampList;

			/// <summary>
			/// Whether the request complied with Certificate Transparency policy
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: CertificateTransparencyCompliance</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("certificateTransparencyCompliance")]
			public string CertificateTransparencyCompliance;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// HTTP response data.
		/// </summary>
		[JsonObject]
		public class Response {
			/// <summary>
			/// Response URL. This URL can be different from CachedResource.url in case of redirect.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// HTTP response status code.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("status")]
			public int Status;

			/// <summary>
			/// HTTP response status text.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("statusText")]
			public string StatusText;

			/// <summary>
			/// HTTP response headers.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Headers</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("headers")]
			public Headers Headers;

			/// <summary>
			/// HTTP response headers text.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("headersText")]
			public string HeadersText;

			/// <summary>
			/// Resource mimeType as determined by the browser.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("mimeType")]
			public string MimeType;

			/// <summary>
			/// Refined HTTP request headers that were actually transmitted over the network.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Headers</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestHeaders")]
			public Headers RequestHeaders;

			/// <summary>
			/// HTTP request headers text.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestHeadersText")]
			public string RequestHeadersText;

			/// <summary>
			/// Specifies whether physical connection was actually reused for this request.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("connectionReused")]
			public bool ConnectionReused;

			/// <summary>
			/// Physical connection id that was actually used for this request.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("connectionId")]
			public double ConnectionId;

			/// <summary>
			/// Remote IP address.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("remoteIPAddress")]
			public string RemoteIPAddress;

			/// <summary>
			/// Remote port.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("remotePort")]
			public int RemotePort;

			/// <summary>
			/// Specifies that the request was served from the disk cache.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("fromDiskCache")]
			public bool FromDiskCache;

			/// <summary>
			/// Specifies that the request was served from the ServiceWorker.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("fromServiceWorker")]
			public bool FromServiceWorker;

			/// <summary>
			/// Specifies that the request was served from the prefetch cache.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("fromPrefetchCache")]
			public bool FromPrefetchCache;

			/// <summary>
			/// Total number of bytes received for this request so far.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("encodedDataLength")]
			public double EncodedDataLength;

			/// <summary>
			/// Timing information for the given request.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: ResourceTiming</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timing")]
			public ResourceTiming Timing;

			/// <summary>
			/// Protocol used to fetch this request.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("protocol")]
			public string Protocol;

			/// <summary>
			/// Security state of the request resource.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Security.SecurityState</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("securityState")]
			public string SecurityState;

			/// <summary>
			/// Security details for the request.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: SecurityDetails</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("securityDetails")]
			public SecurityDetails SecurityDetails;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// WebSocket request data.
		/// </summary>
		[JsonObject]
		public class WebSocketRequest {
			/// <summary>
			/// HTTP request headers.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Headers</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("headers")]
			public Headers Headers;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// WebSocket response data.
		/// </summary>
		[JsonObject]
		public class WebSocketResponse {
			/// <summary>
			/// HTTP response status code.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("status")]
			public int Status;

			/// <summary>
			/// HTTP response status text.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("statusText")]
			public string StatusText;

			/// <summary>
			/// HTTP response headers.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Headers</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("headers")]
			public Headers Headers;

			/// <summary>
			/// HTTP response headers text.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("headersText")]
			public string HeadersText;

			/// <summary>
			/// HTTP request headers.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Headers</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestHeaders")]
			public Headers RequestHeaders;

			/// <summary>
			/// HTTP request headers text.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestHeadersText")]
			public string RequestHeadersText;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// WebSocket message data. This represents an entire WebSocket message, not just a fragmented frame as the name suggests.
		/// </summary>
		[JsonObject]
		public class WebSocketFrame {
			/// <summary>
			/// WebSocket message opcode.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("opcode")]
			public double Opcode;

			/// <summary>
			/// WebSocket message mask.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("mask")]
			public bool Mask;

			/// <summary>
			/// WebSocket message payload data. If the opcode is 1, this is a text message and payloadData is a UTF-8 string. If the opcode isn't 1, then payloadData is a base64 encoded string representing binary data.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("payloadData")]
			public string PayloadData;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Information about the cached resource.
		/// </summary>
		[JsonObject]
		public class CachedResource {
			/// <summary>
			/// Resource URL. This is the url of the original network request.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Type of this resource.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ResourceType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// Cached response data.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Response</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("response")]
			public Response Response;

			/// <summary>
			/// Cached response body size.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("bodySize")]
			public double BodySize;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Information about the request initiator.
		/// </summary>
		[JsonObject]
		public class Initiator {
			/// <summary>
			/// Type of this initiator.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: parser, script, preload, SignedExchange, other
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// Initiator JavaScript stack trace, set for Script only.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Runtime.StackTrace</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("stack")]
			public RuntimeDomain.StackTrace Stack;

			/// <summary>
			/// Initiator URL, set for Parser type or for Script type (when script is importing module) or for SignedExchange type.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Initiator line number, set for Parser type or for Script type (when script is importing module) (0-based).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("lineNumber")]
			public double LineNumber;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Cookie object
		/// </summary>
		[JsonObject]
		public class Cookie {
			/// <summary>
			/// Cookie name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// Cookie value.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public string Value;

			/// <summary>
			/// Cookie domain.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("domain")]
			public string Domain;

			/// <summary>
			/// Cookie path.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("path")]
			public string Path;

			/// <summary>
			/// Cookie expiration date as the number of seconds since the UNIX epoch.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("expires")]
			public double Expires;

			/// <summary>
			/// Cookie size.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("size")]
			public int Size;

			/// <summary>
			/// True if cookie is http-only.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("httpOnly")]
			public bool HttpOnly;

			/// <summary>
			/// True if cookie is secure.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("secure")]
			public bool Secure;

			/// <summary>
			/// True in case of session cookie.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("session")]
			public bool Session;

			/// <summary>
			/// Cookie SameSite type.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: CookieSameSite</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sameSite")]
			public string SameSite;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// A cookie which was not stored from a response with the corresponding reason.
		/// </summary>
		[JsonObject]
		public class BlockedSetCookieWithReason {
			/// <summary>
			/// The reason(s) this cookie was blocked.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("blockedReasons")]
			public string[] BlockedReasons;

			/// <summary>
			/// The string representing this individual cookie as it would appear in the header. This is not the entire "cookie" or "set-cookie" header which could have multiple cookies.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("cookieLine")]
			public string CookieLine;

			/// <summary>
			/// The cookie object which represents the cookie which was not stored. It is optional because sometimes complete cookie information is not available, such as in the case of parsing errors.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Cookie</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("cookie")]
			public Cookie Cookie;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// A cookie with was not sent with a request with the corresponding reason.
		/// </summary>
		[JsonObject]
		public class BlockedCookieWithReason {
			/// <summary>
			/// The reason(s) the cookie was blocked.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("blockedReasons")]
			public string[] BlockedReasons;

			/// <summary>
			/// The cookie object representing the cookie which was not sent.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Cookie</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("cookie")]
			public Cookie Cookie;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Cookie parameter object
		/// </summary>
		[JsonObject]
		public class CookieParam {
			/// <summary>
			/// Cookie name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// Cookie value.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public string Value;

			/// <summary>
			/// The request-URI to associate with the setting of the cookie. This value can affect the default domain and path values of the created cookie.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Cookie domain.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("domain")]
			public string Domain;

			/// <summary>
			/// Cookie path.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("path")]
			public string Path;

			/// <summary>
			/// True if cookie is secure.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("secure")]
			public bool Secure;

			/// <summary>
			/// True if cookie is http-only.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("httpOnly")]
			public bool HttpOnly;

			/// <summary>
			/// Cookie SameSite type.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: CookieSameSite</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sameSite")]
			public string SameSite;

			/// <summary>
			/// Cookie expiration date, session cookie if not set
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: TimeSinceEpoch</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("expires")]
			public double Expires;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Authorization challenge for HTTP status code 401 or 407.
		/// </summary>
		[JsonObject]
		public class AuthChallenge {
			/// <summary>
			/// Source of the authentication challenge.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: Server, Proxy
			/// </summary>
			[JsonProperty("source")]
			public string Source;

			/// <summary>
			/// Origin of the challenger.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("origin")]
			public string Origin;

			/// <summary>
			/// The authentication scheme used, such as basic or digest
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scheme")]
			public string Scheme;

			/// <summary>
			/// The realm of the challenge. May be empty.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("realm")]
			public string Realm;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Response to an AuthChallenge.
		/// </summary>
		[JsonObject]
		public class AuthChallengeResponse {
			/// <summary>
			/// The decision on what to do in response to the authorization challenge.  Default means deferring to the default behavior of the net stack, which will likely either the Cancel authentication or display a popup dialog box.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: Default, CancelAuth, ProvideCredentials
			/// </summary>
			[JsonProperty("response")]
			public string Response;

			/// <summary>
			/// The username to provide, possibly empty. Should only be set if response is ProvideCredentials.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("username")]
			public string Username;

			/// <summary>
			/// The password to provide, possibly empty. Should only be set if response is ProvideCredentials.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("password")]
			public string Password;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Request pattern for interception.
		/// </summary>
		[JsonObject]
		public class RequestPattern {
			/// <summary>
			/// Wildcards ('*' -&gt; zero or more, '?' -&gt; exactly one) are allowed. Escape character is backslash. Omitting is equivalent to "*".
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("urlPattern")]
			public string UrlPattern;

			/// <summary>
			/// If set, only requests for matching resource types will be intercepted.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: ResourceType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("resourceType")]
			public string ResourceType;

			/// <summary>
			/// Stage at wich to begin intercepting requests. Default is Request.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: InterceptionStage</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("interceptionStage")]
			public string InterceptionStage;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Information about a signed exchange signature. https://wicg.github.io/webpackage/draft-yasskin-httpbis-origin-signed-exchanges-impl.html#rfc.section.3.1
		/// </summary>
		[JsonObject]
		public class SignedExchangeSignature {
			/// <summary>
			/// Signed exchange signature label.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("label")]
			public string Label;

			/// <summary>
			/// The hex string of signed exchange signature.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("signature")]
			public string Signature;

			/// <summary>
			/// Signed exchange signature integrity.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("integrity")]
			public string Integrity;

			/// <summary>
			/// Signed exchange signature cert Url.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("certUrl")]
			public string CertUrl;

			/// <summary>
			/// The hex string of signed exchange signature cert sha256.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("certSha256")]
			public string CertSha256;

			/// <summary>
			/// Signed exchange signature validity Url.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("validityUrl")]
			public string ValidityUrl;

			/// <summary>
			/// Signed exchange signature date.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("date")]
			public int Date;

			/// <summary>
			/// Signed exchange signature expires.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("expires")]
			public int Expires;

			/// <summary>
			/// The encoded certificates.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("certificates")]
			public string[] Certificates;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Information about a signed exchange header. https://wicg.github.io/webpackage/draft-yasskin-httpbis-origin-signed-exchanges-impl.html#cbor-representation
		/// </summary>
		[JsonObject]
		public class SignedExchangeHeader {
			/// <summary>
			/// Signed exchange request URL.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestUrl")]
			public string RequestUrl;

			/// <summary>
			/// Signed exchange response code.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("responseCode")]
			public int ResponseCode;

			/// <summary>
			/// Signed exchange response headers.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Headers</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("responseHeaders")]
			public Headers ResponseHeaders;

			/// <summary>
			/// Signed exchange response signature.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("signatures")]
			public SignedExchangeSignature[] Signatures;

			/// <summary>
			/// Signed exchange header integrity hash in the form of "sha256-&lt;base64-hash-value&gt;".
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("headerIntegrity")]
			public string HeaderIntegrity;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Information about a signed exchange response.
		/// </summary>
		[JsonObject]
		public class SignedExchangeError {
			/// <summary>
			/// Error message.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("message")]
			public string Message;

			/// <summary>
			/// The index of the signature which caused the error.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("signatureIndex")]
			public int SignatureIndex;

			/// <summary>
			/// The field which caused the error.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: SignedExchangeErrorField</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("errorField")]
			public string ErrorField;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Information about a signed exchange response.
		/// </summary>
		[JsonObject]
		public class SignedExchangeInfo {
			/// <summary>
			/// The outer response of signed HTTP exchange which was received from network.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Response</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("outerResponse")]
			public Response OuterResponse;

			/// <summary>
			/// Information about the signed exchange header.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: SignedExchangeHeader</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("header")]
			public SignedExchangeHeader Header;

			/// <summary>
			/// Security details for the signed exchange header.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: SecurityDetails</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("securityDetails")]
			public SecurityDetails SecurityDetails;

			/// <summary>
			/// Errors occurred while handling the signed exchagne.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("errors")]
			public SignedExchangeError[] Errors;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Tells whether clearing browser cache is supported.
		/// </summary>
		[JsonObject]
		public class CanClearBrowserCacheResult {
			/// <summary>
			/// True if browser cache can be cleared.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("result")]
			public bool Result;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Tells whether clearing browser cache is supported.
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[Obsolete]
		public CanClearBrowserCacheResult CanClearBrowserCache() {
			string s = _chrome.Send(
				"Network.canClearBrowserCache"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<CanClearBrowserCacheResult>();
		}

		/// <summary>
		/// Tells whether clearing browser cookies is supported.
		/// </summary>
		[JsonObject]
		public class CanClearBrowserCookiesResult {
			/// <summary>
			/// True if browser cookies can be cleared.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("result")]
			public bool Result;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Tells whether clearing browser cookies is supported.
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[Obsolete]
		public CanClearBrowserCookiesResult CanClearBrowserCookies() {
			string s = _chrome.Send(
				"Network.canClearBrowserCookies"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<CanClearBrowserCookiesResult>();
		}

		/// <summary>
		/// Tells whether emulation of network conditions is supported.
		/// </summary>
		[JsonObject]
		public class CanEmulateNetworkConditionsResult {
			/// <summary>
			/// True if emulation of network conditions is supported.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("result")]
			public bool Result;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Tells whether emulation of network conditions is supported.
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[Obsolete]
		public CanEmulateNetworkConditionsResult CanEmulateNetworkConditions() {
			string s = _chrome.Send(
				"Network.canEmulateNetworkConditions"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<CanEmulateNetworkConditionsResult>();
		}

		/// <summary>
		/// Clears browser cache.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void ClearBrowserCache() {
			_chrome.Send(
				"Network.clearBrowserCache"
			);
		}

		/// <summary>
		/// Clears browser cookies.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void ClearBrowserCookies() {
			_chrome.Send(
				"Network.clearBrowserCookies"
			);
		}

		/// <summary>
		/// Response to Network.requestIntercepted which either modifies the request to continue with any modifications, or blocks it, or completes it with the provided response bytes. If a network fetch occurs as a result which encounters a redirect an additional Network.requestIntercepted event will be sent with the same InterceptionId. Deprecated, use Fetch.continueRequest, Fetch.fulfillRequest and Fetch.failRequest instead.
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="interceptionId"></param>
		/// <param name="errorReason">If set this causes the request to fail with the given reason. Passing `Aborted` for requests marked with `isNavigationRequest` also cancels the navigation. Must not be set in response to an authChallenge.</param>
		/// <param name="rawResponse">If set the requests completes using with the provided base64 encoded raw response, including HTTP status line and headers etc... Must not be set in response to an authChallenge.</param>
		/// <param name="url">If set the request url will be modified in a way that's not observable by page. Must not be set in response to an authChallenge.</param>
		/// <param name="method">If set this allows the request method to be overridden. Must not be set in response to an authChallenge.</param>
		/// <param name="postData">If set this allows postData to be set. Must not be set in response to an authChallenge.</param>
		/// <param name="headers">If set this allows the request headers to be changed. Must not be set in response to an authChallenge.</param>
		/// <param name="authChallengeResponse">Response to a requestIntercepted with an authChallenge. Must not be set otherwise.</param>
		[Obsolete]
		public void ContinueInterceptedRequest(string interceptionId, string errorReason = null, byte[] rawResponse = null, string url = null, string method = null, string postData = null, Headers headers = null, AuthChallengeResponse authChallengeResponse = null) {
			_chrome.Send(
				"Network.continueInterceptedRequest",
				new {
					interceptionId,
					errorReason,
					rawResponse,
					url,
					method,
					postData,
					headers,
					authChallengeResponse
				}
			);
		}

		/// <summary>
		/// Deletes browser cookies with matching name and url or domain/path pair.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="name">Name of the cookies to remove.</param>
		/// <param name="url">If specified, deletes all the cookies with the given name where domain and path match provided URL.</param>
		/// <param name="domain">If specified, deletes only cookies with the exact domain.</param>
		/// <param name="path">If specified, deletes only cookies with the exact path.</param>
		public void DeleteCookies(string name, string url = null, string domain = null, string path = null) {
			_chrome.Send(
				"Network.deleteCookies",
				new {
					name,
					url,
					domain,
					path
				}
			);
		}

		/// <summary>
		/// Disables network tracking, prevents network events from being sent to the client.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"Network.disable"
			);
		}

		/// <summary>
		/// Activates emulation of network conditions.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="offline">True to emulate internet disconnection.</param>
		/// <param name="latency">Minimum latency from request sent to response headers received (ms).</param>
		/// <param name="downloadThroughput">Maximal aggregated download throughput (bytes/sec). -1 disables download throttling.</param>
		/// <param name="uploadThroughput">Maximal aggregated upload throughput (bytes/sec).  -1 disables upload throttling.</param>
		/// <param name="connectionType">Connection type if known.</param>
		public void EmulateNetworkConditions(bool offline, double latency, double downloadThroughput, double uploadThroughput, string connectionType = null) {
			_chrome.Send(
				"Network.emulateNetworkConditions",
				new {
					offline,
					latency,
					downloadThroughput,
					uploadThroughput,
					connectionType
				}
			);
		}

		/// <summary>
		/// Enables network tracking, network events will now be delivered to the client.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="maxTotalBufferSize">Buffer size in bytes to use when preserving network payloads (XHRs, etc).</param>
		/// <param name="maxResourceBufferSize">Per-resource buffer size in bytes to use when preserving network payloads (XHRs, etc).</param>
		/// <param name="maxPostDataSize">Longest post body size (in bytes) that would be included in requestWillBeSent notification</param>
		public void Enable(int? maxTotalBufferSize = null, int? maxResourceBufferSize = null, int? maxPostDataSize = null) {
			_chrome.Send(
				"Network.enable",
				new {
					maxTotalBufferSize,
					maxResourceBufferSize,
					maxPostDataSize
				}
			);
		}

		/// <summary>
		/// Returns all browser cookies. Depending on the backend support, will return detailed cookie information in the `cookies` field.
		/// </summary>
		[JsonObject]
		public class GetAllCookiesResult {
			/// <summary>
			/// Array of cookie objects.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("cookies")]
			public Cookie[] Cookies;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns all browser cookies. Depending on the backend support, will return detailed cookie information in the `cookies` field.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public GetAllCookiesResult GetAllCookies() {
			string s = _chrome.Send(
				"Network.getAllCookies"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetAllCookiesResult>();
		}

		/// <summary>
		/// Returns the DER-encoded certificate.
		/// </summary>
		[JsonObject]
		public class GetCertificateResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("tableNames")]
			public string[] TableNames;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns the DER-encoded certificate.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="origin">Origin to get certificate for.</param>
		public GetCertificateResult GetCertificate(string origin) {
			string s = _chrome.Send(
				"Network.getCertificate",
				new {
					origin
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetCertificateResult>();
		}

		/// <summary>
		/// Returns all browser cookies for the current URL. Depending on the backend support, will return detailed cookie information in the `cookies` field.
		/// </summary>
		[JsonObject]
		public class GetCookiesResult {
			/// <summary>
			/// Array of cookie objects.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("cookies")]
			public Cookie[] Cookies;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns all browser cookies for the current URL. Depending on the backend support, will return detailed cookie information in the `cookies` field.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="urls">The list of URLs for which applicable cookies will be fetched</param>
		public GetCookiesResult GetCookies(string[] urls = null) {
			string s = _chrome.Send(
				"Network.getCookies",
				new {
					urls
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetCookiesResult>();
		}

		/// <summary>
		/// Returns content served for the given request.
		/// </summary>
		[JsonObject]
		public class GetResponseBodyResult {
			/// <summary>
			/// Response body.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("body")]
			public string Body;

			/// <summary>
			/// True, if content was sent as base64.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("base64Encoded")]
			public bool Base64Encoded;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns content served for the given request.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="requestId">Identifier of the network request to get content for.</param>
		public GetResponseBodyResult GetResponseBody(string requestId) {
			string s = _chrome.Send(
				"Network.getResponseBody",
				new {
					requestId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetResponseBodyResult>();
		}

		/// <summary>
		/// Returns post data sent with the request. Returns an error when no data was sent with the request.
		/// </summary>
		[JsonObject]
		public class GetRequestPostDataResult {
			/// <summary>
			/// Request body string, omitting files from multipart requests
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("postData")]
			public string PostData;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns post data sent with the request. Returns an error when no data was sent with the request.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="requestId">Identifier of the network request to get content for.</param>
		public GetRequestPostDataResult GetRequestPostData(string requestId) {
			string s = _chrome.Send(
				"Network.getRequestPostData",
				new {
					requestId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetRequestPostDataResult>();
		}

		/// <summary>
		/// Returns content served for the given currently intercepted request.
		/// </summary>
		[JsonObject]
		public class GetResponseBodyForInterceptionResult {
			/// <summary>
			/// Response body.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("body")]
			public string Body;

			/// <summary>
			/// True, if content was sent as base64.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("base64Encoded")]
			public bool Base64Encoded;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns content served for the given currently intercepted request.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="interceptionId">Identifier for the intercepted request to get body for.</param>
		public GetResponseBodyForInterceptionResult GetResponseBodyForInterception(string interceptionId) {
			string s = _chrome.Send(
				"Network.getResponseBodyForInterception",
				new {
					interceptionId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetResponseBodyForInterceptionResult>();
		}

		/// <summary>
		/// Returns a handle to the stream representing the response body. Note that after this command, the intercepted request can't be continued as is -- you either need to cancel it or to provide the response body. The stream only supports sequential read, IO.read will fail if the position is specified.
		/// </summary>
		[JsonObject]
		public class TakeResponseBodyForInterceptionAsStreamResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: IO.StreamHandle</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("stream")]
			public string Stream;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns a handle to the stream representing the response body. Note that after this command, the intercepted request can't be continued as is -- you either need to cancel it or to provide the response body. The stream only supports sequential read, IO.read will fail if the position is specified.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="interceptionId"></param>
		public TakeResponseBodyForInterceptionAsStreamResult TakeResponseBodyForInterceptionAsStream(string interceptionId) {
			string s = _chrome.Send(
				"Network.takeResponseBodyForInterceptionAsStream",
				new {
					interceptionId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<TakeResponseBodyForInterceptionAsStreamResult>();
		}

		/// <summary>
		/// This method sends a new XMLHttpRequest which is identical to the original one. The following parameters should be identical: method, url, async, request body, extra headers, withCredentials attribute, user, password.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="requestId">Identifier of XHR to replay.</param>
		public void ReplayXHR(string requestId) {
			_chrome.Send(
				"Network.replayXHR",
				new {
					requestId
				}
			);
		}

		/// <summary>
		/// Searches for given string in response content.
		/// </summary>
		[JsonObject]
		public class SearchInResponseBodyResult {
			/// <summary>
			/// List of search matches.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("result")]
			public DebuggerDomain.SearchMatch[] Result;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Searches for given string in response content.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="requestId">Identifier of the network response to search.</param>
		/// <param name="query">String to search for.</param>
		/// <param name="caseSensitive">If true, search is case sensitive.</param>
		/// <param name="isRegex">If true, treats string parameter as regex.</param>
		public SearchInResponseBodyResult SearchInResponseBody(string requestId, string query, bool? caseSensitive = null, bool? isRegex = null) {
			string s = _chrome.Send(
				"Network.searchInResponseBody",
				new {
					requestId,
					query,
					caseSensitive,
					isRegex
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<SearchInResponseBodyResult>();
		}

		/// <summary>
		/// Blocks URLs from loading.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="urls">URL patterns to block. Wildcards ('*') are allowed.</param>
		public void SetBlockedURLs(string[] urls) {
			_chrome.Send(
				"Network.setBlockedURLs",
				new {
					urls
				}
			);
		}

		/// <summary>
		/// Toggles ignoring of service worker for each request.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="bypass">Bypass service worker and load from network.</param>
		public void SetBypassServiceWorker(bool bypass) {
			_chrome.Send(
				"Network.setBypassServiceWorker",
				new {
					bypass
				}
			);
		}

		/// <summary>
		/// Toggles ignoring cache for each request. If `true`, cache will not be used.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="cacheDisabled">Cache disabled state.</param>
		public void SetCacheDisabled(bool cacheDisabled) {
			_chrome.Send(
				"Network.setCacheDisabled",
				new {
					cacheDisabled
				}
			);
		}

		/// <summary>
		/// Sets a cookie with the given cookie data; may overwrite equivalent cookies if they exist.
		/// </summary>
		[JsonObject]
		public class SetCookieResult {
			/// <summary>
			/// True if successfully set cookie.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("success")]
			public bool Success;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Sets a cookie with the given cookie data; may overwrite equivalent cookies if they exist.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="name">Cookie name.</param>
		/// <param name="value">Cookie value.</param>
		/// <param name="url">The request-URI to associate with the setting of the cookie. This value can affect the default domain and path values of the created cookie.</param>
		/// <param name="domain">Cookie domain.</param>
		/// <param name="path">Cookie path.</param>
		/// <param name="secure">True if cookie is secure.</param>
		/// <param name="httpOnly">True if cookie is http-only.</param>
		/// <param name="sameSite">Cookie SameSite type.</param>
		/// <param name="expires">Cookie expiration date, session cookie if not set</param>
		public SetCookieResult SetCookie(string name, string value, string url = null, string domain = null, string path = null, bool? secure = null, bool? httpOnly = null, string sameSite = null, double? expires = null) {
			string s = _chrome.Send(
				"Network.setCookie",
				new {
					name,
					value,
					url,
					domain,
					path,
					secure,
					httpOnly,
					sameSite,
					expires
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<SetCookieResult>();
		}

		/// <summary>
		/// Sets given cookies.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="cookies">Cookies to be set.</param>
		public void SetCookies(CookieParam[] cookies) {
			_chrome.Send(
				"Network.setCookies",
				new {
					cookies
				}
			);
		}

		/// <summary>
		/// For testing.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="maxTotalSize">Maximum total buffer size.</param>
		/// <param name="maxResourceSize">Maximum per-resource size.</param>
		public void SetDataSizeLimitsForTest(int maxTotalSize, int maxResourceSize) {
			_chrome.Send(
				"Network.setDataSizeLimitsForTest",
				new {
					maxTotalSize,
					maxResourceSize
				}
			);
		}

		/// <summary>
		/// Specifies whether to always send extra HTTP headers with the requests from this page.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="headers">Map with extra HTTP headers.</param>
		public void SetExtraHTTPHeaders(Headers headers) {
			_chrome.Send(
				"Network.setExtraHTTPHeaders",
				new {
					headers
				}
			);
		}

		/// <summary>
		/// Sets the requests to intercept that match the provided patterns and optionally resource types. Deprecated, please use Fetch.enable instead.
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="patterns">Requests matching any of these patterns will be forwarded and wait for the corresponding continueInterceptedRequest call.</param>
		[Obsolete]
		public void SetRequestInterception(RequestPattern[] patterns) {
			_chrome.Send(
				"Network.setRequestInterception",
				new {
					patterns
				}
			);
		}

		/// <summary>
		/// Allows overriding user agent with the given string.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="userAgent">User agent to use.</param>
		/// <param name="acceptLanguage">Browser langugage to emulate.</param>
		/// <param name="platform">The platform navigator.platform should return.</param>
		public void SetUserAgentOverride(string userAgent, string acceptLanguage = null, string platform = null) {
			_chrome.Send(
				"Network.setUserAgentOverride",
				new {
					userAgent,
					acceptLanguage,
					platform
				}
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("Network.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "dataReceived":
					if (DataReceived == null) {
						return;
					}
					DataReceived(
						_chrome,
						JsonConvert.DeserializeObject
							<DataReceivedEventArgs>(@params.ToString())
					);
					break;
				case "eventSourceMessageReceived":
					if (EventSourceMessageReceived == null) {
						return;
					}
					EventSourceMessageReceived(
						_chrome,
						JsonConvert.DeserializeObject
							<EventSourceMessageReceivedEventArgs>(@params.ToString())
					);
					break;
				case "loadingFailed":
					if (LoadingFailed == null) {
						return;
					}
					LoadingFailed(
						_chrome,
						JsonConvert.DeserializeObject
							<LoadingFailedEventArgs>(@params.ToString())
					);
					break;
				case "loadingFinished":
					if (LoadingFinished == null) {
						return;
					}
					LoadingFinished(
						_chrome,
						JsonConvert.DeserializeObject
							<LoadingFinishedEventArgs>(@params.ToString())
					);
					break;
				case "requestIntercepted":
					if (RequestIntercepted == null) {
						return;
					}
					RequestIntercepted(
						_chrome,
						JsonConvert.DeserializeObject
							<RequestInterceptedEventArgs>(@params.ToString())
					);
					break;
				case "requestServedFromCache":
					if (RequestServedFromCache == null) {
						return;
					}
					RequestServedFromCache(
						_chrome,
						JsonConvert.DeserializeObject
							<RequestServedFromCacheEventArgs>(@params.ToString())
					);
					break;
				case "requestWillBeSent":
					if (RequestWillBeSent == null) {
						return;
					}
					RequestWillBeSent(
						_chrome,
						JsonConvert.DeserializeObject
							<RequestWillBeSentEventArgs>(@params.ToString())
					);
					break;
				case "resourceChangedPriority":
					if (ResourceChangedPriority == null) {
						return;
					}
					ResourceChangedPriority(
						_chrome,
						JsonConvert.DeserializeObject
							<ResourceChangedPriorityEventArgs>(@params.ToString())
					);
					break;
				case "signedExchangeReceived":
					if (SignedExchangeReceived == null) {
						return;
					}
					SignedExchangeReceived(
						_chrome,
						JsonConvert.DeserializeObject
							<SignedExchangeReceivedEventArgs>(@params.ToString())
					);
					break;
				case "responseReceived":
					if (ResponseReceived == null) {
						return;
					}
					ResponseReceived(
						_chrome,
						JsonConvert.DeserializeObject
							<ResponseReceivedEventArgs>(@params.ToString())
					);
					break;
				case "webSocketClosed":
					if (WebSocketClosed == null) {
						return;
					}
					WebSocketClosed(
						_chrome,
						JsonConvert.DeserializeObject
							<WebSocketClosedEventArgs>(@params.ToString())
					);
					break;
				case "webSocketCreated":
					if (WebSocketCreated == null) {
						return;
					}
					WebSocketCreated(
						_chrome,
						JsonConvert.DeserializeObject
							<WebSocketCreatedEventArgs>(@params.ToString())
					);
					break;
				case "webSocketFrameError":
					if (WebSocketFrameError == null) {
						return;
					}
					WebSocketFrameError(
						_chrome,
						JsonConvert.DeserializeObject
							<WebSocketFrameErrorEventArgs>(@params.ToString())
					);
					break;
				case "webSocketFrameReceived":
					if (WebSocketFrameReceived == null) {
						return;
					}
					WebSocketFrameReceived(
						_chrome,
						JsonConvert.DeserializeObject
							<WebSocketFrameReceivedEventArgs>(@params.ToString())
					);
					break;
				case "webSocketFrameSent":
					if (WebSocketFrameSent == null) {
						return;
					}
					WebSocketFrameSent(
						_chrome,
						JsonConvert.DeserializeObject
							<WebSocketFrameSentEventArgs>(@params.ToString())
					);
					break;
				case "webSocketHandshakeResponseReceived":
					if (WebSocketHandshakeResponseReceived == null) {
						return;
					}
					WebSocketHandshakeResponseReceived(
						_chrome,
						JsonConvert.DeserializeObject
							<WebSocketHandshakeResponseReceivedEventArgs>(@params.ToString())
					);
					break;
				case "webSocketWillSendHandshakeRequest":
					if (WebSocketWillSendHandshakeRequest == null) {
						return;
					}
					WebSocketWillSendHandshakeRequest(
						_chrome,
						JsonConvert.DeserializeObject
							<WebSocketWillSendHandshakeRequestEventArgs>(@params.ToString())
					);
					break;
				case "requestWillBeSentExtraInfo":
					if (RequestWillBeSentExtraInfo == null) {
						return;
					}
					RequestWillBeSentExtraInfo(
						_chrome,
						JsonConvert.DeserializeObject
							<RequestWillBeSentExtraInfoEventArgs>(@params.ToString())
					);
					break;
				case "responseReceivedExtraInfo":
					if (ResponseReceivedExtraInfo == null) {
						return;
					}
					ResponseReceivedExtraInfo(
						_chrome,
						JsonConvert.DeserializeObject
							<ResponseReceivedExtraInfoEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
