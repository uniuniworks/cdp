// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// Audits domain allows investigation of page violations and possible improvements.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: Network</description></item>
	/// </list>
	/// </summary>
	public class AuditsDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the AuditsDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public AuditsDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}


		/// <summary>
		/// Returns the response body and size if it were re-encoded with the specified settings. Only applies to images.
		/// </summary>
		[JsonObject]
		public class GetEncodedResponseResult {
			/// <summary>
			/// The encoded body as a base64 string. Omitted if sizeOnly is true.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: binary</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("body")]
			public byte[] Body;

			/// <summary>
			/// Size before re-encoding.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("originalSize")]
			public int OriginalSize;

			/// <summary>
			/// Size after re-encoding.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("encodedSize")]
			public int EncodedSize;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns the response body and size if it were re-encoded with the specified settings. Only applies to images.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="requestId">Identifier of the network request to get content for.</param>
		/// <param name="encoding">The encoding to use.</param>
		/// <param name="quality">The quality of the encoding (0-1). (defaults to 1)</param>
		/// <param name="sizeOnly">Whether to only return the size information (defaults to false).</param>
		public GetEncodedResponseResult GetEncodedResponse(string requestId, string encoding, double? quality = null, bool? sizeOnly = null) {
			string s = _chrome.Send(
				"Audits.getEncodedResponse",
				new {
					requestId,
					encoding,
					quality,
					sizeOnly
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetEncodedResponseResult>();
		}



	}
}
