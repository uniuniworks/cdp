// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// 
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class InspectorDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the InspectorDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public InspectorDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Fired when remote debugging connection is about to be terminated. Contains detach reason.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class DetachedEventArgs : EventArgs {
			/// <summary>
			/// The reason why connection has been terminated.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("reason")]
			public string Reason;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when remote debugging connection is about to be terminated. Contains detach reason.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<DetachedEventArgs> Detached;

		/// <summary>
		/// Fired when debugging target has crashed
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler TargetCrashed;

		/// <summary>
		/// Fired when debugging target has reloaded after crash
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler TargetReloadedAfterCrash;


		/// <summary>
		/// Disables inspector domain notifications.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"Inspector.disable"
			);
		}

		/// <summary>
		/// Enables inspector domain notifications.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"Inspector.enable"
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("Inspector.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "detached":
					if (Detached == null) {
						return;
					}
					Detached(
						_chrome,
						JsonConvert.DeserializeObject
							<DetachedEventArgs>(@params.ToString())
					);
					break;
				case "targetCrashed":
					if (TargetCrashed == null) {
						return;
					}
					TargetCrashed(_chrome, EventArgs.Empty);
					break;
				case "targetReloadedAfterCrash":
					if (TargetReloadedAfterCrash == null) {
						return;
					}
					TargetReloadedAfterCrash(_chrome, EventArgs.Empty);
					break;

			}
		}

	}
}
