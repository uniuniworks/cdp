// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// A domain for interacting with Cast, Presentation API, and Remote Playback API functionalities.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class CastDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the CastDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public CastDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// This is fired whenever the list of available sinks changes. A sink is a device or a software surface that you can cast to.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class SinksUpdatedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sinks")]
			public Sink[] Sinks;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// This is fired whenever the list of available sinks changes. A sink is a device or a software surface that you can cast to.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<SinksUpdatedEventArgs> SinksUpdated;

		/// <summary>
		/// This is fired whenever the outstanding issue/error message changes. |issueMessage| is empty if there is no issue.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class IssueUpdatedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("issueMessage")]
			public string IssueMessage;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// This is fired whenever the outstanding issue/error message changes. |issueMessage| is empty if there is no issue.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<IssueUpdatedEventArgs> IssueUpdated;

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class Sink {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("id")]
			public string Id;

			/// <summary>
			/// Text describing the current session. Present only if there is an active session on the sink.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("session")]
			public string Session;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Starts observing for sinks that can be used for tab mirroring, and if set, sinks compatible with |presentationUrl| as well. When sinks are found, a |sinksUpdated| event is fired. Also starts observing for issue messages. When an issue is added or removed, an |issueUpdated| event is fired.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="presentationUrl"></param>
		public void Enable(string presentationUrl = null) {
			_chrome.Send(
				"Cast.enable",
				new {
					presentationUrl
				}
			);
		}

		/// <summary>
		/// Stops observing for sinks and issues.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"Cast.disable"
			);
		}

		/// <summary>
		/// Sets a sink to be used when the web page requests the browser to choose a sink via Presentation API, Remote Playback API, or Cast SDK.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="sinkName"></param>
		public void SetSinkToUse(string sinkName) {
			_chrome.Send(
				"Cast.setSinkToUse",
				new {
					sinkName
				}
			);
		}

		/// <summary>
		/// Starts mirroring the tab to the sink.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="sinkName"></param>
		public void StartTabMirroring(string sinkName) {
			_chrome.Send(
				"Cast.startTabMirroring",
				new {
					sinkName
				}
			);
		}

		/// <summary>
		/// Stops the active Cast session on the sink.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="sinkName"></param>
		public void StopCasting(string sinkName) {
			_chrome.Send(
				"Cast.stopCasting",
				new {
					sinkName
				}
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("Cast.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "sinksUpdated":
					if (SinksUpdated == null) {
						return;
					}
					SinksUpdated(
						_chrome,
						JsonConvert.DeserializeObject
							<SinksUpdatedEventArgs>(@params.ToString())
					);
					break;
				case "issueUpdated":
					if (IssueUpdated == null) {
						return;
					}
					IssueUpdated(
						_chrome,
						JsonConvert.DeserializeObject
							<IssueUpdatedEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
