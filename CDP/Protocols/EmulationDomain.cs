// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// This domain emulates different environments for the page.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: false</description></item>
	/// <item><description>dependencies: DOM, Page, Runtime</description></item>
	/// </list>
	/// </summary>
	public class EmulationDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the EmulationDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public EmulationDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Notification sent after the virtual time budget for the current VirtualTimePolicy has run out.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler VirtualTimeBudgetExpired;

		/// <summary>
		/// Screen orientation.
		/// </summary>
		[JsonObject]
		public class ScreenOrientation {
			/// <summary>
			/// Orientation type.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: portraitPrimary, portraitSecondary, landscapePrimary, landscapeSecondary
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// Orientation angle.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("angle")]
			public int Angle;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class MediaFeature {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public string Value;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Tells whether emulation is supported.
		/// </summary>
		[JsonObject]
		public class CanEmulateResult {
			/// <summary>
			/// True if emulation is supported.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("result")]
			public bool Result;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Tells whether emulation is supported.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public CanEmulateResult CanEmulate() {
			string s = _chrome.Send(
				"Emulation.canEmulate"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<CanEmulateResult>();
		}

		/// <summary>
		/// Clears the overriden device metrics.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void ClearDeviceMetricsOverride() {
			_chrome.Send(
				"Emulation.clearDeviceMetricsOverride"
			);
		}

		/// <summary>
		/// Clears the overriden Geolocation Position and Error.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void ClearGeolocationOverride() {
			_chrome.Send(
				"Emulation.clearGeolocationOverride"
			);
		}

		/// <summary>
		/// Requests that page scale factor is reset to initial values.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public void ResetPageScaleFactor() {
			_chrome.Send(
				"Emulation.resetPageScaleFactor"
			);
		}

		/// <summary>
		/// Enables or disables simulating a focused and active page.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="enabled">Whether to enable to disable focus emulation.</param>
		public void SetFocusEmulationEnabled(bool enabled) {
			_chrome.Send(
				"Emulation.setFocusEmulationEnabled",
				new {
					enabled
				}
			);
		}

		/// <summary>
		/// Enables CPU throttling to emulate slow CPUs.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="rate">Throttling rate as a slowdown factor (1 is no throttle, 2 is 2x slowdown, etc).</param>
		public void SetCPUThrottlingRate(double rate) {
			_chrome.Send(
				"Emulation.setCPUThrottlingRate",
				new {
					rate
				}
			);
		}

		/// <summary>
		/// Sets or clears an override of the default background color of the frame. This override is used if the content does not specify one.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="color">RGBA of the default background color. If not specified, any existing override will be cleared.</param>
		public void SetDefaultBackgroundColorOverride(DOMDomain.RGBA color = null) {
			_chrome.Send(
				"Emulation.setDefaultBackgroundColorOverride",
				new {
					color
				}
			);
		}

		/// <summary>
		/// Overrides the values of device screen dimensions (window.screen.width, window.screen.height, window.innerWidth, window.innerHeight, and "device-width"/"device-height"-related CSS media query results).
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="width">Overriding width value in pixels (minimum 0, maximum 10000000). 0 disables the override.</param>
		/// <param name="height">Overriding height value in pixels (minimum 0, maximum 10000000). 0 disables the override.</param>
		/// <param name="deviceScaleFactor">Overriding device scale factor value. 0 disables the override.</param>
		/// <param name="mobile">Whether to emulate mobile device. This includes viewport meta tag, overlay scrollbars, text autosizing and more.</param>
		/// <param name="scale">Scale to apply to resulting view image.</param>
		/// <param name="screenWidth">Overriding screen width value in pixels (minimum 0, maximum 10000000).</param>
		/// <param name="screenHeight">Overriding screen height value in pixels (minimum 0, maximum 10000000).</param>
		/// <param name="positionX">Overriding view X position on screen in pixels (minimum 0, maximum 10000000).</param>
		/// <param name="positionY">Overriding view Y position on screen in pixels (minimum 0, maximum 10000000).</param>
		/// <param name="dontSetVisibleSize">Do not set visible view size, rely upon explicit setVisibleSize call.</param>
		/// <param name="screenOrientation">Screen orientation override.</param>
		/// <param name="viewport">If set, the visible area of the page will be overridden to this viewport. This viewport change is not observed by the page, e.g. viewport-relative elements do not change positions.</param>
		public void SetDeviceMetricsOverride(int width, int height, double deviceScaleFactor, bool mobile, double? scale = null, int? screenWidth = null, int? screenHeight = null, int? positionX = null, int? positionY = null, bool? dontSetVisibleSize = null, ScreenOrientation screenOrientation = null, PageDomain.Viewport viewport = null) {
			_chrome.Send(
				"Emulation.setDeviceMetricsOverride",
				new {
					width,
					height,
					deviceScaleFactor,
					mobile,
					scale,
					screenWidth,
					screenHeight,
					positionX,
					positionY,
					dontSetVisibleSize,
					screenOrientation,
					viewport
				}
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="hidden">Whether scrollbars should be always hidden.</param>
		public void SetScrollbarsHidden(bool hidden) {
			_chrome.Send(
				"Emulation.setScrollbarsHidden",
				new {
					hidden
				}
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="disabled">Whether document.coookie API should be disabled.</param>
		public void SetDocumentCookieDisabled(bool disabled) {
			_chrome.Send(
				"Emulation.setDocumentCookieDisabled",
				new {
					disabled
				}
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="enabled">Whether touch emulation based on mouse input should be enabled.</param>
		/// <param name="configuration">Touch/gesture events configuration. Default: current platform.</param>
		public void SetEmitTouchEventsForMouse(bool enabled, string configuration = null) {
			_chrome.Send(
				"Emulation.setEmitTouchEventsForMouse",
				new {
					enabled,
					configuration
				}
			);
		}

		/// <summary>
		/// Emulates the given media type or media feature for CSS media queries.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="media">Media type to emulate. Empty string disables the override.</param>
		/// <param name="features">Media features to emulate.</param>
		public void SetEmulatedMedia(string media = null, MediaFeature[] features = null) {
			_chrome.Send(
				"Emulation.setEmulatedMedia",
				new {
					media,
					features
				}
			);
		}

		/// <summary>
		/// Overrides the Geolocation Position or Error. Omitting any of the parameters emulates position unavailable.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="latitude">Mock latitude</param>
		/// <param name="longitude">Mock longitude</param>
		/// <param name="accuracy">Mock accuracy</param>
		public void SetGeolocationOverride(double? latitude = null, double? longitude = null, double? accuracy = null) {
			_chrome.Send(
				"Emulation.setGeolocationOverride",
				new {
					latitude,
					longitude,
					accuracy
				}
			);
		}

		/// <summary>
		/// Overrides value returned by the javascript navigator object.
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="platform">The platform navigator.platform should return.</param>
		[Obsolete]
		public void SetNavigatorOverrides(string platform) {
			_chrome.Send(
				"Emulation.setNavigatorOverrides",
				new {
					platform
				}
			);
		}

		/// <summary>
		/// Sets a specified page scale factor.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="pageScaleFactor">Page scale factor.</param>
		public void SetPageScaleFactor(double pageScaleFactor) {
			_chrome.Send(
				"Emulation.setPageScaleFactor",
				new {
					pageScaleFactor
				}
			);
		}

		/// <summary>
		/// Switches script execution in the page.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="value">Whether script execution should be disabled in the page.</param>
		public void SetScriptExecutionDisabled(bool value) {
			_chrome.Send(
				"Emulation.setScriptExecutionDisabled",
				new {
					value
				}
			);
		}

		/// <summary>
		/// Enables touch on platforms which do not support them.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="enabled">Whether the touch event emulation should be enabled.</param>
		/// <param name="maxTouchPoints">Maximum touch points supported. Defaults to one.</param>
		public void SetTouchEmulationEnabled(bool enabled, int? maxTouchPoints = null) {
			_chrome.Send(
				"Emulation.setTouchEmulationEnabled",
				new {
					enabled,
					maxTouchPoints
				}
			);
		}

		/// <summary>
		/// Turns on virtual time for all frames (replacing real-time with a synthetic time source) and sets the current virtual time policy.  Note this supersedes any previous time budget.
		/// </summary>
		[JsonObject]
		public class SetVirtualTimePolicyResult {
			/// <summary>
			/// Absolute timestamp at which virtual time was first enabled (up time in milliseconds).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("virtualTimeTicksBase")]
			public double VirtualTimeTicksBase;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Turns on virtual time for all frames (replacing real-time with a synthetic time source) and sets the current virtual time policy.  Note this supersedes any previous time budget.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="policy"></param>
		/// <param name="budget">If set, after this many virtual milliseconds have elapsed virtual time will be paused and a virtualTimeBudgetExpired event is sent.</param>
		/// <param name="maxVirtualTimeTaskStarvationCount">If set this specifies the maximum number of tasks that can be run before virtual is forced forwards to prevent deadlock.</param>
		/// <param name="waitForNavigation">If set the virtual time policy change should be deferred until any frame starts navigating. Note any previous deferred policy change is superseded.</param>
		/// <param name="initialVirtualTime">If set, base::Time::Now will be overriden to initially return this value.</param>
		public SetVirtualTimePolicyResult SetVirtualTimePolicy(string policy, double? budget = null, int? maxVirtualTimeTaskStarvationCount = null, bool? waitForNavigation = null, double? initialVirtualTime = null) {
			string s = _chrome.Send(
				"Emulation.setVirtualTimePolicy",
				new {
					policy,
					budget,
					maxVirtualTimeTaskStarvationCount,
					waitForNavigation,
					initialVirtualTime
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<SetVirtualTimePolicyResult>();
		}

		/// <summary>
		/// Overrides default host system timezone with the specified one.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="timezoneId">The timezone identifier. If empty, disables the override and restores default host system timezone.</param>
		public void SetTimezoneOverride(string timezoneId) {
			_chrome.Send(
				"Emulation.setTimezoneOverride",
				new {
					timezoneId
				}
			);
		}

		/// <summary>
		/// Resizes the frame/viewport of the page. Note that this does not affect the frame's container (e.g. browser window). Can be used to produce screenshots of the specified size. Not supported on Android.
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="width">Frame width (DIP).</param>
		/// <param name="height">Frame height (DIP).</param>
		[Obsolete]
		public void SetVisibleSize(int width, int height) {
			_chrome.Send(
				"Emulation.setVisibleSize",
				new {
					width,
					height
				}
			);
		}

		/// <summary>
		/// Allows overriding user agent with the given string.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="userAgent">User agent to use.</param>
		/// <param name="acceptLanguage">Browser langugage to emulate.</param>
		/// <param name="platform">The platform navigator.platform should return.</param>
		public void SetUserAgentOverride(string userAgent, string acceptLanguage = null, string platform = null) {
			_chrome.Send(
				"Emulation.setUserAgentOverride",
				new {
					userAgent,
					acceptLanguage,
					platform
				}
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("Emulation.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "virtualTimeBudgetExpired":
					if (VirtualTimeBudgetExpired == null) {
						return;
					}
					VirtualTimeBudgetExpired(_chrome, EventArgs.Empty);
					break;

			}
		}

	}
}
