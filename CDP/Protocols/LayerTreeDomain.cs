// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// 
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: DOM</description></item>
	/// </list>
	/// </summary>
	public class LayerTreeDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the LayerTreeDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public LayerTreeDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class LayerPaintedEventArgs : EventArgs {
			/// <summary>
			/// The id of the painted layer.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: LayerId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("layerId")]
			public string LayerId;

			/// <summary>
			/// Clip rectangle.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: DOM.Rect</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("clip")]
			public DOMDomain.Rect Clip;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<LayerPaintedEventArgs> LayerPainted;

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class LayerTreeDidChangeEventArgs : EventArgs {
			/// <summary>
			/// Layer tree, absent if not in the comspositing mode.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("layers")]
			public Layer[] Layers;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<LayerTreeDidChangeEventArgs> LayerTreeDidChange;

		/// <summary>
		/// Rectangle where scrolling happens on the main thread.
		/// </summary>
		[JsonObject]
		public class ScrollRect {
			/// <summary>
			/// Rectangle itself.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: DOM.Rect</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("rect")]
			public DOMDomain.Rect Rect;

			/// <summary>
			/// Reason for rectangle to force scrolling on the main thread
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: RepaintsOnScroll, TouchEventHandler, WheelEventHandler
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Sticky position constraints.
		/// </summary>
		[JsonObject]
		public class StickyPositionConstraint {
			/// <summary>
			/// Layout rectangle of the sticky element before being shifted
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: DOM.Rect</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("stickyBoxRect")]
			public DOMDomain.Rect StickyBoxRect;

			/// <summary>
			/// Layout rectangle of the containing block of the sticky element
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: DOM.Rect</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("containingBlockRect")]
			public DOMDomain.Rect ContainingBlockRect;

			/// <summary>
			/// The nearest sticky layer that shifts the sticky box
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: LayerId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nearestLayerShiftingStickyBox")]
			public string NearestLayerShiftingStickyBox;

			/// <summary>
			/// The nearest sticky layer that shifts the containing block
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: LayerId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nearestLayerShiftingContainingBlock")]
			public string NearestLayerShiftingContainingBlock;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Serialized fragment of layer picture along with its offset within the layer.
		/// </summary>
		[JsonObject]
		public class PictureTile {
			/// <summary>
			/// Offset from owning layer left boundary
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("x")]
			public double X;

			/// <summary>
			/// Offset from owning layer top boundary
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("y")]
			public double Y;

			/// <summary>
			/// Base64-encoded snapshot data.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: binary</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("picture")]
			public byte[] Picture;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Information about a compositing layer.
		/// </summary>
		[JsonObject]
		public class Layer {
			/// <summary>
			/// The unique id for this layer.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: LayerId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("layerId")]
			public string LayerId;

			/// <summary>
			/// The id of parent (not present for root).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: LayerId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("parentLayerId")]
			public string ParentLayerId;

			/// <summary>
			/// The backend id for the node associated with this layer.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: DOM.BackendNodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("backendNodeId")]
			public int BackendNodeId;

			/// <summary>
			/// Offset from parent layer, X coordinate.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("offsetX")]
			public double OffsetX;

			/// <summary>
			/// Offset from parent layer, Y coordinate.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("offsetY")]
			public double OffsetY;

			/// <summary>
			/// Layer width.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("width")]
			public double Width;

			/// <summary>
			/// Layer height.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("height")]
			public double Height;

			/// <summary>
			/// Transformation matrix for layer, default is identity matrix
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("transform")]
			public double[] Transform;

			/// <summary>
			/// Transform anchor point X, absent if no transform specified
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("anchorX")]
			public double AnchorX;

			/// <summary>
			/// Transform anchor point Y, absent if no transform specified
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("anchorY")]
			public double AnchorY;

			/// <summary>
			/// Transform anchor point Z, absent if no transform specified
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("anchorZ")]
			public double AnchorZ;

			/// <summary>
			/// Indicates how many time this layer has painted.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("paintCount")]
			public int PaintCount;

			/// <summary>
			/// Indicates whether this layer hosts any content, rather than being used for transform/scrolling purposes only.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("drawsContent")]
			public bool DrawsContent;

			/// <summary>
			/// Set if layer is not visible.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("invisible")]
			public bool Invisible;

			/// <summary>
			/// Rectangles scrolling on main thread only.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scrollRects")]
			public ScrollRect[] ScrollRects;

			/// <summary>
			/// Sticky position constraint information
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: StickyPositionConstraint</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("stickyPositionConstraint")]
			public StickyPositionConstraint StickyPositionConstraint;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Provides the reasons why the given layer was composited.
		/// </summary>
		[JsonObject]
		public class CompositingReasonsResult {
			/// <summary>
			/// A list of strings specifying reasons for the given layer to become composited.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("compositingReasons")]
			public string[] CompositingReasons;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Provides the reasons why the given layer was composited.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="layerId">The id of the layer for which we want to get the reasons it was composited.</param>
		public CompositingReasonsResult CompositingReasons(string layerId) {
			string s = _chrome.Send(
				"LayerTree.compositingReasons",
				new {
					layerId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<CompositingReasonsResult>();
		}

		/// <summary>
		/// Disables compositing tree inspection.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"LayerTree.disable"
			);
		}

		/// <summary>
		/// Enables compositing tree inspection.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"LayerTree.enable"
			);
		}

		/// <summary>
		/// Returns the snapshot identifier.
		/// </summary>
		[JsonObject]
		public class LoadSnapshotResult {
			/// <summary>
			/// The id of the snapshot.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SnapshotId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("snapshotId")]
			public string SnapshotId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns the snapshot identifier.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="tiles">An array of tiles composing the snapshot.</param>
		public LoadSnapshotResult LoadSnapshot(PictureTile[] tiles) {
			string s = _chrome.Send(
				"LayerTree.loadSnapshot",
				new {
					tiles
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<LoadSnapshotResult>();
		}

		/// <summary>
		/// Returns the layer snapshot identifier.
		/// </summary>
		[JsonObject]
		public class MakeSnapshotResult {
			/// <summary>
			/// The id of the layer snapshot.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SnapshotId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("snapshotId")]
			public string SnapshotId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns the layer snapshot identifier.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="layerId">The id of the layer.</param>
		public MakeSnapshotResult MakeSnapshot(string layerId) {
			string s = _chrome.Send(
				"LayerTree.makeSnapshot",
				new {
					layerId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<MakeSnapshotResult>();
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class ProfileSnapshotResult {
			/// <summary>
			/// The array of paint profiles, one per run.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timings")]
			public double[][] Timings;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="snapshotId">The id of the layer snapshot.</param>
		/// <param name="minRepeatCount">The maximum number of times to replay the snapshot (1, if not specified).</param>
		/// <param name="minDuration">The minimum duration (in seconds) to replay the snapshot.</param>
		/// <param name="clipRect">The clip rectangle to apply when replaying the snapshot.</param>
		public ProfileSnapshotResult ProfileSnapshot(string snapshotId, int? minRepeatCount = null, double? minDuration = null, DOMDomain.Rect clipRect = null) {
			string s = _chrome.Send(
				"LayerTree.profileSnapshot",
				new {
					snapshotId,
					minRepeatCount,
					minDuration,
					clipRect
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<ProfileSnapshotResult>();
		}

		/// <summary>
		/// Releases layer snapshot captured by the back-end.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="snapshotId">The id of the layer snapshot.</param>
		public void ReleaseSnapshot(string snapshotId) {
			_chrome.Send(
				"LayerTree.releaseSnapshot",
				new {
					snapshotId
				}
			);
		}

		/// <summary>
		/// Replays the layer snapshot and returns the resulting bitmap.
		/// </summary>
		[JsonObject]
		public class ReplaySnapshotResult {
			/// <summary>
			/// A data: URL for resulting image.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("dataURL")]
			public string DataURL;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Replays the layer snapshot and returns the resulting bitmap.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="snapshotId">The id of the layer snapshot.</param>
		/// <param name="fromStep">The first step to replay from (replay from the very start if not specified).</param>
		/// <param name="toStep">The last step to replay to (replay till the end if not specified).</param>
		/// <param name="scale">The scale to apply while replaying (defaults to 1).</param>
		public ReplaySnapshotResult ReplaySnapshot(string snapshotId, int? fromStep = null, int? toStep = null, double? scale = null) {
			string s = _chrome.Send(
				"LayerTree.replaySnapshot",
				new {
					snapshotId,
					fromStep,
					toStep,
					scale
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<ReplaySnapshotResult>();
		}

		/// <summary>
		/// Replays the layer snapshot and returns canvas log.
		/// </summary>
		[JsonObject]
		public class SnapshotCommandLogResult {
			/// <summary>
			/// The array of canvas function calls.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("commandLog")]
			public object[] CommandLog;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Replays the layer snapshot and returns canvas log.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="snapshotId">The id of the layer snapshot.</param>
		public SnapshotCommandLogResult SnapshotCommandLog(string snapshotId) {
			string s = _chrome.Send(
				"LayerTree.snapshotCommandLog",
				new {
					snapshotId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<SnapshotCommandLogResult>();
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("LayerTree.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "layerPainted":
					if (LayerPainted == null) {
						return;
					}
					LayerPainted(
						_chrome,
						JsonConvert.DeserializeObject
							<LayerPaintedEventArgs>(@params.ToString())
					);
					break;
				case "layerTreeDidChange":
					if (LayerTreeDidChange == null) {
						return;
					}
					LayerTreeDidChange(
						_chrome,
						JsonConvert.DeserializeObject
							<LayerTreeDidChangeEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
