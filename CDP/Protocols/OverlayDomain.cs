// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// This domain provides various functionality related to drawing atop the inspected page.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: DOM, Page, Runtime</description></item>
	/// </list>
	/// </summary>
	public class OverlayDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the OverlayDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public OverlayDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Fired when the node should be inspected. This happens after call to `setInspectMode` or when user manually inspects an element.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class InspectNodeRequestedEventArgs : EventArgs {
			/// <summary>
			/// Id of the node to inspect.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: DOM.BackendNodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("backendNodeId")]
			public int BackendNodeId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when the node should be inspected. This happens after call to `setInspectMode` or when user manually inspects an element.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<InspectNodeRequestedEventArgs> InspectNodeRequested;

		/// <summary>
		/// Fired when the node should be highlighted. This happens after call to `setInspectMode`.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class NodeHighlightRequestedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: DOM.NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public int NodeId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when the node should be highlighted. This happens after call to `setInspectMode`.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<NodeHighlightRequestedEventArgs> NodeHighlightRequested;

		/// <summary>
		/// Fired when user asks to capture screenshot of some area on the page.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ScreenshotRequestedEventArgs : EventArgs {
			/// <summary>
			/// Viewport to capture, in device independent pixels (dip).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Page.Viewport</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("viewport")]
			public PageDomain.Viewport Viewport;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when user asks to capture screenshot of some area on the page.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ScreenshotRequestedEventArgs> ScreenshotRequested;

		/// <summary>
		/// Fired when user cancels the inspect mode.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler InspectModeCanceled;

		/// <summary>
		/// Configuration data for the highlighting of page elements.
		/// </summary>
		[JsonObject]
		public class HighlightConfig {
			/// <summary>
			/// Whether the node info tooltip should be shown (default: false).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("showInfo")]
			public bool ShowInfo;

			/// <summary>
			/// Whether the node styles in the tooltip (default: false).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("showStyles")]
			public bool ShowStyles;

			/// <summary>
			/// Whether the rulers should be shown (default: false).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("showRulers")]
			public bool ShowRulers;

			/// <summary>
			/// Whether the extension lines from node to the rulers should be shown (default: false).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("showExtensionLines")]
			public bool ShowExtensionLines;

			/// <summary>
			/// The content box highlight fill color (default: transparent).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: DOM.RGBA</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contentColor")]
			public DOMDomain.RGBA ContentColor;

			/// <summary>
			/// The padding highlight fill color (default: transparent).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: DOM.RGBA</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("paddingColor")]
			public DOMDomain.RGBA PaddingColor;

			/// <summary>
			/// The border highlight fill color (default: transparent).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: DOM.RGBA</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("borderColor")]
			public DOMDomain.RGBA BorderColor;

			/// <summary>
			/// The margin highlight fill color (default: transparent).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: DOM.RGBA</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("marginColor")]
			public DOMDomain.RGBA MarginColor;

			/// <summary>
			/// The event target element highlight fill color (default: transparent).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: DOM.RGBA</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("eventTargetColor")]
			public DOMDomain.RGBA EventTargetColor;

			/// <summary>
			/// The shape outside fill color (default: transparent).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: DOM.RGBA</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("shapeColor")]
			public DOMDomain.RGBA ShapeColor;

			/// <summary>
			/// The shape margin fill color (default: transparent).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: DOM.RGBA</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("shapeMarginColor")]
			public DOMDomain.RGBA ShapeMarginColor;

			/// <summary>
			/// The grid layout color (default: transparent).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: DOM.RGBA</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("cssGridColor")]
			public DOMDomain.RGBA CssGridColor;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Disables domain notifications.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"Overlay.disable"
			);
		}

		/// <summary>
		/// Enables domain notifications.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"Overlay.enable"
			);
		}

		/// <summary>
		/// For testing.
		/// </summary>
		[JsonObject]
		public class GetHighlightObjectForTestResult {
			/// <summary>
			/// Highlight data for the node.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: object</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("highlight")]
			public object Highlight;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// For testing.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Id of the node to get highlight object for.</param>
		/// <param name="includeDistance">Whether to include distance info.</param>
		/// <param name="includeStyle">Whether to include style info.</param>
		public GetHighlightObjectForTestResult GetHighlightObjectForTest(int nodeId, bool? includeDistance = null, bool? includeStyle = null) {
			string s = _chrome.Send(
				"Overlay.getHighlightObjectForTest",
				new {
					nodeId,
					includeDistance,
					includeStyle
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetHighlightObjectForTestResult>();
		}

		/// <summary>
		/// Hides any highlight.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void HideHighlight() {
			_chrome.Send(
				"Overlay.hideHighlight"
			);
		}

		/// <summary>
		/// Highlights owner element of the frame with given id.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="frameId">Identifier of the frame to highlight.</param>
		/// <param name="contentColor">The content box highlight fill color (default: transparent).</param>
		/// <param name="contentOutlineColor">The content box highlight outline color (default: transparent).</param>
		public void HighlightFrame(string frameId, DOMDomain.RGBA contentColor = null, DOMDomain.RGBA contentOutlineColor = null) {
			_chrome.Send(
				"Overlay.highlightFrame",
				new {
					frameId,
					contentColor,
					contentOutlineColor
				}
			);
		}

		/// <summary>
		/// Highlights DOM node with given id or with the given JavaScript object wrapper. Either nodeId or objectId must be specified.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="highlightConfig">A descriptor for the highlight appearance.</param>
		/// <param name="nodeId">Identifier of the node to highlight.</param>
		/// <param name="backendNodeId">Identifier of the backend node to highlight.</param>
		/// <param name="objectId">JavaScript object id of the node to be highlighted.</param>
		/// <param name="selector">Selectors to highlight relevant nodes.</param>
		public void HighlightNode(HighlightConfig highlightConfig, int? nodeId = null, int? backendNodeId = null, string objectId = null, string selector = null) {
			_chrome.Send(
				"Overlay.highlightNode",
				new {
					highlightConfig,
					nodeId,
					backendNodeId,
					objectId,
					selector
				}
			);
		}

		/// <summary>
		/// Highlights given quad. Coordinates are absolute with respect to the main frame viewport.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="quad">Quad to highlight</param>
		/// <param name="color">The highlight fill color (default: transparent).</param>
		/// <param name="outlineColor">The highlight outline color (default: transparent).</param>
		public void HighlightQuad(double[] quad, DOMDomain.RGBA color = null, DOMDomain.RGBA outlineColor = null) {
			_chrome.Send(
				"Overlay.highlightQuad",
				new {
					quad,
					color,
					outlineColor
				}
			);
		}

		/// <summary>
		/// Highlights given rectangle. Coordinates are absolute with respect to the main frame viewport.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="x">X coordinate</param>
		/// <param name="y">Y coordinate</param>
		/// <param name="width">Rectangle width</param>
		/// <param name="height">Rectangle height</param>
		/// <param name="color">The highlight fill color (default: transparent).</param>
		/// <param name="outlineColor">The highlight outline color (default: transparent).</param>
		public void HighlightRect(int x, int y, int width, int height, DOMDomain.RGBA color = null, DOMDomain.RGBA outlineColor = null) {
			_chrome.Send(
				"Overlay.highlightRect",
				new {
					x,
					y,
					width,
					height,
					color,
					outlineColor
				}
			);
		}

		/// <summary>
		/// Enters the 'inspect' mode. In this mode, elements that user is hovering over are highlighted. Backend then generates 'inspectNodeRequested' event upon element selection.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="mode">Set an inspection mode.</param>
		/// <param name="highlightConfig">A descriptor for the highlight appearance of hovered-over nodes. May be omitted if `enabled == false`.</param>
		public void SetInspectMode(string mode, HighlightConfig highlightConfig = null) {
			_chrome.Send(
				"Overlay.setInspectMode",
				new {
					mode,
					highlightConfig
				}
			);
		}

		/// <summary>
		/// Highlights owner element of all frames detected to be ads.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="show">True for showing ad highlights</param>
		public void SetShowAdHighlights(bool show) {
			_chrome.Send(
				"Overlay.setShowAdHighlights",
				new {
					show
				}
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="message">The message to display, also triggers resume and step over controls.</param>
		public void SetPausedInDebuggerMessage(string message = null) {
			_chrome.Send(
				"Overlay.setPausedInDebuggerMessage",
				new {
					message
				}
			);
		}

		/// <summary>
		/// Requests that backend shows debug borders on layers
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="show">True for showing debug borders</param>
		public void SetShowDebugBorders(bool show) {
			_chrome.Send(
				"Overlay.setShowDebugBorders",
				new {
					show
				}
			);
		}

		/// <summary>
		/// Requests that backend shows the FPS counter
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="show">True for showing the FPS counter</param>
		public void SetShowFPSCounter(bool show) {
			_chrome.Send(
				"Overlay.setShowFPSCounter",
				new {
					show
				}
			);
		}

		/// <summary>
		/// Requests that backend shows paint rectangles
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="result">True for showing paint rectangles</param>
		public void SetShowPaintRects(bool result) {
			_chrome.Send(
				"Overlay.setShowPaintRects",
				new {
					result
				}
			);
		}

		/// <summary>
		/// Requests that backend shows layout shift regions
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="result">True for showing layout shift regions</param>
		public void SetShowLayoutShiftRegions(bool result) {
			_chrome.Send(
				"Overlay.setShowLayoutShiftRegions",
				new {
					result
				}
			);
		}

		/// <summary>
		/// Requests that backend shows scroll bottleneck rects
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="show">True for showing scroll bottleneck rects</param>
		public void SetShowScrollBottleneckRects(bool show) {
			_chrome.Send(
				"Overlay.setShowScrollBottleneckRects",
				new {
					show
				}
			);
		}

		/// <summary>
		/// Requests that backend shows hit-test borders on layers
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="show">True for showing hit-test borders</param>
		public void SetShowHitTestBorders(bool show) {
			_chrome.Send(
				"Overlay.setShowHitTestBorders",
				new {
					show
				}
			);
		}

		/// <summary>
		/// Paints viewport size upon main frame resize.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="show">Whether to paint size or not.</param>
		public void SetShowViewportSizeOnResize(bool show) {
			_chrome.Send(
				"Overlay.setShowViewportSizeOnResize",
				new {
					show
				}
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("Overlay.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "inspectNodeRequested":
					if (InspectNodeRequested == null) {
						return;
					}
					InspectNodeRequested(
						_chrome,
						JsonConvert.DeserializeObject
							<InspectNodeRequestedEventArgs>(@params.ToString())
					);
					break;
				case "nodeHighlightRequested":
					if (NodeHighlightRequested == null) {
						return;
					}
					NodeHighlightRequested(
						_chrome,
						JsonConvert.DeserializeObject
							<NodeHighlightRequestedEventArgs>(@params.ToString())
					);
					break;
				case "screenshotRequested":
					if (ScreenshotRequested == null) {
						return;
					}
					ScreenshotRequested(
						_chrome,
						JsonConvert.DeserializeObject
							<ScreenshotRequestedEventArgs>(@params.ToString())
					);
					break;
				case "inspectModeCanceled":
					if (InspectModeCanceled == null) {
						return;
					}
					InspectModeCanceled(_chrome, EventArgs.Empty);
					break;

			}
		}

	}
}
