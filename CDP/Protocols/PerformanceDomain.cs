// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// 
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: false</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class PerformanceDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the PerformanceDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public PerformanceDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Current values of the metrics.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class MetricsEventArgs : EventArgs {
			/// <summary>
			/// Current values of the metrics.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("metrics")]
			public Metric[] Metrics;

			/// <summary>
			/// Timestamp title.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("title")]
			public string Title;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Current values of the metrics.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<MetricsEventArgs> Metrics;

		/// <summary>
		/// Run-time execution metric.
		/// </summary>
		[JsonObject]
		public class Metric {
			/// <summary>
			/// Metric name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// Metric value.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public double Value;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Disable collecting and reporting metrics.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"Performance.disable"
			);
		}

		/// <summary>
		/// Enable collecting and reporting metrics.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"Performance.enable"
			);
		}

		/// <summary>
		/// Sets time domain to use for collecting and reporting duration metrics. Note that this must be called before enabling metrics collection. Calling this method while metrics collection is enabled returns an error.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="timeDomain">Time domain</param>
		public void SetTimeDomain(string timeDomain) {
			_chrome.Send(
				"Performance.setTimeDomain",
				new {
					timeDomain
				}
			);
		}

		/// <summary>
		/// Retrieve current values of run-time metrics.
		/// </summary>
		[JsonObject]
		public class GetMetricsResult {
			/// <summary>
			/// Current values for run-time metrics.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("metrics")]
			public Metric[] Metrics;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Retrieve current values of run-time metrics.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public GetMetricsResult GetMetrics() {
			string s = _chrome.Send(
				"Performance.getMetrics"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetMetricsResult>();
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("Performance.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "metrics":
					if (Metrics == null) {
						return;
					}
					Metrics(
						_chrome,
						JsonConvert.DeserializeObject
							<MetricsEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
