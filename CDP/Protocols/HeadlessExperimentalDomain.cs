// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// This domain provides experimental commands only supported in headless mode.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: Page, Runtime</description></item>
	/// </list>
	/// </summary>
	public class HeadlessExperimentalDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the HeadlessExperimentalDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public HeadlessExperimentalDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Issued when the target starts or stops needing BeginFrames. Deprecated. Issue beginFrame unconditionally instead and use result from beginFrame to detect whether the frames were suppressed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class NeedsBeginFramesChangedEventArgs : EventArgs {
			/// <summary>
			/// True if BeginFrames are needed, false otherwise.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("needsBeginFrames")]
			public bool NeedsBeginFrames;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Issued when the target starts or stops needing BeginFrames. Deprecated. Issue beginFrame unconditionally instead and use result from beginFrame to detect whether the frames were suppressed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<NeedsBeginFramesChangedEventArgs> NeedsBeginFramesChanged;

		/// <summary>
		/// Encoding options for a screenshot.
		/// </summary>
		[JsonObject]
		public class ScreenshotParams {
			/// <summary>
			/// Image compression format (defaults to png).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: jpeg, png
			/// </summary>
			[JsonProperty("format")]
			public string Format;

			/// <summary>
			/// Compression quality from range [0..100] (jpeg only).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("quality")]
			public int Quality;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Sends a BeginFrame to the target and returns when the frame was completed. Optionally captures a screenshot from the resulting frame. Requires that the target was created with enabled BeginFrameControl. Designed for use with --run-all-compositor-stages-before-draw, see also https://goo.gl/3zHXhB for more background.
		/// </summary>
		[JsonObject]
		public class BeginFrameResult {
			/// <summary>
			/// Whether the BeginFrame resulted in damage and, thus, a new frame was committed to the display. Reported for diagnostic uses, may be removed in the future.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("hasDamage")]
			public bool HasDamage;

			/// <summary>
			/// Base64-encoded image data of the screenshot, if one was requested and successfully taken.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: binary</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("screenshotData")]
			public byte[] ScreenshotData;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Sends a BeginFrame to the target and returns when the frame was completed. Optionally captures a screenshot from the resulting frame. Requires that the target was created with enabled BeginFrameControl. Designed for use with --run-all-compositor-stages-before-draw, see also https://goo.gl/3zHXhB for more background.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="frameTimeTicks">Timestamp of this BeginFrame in Renderer TimeTicks (milliseconds of uptime). If not set, the current time will be used.</param>
		/// <param name="interval">The interval between BeginFrames that is reported to the compositor, in milliseconds. Defaults to a 60 frames/second interval, i.e. about 16.666 milliseconds.</param>
		/// <param name="noDisplayUpdates">Whether updates should not be committed and drawn onto the display. False by default. If true, only side effects of the BeginFrame will be run, such as layout and animations, but any visual updates may not be visible on the display or in screenshots.</param>
		/// <param name="screenshot">If set, a screenshot of the frame will be captured and returned in the response. Otherwise, no screenshot will be captured. Note that capturing a screenshot can fail, for example, during renderer initialization. In such a case, no screenshot data will be returned.</param>
		public BeginFrameResult BeginFrame(double? frameTimeTicks = null, double? interval = null, bool? noDisplayUpdates = null, ScreenshotParams screenshot = null) {
			string s = _chrome.Send(
				"HeadlessExperimental.beginFrame",
				new {
					frameTimeTicks,
					interval,
					noDisplayUpdates,
					screenshot
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<BeginFrameResult>();
		}

		/// <summary>
		/// Disables headless events for the target.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"HeadlessExperimental.disable"
			);
		}

		/// <summary>
		/// Enables headless events for the target.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"HeadlessExperimental.enable"
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("HeadlessExperimental.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "needsBeginFramesChanged":
					if (NeedsBeginFramesChanged == null) {
						return;
					}
					NeedsBeginFramesChanged(
						_chrome,
						JsonConvert.DeserializeObject
							<NeedsBeginFramesChangedEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
