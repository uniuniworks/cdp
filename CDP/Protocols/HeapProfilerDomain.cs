// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// 
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: Runtime</description></item>
	/// </list>
	/// </summary>
	public class HeapProfilerDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the HeapProfilerDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public HeapProfilerDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class AddHeapSnapshotChunkEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("chunk")]
			public string Chunk;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<AddHeapSnapshotChunkEventArgs> AddHeapSnapshotChunk;

		/// <summary>
		/// If heap objects tracking has been started then backend may send update for one or more fragments
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class HeapStatsUpdateEventArgs : EventArgs {
			/// <summary>
			/// An array of triplets. Each triplet describes a fragment. The first integer is the fragment index, the second integer is a total count of objects for the fragment, the third integer is a total size of the objects for the fragment.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("statsUpdate")]
			public int[] StatsUpdate;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// If heap objects tracking has been started then backend may send update for one or more fragments
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<HeapStatsUpdateEventArgs> HeapStatsUpdate;

		/// <summary>
		/// If heap objects tracking has been started then backend regularly sends a current value for last seen object id and corresponding timestamp. If the were changes in the heap since last event then one or more heapStatsUpdate events will be sent before a new lastSeenObjectId event.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class LastSeenObjectIdEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("lastSeenObjectId")]
			public int LastSeenObjectId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// If heap objects tracking has been started then backend regularly sends a current value for last seen object id and corresponding timestamp. If the were changes in the heap since last event then one or more heapStatsUpdate events will be sent before a new lastSeenObjectId event.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<LastSeenObjectIdEventArgs> LastSeenObjectId;

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ReportHeapSnapshotProgressEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("done")]
			public int Done;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("total")]
			public int Total;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("finished")]
			public bool Finished;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ReportHeapSnapshotProgressEventArgs> ReportHeapSnapshotProgress;

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler ResetProfiles;

		/// <summary>
		/// Sampling Heap Profile node. Holds callsite information, allocation statistics and child nodes.
		/// </summary>
		[JsonObject]
		public class SamplingHeapProfileNode {
			/// <summary>
			/// Function location.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.CallFrame</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("callFrame")]
			public RuntimeDomain.CallFrame CallFrame;

			/// <summary>
			/// Allocations size in bytes for the node excluding children.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("selfSize")]
			public double SelfSize;

			/// <summary>
			/// Node id. Ids are unique across all profiles collected between startSampling and stopSampling.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("id")]
			public int Id;

			/// <summary>
			/// Child nodes.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("children")]
			public SamplingHeapProfileNode[] Children;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// A single sample from a sampling profile.
		/// </summary>
		[JsonObject]
		public class SamplingHeapProfileSample {
			/// <summary>
			/// Allocation size in bytes attributed to the sample.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("size")]
			public double Size;

			/// <summary>
			/// Id of the corresponding profile tree node.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public int NodeId;

			/// <summary>
			/// Time-ordered sample ordinal number. It is unique across all profiles retrieved between startSampling and stopSampling.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("ordinal")]
			public double Ordinal;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Sampling profile.
		/// </summary>
		[JsonObject]
		public class SamplingHeapProfile {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SamplingHeapProfileNode</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("head")]
			public SamplingHeapProfileNode Head;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("samples")]
			public SamplingHeapProfileSample[] Samples;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Enables console to refer to the node with given id via $x (see Command Line API for more details $x functions).
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="heapObjectId">Heap snapshot object id to be accessible by means of $x command line API.</param>
		public void AddInspectedHeapObject(string heapObjectId) {
			_chrome.Send(
				"HeapProfiler.addInspectedHeapObject",
				new {
					heapObjectId
				}
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void CollectGarbage() {
			_chrome.Send(
				"HeapProfiler.collectGarbage"
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"HeapProfiler.disable"
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"HeapProfiler.enable"
			);
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class GetHeapObjectIdResult {
			/// <summary>
			/// Id of the heap snapshot object corresponding to the passed remote object id.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: HeapSnapshotObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("heapSnapshotObjectId")]
			public string HeapSnapshotObjectId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="objectId">Identifier of the object to get heap object id for.</param>
		public GetHeapObjectIdResult GetHeapObjectId(string objectId) {
			string s = _chrome.Send(
				"HeapProfiler.getHeapObjectId",
				new {
					objectId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetHeapObjectIdResult>();
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class GetObjectByHeapObjectIdResult {
			/// <summary>
			/// Evaluation result.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("result")]
			public RuntimeDomain.RemoteObject Result;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="objectId"></param>
		/// <param name="objectGroup">Symbolic group name that can be used to release multiple objects.</param>
		public GetObjectByHeapObjectIdResult GetObjectByHeapObjectId(string objectId, string objectGroup = null) {
			string s = _chrome.Send(
				"HeapProfiler.getObjectByHeapObjectId",
				new {
					objectId,
					objectGroup
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetObjectByHeapObjectIdResult>();
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class GetSamplingProfileResult {
			/// <summary>
			/// Return the sampling profile being collected.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SamplingHeapProfile</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("profile")]
			public SamplingHeapProfile Profile;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public GetSamplingProfileResult GetSamplingProfile() {
			string s = _chrome.Send(
				"HeapProfiler.getSamplingProfile"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetSamplingProfileResult>();
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="samplingInterval">Average sample interval in bytes. Poisson distribution is used for the intervals. The default value is 32768 bytes.</param>
		public void StartSampling(double? samplingInterval = null) {
			_chrome.Send(
				"HeapProfiler.startSampling",
				new {
					samplingInterval
				}
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="trackAllocations"></param>
		public void StartTrackingHeapObjects(bool? trackAllocations = null) {
			_chrome.Send(
				"HeapProfiler.startTrackingHeapObjects",
				new {
					trackAllocations
				}
			);
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class StopSamplingResult {
			/// <summary>
			/// Recorded sampling heap profile.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SamplingHeapProfile</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("profile")]
			public SamplingHeapProfile Profile;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public StopSamplingResult StopSampling() {
			string s = _chrome.Send(
				"HeapProfiler.stopSampling"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<StopSamplingResult>();
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="reportProgress">If true 'reportHeapSnapshotProgress' events will be generated while snapshot is being taken when the tracking is stopped.</param>
		public void StopTrackingHeapObjects(bool? reportProgress = null) {
			_chrome.Send(
				"HeapProfiler.stopTrackingHeapObjects",
				new {
					reportProgress
				}
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="reportProgress">If true 'reportHeapSnapshotProgress' events will be generated while snapshot is being taken.</param>
		public void TakeHeapSnapshot(bool? reportProgress = null) {
			_chrome.Send(
				"HeapProfiler.takeHeapSnapshot",
				new {
					reportProgress
				}
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("HeapProfiler.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "addHeapSnapshotChunk":
					if (AddHeapSnapshotChunk == null) {
						return;
					}
					AddHeapSnapshotChunk(
						_chrome,
						JsonConvert.DeserializeObject
							<AddHeapSnapshotChunkEventArgs>(@params.ToString())
					);
					break;
				case "heapStatsUpdate":
					if (HeapStatsUpdate == null) {
						return;
					}
					HeapStatsUpdate(
						_chrome,
						JsonConvert.DeserializeObject
							<HeapStatsUpdateEventArgs>(@params.ToString())
					);
					break;
				case "lastSeenObjectId":
					if (LastSeenObjectId == null) {
						return;
					}
					LastSeenObjectId(
						_chrome,
						JsonConvert.DeserializeObject
							<LastSeenObjectIdEventArgs>(@params.ToString())
					);
					break;
				case "reportHeapSnapshotProgress":
					if (ReportHeapSnapshotProgress == null) {
						return;
					}
					ReportHeapSnapshotProgress(
						_chrome,
						JsonConvert.DeserializeObject
							<ReportHeapSnapshotProgressEventArgs>(@params.ToString())
					);
					break;
				case "resetProfiles":
					if (ResetProfiles == null) {
						return;
					}
					ResetProfiles(_chrome, EventArgs.Empty);
					break;

			}
		}

	}
}
