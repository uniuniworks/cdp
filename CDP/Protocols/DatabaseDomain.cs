// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// 
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class DatabaseDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the DatabaseDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public DatabaseDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class AddDatabaseEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Database</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("database")]
			public Database Database;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<AddDatabaseEventArgs> AddDatabase;

		/// <summary>
		/// Database object.
		/// </summary>
		[JsonObject]
		public class Database {
			/// <summary>
			/// Database ID.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: DatabaseId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("id")]
			public string Id;

			/// <summary>
			/// Database domain.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("domain")]
			public string Domain;

			/// <summary>
			/// Database name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// Database version.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("version")]
			public string Version;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Database error.
		/// </summary>
		[JsonObject]
		public class Error {
			/// <summary>
			/// Error message.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("message")]
			public string Message;

			/// <summary>
			/// Error code.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("code")]
			public int Code;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Disables database tracking, prevents database events from being sent to the client.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"Database.disable"
			);
		}

		/// <summary>
		/// Enables database tracking, database events will now be delivered to the client.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"Database.enable"
			);
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class ExecuteSQLResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("columnNames")]
			public string[] ColumnNames;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("values")]
			public object[] Values;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Error</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sqlError")]
			public Error SqlError;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="databaseId"></param>
		/// <param name="query"></param>
		public ExecuteSQLResult ExecuteSQL(string databaseId, string query) {
			string s = _chrome.Send(
				"Database.executeSQL",
				new {
					databaseId,
					query
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<ExecuteSQLResult>();
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class GetDatabaseTableNamesResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("tableNames")]
			public string[] TableNames;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="databaseId"></param>
		public GetDatabaseTableNamesResult GetDatabaseTableNames(string databaseId) {
			string s = _chrome.Send(
				"Database.getDatabaseTableNames",
				new {
					databaseId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetDatabaseTableNamesResult>();
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("Database.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "addDatabase":
					if (AddDatabase == null) {
						return;
					}
					AddDatabase(
						_chrome,
						JsonConvert.DeserializeObject
							<AddDatabaseEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
