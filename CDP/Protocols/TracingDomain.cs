// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// 
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: IO</description></item>
	/// </list>
	/// </summary>
	public class TracingDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the TracingDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public TracingDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class BufferUsageEventArgs : EventArgs {
			/// <summary>
			/// A number in range [0..1] that indicates the used size of event buffer as a fraction of its total size.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("percentFull")]
			public double PercentFull;

			/// <summary>
			/// An approximate number of events in the trace log.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("eventCount")]
			public double EventCount;

			/// <summary>
			/// A number in range [0..1] that indicates the used size of event buffer as a fraction of its total size.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public double Value;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<BufferUsageEventArgs> BufferUsage;

		/// <summary>
		/// Contains an bucket of collected trace events. When tracing is stopped collected events will be send as a sequence of dataCollected events followed by tracingComplete event.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class DataCollectedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public object[] Value;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Contains an bucket of collected trace events. When tracing is stopped collected events will be send as a sequence of dataCollected events followed by tracingComplete event.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<DataCollectedEventArgs> DataCollected;

		/// <summary>
		/// Signals that tracing is stopped and there is no trace buffers pending flush, all data were delivered via dataCollected events.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class TracingCompleteEventArgs : EventArgs {
			/// <summary>
			/// Indicates whether some trace data is known to have been lost, e.g. because the trace ring buffer wrapped around.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("dataLossOccurred")]
			public bool DataLossOccurred;

			/// <summary>
			/// A handle of the stream that holds resulting trace data.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: IO.StreamHandle</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("stream")]
			public string Stream;

			/// <summary>
			/// Trace data format of returned stream.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: StreamFormat</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("traceFormat")]
			public string TraceFormat;

			/// <summary>
			/// Compression format of returned stream.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: StreamCompression</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("streamCompression")]
			public string StreamCompression;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Signals that tracing is stopped and there is no trace buffers pending flush, all data were delivered via dataCollected events.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<TracingCompleteEventArgs> TracingComplete;

		/// <summary>
		/// Configuration for memory dump. Used only when "memory-infra" category is enabled.
		/// </summary>
		[JsonObject]
		public class MemoryDumpConfig {


			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class TraceConfig {
			/// <summary>
			/// Controls how the trace buffer stores data.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: recordUntilFull, recordContinuously, recordAsMuchAsPossible, echoToConsole
			/// </summary>
			[JsonProperty("recordMode")]
			public string RecordMode;

			/// <summary>
			/// Turns on JavaScript stack sampling.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("enableSampling")]
			public bool EnableSampling;

			/// <summary>
			/// Turns on system tracing.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("enableSystrace")]
			public bool EnableSystrace;

			/// <summary>
			/// Turns on argument filter.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("enableArgumentFilter")]
			public bool EnableArgumentFilter;

			/// <summary>
			/// Included category filters.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("includedCategories")]
			public string[] IncludedCategories;

			/// <summary>
			/// Excluded category filters.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("excludedCategories")]
			public string[] ExcludedCategories;

			/// <summary>
			/// Configuration to synthesize the delays in tracing.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("syntheticDelays")]
			public string[] SyntheticDelays;

			/// <summary>
			/// Configuration for memory dump triggers. Used only when "memory-infra" category is enabled.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: MemoryDumpConfig</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("memoryDumpConfig")]
			public MemoryDumpConfig MemoryDumpConfig;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Stop trace events collection.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void End() {
			_chrome.Send(
				"Tracing.end"
			);
		}

		/// <summary>
		/// Gets supported tracing categories.
		/// </summary>
		[JsonObject]
		public class GetCategoriesResult {
			/// <summary>
			/// A list of supported tracing categories.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("categories")]
			public string[] Categories;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Gets supported tracing categories.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public GetCategoriesResult GetCategories() {
			string s = _chrome.Send(
				"Tracing.getCategories"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetCategoriesResult>();
		}

		/// <summary>
		/// Record a clock sync marker in the trace.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="syncId">The ID of this clock sync marker</param>
		public void RecordClockSyncMarker(string syncId) {
			_chrome.Send(
				"Tracing.recordClockSyncMarker",
				new {
					syncId
				}
			);
		}

		/// <summary>
		/// Request a global memory dump.
		/// </summary>
		[JsonObject]
		public class RequestMemoryDumpResult {
			/// <summary>
			/// GUID of the resulting global memory dump.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("dumpGuid")]
			public string DumpGuid;

			/// <summary>
			/// True iff the global memory dump succeeded.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("success")]
			public bool Success;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Request a global memory dump.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="deterministic">Enables more deterministic results by forcing garbage collection</param>
		public RequestMemoryDumpResult RequestMemoryDump(bool? deterministic = null) {
			string s = _chrome.Send(
				"Tracing.requestMemoryDump",
				new {
					deterministic
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<RequestMemoryDumpResult>();
		}

		/// <summary>
		/// Start trace events collection.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="categories">Category/tag filter</param>
		/// <param name="options">Tracing options</param>
		/// <param name="bufferUsageReportingInterval">If set, the agent will issue bufferUsage events at this interval, specified in milliseconds</param>
		/// <param name="transferMode">Whether to report trace events as series of dataCollected events or to save trace to a stream (defaults to `ReportEvents`).</param>
		/// <param name="streamFormat">Trace data format to use. This only applies when using `ReturnAsStream` transfer mode (defaults to `json`).</param>
		/// <param name="streamCompression">Compression format to use. This only applies when using `ReturnAsStream` transfer mode (defaults to `none`)</param>
		/// <param name="traceConfig"></param>
		public void Start(string categories = null, string options = null, double? bufferUsageReportingInterval = null, string transferMode = null, string streamFormat = null, string streamCompression = null, TraceConfig traceConfig = null) {
			_chrome.Send(
				"Tracing.start",
				new {
					categories,
					options,
					bufferUsageReportingInterval,
					transferMode,
					streamFormat,
					streamCompression,
					traceConfig
				}
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("Tracing.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "bufferUsage":
					if (BufferUsage == null) {
						return;
					}
					BufferUsage(
						_chrome,
						JsonConvert.DeserializeObject
							<BufferUsageEventArgs>(@params.ToString())
					);
					break;
				case "dataCollected":
					if (DataCollected == null) {
						return;
					}
					DataCollected(
						_chrome,
						JsonConvert.DeserializeObject
							<DataCollectedEventArgs>(@params.ToString())
					);
					break;
				case "tracingComplete":
					if (TracingComplete == null) {
						return;
					}
					TracingComplete(
						_chrome,
						JsonConvert.DeserializeObject
							<TracingCompleteEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
