// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// Security
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: false</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class SecurityDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the SecurityDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public SecurityDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// There is a certificate error. If overriding certificate errors is enabled, then it should be handled with the `handleCertificateError` command. Note: this event does not fire if the certificate error has been allowed internally. Only one client per target should override certificate errors at the same time.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class CertificateErrorEventArgs : EventArgs {
			/// <summary>
			/// The ID of the event.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("eventId")]
			public int EventId;

			/// <summary>
			/// The type of the error.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("errorType")]
			public string ErrorType;

			/// <summary>
			/// The url that was requested.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestURL")]
			public string RequestURL;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// There is a certificate error. If overriding certificate errors is enabled, then it should be handled with the `handleCertificateError` command. Note: this event does not fire if the certificate error has been allowed internally. Only one client per target should override certificate errors at the same time.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<CertificateErrorEventArgs> CertificateError;

		/// <summary>
		/// The security state of the page changed.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class VisibleSecurityStateChangedEventArgs : EventArgs {
			/// <summary>
			/// Security state information about the page.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: VisibleSecurityState</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("visibleSecurityState")]
			public VisibleSecurityState VisibleSecurityState;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// The security state of the page changed.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<VisibleSecurityStateChangedEventArgs> VisibleSecurityStateChanged;

		/// <summary>
		/// The security state of the page changed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class SecurityStateChangedEventArgs : EventArgs {
			/// <summary>
			/// Security state.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SecurityState</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("securityState")]
			public string SecurityState;

			/// <summary>
			/// True if the page was loaded over cryptographic transport such as HTTPS.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("schemeIsCryptographic")]
			public bool SchemeIsCryptographic;

			/// <summary>
			/// List of explanations for the security state. If the overall security state is `insecure` or `warning`, at least one corresponding explanation should be included.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("explanations")]
			public SecurityStateExplanation[] Explanations;

			/// <summary>
			/// Information about insecure content on the page.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: InsecureContentStatus</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("insecureContentStatus")]
			public InsecureContentStatus InsecureContentStatus;

			/// <summary>
			/// Overrides user-visible description of the state.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("summary")]
			public string Summary;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// The security state of the page changed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<SecurityStateChangedEventArgs> SecurityStateChanged;

		/// <summary>
		/// Details about the security state of the page certificate.
		/// </summary>
		[JsonObject]
		public class CertificateSecurityState {
			/// <summary>
			/// Protocol name (e.g. "TLS 1.2" or "QUIC").
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("protocol")]
			public string Protocol;

			/// <summary>
			/// Key Exchange used by the connection, or the empty string if not applicable.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("keyExchange")]
			public string KeyExchange;

			/// <summary>
			/// (EC)DH group used by the connection, if applicable.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("keyExchangeGroup")]
			public string KeyExchangeGroup;

			/// <summary>
			/// Cipher name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("cipher")]
			public string Cipher;

			/// <summary>
			/// TLS MAC. Note that AEAD ciphers do not have separate MACs.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("mac")]
			public string Mac;

			/// <summary>
			/// Page certificate.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("certificate")]
			public string[] Certificate;

			/// <summary>
			/// Certificate subject name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("subjectName")]
			public string SubjectName;

			/// <summary>
			/// Name of the issuing CA.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("issuer")]
			public string Issuer;

			/// <summary>
			/// Certificate valid from date.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Network.TimeSinceEpoch</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("validFrom")]
			public double ValidFrom;

			/// <summary>
			/// Certificate valid to (expiration) date
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Network.TimeSinceEpoch</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("validTo")]
			public double ValidTo;

			/// <summary>
			/// True if the certificate uses a weak signature aglorithm.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("certifcateHasWeakSignature")]
			public bool CertifcateHasWeakSignature;

			/// <summary>
			/// True if modern SSL
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("modernSSL")]
			public bool ModernSSL;

			/// <summary>
			/// True if the connection is using an obsolete SSL protocol.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("obsoleteSslProtocol")]
			public bool ObsoleteSslProtocol;

			/// <summary>
			/// True if the connection is using an obsolete SSL key exchange.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("obsoleteSslKeyExchange")]
			public bool ObsoleteSslKeyExchange;

			/// <summary>
			/// True if the connection is using an obsolete SSL cipher.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("obsoleteSslCipher")]
			public bool ObsoleteSslCipher;

			/// <summary>
			/// True if the connection is using an obsolete SSL signature.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("obsoleteSslSignature")]
			public bool ObsoleteSslSignature;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Security state information about the page.
		/// </summary>
		[JsonObject]
		public class VisibleSecurityState {
			/// <summary>
			/// The security level of the page.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SecurityState</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("securityState")]
			public string SecurityState;

			/// <summary>
			/// Security state details about the page certificate.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: CertificateSecurityState</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("certificateSecurityState")]
			public CertificateSecurityState CertificateSecurityState;

			/// <summary>
			/// Array of security state issues ids.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("securityStateIssueIds")]
			public string[] SecurityStateIssueIds;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// An explanation of an factor contributing to the security state.
		/// </summary>
		[JsonObject]
		public class SecurityStateExplanation {
			/// <summary>
			/// Security state representing the severity of the factor being explained.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SecurityState</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("securityState")]
			public string SecurityState;

			/// <summary>
			/// Title describing the type of factor.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("title")]
			public string Title;

			/// <summary>
			/// Short phrase describing the type of factor.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("summary")]
			public string Summary;

			/// <summary>
			/// Full text explanation of the factor.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("description")]
			public string Description;

			/// <summary>
			/// The type of mixed content described by the explanation.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: MixedContentType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("mixedContentType")]
			public string MixedContentType;

			/// <summary>
			/// Page certificate.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("certificate")]
			public string[] Certificate;

			/// <summary>
			/// Recommendations to fix any issues.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("recommendations")]
			public string[] Recommendations;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Information about insecure content on the page.
		/// </summary>
		[JsonObject]
		public class InsecureContentStatus {
			/// <summary>
			/// Always false.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("ranMixedContent")]
			public bool RanMixedContent;

			/// <summary>
			/// Always false.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("displayedMixedContent")]
			public bool DisplayedMixedContent;

			/// <summary>
			/// Always false.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("containedMixedForm")]
			public bool ContainedMixedForm;

			/// <summary>
			/// Always false.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("ranContentWithCertErrors")]
			public bool RanContentWithCertErrors;

			/// <summary>
			/// Always false.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("displayedContentWithCertErrors")]
			public bool DisplayedContentWithCertErrors;

			/// <summary>
			/// Always set to unknown.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SecurityState</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("ranInsecureContentStyle")]
			public string RanInsecureContentStyle;

			/// <summary>
			/// Always set to unknown.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SecurityState</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("displayedInsecureContentStyle")]
			public string DisplayedInsecureContentStyle;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Disables tracking security state changes.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"Security.disable"
			);
		}

		/// <summary>
		/// Enables tracking security state changes.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"Security.enable"
			);
		}

		/// <summary>
		/// Enable/disable whether all certificate errors should be ignored.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="ignore">If true, all certificate errors will be ignored.</param>
		public void SetIgnoreCertificateErrors(bool ignore) {
			_chrome.Send(
				"Security.setIgnoreCertificateErrors",
				new {
					ignore
				}
			);
		}

		/// <summary>
		/// Handles a certificate error that fired a certificateError event.
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="eventId">The ID of the event.</param>
		/// <param name="action">The action to take on the certificate error.</param>
		[Obsolete]
		public void HandleCertificateError(int eventId, string action) {
			_chrome.Send(
				"Security.handleCertificateError",
				new {
					eventId,
					action
				}
			);
		}

		/// <summary>
		/// Enable/disable overriding certificate errors. If enabled, all certificate error events need to be handled by the DevTools client and should be answered with `handleCertificateError` commands.
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="override">If true, certificate errors will be overridden.</param>
		[Obsolete]
		public void SetOverrideCertificateErrors(bool @override) {
			_chrome.Send(
				"Security.setOverrideCertificateErrors",
				new {
					@override
				}
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("Security.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "certificateError":
					if (CertificateError == null) {
						return;
					}
					CertificateError(
						_chrome,
						JsonConvert.DeserializeObject
							<CertificateErrorEventArgs>(@params.ToString())
					);
					break;
				case "visibleSecurityStateChanged":
					if (VisibleSecurityStateChanged == null) {
						return;
					}
					VisibleSecurityStateChanged(
						_chrome,
						JsonConvert.DeserializeObject
							<VisibleSecurityStateChangedEventArgs>(@params.ToString())
					);
					break;
				case "securityStateChanged":
					if (SecurityStateChanged == null) {
						return;
					}
					SecurityStateChanged(
						_chrome,
						JsonConvert.DeserializeObject
							<SecurityStateChangedEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
