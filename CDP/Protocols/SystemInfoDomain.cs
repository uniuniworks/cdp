// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// The SystemInfo domain defines methods and events for querying low-level system information.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class SystemInfoDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the SystemInfoDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public SystemInfoDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Describes a single graphics processor (GPU).
		/// </summary>
		[JsonObject]
		public class GPUDevice {
			/// <summary>
			/// PCI ID of the GPU vendor, if available; 0 otherwise.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("vendorId")]
			public double VendorId;

			/// <summary>
			/// PCI ID of the GPU device, if available; 0 otherwise.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("deviceId")]
			public double DeviceId;

			/// <summary>
			/// Sub sys ID of the GPU, only available on Windows.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("subSysId")]
			public double SubSysId;

			/// <summary>
			/// Revision of the GPU, only available on Windows.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("revision")]
			public double Revision;

			/// <summary>
			/// String description of the GPU vendor, if the PCI ID is not available.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("vendorString")]
			public string VendorString;

			/// <summary>
			/// String description of the GPU device, if the PCI ID is not available.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("deviceString")]
			public string DeviceString;

			/// <summary>
			/// String description of the GPU driver vendor.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("driverVendor")]
			public string DriverVendor;

			/// <summary>
			/// String description of the GPU driver version.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("driverVersion")]
			public string DriverVersion;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Describes the width and height dimensions of an entity.
		/// </summary>
		[JsonObject]
		public class Size {
			/// <summary>
			/// Width in pixels.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("width")]
			public int Width;

			/// <summary>
			/// Height in pixels.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("height")]
			public int Height;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Describes a supported video decoding profile with its associated minimum and maximum resolutions.
		/// </summary>
		[JsonObject]
		public class VideoDecodeAcceleratorCapability {
			/// <summary>
			/// Video codec profile that is supported, e.g. VP9 Profile 2.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("profile")]
			public string Profile;

			/// <summary>
			/// Maximum video dimensions in pixels supported for this |profile|.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Size</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("maxResolution")]
			public Size MaxResolution;

			/// <summary>
			/// Minimum video dimensions in pixels supported for this |profile|.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Size</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("minResolution")]
			public Size MinResolution;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Describes a supported video encoding profile with its associated maximum resolution and maximum framerate.
		/// </summary>
		[JsonObject]
		public class VideoEncodeAcceleratorCapability {
			/// <summary>
			/// Video codec profile that is supported, e.g H264 Main.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("profile")]
			public string Profile;

			/// <summary>
			/// Maximum video dimensions in pixels supported for this |profile|.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Size</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("maxResolution")]
			public Size MaxResolution;

			/// <summary>
			/// Maximum encoding framerate in frames per second supported for this |profile|, as fraction's numerator and denominator, e.g. 24/1 fps, 24000/1001 fps, etc.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("maxFramerateNumerator")]
			public int MaxFramerateNumerator;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("maxFramerateDenominator")]
			public int MaxFramerateDenominator;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Describes a supported image decoding profile with its associated minimum and maximum resolutions and subsampling.
		/// </summary>
		[JsonObject]
		public class ImageDecodeAcceleratorCapability {
			/// <summary>
			/// Image coded, e.g. Jpeg.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ImageType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("imageType")]
			public string ImageType;

			/// <summary>
			/// Maximum supported dimensions of the image in pixels.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Size</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("maxDimensions")]
			public Size MaxDimensions;

			/// <summary>
			/// Minimum supported dimensions of the image in pixels.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Size</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("minDimensions")]
			public Size MinDimensions;

			/// <summary>
			/// Optional array of supported subsampling formats, e.g. 4:2:0, if known.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("subsamplings")]
			public string[] Subsamplings;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Provides information about the GPU(s) on the system.
		/// </summary>
		[JsonObject]
		public class GPUInfo {
			/// <summary>
			/// The graphics devices on the system. Element 0 is the primary GPU.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("devices")]
			public GPUDevice[] Devices;

			/// <summary>
			/// An optional dictionary of additional GPU related attributes.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: object</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("auxAttributes")]
			public object AuxAttributes;

			/// <summary>
			/// An optional dictionary of graphics features and their status.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: object</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("featureStatus")]
			public object FeatureStatus;

			/// <summary>
			/// An optional array of GPU driver bug workarounds.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("driverBugWorkarounds")]
			public string[] DriverBugWorkarounds;

			/// <summary>
			/// Supported accelerated video decoding capabilities.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("videoDecoding")]
			public VideoDecodeAcceleratorCapability[] VideoDecoding;

			/// <summary>
			/// Supported accelerated video encoding capabilities.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("videoEncoding")]
			public VideoEncodeAcceleratorCapability[] VideoEncoding;

			/// <summary>
			/// Supported accelerated image decoding capabilities.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("imageDecoding")]
			public ImageDecodeAcceleratorCapability[] ImageDecoding;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Represents process info.
		/// </summary>
		[JsonObject]
		public class ProcessInfo {
			/// <summary>
			/// Specifies process type.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// Specifies process id.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("id")]
			public int Id;

			/// <summary>
			/// Specifies cumulative CPU usage in seconds across all threads of the process since the process start.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("cpuTime")]
			public double CpuTime;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Returns information about the system.
		/// </summary>
		[JsonObject]
		public class GetInfoResult {
			/// <summary>
			/// Information about the GPUs on the system.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GPUInfo</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("gpu")]
			public GPUInfo Gpu;

			/// <summary>
			/// A platform-dependent description of the model of the machine. On Mac OS, this is, for example, 'MacBookPro'. Will be the empty string if not supported.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("modelName")]
			public string ModelName;

			/// <summary>
			/// A platform-dependent description of the version of the machine. On Mac OS, this is, for example, '10.1'. Will be the empty string if not supported.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("modelVersion")]
			public string ModelVersion;

			/// <summary>
			/// The command line string used to launch the browser. Will be the empty string if not supported.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("commandLine")]
			public string CommandLine;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns information about the system.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public GetInfoResult GetInfo() {
			string s = _chrome.Send(
				"SystemInfo.getInfo"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetInfoResult>();
		}

		/// <summary>
		/// Returns information about all running processes.
		/// </summary>
		[JsonObject]
		public class GetProcessInfoResult {
			/// <summary>
			/// An array of process info blocks.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("processInfo")]
			public ProcessInfo[] ProcessInfo;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns information about all running processes.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public GetProcessInfoResult GetProcessInfo() {
			string s = _chrome.Send(
				"SystemInfo.getProcessInfo"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetProcessInfoResult>();
		}



	}
}
