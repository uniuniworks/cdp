// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// This domain exposes CSS read/write operations. All CSS objects (stylesheets, rules, and styles) have an associated `id` used in subsequent operations on the related object. Each object type has a specific `id` structure, and those are not interchangeable between objects of different kinds. CSS objects can be loaded using the `get*ForNode()` calls (which accept a DOM node id). A client can also keep track of stylesheets via the `styleSheetAdded`/`styleSheetRemoved` events and subsequently load the required stylesheet contents using the `getStyleSheet[Text]()` methods.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: DOM</description></item>
	/// </list>
	/// </summary>
	public class CSSDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the CSSDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public CSSDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Fires whenever a web font is updated.  A non-empty font parameter indicates a successfully loaded web font
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class FontsUpdatedEventArgs : EventArgs {
			/// <summary>
			/// The web font that has loaded.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: FontFace</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("font")]
			public FontFace Font;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fires whenever a web font is updated.  A non-empty font parameter indicates a successfully loaded web font
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<FontsUpdatedEventArgs> FontsUpdated;

		/// <summary>
		/// Fires whenever a MediaQuery result changes (for example, after a browser window has been resized.) The current implementation considers only viewport-dependent media features.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler MediaQueryResultChanged;

		/// <summary>
		/// Fired whenever an active document stylesheet is added.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class StyleSheetAddedEventArgs : EventArgs {
			/// <summary>
			/// Added stylesheet metainfo.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: CSSStyleSheetHeader</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("header")]
			public CSSStyleSheetHeader Header;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired whenever an active document stylesheet is added.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<StyleSheetAddedEventArgs> StyleSheetAdded;

		/// <summary>
		/// Fired whenever a stylesheet is changed as a result of the client operation.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class StyleSheetChangedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StyleSheetId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("styleSheetId")]
			public string StyleSheetId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired whenever a stylesheet is changed as a result of the client operation.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<StyleSheetChangedEventArgs> StyleSheetChanged;

		/// <summary>
		/// Fired whenever an active document stylesheet is removed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class StyleSheetRemovedEventArgs : EventArgs {
			/// <summary>
			/// Identifier of the removed stylesheet.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StyleSheetId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("styleSheetId")]
			public string StyleSheetId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired whenever an active document stylesheet is removed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<StyleSheetRemovedEventArgs> StyleSheetRemoved;

		/// <summary>
		/// CSS rule collection for a single pseudo style.
		/// </summary>
		[JsonObject]
		public class PseudoElementMatches {
			/// <summary>
			/// Pseudo element type.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: DOM.PseudoType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("pseudoType")]
			public string PseudoType;

			/// <summary>
			/// Matches of CSS rules applicable to the pseudo style.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("matches")]
			public RuleMatch[] Matches;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Inherited CSS rule collection from ancestor node.
		/// </summary>
		[JsonObject]
		public class InheritedStyleEntry {
			/// <summary>
			/// The ancestor node's inline style, if any, in the style inheritance chain.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: CSSStyle</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("inlineStyle")]
			public CSSStyle InlineStyle;

			/// <summary>
			/// Matches of CSS rules matching the ancestor node in the style inheritance chain.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("matchedCSSRules")]
			public RuleMatch[] MatchedCSSRules;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Match data for a CSS rule.
		/// </summary>
		[JsonObject]
		public class RuleMatch {
			/// <summary>
			/// CSS rule in the match.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: CSSRule</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("rule")]
			public CSSRule Rule;

			/// <summary>
			/// Matching selector indices in the rule's selectorList selectors (0-based).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("matchingSelectors")]
			public int[] MatchingSelectors;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Data for a simple selector (these are delimited by commas in a selector list).
		/// </summary>
		[JsonObject]
		public class Value {
			/// <summary>
			/// Value text.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("text")]
			public string Text;

			/// <summary>
			/// Value range in the underlying resource (if available).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: SourceRange</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("range")]
			public SourceRange Range;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Selector list data.
		/// </summary>
		[JsonObject]
		public class SelectorList {
			/// <summary>
			/// Selectors in the list.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("selectors")]
			public Value[] Selectors;

			/// <summary>
			/// Rule selector text.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("text")]
			public string Text;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// CSS stylesheet metainformation.
		/// </summary>
		[JsonObject]
		public class CSSStyleSheetHeader {
			/// <summary>
			/// The stylesheet identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StyleSheetId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("styleSheetId")]
			public string StyleSheetId;

			/// <summary>
			/// Owner frame identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Page.FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// Stylesheet resource URL.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sourceURL")]
			public string SourceURL;

			/// <summary>
			/// URL of source map associated with the stylesheet (if any).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sourceMapURL")]
			public string SourceMapURL;

			/// <summary>
			/// Stylesheet origin.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StyleSheetOrigin</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("origin")]
			public string Origin;

			/// <summary>
			/// Stylesheet title.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("title")]
			public string Title;

			/// <summary>
			/// The backend id for the owner node of the stylesheet.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: DOM.BackendNodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("ownerNode")]
			public int OwnerNode;

			/// <summary>
			/// Denotes whether the stylesheet is disabled.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("disabled")]
			public bool Disabled;

			/// <summary>
			/// Whether the sourceURL field value comes from the sourceURL comment.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("hasSourceURL")]
			public bool HasSourceURL;

			/// <summary>
			/// Whether this stylesheet is created for STYLE tag by parser. This flag is not set for document.written STYLE tags.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("isInline")]
			public bool IsInline;

			/// <summary>
			/// Line offset of the stylesheet within the resource (zero based).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("startLine")]
			public double StartLine;

			/// <summary>
			/// Column offset of the stylesheet within the resource (zero based).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("startColumn")]
			public double StartColumn;

			/// <summary>
			/// Size of the content (in characters).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("length")]
			public double Length;

			/// <summary>
			/// Line offset of the end of the stylesheet within the resource (zero based).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("endLine")]
			public double EndLine;

			/// <summary>
			/// Column offset of the end of the stylesheet within the resource (zero based).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("endColumn")]
			public double EndColumn;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// CSS rule representation.
		/// </summary>
		[JsonObject]
		public class CSSRule {
			/// <summary>
			/// The css style sheet identifier (absent for user agent stylesheet and user-specified stylesheet rules) this rule came from.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: StyleSheetId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("styleSheetId")]
			public string StyleSheetId;

			/// <summary>
			/// Rule selector data.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SelectorList</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("selectorList")]
			public SelectorList SelectorList;

			/// <summary>
			/// Parent stylesheet's origin.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StyleSheetOrigin</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("origin")]
			public string Origin;

			/// <summary>
			/// Associated style declaration.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: CSSStyle</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("style")]
			public CSSStyle Style;

			/// <summary>
			/// Media list array (for rules involving media queries). The array enumerates media queries starting with the innermost one, going outwards.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("media")]
			public CSSMedia[] Media;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// CSS coverage information.
		/// </summary>
		[JsonObject]
		public class RuleUsage {
			/// <summary>
			/// The css style sheet identifier (absent for user agent stylesheet and user-specified stylesheet rules) this rule came from.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StyleSheetId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("styleSheetId")]
			public string StyleSheetId;

			/// <summary>
			/// Offset of the start of the rule (including selector) from the beginning of the stylesheet.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("startOffset")]
			public double StartOffset;

			/// <summary>
			/// Offset of the end of the rule body from the beginning of the stylesheet.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("endOffset")]
			public double EndOffset;

			/// <summary>
			/// Indicates whether the rule was actually used by some element in the page.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("used")]
			public bool Used;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Text range within a resource. All numbers are zero-based.
		/// </summary>
		[JsonObject]
		public class SourceRange {
			/// <summary>
			/// Start line of range.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("startLine")]
			public int StartLine;

			/// <summary>
			/// Start column of range (inclusive).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("startColumn")]
			public int StartColumn;

			/// <summary>
			/// End line of range
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("endLine")]
			public int EndLine;

			/// <summary>
			/// End column of range (exclusive).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("endColumn")]
			public int EndColumn;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class ShorthandEntry {
			/// <summary>
			/// Shorthand name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// Shorthand value.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public string Value;

			/// <summary>
			/// Whether the property has "!important" annotation (implies `false` if absent).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("important")]
			public bool Important;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class CSSComputedStyleProperty {
			/// <summary>
			/// Computed style property name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// Computed style property value.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public string Value;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// CSS style representation.
		/// </summary>
		[JsonObject]
		public class CSSStyle {
			/// <summary>
			/// The css style sheet identifier (absent for user agent stylesheet and user-specified stylesheet rules) this rule came from.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: StyleSheetId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("styleSheetId")]
			public string StyleSheetId;

			/// <summary>
			/// CSS properties in the style.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("cssProperties")]
			public CSSProperty[] CssProperties;

			/// <summary>
			/// Computed values for all shorthands found in the style.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("shorthandEntries")]
			public ShorthandEntry[] ShorthandEntries;

			/// <summary>
			/// Style declaration text (if available).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("cssText")]
			public string CssText;

			/// <summary>
			/// Style declaration range in the enclosing stylesheet (if available).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: SourceRange</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("range")]
			public SourceRange Range;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// CSS property declaration data.
		/// </summary>
		[JsonObject]
		public class CSSProperty {
			/// <summary>
			/// The property name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// The property value.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public string Value;

			/// <summary>
			/// Whether the property has "!important" annotation (implies `false` if absent).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("important")]
			public bool Important;

			/// <summary>
			/// Whether the property is implicit (implies `false` if absent).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("implicit")]
			public bool Implicit;

			/// <summary>
			/// The full property text as specified in the style.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("text")]
			public string Text;

			/// <summary>
			/// Whether the property is understood by the browser (implies `true` if absent).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("parsedOk")]
			public bool ParsedOk;

			/// <summary>
			/// Whether the property is disabled by the user (present for source-based properties only).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("disabled")]
			public bool Disabled;

			/// <summary>
			/// The entire property range in the enclosing style declaration (if available).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: SourceRange</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("range")]
			public SourceRange Range;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// CSS media rule descriptor.
		/// </summary>
		[JsonObject]
		public class CSSMedia {
			/// <summary>
			/// Media query text.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("text")]
			public string Text;

			/// <summary>
			/// Source of the media query: "mediaRule" if specified by a @media rule, "importRule" if specified by an @import rule, "linkedSheet" if specified by a "media" attribute in a linked stylesheet's LINK tag, "inlineSheet" if specified by a "media" attribute in an inline stylesheet's STYLE tag.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: mediaRule, importRule, linkedSheet, inlineSheet
			/// </summary>
			[JsonProperty("source")]
			public string Source;

			/// <summary>
			/// URL of the document containing the media query description.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sourceURL")]
			public string SourceURL;

			/// <summary>
			/// The associated rule (@media or @import) header range in the enclosing stylesheet (if available).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: SourceRange</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("range")]
			public SourceRange Range;

			/// <summary>
			/// Identifier of the stylesheet containing this object (if exists).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: StyleSheetId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("styleSheetId")]
			public string StyleSheetId;

			/// <summary>
			/// Array of media queries.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("mediaList")]
			public MediaQuery[] MediaList;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Media query descriptor.
		/// </summary>
		[JsonObject]
		public class MediaQuery {
			/// <summary>
			/// Array of media query expressions.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("expressions")]
			public MediaQueryExpression[] Expressions;

			/// <summary>
			/// Whether the media query condition is satisfied.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("active")]
			public bool Active;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Media query expression descriptor.
		/// </summary>
		[JsonObject]
		public class MediaQueryExpression {
			/// <summary>
			/// Media query expression value.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public double Value;

			/// <summary>
			/// Media query expression units.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("unit")]
			public string Unit;

			/// <summary>
			/// Media query expression feature.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("feature")]
			public string Feature;

			/// <summary>
			/// The associated range of the value text in the enclosing stylesheet (if available).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: SourceRange</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("valueRange")]
			public SourceRange ValueRange;

			/// <summary>
			/// Computed length of media query expression (if applicable).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("computedLength")]
			public double ComputedLength;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Information about amount of glyphs that were rendered with given font.
		/// </summary>
		[JsonObject]
		public class PlatformFontUsage {
			/// <summary>
			/// Font's family name reported by platform.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("familyName")]
			public string FamilyName;

			/// <summary>
			/// Indicates if the font was downloaded or resolved locally.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("isCustomFont")]
			public bool IsCustomFont;

			/// <summary>
			/// Amount of glyphs that were rendered with this font.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("glyphCount")]
			public double GlyphCount;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Properties of a web font: https://www.w3.org/TR/2008/REC-CSS2-20080411/fonts.html#font-descriptions
		/// </summary>
		[JsonObject]
		public class FontFace {
			/// <summary>
			/// The font-family.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("fontFamily")]
			public string FontFamily;

			/// <summary>
			/// The font-style.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("fontStyle")]
			public string FontStyle;

			/// <summary>
			/// The font-variant.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("fontVariant")]
			public string FontVariant;

			/// <summary>
			/// The font-weight.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("fontWeight")]
			public string FontWeight;

			/// <summary>
			/// The font-stretch.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("fontStretch")]
			public string FontStretch;

			/// <summary>
			/// The unicode-range.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("unicodeRange")]
			public string UnicodeRange;

			/// <summary>
			/// The src.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("src")]
			public string Src;

			/// <summary>
			/// The resolved platform font family
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("platformFontFamily")]
			public string PlatformFontFamily;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// CSS keyframes rule representation.
		/// </summary>
		[JsonObject]
		public class CSSKeyframesRule {
			/// <summary>
			/// Animation name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Value</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("animationName")]
			public Value AnimationName;

			/// <summary>
			/// List of keyframes.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("keyframes")]
			public CSSKeyframeRule[] Keyframes;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// CSS keyframe rule representation.
		/// </summary>
		[JsonObject]
		public class CSSKeyframeRule {
			/// <summary>
			/// The css style sheet identifier (absent for user agent stylesheet and user-specified stylesheet rules) this rule came from.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: StyleSheetId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("styleSheetId")]
			public string StyleSheetId;

			/// <summary>
			/// Parent stylesheet's origin.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StyleSheetOrigin</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("origin")]
			public string Origin;

			/// <summary>
			/// Associated key text.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Value</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("keyText")]
			public Value KeyText;

			/// <summary>
			/// Associated style declaration.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: CSSStyle</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("style")]
			public CSSStyle Style;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// A descriptor of operation to mutate style declaration text.
		/// </summary>
		[JsonObject]
		public class StyleDeclarationEdit {
			/// <summary>
			/// The css style sheet identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StyleSheetId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("styleSheetId")]
			public string StyleSheetId;

			/// <summary>
			/// The range of the style text in the enclosing stylesheet.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SourceRange</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("range")]
			public SourceRange Range;

			/// <summary>
			/// New style text.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("text")]
			public string Text;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Inserts a new rule with the given `ruleText` in a stylesheet with given `styleSheetId`, at the position specified by `location`.
		/// </summary>
		[JsonObject]
		public class AddRuleResult {
			/// <summary>
			/// The newly created rule.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: CSSRule</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("rule")]
			public CSSRule Rule;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Inserts a new rule with the given `ruleText` in a stylesheet with given `styleSheetId`, at the position specified by `location`.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="styleSheetId">The css style sheet identifier where a new rule should be inserted.</param>
		/// <param name="ruleText">The text of a new rule.</param>
		/// <param name="location">Text position of a new rule in the target style sheet.</param>
		public AddRuleResult AddRule(string styleSheetId, string ruleText, SourceRange location) {
			string s = _chrome.Send(
				"CSS.addRule",
				new {
					styleSheetId,
					ruleText,
					location
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<AddRuleResult>();
		}

		/// <summary>
		/// Returns all class names from specified stylesheet.
		/// </summary>
		[JsonObject]
		public class CollectClassNamesResult {
			/// <summary>
			/// Class name list.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("classNames")]
			public string[] ClassNames;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns all class names from specified stylesheet.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="styleSheetId"></param>
		public CollectClassNamesResult CollectClassNames(string styleSheetId) {
			string s = _chrome.Send(
				"CSS.collectClassNames",
				new {
					styleSheetId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<CollectClassNamesResult>();
		}

		/// <summary>
		/// Creates a new special "via-inspector" stylesheet in the frame with given `frameId`.
		/// </summary>
		[JsonObject]
		public class CreateStyleSheetResult {
			/// <summary>
			/// Identifier of the created "via-inspector" stylesheet.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StyleSheetId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("styleSheetId")]
			public string StyleSheetId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Creates a new special "via-inspector" stylesheet in the frame with given `frameId`.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="frameId">Identifier of the frame where "via-inspector" stylesheet should be created.</param>
		public CreateStyleSheetResult CreateStyleSheet(string frameId) {
			string s = _chrome.Send(
				"CSS.createStyleSheet",
				new {
					frameId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<CreateStyleSheetResult>();
		}

		/// <summary>
		/// Disables the CSS agent for the given page.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"CSS.disable"
			);
		}

		/// <summary>
		/// Enables the CSS agent for the given page. Clients should not assume that the CSS agent has been enabled until the result of this command is received.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"CSS.enable"
			);
		}

		/// <summary>
		/// Ensures that the given node will have specified pseudo-classes whenever its style is computed by the browser.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">The element id for which to force the pseudo state.</param>
		/// <param name="forcedPseudoClasses">Element pseudo classes to force when computing the element's style.</param>
		public void ForcePseudoState(int nodeId, string[] forcedPseudoClasses) {
			_chrome.Send(
				"CSS.forcePseudoState",
				new {
					nodeId,
					forcedPseudoClasses
				}
			);
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class GetBackgroundColorsResult {
			/// <summary>
			/// The range of background colors behind this element, if it contains any visible text. If no visible text is present, this will be undefined. In the case of a flat background color, this will consist of simply that color. In the case of a gradient, this will consist of each of the color stops. For anything more complicated, this will be an empty array. Images will be ignored (as if the image had failed to load).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("backgroundColors")]
			public string[] BackgroundColors;

			/// <summary>
			/// The computed font size for this node, as a CSS computed value string (e.g. '12px').
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("computedFontSize")]
			public string ComputedFontSize;

			/// <summary>
			/// The computed font weight for this node, as a CSS computed value string (e.g. 'normal' or '100').
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("computedFontWeight")]
			public string ComputedFontWeight;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Id of the node to get background colors for.</param>
		public GetBackgroundColorsResult GetBackgroundColors(int nodeId) {
			string s = _chrome.Send(
				"CSS.getBackgroundColors",
				new {
					nodeId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetBackgroundColorsResult>();
		}

		/// <summary>
		/// Returns the computed style for a DOM node identified by `nodeId`.
		/// </summary>
		[JsonObject]
		public class GetComputedStyleForNodeResult {
			/// <summary>
			/// Computed style for the specified DOM node.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("computedStyle")]
			public CSSComputedStyleProperty[] ComputedStyle;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns the computed style for a DOM node identified by `nodeId`.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId"></param>
		public GetComputedStyleForNodeResult GetComputedStyleForNode(int nodeId) {
			string s = _chrome.Send(
				"CSS.getComputedStyleForNode",
				new {
					nodeId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetComputedStyleForNodeResult>();
		}

		/// <summary>
		/// Returns the styles defined inline (explicitly in the "style" attribute and implicitly, using DOM attributes) for a DOM node identified by `nodeId`.
		/// </summary>
		[JsonObject]
		public class GetInlineStylesForNodeResult {
			/// <summary>
			/// Inline style for the specified DOM node.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: CSSStyle</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("inlineStyle")]
			public CSSStyle InlineStyle;

			/// <summary>
			/// Attribute-defined element style (e.g. resulting from "width=20 height=100%").
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: CSSStyle</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("attributesStyle")]
			public CSSStyle AttributesStyle;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns the styles defined inline (explicitly in the "style" attribute and implicitly, using DOM attributes) for a DOM node identified by `nodeId`.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId"></param>
		public GetInlineStylesForNodeResult GetInlineStylesForNode(int nodeId) {
			string s = _chrome.Send(
				"CSS.getInlineStylesForNode",
				new {
					nodeId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetInlineStylesForNodeResult>();
		}

		/// <summary>
		/// Returns requested styles for a DOM node identified by `nodeId`.
		/// </summary>
		[JsonObject]
		public class GetMatchedStylesForNodeResult {
			/// <summary>
			/// Inline style for the specified DOM node.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: CSSStyle</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("inlineStyle")]
			public CSSStyle InlineStyle;

			/// <summary>
			/// Attribute-defined element style (e.g. resulting from "width=20 height=100%").
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: CSSStyle</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("attributesStyle")]
			public CSSStyle AttributesStyle;

			/// <summary>
			/// CSS rules matching this node, from all applicable stylesheets.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("matchedCSSRules")]
			public RuleMatch[] MatchedCSSRules;

			/// <summary>
			/// Pseudo style matches for this node.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("pseudoElements")]
			public PseudoElementMatches[] PseudoElements;

			/// <summary>
			/// A chain of inherited styles (from the immediate node parent up to the DOM tree root).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("inherited")]
			public InheritedStyleEntry[] Inherited;

			/// <summary>
			/// A list of CSS keyframed animations matching this node.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("cssKeyframesRules")]
			public CSSKeyframesRule[] CssKeyframesRules;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns requested styles for a DOM node identified by `nodeId`.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId"></param>
		public GetMatchedStylesForNodeResult GetMatchedStylesForNode(int nodeId) {
			string s = _chrome.Send(
				"CSS.getMatchedStylesForNode",
				new {
					nodeId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetMatchedStylesForNodeResult>();
		}

		/// <summary>
		/// Returns all media queries parsed by the rendering engine.
		/// </summary>
		[JsonObject]
		public class GetMediaQueriesResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("medias")]
			public CSSMedia[] Medias;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns all media queries parsed by the rendering engine.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public GetMediaQueriesResult GetMediaQueries() {
			string s = _chrome.Send(
				"CSS.getMediaQueries"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetMediaQueriesResult>();
		}

		/// <summary>
		/// Requests information about platform fonts which we used to render child TextNodes in the given node.
		/// </summary>
		[JsonObject]
		public class GetPlatformFontsForNodeResult {
			/// <summary>
			/// Usage statistics for every employed platform font.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("fonts")]
			public PlatformFontUsage[] Fonts;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Requests information about platform fonts which we used to render child TextNodes in the given node.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId"></param>
		public GetPlatformFontsForNodeResult GetPlatformFontsForNode(int nodeId) {
			string s = _chrome.Send(
				"CSS.getPlatformFontsForNode",
				new {
					nodeId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetPlatformFontsForNodeResult>();
		}

		/// <summary>
		/// Returns the current textual content for a stylesheet.
		/// </summary>
		[JsonObject]
		public class GetStyleSheetTextResult {
			/// <summary>
			/// The stylesheet text.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("text")]
			public string Text;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns the current textual content for a stylesheet.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="styleSheetId"></param>
		public GetStyleSheetTextResult GetStyleSheetText(string styleSheetId) {
			string s = _chrome.Send(
				"CSS.getStyleSheetText",
				new {
					styleSheetId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetStyleSheetTextResult>();
		}

		/// <summary>
		/// Find a rule with the given active property for the given node and set the new value for this property
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">The element id for which to set property.</param>
		/// <param name="propertyName"></param>
		/// <param name="value"></param>
		public void SetEffectivePropertyValueForNode(int nodeId, string propertyName, string value) {
			_chrome.Send(
				"CSS.setEffectivePropertyValueForNode",
				new {
					nodeId,
					propertyName,
					value
				}
			);
		}

		/// <summary>
		/// Modifies the keyframe rule key text.
		/// </summary>
		[JsonObject]
		public class SetKeyframeKeyResult {
			/// <summary>
			/// The resulting key text after modification.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Value</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("keyText")]
			public Value KeyText;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Modifies the keyframe rule key text.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="styleSheetId"></param>
		/// <param name="range"></param>
		/// <param name="keyText"></param>
		public SetKeyframeKeyResult SetKeyframeKey(string styleSheetId, SourceRange range, string keyText) {
			string s = _chrome.Send(
				"CSS.setKeyframeKey",
				new {
					styleSheetId,
					range,
					keyText
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<SetKeyframeKeyResult>();
		}

		/// <summary>
		/// Modifies the rule selector.
		/// </summary>
		[JsonObject]
		public class SetMediaTextResult {
			/// <summary>
			/// The resulting CSS media rule after modification.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: CSSMedia</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("media")]
			public CSSMedia Media;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Modifies the rule selector.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="styleSheetId"></param>
		/// <param name="range"></param>
		/// <param name="text"></param>
		public SetMediaTextResult SetMediaText(string styleSheetId, SourceRange range, string text) {
			string s = _chrome.Send(
				"CSS.setMediaText",
				new {
					styleSheetId,
					range,
					text
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<SetMediaTextResult>();
		}

		/// <summary>
		/// Modifies the rule selector.
		/// </summary>
		[JsonObject]
		public class SetRuleSelectorResult {
			/// <summary>
			/// The resulting selector list after modification.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SelectorList</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("selectorList")]
			public SelectorList SelectorList;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Modifies the rule selector.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="styleSheetId"></param>
		/// <param name="range"></param>
		/// <param name="selector"></param>
		public SetRuleSelectorResult SetRuleSelector(string styleSheetId, SourceRange range, string selector) {
			string s = _chrome.Send(
				"CSS.setRuleSelector",
				new {
					styleSheetId,
					range,
					selector
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<SetRuleSelectorResult>();
		}

		/// <summary>
		/// Sets the new stylesheet text.
		/// </summary>
		[JsonObject]
		public class SetStyleSheetTextResult {
			/// <summary>
			/// URL of source map associated with script (if any).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sourceMapURL")]
			public string SourceMapURL;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Sets the new stylesheet text.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="styleSheetId"></param>
		/// <param name="text"></param>
		public SetStyleSheetTextResult SetStyleSheetText(string styleSheetId, string text) {
			string s = _chrome.Send(
				"CSS.setStyleSheetText",
				new {
					styleSheetId,
					text
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<SetStyleSheetTextResult>();
		}

		/// <summary>
		/// Applies specified style edits one after another in the given order.
		/// </summary>
		[JsonObject]
		public class SetStyleTextsResult {
			/// <summary>
			/// The resulting styles after modification.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("styles")]
			public CSSStyle[] Styles;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Applies specified style edits one after another in the given order.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="edits"></param>
		public SetStyleTextsResult SetStyleTexts(StyleDeclarationEdit[] edits) {
			string s = _chrome.Send(
				"CSS.setStyleTexts",
				new {
					edits
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<SetStyleTextsResult>();
		}

		/// <summary>
		/// Enables the selector recording.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void StartRuleUsageTracking() {
			_chrome.Send(
				"CSS.startRuleUsageTracking"
			);
		}

		/// <summary>
		/// Stop tracking rule usage and return the list of rules that were used since last call to `takeCoverageDelta` (or since start of coverage instrumentation)
		/// </summary>
		[JsonObject]
		public class StopRuleUsageTrackingResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("ruleUsage")]
			public RuleUsage[] RuleUsage;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Stop tracking rule usage and return the list of rules that were used since last call to `takeCoverageDelta` (or since start of coverage instrumentation)
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public StopRuleUsageTrackingResult StopRuleUsageTracking() {
			string s = _chrome.Send(
				"CSS.stopRuleUsageTracking"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<StopRuleUsageTrackingResult>();
		}

		/// <summary>
		/// Obtain list of rules that became used since last call to this method (or since start of coverage instrumentation)
		/// </summary>
		[JsonObject]
		public class TakeCoverageDeltaResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("coverage")]
			public RuleUsage[] Coverage;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Obtain list of rules that became used since last call to this method (or since start of coverage instrumentation)
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public TakeCoverageDeltaResult TakeCoverageDelta() {
			string s = _chrome.Send(
				"CSS.takeCoverageDelta"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<TakeCoverageDeltaResult>();
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("CSS.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "fontsUpdated":
					if (FontsUpdated == null) {
						return;
					}
					FontsUpdated(
						_chrome,
						JsonConvert.DeserializeObject
							<FontsUpdatedEventArgs>(@params.ToString())
					);
					break;
				case "mediaQueryResultChanged":
					if (MediaQueryResultChanged == null) {
						return;
					}
					MediaQueryResultChanged(_chrome, EventArgs.Empty);
					break;
				case "styleSheetAdded":
					if (StyleSheetAdded == null) {
						return;
					}
					StyleSheetAdded(
						_chrome,
						JsonConvert.DeserializeObject
							<StyleSheetAddedEventArgs>(@params.ToString())
					);
					break;
				case "styleSheetChanged":
					if (StyleSheetChanged == null) {
						return;
					}
					StyleSheetChanged(
						_chrome,
						JsonConvert.DeserializeObject
							<StyleSheetChangedEventArgs>(@params.ToString())
					);
					break;
				case "styleSheetRemoved":
					if (StyleSheetRemoved == null) {
						return;
					}
					StyleSheetRemoved(
						_chrome,
						JsonConvert.DeserializeObject
							<StyleSheetRemovedEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
