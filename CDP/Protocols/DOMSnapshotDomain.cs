// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// This domain facilitates obtaining document snapshots with DOM, layout, and style information.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: CSS, DOM, DOMDebugger, Page</description></item>
	/// </list>
	/// </summary>
	public class DOMSnapshotDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the DOMSnapshotDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public DOMSnapshotDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// A Node in the DOM tree.
		/// </summary>
		[JsonObject]
		public class DOMNode {
			/// <summary>
			/// `Node`'s nodeType.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeType")]
			public int NodeType;

			/// <summary>
			/// `Node`'s nodeName.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeName")]
			public string NodeName;

			/// <summary>
			/// `Node`'s nodeValue.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeValue")]
			public string NodeValue;

			/// <summary>
			/// Only set for textarea elements, contains the text value.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("textValue")]
			public string TextValue;

			/// <summary>
			/// Only set for input elements, contains the input's associated text value.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("inputValue")]
			public string InputValue;

			/// <summary>
			/// Only set for radio and checkbox input elements, indicates if the element has been checked
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("inputChecked")]
			public bool InputChecked;

			/// <summary>
			/// Only set for option elements, indicates if the element has been selected
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("optionSelected")]
			public bool OptionSelected;

			/// <summary>
			/// `Node`'s id, corresponds to DOM.Node.backendNodeId.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: DOM.BackendNodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("backendNodeId")]
			public int BackendNodeId;

			/// <summary>
			/// The indexes of the node's child nodes in the `domNodes` array returned by `getSnapshot`, if any.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("childNodeIndexes")]
			public int[] ChildNodeIndexes;

			/// <summary>
			/// Attributes of an `Element` node.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("attributes")]
			public NameValue[] Attributes;

			/// <summary>
			/// Indexes of pseudo elements associated with this node in the `domNodes` array returned by `getSnapshot`, if any.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("pseudoElementIndexes")]
			public int[] PseudoElementIndexes;

			/// <summary>
			/// The index of the node's related layout tree node in the `layoutTreeNodes` array returned by `getSnapshot`, if any.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("layoutNodeIndex")]
			public int LayoutNodeIndex;

			/// <summary>
			/// Document URL that `Document` or `FrameOwner` node points to.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("documentURL")]
			public string DocumentURL;

			/// <summary>
			/// Base URL that `Document` or `FrameOwner` node uses for URL completion.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("baseURL")]
			public string BaseURL;

			/// <summary>
			/// Only set for documents, contains the document's content language.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contentLanguage")]
			public string ContentLanguage;

			/// <summary>
			/// Only set for documents, contains the document's character set encoding.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("documentEncoding")]
			public string DocumentEncoding;

			/// <summary>
			/// `DocumentType` node's publicId.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("publicId")]
			public string PublicId;

			/// <summary>
			/// `DocumentType` node's systemId.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("systemId")]
			public string SystemId;

			/// <summary>
			/// Frame ID for frame owner elements and also for the document node.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Page.FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// The index of a frame owner element's content document in the `domNodes` array returned by `getSnapshot`, if any.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contentDocumentIndex")]
			public int ContentDocumentIndex;

			/// <summary>
			/// Type of a pseudo element node.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: DOM.PseudoType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("pseudoType")]
			public string PseudoType;

			/// <summary>
			/// Shadow root type.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: DOM.ShadowRootType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("shadowRootType")]
			public string ShadowRootType;

			/// <summary>
			/// Whether this DOM node responds to mouse clicks. This includes nodes that have had click event listeners attached via JavaScript as well as anchor tags that naturally navigate when clicked.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("isClickable")]
			public bool IsClickable;

			/// <summary>
			/// Details of the node's event listeners, if any.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("eventListeners")]
			public DOMDebuggerDomain.EventListener[] EventListeners;

			/// <summary>
			/// The selected url for nodes with a srcset attribute.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("currentSourceURL")]
			public string CurrentSourceURL;

			/// <summary>
			/// The url of the script (if any) that generates this node.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("originURL")]
			public string OriginURL;

			/// <summary>
			/// Scroll offsets, set when this node is a Document.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scrollOffsetX")]
			public double ScrollOffsetX;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scrollOffsetY")]
			public double ScrollOffsetY;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Details of post layout rendered text positions. The exact layout should not be regarded as stable and may change between versions.
		/// </summary>
		[JsonObject]
		public class InlineTextBox {
			/// <summary>
			/// The bounding box in document coordinates. Note that scroll offset of the document is ignored.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: DOM.Rect</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("boundingBox")]
			public DOMDomain.Rect BoundingBox;

			/// <summary>
			/// The starting index in characters, for this post layout textbox substring. Characters that would be represented as a surrogate pair in UTF-16 have length 2.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("startCharacterIndex")]
			public int StartCharacterIndex;

			/// <summary>
			/// The number of characters in this post layout textbox substring. Characters that would be represented as a surrogate pair in UTF-16 have length 2.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("numCharacters")]
			public int NumCharacters;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Details of an element in the DOM tree with a LayoutObject.
		/// </summary>
		[JsonObject]
		public class LayoutTreeNode {
			/// <summary>
			/// The index of the related DOM node in the `domNodes` array returned by `getSnapshot`.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("domNodeIndex")]
			public int DomNodeIndex;

			/// <summary>
			/// The bounding box in document coordinates. Note that scroll offset of the document is ignored.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: DOM.Rect</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("boundingBox")]
			public DOMDomain.Rect BoundingBox;

			/// <summary>
			/// Contents of the LayoutText, if any.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("layoutText")]
			public string LayoutText;

			/// <summary>
			/// The post-layout inline text nodes, if any.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("inlineTextNodes")]
			public InlineTextBox[] InlineTextNodes;

			/// <summary>
			/// Index into the `computedStyles` array returned by `getSnapshot`.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("styleIndex")]
			public int StyleIndex;

			/// <summary>
			/// Global paint order index, which is determined by the stacking order of the nodes. Nodes that are painted together will have the same index. Only provided if includePaintOrder in getSnapshot was true.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("paintOrder")]
			public int PaintOrder;

			/// <summary>
			/// Set to true to indicate the element begins a new stacking context.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("isStackingContext")]
			public bool IsStackingContext;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// A subset of the full ComputedStyle as defined by the request whitelist.
		/// </summary>
		[JsonObject]
		public class ComputedStyle {
			/// <summary>
			/// Name/value pairs of computed style properties.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("properties")]
			public NameValue[] Properties;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// A name/value pair.
		/// </summary>
		[JsonObject]
		public class NameValue {
			/// <summary>
			/// Attribute/property name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// Attribute/property value.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public string Value;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Data that is only present on rare nodes.
		/// </summary>
		[JsonObject]
		public class RareStringData {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("index")]
			public int[] Index;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public int[] Value;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class RareBooleanData {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("index")]
			public int[] Index;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class RareIntegerData {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("index")]
			public int[] Index;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public int[] Value;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Document snapshot.
		/// </summary>
		[JsonObject]
		public class DocumentSnapshot {
			/// <summary>
			/// Document URL that `Document` or `FrameOwner` node points to.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StringIndex</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("documentURL")]
			public int DocumentURL;

			/// <summary>
			/// Document title.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StringIndex</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("title")]
			public int Title;

			/// <summary>
			/// Base URL that `Document` or `FrameOwner` node uses for URL completion.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StringIndex</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("baseURL")]
			public int BaseURL;

			/// <summary>
			/// Contains the document's content language.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StringIndex</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contentLanguage")]
			public int ContentLanguage;

			/// <summary>
			/// Contains the document's character set encoding.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StringIndex</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("encodingName")]
			public int EncodingName;

			/// <summary>
			/// `DocumentType` node's publicId.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StringIndex</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("publicId")]
			public int PublicId;

			/// <summary>
			/// `DocumentType` node's systemId.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StringIndex</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("systemId")]
			public int SystemId;

			/// <summary>
			/// Frame ID for frame owner elements and also for the document node.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StringIndex</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public int FrameId;

			/// <summary>
			/// A table with dom nodes.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeTreeSnapshot</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodes")]
			public NodeTreeSnapshot Nodes;

			/// <summary>
			/// The nodes in the layout tree.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: LayoutTreeSnapshot</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("layout")]
			public LayoutTreeSnapshot Layout;

			/// <summary>
			/// The post-layout inline text nodes.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: TextBoxSnapshot</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("textBoxes")]
			public TextBoxSnapshot TextBoxes;

			/// <summary>
			/// Horizontal scroll offset.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scrollOffsetX")]
			public double ScrollOffsetX;

			/// <summary>
			/// Vertical scroll offset.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scrollOffsetY")]
			public double ScrollOffsetY;

			/// <summary>
			/// Document content width.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contentWidth")]
			public double ContentWidth;

			/// <summary>
			/// Document content height.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contentHeight")]
			public double ContentHeight;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Table containing nodes.
		/// </summary>
		[JsonObject]
		public class NodeTreeSnapshot {
			/// <summary>
			/// Parent node index.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("parentIndex")]
			public int[] ParentIndex;

			/// <summary>
			/// `Node`'s nodeType.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeType")]
			public int[] NodeType;

			/// <summary>
			/// `Node`'s nodeName.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeName")]
			public int[] NodeName;

			/// <summary>
			/// `Node`'s nodeValue.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeValue")]
			public int[] NodeValue;

			/// <summary>
			/// `Node`'s id, corresponds to DOM.Node.backendNodeId.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("backendNodeId")]
			public int[] BackendNodeId;

			/// <summary>
			/// Attributes of an `Element` node. Flatten name, value pairs.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("attributes")]
			public int[][] Attributes;

			/// <summary>
			/// Only set for textarea elements, contains the text value.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: RareStringData</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("textValue")]
			public RareStringData TextValue;

			/// <summary>
			/// Only set for input elements, contains the input's associated text value.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: RareStringData</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("inputValue")]
			public RareStringData InputValue;

			/// <summary>
			/// Only set for radio and checkbox input elements, indicates if the element has been checked
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: RareBooleanData</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("inputChecked")]
			public RareBooleanData InputChecked;

			/// <summary>
			/// Only set for option elements, indicates if the element has been selected
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: RareBooleanData</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("optionSelected")]
			public RareBooleanData OptionSelected;

			/// <summary>
			/// The index of the document in the list of the snapshot documents.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: RareIntegerData</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contentDocumentIndex")]
			public RareIntegerData ContentDocumentIndex;

			/// <summary>
			/// Type of a pseudo element node.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: RareStringData</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("pseudoType")]
			public RareStringData PseudoType;

			/// <summary>
			/// Whether this DOM node responds to mouse clicks. This includes nodes that have had click event listeners attached via JavaScript as well as anchor tags that naturally navigate when clicked.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: RareBooleanData</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("isClickable")]
			public RareBooleanData IsClickable;

			/// <summary>
			/// The selected url for nodes with a srcset attribute.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: RareStringData</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("currentSourceURL")]
			public RareStringData CurrentSourceURL;

			/// <summary>
			/// The url of the script (if any) that generates this node.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: RareStringData</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("originURL")]
			public RareStringData OriginURL;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Table of details of an element in the DOM tree with a LayoutObject.
		/// </summary>
		[JsonObject]
		public class LayoutTreeSnapshot {
			/// <summary>
			/// Index of the corresponding node in the `NodeTreeSnapshot` array returned by `captureSnapshot`.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeIndex")]
			public int[] NodeIndex;

			/// <summary>
			/// Array of indexes specifying computed style strings, filtered according to the `computedStyles` parameter passed to `captureSnapshot`.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("styles")]
			public int[][] Styles;

			/// <summary>
			/// The absolute position bounding box.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("bounds")]
			public double[][] Bounds;

			/// <summary>
			/// Contents of the LayoutText, if any.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("text")]
			public int[] Text;

			/// <summary>
			/// Stacking context information.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RareBooleanData</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("stackingContexts")]
			public RareBooleanData StackingContexts;

			/// <summary>
			/// Global paint order index, which is determined by the stacking order of the nodes. Nodes that are painted together will have the same index. Only provided if includePaintOrder in captureSnapshot was true.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("paintOrders")]
			public int[] PaintOrders;

			/// <summary>
			/// The offset rect of nodes. Only available when includeDOMRects is set to true
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("offsetRects")]
			public double[][] OffsetRects;

			/// <summary>
			/// The scroll rect of nodes. Only available when includeDOMRects is set to true
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scrollRects")]
			public double[][] ScrollRects;

			/// <summary>
			/// The client rect of nodes. Only available when includeDOMRects is set to true
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("clientRects")]
			public double[][] ClientRects;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Table of details of the post layout rendered text positions. The exact layout should not be regarded as stable and may change between versions.
		/// </summary>
		[JsonObject]
		public class TextBoxSnapshot {
			/// <summary>
			/// Index of the layout tree node that owns this box collection.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("layoutIndex")]
			public int[] LayoutIndex;

			/// <summary>
			/// The absolute position bounding box.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("bounds")]
			public double[][] Bounds;

			/// <summary>
			/// The starting index in characters, for this post layout textbox substring. Characters that would be represented as a surrogate pair in UTF-16 have length 2.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("start")]
			public int[] Start;

			/// <summary>
			/// The number of characters in this post layout textbox substring. Characters that would be represented as a surrogate pair in UTF-16 have length 2.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("length")]
			public int[] Length;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Disables DOM snapshot agent for the given page.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"DOMSnapshot.disable"
			);
		}

		/// <summary>
		/// Enables DOM snapshot agent for the given page.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"DOMSnapshot.enable"
			);
		}

		/// <summary>
		/// Returns a document snapshot, including the full DOM tree of the root node (including iframes, template contents, and imported documents) in a flattened array, as well as layout and white-listed computed style information for the nodes. Shadow DOM in the returned DOM tree is flattened.
		/// </summary>
		[JsonObject]
		public class GetSnapshotResult {
			/// <summary>
			/// The nodes in the DOM tree. The DOMNode at index 0 corresponds to the root document.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("domNodes")]
			public DOMNode[] DomNodes;

			/// <summary>
			/// The nodes in the layout tree.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("layoutTreeNodes")]
			public LayoutTreeNode[] LayoutTreeNodes;

			/// <summary>
			/// Whitelisted ComputedStyle properties for each node in the layout tree.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("computedStyles")]
			public ComputedStyle[] ComputedStyles;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns a document snapshot, including the full DOM tree of the root node (including iframes, template contents, and imported documents) in a flattened array, as well as layout and white-listed computed style information for the nodes. Shadow DOM in the returned DOM tree is flattened.
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="computedStyleWhitelist">Whitelist of computed styles to return.</param>
		/// <param name="includeEventListeners">Whether or not to retrieve details of DOM listeners (default false).</param>
		/// <param name="includePaintOrder">Whether to determine and include the paint order index of LayoutTreeNodes (default false).</param>
		/// <param name="includeUserAgentShadowTree">Whether to include UA shadow tree in the snapshot (default false).</param>
		[Obsolete]
		public GetSnapshotResult GetSnapshot(string[] computedStyleWhitelist, bool? includeEventListeners = null, bool? includePaintOrder = null, bool? includeUserAgentShadowTree = null) {
			string s = _chrome.Send(
				"DOMSnapshot.getSnapshot",
				new {
					computedStyleWhitelist,
					includeEventListeners,
					includePaintOrder,
					includeUserAgentShadowTree
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetSnapshotResult>();
		}

		/// <summary>
		/// Returns a document snapshot, including the full DOM tree of the root node (including iframes, template contents, and imported documents) in a flattened array, as well as layout and white-listed computed style information for the nodes. Shadow DOM in the returned DOM tree is flattened.
		/// </summary>
		[JsonObject]
		public class CaptureSnapshotResult {
			/// <summary>
			/// The nodes in the DOM tree. The DOMNode at index 0 corresponds to the root document.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("documents")]
			public DocumentSnapshot[] Documents;

			/// <summary>
			/// Shared string table that all string properties refer to with indexes.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("strings")]
			public string[] Strings;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns a document snapshot, including the full DOM tree of the root node (including iframes, template contents, and imported documents) in a flattened array, as well as layout and white-listed computed style information for the nodes. Shadow DOM in the returned DOM tree is flattened.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="computedStyles">Whitelist of computed styles to return.</param>
		/// <param name="includePaintOrder">Whether to include layout object paint orders into the snapshot.</param>
		/// <param name="includeDOMRects">Whether to include DOM rectangles (offsetRects, clientRects, scrollRects) into the snapshot</param>
		public CaptureSnapshotResult CaptureSnapshot(string[] computedStyles, bool? includePaintOrder = null, bool? includeDOMRects = null) {
			string s = _chrome.Send(
				"DOMSnapshot.captureSnapshot",
				new {
					computedStyles,
					includePaintOrder,
					includeDOMRects
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<CaptureSnapshotResult>();
		}



	}
}
