// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// A domain for letting clients substitute browser's network layer with client code.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: Network, IO, Page</description></item>
	/// </list>
	/// </summary>
	public class FetchDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the FetchDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public FetchDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Issued when the domain is enabled and the request URL matches the specified filter. The request is paused until the client responds with one of continueRequest, failRequest or fulfillRequest. The stage of the request can be determined by presence of responseErrorReason and responseStatusCode -- the request is at the response stage if either of these fields is present and in the request stage otherwise.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class RequestPausedEventArgs : EventArgs {
			/// <summary>
			/// Each request the page makes will have a unique id.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestId")]
			public string RequestId;

			/// <summary>
			/// The details of the request.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Network.Request</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("request")]
			public NetworkDomain.Request Request;

			/// <summary>
			/// The id of the frame that initiated the request.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Page.FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// How the requested resource will be used.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Network.ResourceType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("resourceType")]
			public string ResourceType;

			/// <summary>
			/// Response error if intercepted at response stage.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Network.ErrorReason</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("responseErrorReason")]
			public string ResponseErrorReason;

			/// <summary>
			/// Response code if intercepted at response stage.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("responseStatusCode")]
			public int ResponseStatusCode;

			/// <summary>
			/// Response headers if intercepted at the response stage.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("responseHeaders")]
			public HeaderEntry[] ResponseHeaders;

			/// <summary>
			/// If the intercepted request had a corresponding Network.requestWillBeSent event fired for it, then this networkId will be the same as the requestId present in the requestWillBeSent event.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("networkId")]
			public string NetworkId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Issued when the domain is enabled and the request URL matches the specified filter. The request is paused until the client responds with one of continueRequest, failRequest or fulfillRequest. The stage of the request can be determined by presence of responseErrorReason and responseStatusCode -- the request is at the response stage if either of these fields is present and in the request stage otherwise.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<RequestPausedEventArgs> RequestPaused;

		/// <summary>
		/// Issued when the domain is enabled with handleAuthRequests set to true. The request is paused until client responds with continueWithAuth.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class AuthRequiredEventArgs : EventArgs {
			/// <summary>
			/// Each request the page makes will have a unique id.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestId")]
			public string RequestId;

			/// <summary>
			/// The details of the request.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Network.Request</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("request")]
			public NetworkDomain.Request Request;

			/// <summary>
			/// The id of the frame that initiated the request.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Page.FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// How the requested resource will be used.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Network.ResourceType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("resourceType")]
			public string ResourceType;

			/// <summary>
			/// Details of the Authorization Challenge encountered. If this is set, client should respond with continueRequest that contains AuthChallengeResponse.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: AuthChallenge</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("authChallenge")]
			public AuthChallenge AuthChallenge;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Issued when the domain is enabled with handleAuthRequests set to true. The request is paused until client responds with continueWithAuth.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<AuthRequiredEventArgs> AuthRequired;

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class RequestPattern {
			/// <summary>
			/// Wildcards ('*' -&gt; zero or more, '?' -&gt; exactly one) are allowed. Escape character is backslash. Omitting is equivalent to "*".
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("urlPattern")]
			public string UrlPattern;

			/// <summary>
			/// If set, only requests for matching resource types will be intercepted.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Network.ResourceType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("resourceType")]
			public string ResourceType;

			/// <summary>
			/// Stage at wich to begin intercepting requests. Default is Request.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: RequestStage</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestStage")]
			public string RequestStage;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Response HTTP header entry
		/// </summary>
		[JsonObject]
		public class HeaderEntry {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public string Value;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Authorization challenge for HTTP status code 401 or 407.
		/// </summary>
		[JsonObject]
		public class AuthChallenge {
			/// <summary>
			/// Source of the authentication challenge.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: Server, Proxy
			/// </summary>
			[JsonProperty("source")]
			public string Source;

			/// <summary>
			/// Origin of the challenger.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("origin")]
			public string Origin;

			/// <summary>
			/// The authentication scheme used, such as basic or digest
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scheme")]
			public string Scheme;

			/// <summary>
			/// The realm of the challenge. May be empty.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("realm")]
			public string Realm;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Response to an AuthChallenge.
		/// </summary>
		[JsonObject]
		public class AuthChallengeResponse {
			/// <summary>
			/// The decision on what to do in response to the authorization challenge.  Default means deferring to the default behavior of the net stack, which will likely either the Cancel authentication or display a popup dialog box.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: Default, CancelAuth, ProvideCredentials
			/// </summary>
			[JsonProperty("response")]
			public string Response;

			/// <summary>
			/// The username to provide, possibly empty. Should only be set if response is ProvideCredentials.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("username")]
			public string Username;

			/// <summary>
			/// The password to provide, possibly empty. Should only be set if response is ProvideCredentials.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("password")]
			public string Password;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Disables the fetch domain.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"Fetch.disable"
			);
		}

		/// <summary>
		/// Enables issuing of requestPaused events. A request will be paused until client calls one of failRequest, fulfillRequest or continueRequest/continueWithAuth.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="patterns">If specified, only requests matching any of these patterns will produce fetchRequested event and will be paused until clients response. If not set, all requests will be affected.</param>
		/// <param name="handleAuthRequests">If true, authRequired events will be issued and requests will be paused expecting a call to continueWithAuth.</param>
		public void Enable(RequestPattern[] patterns = null, bool? handleAuthRequests = null) {
			_chrome.Send(
				"Fetch.enable",
				new {
					patterns,
					handleAuthRequests
				}
			);
		}

		/// <summary>
		/// Causes the request to fail with specified reason.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="requestId">An id the client received in requestPaused event.</param>
		/// <param name="errorReason">Causes the request to fail with the given reason.</param>
		public void FailRequest(string requestId, string errorReason) {
			_chrome.Send(
				"Fetch.failRequest",
				new {
					requestId,
					errorReason
				}
			);
		}

		/// <summary>
		/// Provides response to the request.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="requestId">An id the client received in requestPaused event.</param>
		/// <param name="responseCode">An HTTP response code.</param>
		/// <param name="responseHeaders">Response headers.</param>
		/// <param name="binaryResponseHeaders">Alternative way of specifying response headers as a \0-separated series of name: value pairs. Prefer the above method unless you need to represent some non-UTF8 values that can't be transmitted over the protocol as text.</param>
		/// <param name="body">A response body.</param>
		/// <param name="responsePhrase">A textual representation of responseCode. If absent, a standard phrase matching responseCode is used.</param>
		public void FulfillRequest(string requestId, int responseCode, HeaderEntry[] responseHeaders = null, byte[] binaryResponseHeaders = null, byte[] body = null, string responsePhrase = null) {
			_chrome.Send(
				"Fetch.fulfillRequest",
				new {
					requestId,
					responseCode,
					responseHeaders,
					binaryResponseHeaders,
					body,
					responsePhrase
				}
			);
		}

		/// <summary>
		/// Continues the request, optionally modifying some of its parameters.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="requestId">An id the client received in requestPaused event.</param>
		/// <param name="url">If set, the request url will be modified in a way that's not observable by page.</param>
		/// <param name="method">If set, the request method is overridden.</param>
		/// <param name="postData">If set, overrides the post data in the request.</param>
		/// <param name="headers">If set, overrides the request headrts.</param>
		public void ContinueRequest(string requestId, string url = null, string method = null, string postData = null, HeaderEntry[] headers = null) {
			_chrome.Send(
				"Fetch.continueRequest",
				new {
					requestId,
					url,
					method,
					postData,
					headers
				}
			);
		}

		/// <summary>
		/// Continues a request supplying authChallengeResponse following authRequired event.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="requestId">An id the client received in authRequired event.</param>
		/// <param name="authChallengeResponse">Response to  with an authChallenge.</param>
		public void ContinueWithAuth(string requestId, AuthChallengeResponse authChallengeResponse) {
			_chrome.Send(
				"Fetch.continueWithAuth",
				new {
					requestId,
					authChallengeResponse
				}
			);
		}

		/// <summary>
		/// Causes the body of the response to be received from the server and returned as a single string. May only be issued for a request that is paused in the Response stage and is mutually exclusive with takeResponseBodyForInterceptionAsStream. Calling other methods that affect the request or disabling fetch domain before body is received results in an undefined behavior.
		/// </summary>
		[JsonObject]
		public class GetResponseBodyResult {
			/// <summary>
			/// Response body.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("body")]
			public string Body;

			/// <summary>
			/// True, if content was sent as base64.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("base64Encoded")]
			public bool Base64Encoded;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Causes the body of the response to be received from the server and returned as a single string. May only be issued for a request that is paused in the Response stage and is mutually exclusive with takeResponseBodyForInterceptionAsStream. Calling other methods that affect the request or disabling fetch domain before body is received results in an undefined behavior.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="requestId">Identifier for the intercepted request to get body for.</param>
		public GetResponseBodyResult GetResponseBody(string requestId) {
			string s = _chrome.Send(
				"Fetch.getResponseBody",
				new {
					requestId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetResponseBodyResult>();
		}

		/// <summary>
		/// Returns a handle to the stream representing the response body. The request must be paused in the HeadersReceived stage. Note that after this command the request can't be continued as is -- client either needs to cancel it or to provide the response body. The stream only supports sequential read, IO.read will fail if the position is specified. This method is mutually exclusive with getResponseBody. Calling other methods that affect the request or disabling fetch domain before body is received results in an undefined behavior.
		/// </summary>
		[JsonObject]
		public class TakeResponseBodyAsStreamResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: IO.StreamHandle</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("stream")]
			public string Stream;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns a handle to the stream representing the response body. The request must be paused in the HeadersReceived stage. Note that after this command the request can't be continued as is -- client either needs to cancel it or to provide the response body. The stream only supports sequential read, IO.read will fail if the position is specified. This method is mutually exclusive with getResponseBody. Calling other methods that affect the request or disabling fetch domain before body is received results in an undefined behavior.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="requestId"></param>
		public TakeResponseBodyAsStreamResult TakeResponseBodyAsStream(string requestId) {
			string s = _chrome.Send(
				"Fetch.takeResponseBodyAsStream",
				new {
					requestId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<TakeResponseBodyAsStreamResult>();
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("Fetch.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "requestPaused":
					if (RequestPaused == null) {
						return;
					}
					RequestPaused(
						_chrome,
						JsonConvert.DeserializeObject
							<RequestPausedEventArgs>(@params.ToString())
					);
					break;
				case "authRequired":
					if (AuthRequired == null) {
						return;
					}
					AuthRequired(
						_chrome,
						JsonConvert.DeserializeObject
							<AuthRequiredEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
