// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// The Browser domain defines methods and events for browser managing.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: false</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class BrowserDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the BrowserDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public BrowserDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Browser window bounds information
		/// </summary>
		[JsonObject]
		public class Bounds {
			/// <summary>
			/// The offset from the left edge of the screen to the window in pixels.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("left")]
			public int Left;

			/// <summary>
			/// The offset from the top edge of the screen to the window in pixels.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("top")]
			public int Top;

			/// <summary>
			/// The window width in pixels.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("width")]
			public int Width;

			/// <summary>
			/// The window height in pixels.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("height")]
			public int Height;

			/// <summary>
			/// The window state. Default to normal.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: WindowState</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("windowState")]
			public string WindowState;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Definition of PermissionDescriptor defined in the Permissions API: https://w3c.github.io/permissions/#dictdef-permissiondescriptor.
		/// </summary>
		[JsonObject]
		public class PermissionDescriptor {
			/// <summary>
			/// Name of permission. See https://cs.chromium.org/chromium/src/third_party/blink/renderer/modules/permissions/permission_descriptor.idl for valid permission names.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// For "midi" permission, may also specify sysex control.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sysex")]
			public bool Sysex;

			/// <summary>
			/// For "push" permission, may specify userVisibleOnly. Note that userVisibleOnly = true is the only currently supported type.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("userVisibleOnly")]
			public bool UserVisibleOnly;

			/// <summary>
			/// For "wake-lock" permission, must specify type as either "screen" or "system".
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Chrome histogram bucket.
		/// </summary>
		[JsonObject]
		public class Bucket {
			/// <summary>
			/// Minimum value (inclusive).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("low")]
			public int Low;

			/// <summary>
			/// Maximum value (exclusive).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("high")]
			public int High;

			/// <summary>
			/// Number of samples.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("count")]
			public int Count;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Chrome histogram.
		/// </summary>
		[JsonObject]
		public class Histogram {
			/// <summary>
			/// Name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// Sum of sample values.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sum")]
			public int Sum;

			/// <summary>
			/// Total number of samples.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("count")]
			public int Count;

			/// <summary>
			/// Buckets.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("buckets")]
			public Bucket[] Buckets;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Set permission settings for given origin.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="origin">Origin the permission applies to.</param>
		/// <param name="permission">Descriptor of permission to override.</param>
		/// <param name="setting">Setting of the permission.</param>
		/// <param name="browserContextId">Context to override. When omitted, default browser context is used.</param>
		public void SetPermission(string origin, PermissionDescriptor permission, string setting, string browserContextId = null) {
			_chrome.Send(
				"Browser.setPermission",
				new {
					origin,
					permission,
					setting,
					browserContextId
				}
			);
		}

		/// <summary>
		/// Grant specific permissions to the given origin and reject all others.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="origin"></param>
		/// <param name="permissions"></param>
		/// <param name="browserContextId">BrowserContext to override permissions. When omitted, default browser context is used.</param>
		public void GrantPermissions(string origin, string[] permissions, string browserContextId = null) {
			_chrome.Send(
				"Browser.grantPermissions",
				new {
					origin,
					permissions,
					browserContextId
				}
			);
		}

		/// <summary>
		/// Reset all permission management for all origins.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="browserContextId">BrowserContext to reset permissions. When omitted, default browser context is used.</param>
		public void ResetPermissions(string browserContextId = null) {
			_chrome.Send(
				"Browser.resetPermissions",
				new {
					browserContextId
				}
			);
		}

		/// <summary>
		/// Close browser gracefully.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Close() {
			_chrome.Send(
				"Browser.close"
			);
		}

		/// <summary>
		/// Crashes browser on the main thread.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public void Crash() {
			_chrome.Send(
				"Browser.crash"
			);
		}

		/// <summary>
		/// Crashes GPU process.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public void CrashGpuProcess() {
			_chrome.Send(
				"Browser.crashGpuProcess"
			);
		}

		/// <summary>
		/// Returns version information.
		/// </summary>
		[JsonObject]
		public class GetVersionResult {
			/// <summary>
			/// Protocol version.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("protocolVersion")]
			public string ProtocolVersion;

			/// <summary>
			/// Product name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("product")]
			public string Product;

			/// <summary>
			/// Product revision.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("revision")]
			public string Revision;

			/// <summary>
			/// User-Agent.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("userAgent")]
			public string UserAgent;

			/// <summary>
			/// V8 version.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("jsVersion")]
			public string JsVersion;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns version information.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public GetVersionResult GetVersion() {
			string s = _chrome.Send(
				"Browser.getVersion"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetVersionResult>();
		}

		/// <summary>
		/// Returns the command line switches for the browser process if, and only if --enable-automation is on the commandline.
		/// </summary>
		[JsonObject]
		public class GetBrowserCommandLineResult {
			/// <summary>
			/// Commandline parameters
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("arguments")]
			public string[] Arguments;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns the command line switches for the browser process if, and only if --enable-automation is on the commandline.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public GetBrowserCommandLineResult GetBrowserCommandLine() {
			string s = _chrome.Send(
				"Browser.getBrowserCommandLine"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetBrowserCommandLineResult>();
		}

		/// <summary>
		/// Get Chrome histograms.
		/// </summary>
		[JsonObject]
		public class GetHistogramsResult {
			/// <summary>
			/// Histograms.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("histograms")]
			public Histogram[] Histograms;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Get Chrome histograms.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="query">Requested substring in name. Only histograms which have query as a substring in their name are extracted. An empty or absent query returns all histograms.</param>
		/// <param name="delta">If true, retrieve delta since last call.</param>
		public GetHistogramsResult GetHistograms(string query = null, bool? delta = null) {
			string s = _chrome.Send(
				"Browser.getHistograms",
				new {
					query,
					delta
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetHistogramsResult>();
		}

		/// <summary>
		/// Get a Chrome histogram by name.
		/// </summary>
		[JsonObject]
		public class GetHistogramResult {
			/// <summary>
			/// Histogram.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Histogram</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("histogram")]
			public Histogram Histogram;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Get a Chrome histogram by name.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="name">Requested histogram name.</param>
		/// <param name="delta">If true, retrieve delta since last call.</param>
		public GetHistogramResult GetHistogram(string name, bool? delta = null) {
			string s = _chrome.Send(
				"Browser.getHistogram",
				new {
					name,
					delta
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetHistogramResult>();
		}

		/// <summary>
		/// Get position and size of the browser window.
		/// </summary>
		[JsonObject]
		public class GetWindowBoundsResult {
			/// <summary>
			/// Bounds information of the window. When window state is 'minimized', the restored window position and size are returned.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Bounds</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("bounds")]
			public Bounds Bounds;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Get position and size of the browser window.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="windowId">Browser window id.</param>
		public GetWindowBoundsResult GetWindowBounds(int windowId) {
			string s = _chrome.Send(
				"Browser.getWindowBounds",
				new {
					windowId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetWindowBoundsResult>();
		}

		/// <summary>
		/// Get the browser window that contains the devtools target.
		/// </summary>
		[JsonObject]
		public class GetWindowForTargetResult {
			/// <summary>
			/// Browser window id.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: WindowID</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("windowId")]
			public int WindowId;

			/// <summary>
			/// Bounds information of the window. When window state is 'minimized', the restored window position and size are returned.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Bounds</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("bounds")]
			public Bounds Bounds;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Get the browser window that contains the devtools target.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="targetId">Devtools agent host id. If called as a part of the session, associated targetId is used.</param>
		public GetWindowForTargetResult GetWindowForTarget(string targetId = null) {
			string s = _chrome.Send(
				"Browser.getWindowForTarget",
				new {
					targetId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetWindowForTargetResult>();
		}

		/// <summary>
		/// Set position and/or size of the browser window.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="windowId">Browser window id.</param>
		/// <param name="bounds">New window bounds. The 'minimized', 'maximized' and 'fullscreen' states cannot be combined with 'left', 'top', 'width' or 'height'. Leaves unspecified fields unchanged.</param>
		public void SetWindowBounds(int windowId, Bounds bounds) {
			_chrome.Send(
				"Browser.setWindowBounds",
				new {
					windowId,
					bounds
				}
			);
		}

		/// <summary>
		/// Set dock tile details, platform-specific.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="badgeLabel"></param>
		/// <param name="image">Png encoded image.</param>
		public void SetDockTile(string badgeLabel = null, byte[] image = null) {
			_chrome.Send(
				"Browser.setDockTile",
				new {
					badgeLabel,
					image
				}
			);
		}



	}
}
