// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// 
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class ServiceWorkerDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the ServiceWorkerDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public ServiceWorkerDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class WorkerErrorReportedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ServiceWorkerErrorMessage</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("errorMessage")]
			public ServiceWorkerErrorMessage ErrorMessage;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<WorkerErrorReportedEventArgs> WorkerErrorReported;

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class WorkerRegistrationUpdatedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("registrations")]
			public ServiceWorkerRegistration[] Registrations;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<WorkerRegistrationUpdatedEventArgs> WorkerRegistrationUpdated;

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class WorkerVersionUpdatedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("versions")]
			public ServiceWorkerVersion[] Versions;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<WorkerVersionUpdatedEventArgs> WorkerVersionUpdated;

		/// <summary>
		/// ServiceWorker registration.
		/// </summary>
		[JsonObject]
		public class ServiceWorkerRegistration {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RegistrationID</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("registrationId")]
			public string RegistrationId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scopeURL")]
			public string ScopeURL;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("isDeleted")]
			public bool IsDeleted;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// ServiceWorker version.
		/// </summary>
		[JsonObject]
		public class ServiceWorkerVersion {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("versionId")]
			public string VersionId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RegistrationID</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("registrationId")]
			public string RegistrationId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scriptURL")]
			public string ScriptURL;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ServiceWorkerVersionRunningStatus</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("runningStatus")]
			public string RunningStatus;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ServiceWorkerVersionStatus</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("status")]
			public string Status;

			/// <summary>
			/// The Last-Modified header value of the main script.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scriptLastModified")]
			public double ScriptLastModified;

			/// <summary>
			/// The time at which the response headers of the main script were received from the server. For cached script it is the last time the cache entry was validated.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scriptResponseTime")]
			public double ScriptResponseTime;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("controlledClients")]
			public string[] ControlledClients;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Target.TargetID</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("targetId")]
			public string TargetId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// ServiceWorker error message.
		/// </summary>
		[JsonObject]
		public class ServiceWorkerErrorMessage {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("errorMessage")]
			public string ErrorMessage;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RegistrationID</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("registrationId")]
			public string RegistrationId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("versionId")]
			public string VersionId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sourceURL")]
			public string SourceURL;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("lineNumber")]
			public int LineNumber;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("columnNumber")]
			public int ColumnNumber;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="origin"></param>
		/// <param name="registrationId"></param>
		/// <param name="data"></param>
		public void DeliverPushMessage(string origin, string registrationId, string data) {
			_chrome.Send(
				"ServiceWorker.deliverPushMessage",
				new {
					origin,
					registrationId,
					data
				}
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"ServiceWorker.disable"
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="origin"></param>
		/// <param name="registrationId"></param>
		/// <param name="tag"></param>
		/// <param name="lastChance"></param>
		public void DispatchSyncEvent(string origin, string registrationId, string tag, bool lastChance) {
			_chrome.Send(
				"ServiceWorker.dispatchSyncEvent",
				new {
					origin,
					registrationId,
					tag,
					lastChance
				}
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="origin"></param>
		/// <param name="registrationId"></param>
		/// <param name="tag"></param>
		public void DispatchPeriodicSyncEvent(string origin, string registrationId, string tag) {
			_chrome.Send(
				"ServiceWorker.dispatchPeriodicSyncEvent",
				new {
					origin,
					registrationId,
					tag
				}
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"ServiceWorker.enable"
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="versionId"></param>
		public void InspectWorker(string versionId) {
			_chrome.Send(
				"ServiceWorker.inspectWorker",
				new {
					versionId
				}
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="forceUpdateOnPageLoad"></param>
		public void SetForceUpdateOnPageLoad(bool forceUpdateOnPageLoad) {
			_chrome.Send(
				"ServiceWorker.setForceUpdateOnPageLoad",
				new {
					forceUpdateOnPageLoad
				}
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="scopeURL"></param>
		public void SkipWaiting(string scopeURL) {
			_chrome.Send(
				"ServiceWorker.skipWaiting",
				new {
					scopeURL
				}
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="scopeURL"></param>
		public void StartWorker(string scopeURL) {
			_chrome.Send(
				"ServiceWorker.startWorker",
				new {
					scopeURL
				}
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void StopAllWorkers() {
			_chrome.Send(
				"ServiceWorker.stopAllWorkers"
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="versionId"></param>
		public void StopWorker(string versionId) {
			_chrome.Send(
				"ServiceWorker.stopWorker",
				new {
					versionId
				}
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="scopeURL"></param>
		public void Unregister(string scopeURL) {
			_chrome.Send(
				"ServiceWorker.unregister",
				new {
					scopeURL
				}
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="scopeURL"></param>
		public void UpdateRegistration(string scopeURL) {
			_chrome.Send(
				"ServiceWorker.updateRegistration",
				new {
					scopeURL
				}
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("ServiceWorker.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "workerErrorReported":
					if (WorkerErrorReported == null) {
						return;
					}
					WorkerErrorReported(
						_chrome,
						JsonConvert.DeserializeObject
							<WorkerErrorReportedEventArgs>(@params.ToString())
					);
					break;
				case "workerRegistrationUpdated":
					if (WorkerRegistrationUpdated == null) {
						return;
					}
					WorkerRegistrationUpdated(
						_chrome,
						JsonConvert.DeserializeObject
							<WorkerRegistrationUpdatedEventArgs>(@params.ToString())
					);
					break;
				case "workerVersionUpdated":
					if (WorkerVersionUpdated == null) {
						return;
					}
					WorkerVersionUpdated(
						_chrome,
						JsonConvert.DeserializeObject
							<WorkerVersionUpdatedEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
