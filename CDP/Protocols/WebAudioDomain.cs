// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// This domain allows inspection of Web Audio API. https://webaudio.github.io/web-audio-api/
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class WebAudioDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the WebAudioDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public WebAudioDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Notifies that a new BaseAudioContext has been created.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ContextCreatedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: BaseAudioContext</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("context")]
			public BaseAudioContext Context;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Notifies that a new BaseAudioContext has been created.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ContextCreatedEventArgs> ContextCreated;

		/// <summary>
		/// Notifies that an existing BaseAudioContext will be destroyed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ContextWillBeDestroyedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contextId")]
			public string ContextId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Notifies that an existing BaseAudioContext will be destroyed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ContextWillBeDestroyedEventArgs> ContextWillBeDestroyed;

		/// <summary>
		/// Notifies that existing BaseAudioContext has changed some properties (id stays the same)..
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ContextChangedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: BaseAudioContext</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("context")]
			public BaseAudioContext Context;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Notifies that existing BaseAudioContext has changed some properties (id stays the same)..
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ContextChangedEventArgs> ContextChanged;

		/// <summary>
		/// Notifies that the construction of an AudioListener has finished.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class AudioListenerCreatedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: AudioListener</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("listener")]
			public AudioListener Listener;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Notifies that the construction of an AudioListener has finished.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<AudioListenerCreatedEventArgs> AudioListenerCreated;

		/// <summary>
		/// Notifies that a new AudioListener has been created.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class AudioListenerWillBeDestroyedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contextId")]
			public string ContextId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("listenerId")]
			public string ListenerId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Notifies that a new AudioListener has been created.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<AudioListenerWillBeDestroyedEventArgs> AudioListenerWillBeDestroyed;

		/// <summary>
		/// Notifies that a new AudioNode has been created.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class AudioNodeCreatedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: AudioNode</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("node")]
			public AudioNode Node;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Notifies that a new AudioNode has been created.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<AudioNodeCreatedEventArgs> AudioNodeCreated;

		/// <summary>
		/// Notifies that an existing AudioNode has been destroyed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class AudioNodeWillBeDestroyedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contextId")]
			public string ContextId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public string NodeId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Notifies that an existing AudioNode has been destroyed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<AudioNodeWillBeDestroyedEventArgs> AudioNodeWillBeDestroyed;

		/// <summary>
		/// Notifies that a new AudioParam has been created.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class AudioParamCreatedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: AudioParam</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("param")]
			public AudioParam Param;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Notifies that a new AudioParam has been created.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<AudioParamCreatedEventArgs> AudioParamCreated;

		/// <summary>
		/// Notifies that an existing AudioParam has been destroyed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class AudioParamWillBeDestroyedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contextId")]
			public string ContextId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public string NodeId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("paramId")]
			public string ParamId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Notifies that an existing AudioParam has been destroyed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<AudioParamWillBeDestroyedEventArgs> AudioParamWillBeDestroyed;

		/// <summary>
		/// Notifies that two AudioNodes are connected.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class NodesConnectedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contextId")]
			public string ContextId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sourceId")]
			public string SourceId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("destinationId")]
			public string DestinationId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sourceOutputIndex")]
			public double SourceOutputIndex;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("destinationInputIndex")]
			public double DestinationInputIndex;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Notifies that two AudioNodes are connected.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<NodesConnectedEventArgs> NodesConnected;

		/// <summary>
		/// Notifies that AudioNodes are disconnected. The destination can be null, and it means all the outgoing connections from the source are disconnected.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class NodesDisconnectedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contextId")]
			public string ContextId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sourceId")]
			public string SourceId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("destinationId")]
			public string DestinationId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sourceOutputIndex")]
			public double SourceOutputIndex;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("destinationInputIndex")]
			public double DestinationInputIndex;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Notifies that AudioNodes are disconnected. The destination can be null, and it means all the outgoing connections from the source are disconnected.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<NodesDisconnectedEventArgs> NodesDisconnected;

		/// <summary>
		/// Notifies that an AudioNode is connected to an AudioParam.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class NodeParamConnectedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contextId")]
			public string ContextId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sourceId")]
			public string SourceId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("destinationId")]
			public string DestinationId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sourceOutputIndex")]
			public double SourceOutputIndex;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Notifies that an AudioNode is connected to an AudioParam.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<NodeParamConnectedEventArgs> NodeParamConnected;

		/// <summary>
		/// Notifies that an AudioNode is disconnected to an AudioParam.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class NodeParamDisconnectedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contextId")]
			public string ContextId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sourceId")]
			public string SourceId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("destinationId")]
			public string DestinationId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sourceOutputIndex")]
			public double SourceOutputIndex;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Notifies that an AudioNode is disconnected to an AudioParam.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<NodeParamDisconnectedEventArgs> NodeParamDisconnected;

		/// <summary>
		/// Fields in AudioContext that change in real-time.
		/// </summary>
		[JsonObject]
		public class ContextRealtimeData {
			/// <summary>
			/// The current context time in second in BaseAudioContext.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("currentTime")]
			public double CurrentTime;

			/// <summary>
			/// The time spent on rendering graph divided by render qunatum duration, and multiplied by 100. 100 means the audio renderer reached the full capacity and glitch may occur.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("renderCapacity")]
			public double RenderCapacity;

			/// <summary>
			/// A running mean of callback interval.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("callbackIntervalMean")]
			public double CallbackIntervalMean;

			/// <summary>
			/// A running variance of callback interval.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("callbackIntervalVariance")]
			public double CallbackIntervalVariance;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Protocol object for BaseAudioContext
		/// </summary>
		[JsonObject]
		public class BaseAudioContext {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contextId")]
			public string ContextId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ContextType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contextType")]
			public string ContextType;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ContextState</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contextState")]
			public string ContextState;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: ContextRealtimeData</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("realtimeData")]
			public ContextRealtimeData RealtimeData;

			/// <summary>
			/// Platform-dependent callback buffer size.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("callbackBufferSize")]
			public double CallbackBufferSize;

			/// <summary>
			/// Number of output channels supported by audio hardware in use.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("maxOutputChannelCount")]
			public double MaxOutputChannelCount;

			/// <summary>
			/// Context sample rate.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sampleRate")]
			public double SampleRate;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Protocol object for AudioListner
		/// </summary>
		[JsonObject]
		public class AudioListener {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("listenerId")]
			public string ListenerId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contextId")]
			public string ContextId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Protocol object for AudioNode
		/// </summary>
		[JsonObject]
		public class AudioNode {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public string NodeId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contextId")]
			public string ContextId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeType")]
			public string NodeType;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("numberOfInputs")]
			public double NumberOfInputs;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("numberOfOutputs")]
			public double NumberOfOutputs;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("channelCount")]
			public double ChannelCount;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ChannelCountMode</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("channelCountMode")]
			public string ChannelCountMode;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ChannelInterpretation</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("channelInterpretation")]
			public string ChannelInterpretation;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Protocol object for AudioParam
		/// </summary>
		[JsonObject]
		public class AudioParam {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("paramId")]
			public string ParamId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public string NodeId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: GraphObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contextId")]
			public string ContextId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ParamType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("paramType")]
			public string ParamType;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: AutomationRate</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("rate")]
			public string Rate;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("defaultValue")]
			public double DefaultValue;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("minValue")]
			public double MinValue;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("maxValue")]
			public double MaxValue;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Enables the WebAudio domain and starts sending context lifetime events.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"WebAudio.enable"
			);
		}

		/// <summary>
		/// Disables the WebAudio domain.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"WebAudio.disable"
			);
		}

		/// <summary>
		/// Fetch the realtime data from the registered contexts.
		/// </summary>
		[JsonObject]
		public class GetRealtimeDataResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ContextRealtimeData</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("realtimeData")]
			public ContextRealtimeData RealtimeData;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fetch the realtime data from the registered contexts.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="contextId"></param>
		public GetRealtimeDataResult GetRealtimeData(string contextId) {
			string s = _chrome.Send(
				"WebAudio.getRealtimeData",
				new {
					contextId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetRealtimeDataResult>();
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("WebAudio.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "contextCreated":
					if (ContextCreated == null) {
						return;
					}
					ContextCreated(
						_chrome,
						JsonConvert.DeserializeObject
							<ContextCreatedEventArgs>(@params.ToString())
					);
					break;
				case "contextWillBeDestroyed":
					if (ContextWillBeDestroyed == null) {
						return;
					}
					ContextWillBeDestroyed(
						_chrome,
						JsonConvert.DeserializeObject
							<ContextWillBeDestroyedEventArgs>(@params.ToString())
					);
					break;
				case "contextChanged":
					if (ContextChanged == null) {
						return;
					}
					ContextChanged(
						_chrome,
						JsonConvert.DeserializeObject
							<ContextChangedEventArgs>(@params.ToString())
					);
					break;
				case "audioListenerCreated":
					if (AudioListenerCreated == null) {
						return;
					}
					AudioListenerCreated(
						_chrome,
						JsonConvert.DeserializeObject
							<AudioListenerCreatedEventArgs>(@params.ToString())
					);
					break;
				case "audioListenerWillBeDestroyed":
					if (AudioListenerWillBeDestroyed == null) {
						return;
					}
					AudioListenerWillBeDestroyed(
						_chrome,
						JsonConvert.DeserializeObject
							<AudioListenerWillBeDestroyedEventArgs>(@params.ToString())
					);
					break;
				case "audioNodeCreated":
					if (AudioNodeCreated == null) {
						return;
					}
					AudioNodeCreated(
						_chrome,
						JsonConvert.DeserializeObject
							<AudioNodeCreatedEventArgs>(@params.ToString())
					);
					break;
				case "audioNodeWillBeDestroyed":
					if (AudioNodeWillBeDestroyed == null) {
						return;
					}
					AudioNodeWillBeDestroyed(
						_chrome,
						JsonConvert.DeserializeObject
							<AudioNodeWillBeDestroyedEventArgs>(@params.ToString())
					);
					break;
				case "audioParamCreated":
					if (AudioParamCreated == null) {
						return;
					}
					AudioParamCreated(
						_chrome,
						JsonConvert.DeserializeObject
							<AudioParamCreatedEventArgs>(@params.ToString())
					);
					break;
				case "audioParamWillBeDestroyed":
					if (AudioParamWillBeDestroyed == null) {
						return;
					}
					AudioParamWillBeDestroyed(
						_chrome,
						JsonConvert.DeserializeObject
							<AudioParamWillBeDestroyedEventArgs>(@params.ToString())
					);
					break;
				case "nodesConnected":
					if (NodesConnected == null) {
						return;
					}
					NodesConnected(
						_chrome,
						JsonConvert.DeserializeObject
							<NodesConnectedEventArgs>(@params.ToString())
					);
					break;
				case "nodesDisconnected":
					if (NodesDisconnected == null) {
						return;
					}
					NodesDisconnected(
						_chrome,
						JsonConvert.DeserializeObject
							<NodesDisconnectedEventArgs>(@params.ToString())
					);
					break;
				case "nodeParamConnected":
					if (NodeParamConnected == null) {
						return;
					}
					NodeParamConnected(
						_chrome,
						JsonConvert.DeserializeObject
							<NodeParamConnectedEventArgs>(@params.ToString())
					);
					break;
				case "nodeParamDisconnected":
					if (NodeParamDisconnected == null) {
						return;
					}
					NodeParamDisconnected(
						_chrome,
						JsonConvert.DeserializeObject
							<NodeParamDisconnectedEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
