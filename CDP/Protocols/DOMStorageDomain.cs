// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// Query and modify DOM storage.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class DOMStorageDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the DOMStorageDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public DOMStorageDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class DomStorageItemAddedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StorageId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("storageId")]
			public StorageId StorageId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("key")]
			public string Key;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("newValue")]
			public string NewValue;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<DomStorageItemAddedEventArgs> DomStorageItemAdded;

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class DomStorageItemRemovedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StorageId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("storageId")]
			public StorageId StorageId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("key")]
			public string Key;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<DomStorageItemRemovedEventArgs> DomStorageItemRemoved;

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class DomStorageItemUpdatedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StorageId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("storageId")]
			public StorageId StorageId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("key")]
			public string Key;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("oldValue")]
			public string OldValue;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("newValue")]
			public string NewValue;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<DomStorageItemUpdatedEventArgs> DomStorageItemUpdated;

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class DomStorageItemsClearedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: StorageId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("storageId")]
			public StorageId StorageId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<DomStorageItemsClearedEventArgs> DomStorageItemsCleared;

		/// <summary>
		/// DOM Storage identifier.
		/// </summary>
		[JsonObject]
		public class StorageId {
			/// <summary>
			/// Security origin for the storage.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("securityOrigin")]
			public string SecurityOrigin;

			/// <summary>
			/// Whether the storage is local storage (not session storage).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("isLocalStorage")]
			public bool IsLocalStorage;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="storageId"></param>
		public void Clear(StorageId storageId) {
			_chrome.Send(
				"DOMStorage.clear",
				new {
					storageId
				}
			);
		}

		/// <summary>
		/// Disables storage tracking, prevents storage events from being sent to the client.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"DOMStorage.disable"
			);
		}

		/// <summary>
		/// Enables storage tracking, storage events will now be delivered to the client.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"DOMStorage.enable"
			);
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class GetDOMStorageItemsResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("entries")]
			public string[][] Entries;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="storageId"></param>
		public GetDOMStorageItemsResult GetDOMStorageItems(StorageId storageId) {
			string s = _chrome.Send(
				"DOMStorage.getDOMStorageItems",
				new {
					storageId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetDOMStorageItemsResult>();
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="storageId"></param>
		/// <param name="key"></param>
		public void RemoveDOMStorageItem(StorageId storageId, string key) {
			_chrome.Send(
				"DOMStorage.removeDOMStorageItem",
				new {
					storageId,
					key
				}
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="storageId"></param>
		/// <param name="key"></param>
		/// <param name="value"></param>
		public void SetDOMStorageItem(StorageId storageId, string key, string value) {
			_chrome.Send(
				"DOMStorage.setDOMStorageItem",
				new {
					storageId,
					key,
					value
				}
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("DOMStorage.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "domStorageItemAdded":
					if (DomStorageItemAdded == null) {
						return;
					}
					DomStorageItemAdded(
						_chrome,
						JsonConvert.DeserializeObject
							<DomStorageItemAddedEventArgs>(@params.ToString())
					);
					break;
				case "domStorageItemRemoved":
					if (DomStorageItemRemoved == null) {
						return;
					}
					DomStorageItemRemoved(
						_chrome,
						JsonConvert.DeserializeObject
							<DomStorageItemRemovedEventArgs>(@params.ToString())
					);
					break;
				case "domStorageItemUpdated":
					if (DomStorageItemUpdated == null) {
						return;
					}
					DomStorageItemUpdated(
						_chrome,
						JsonConvert.DeserializeObject
							<DomStorageItemUpdatedEventArgs>(@params.ToString())
					);
					break;
				case "domStorageItemsCleared":
					if (DomStorageItemsCleared == null) {
						return;
					}
					DomStorageItemsCleared(
						_chrome,
						JsonConvert.DeserializeObject
							<DomStorageItemsClearedEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
