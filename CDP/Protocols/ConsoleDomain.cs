// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// This domain is deprecated - use Runtime or Log instead.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: true</description></item>
	/// <item><description>experimental: false</description></item>
	/// <item><description>dependencies: Runtime</description></item>
	/// </list>
	/// </summary>
	public class ConsoleDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the ConsoleDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public ConsoleDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Issued when new console message is added.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class MessageAddedEventArgs : EventArgs {
			/// <summary>
			/// Console message that has been added.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ConsoleMessage</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("message")]
			public ConsoleMessage Message;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Issued when new console message is added.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<MessageAddedEventArgs> MessageAdded;

		/// <summary>
		/// Console message.
		/// </summary>
		[JsonObject]
		public class ConsoleMessage {
			/// <summary>
			/// Message source.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: xml, javascript, network, console-api, storage, appcache, rendering, security, other, deprecation, worker
			/// </summary>
			[JsonProperty("source")]
			public string Source;

			/// <summary>
			/// Message severity.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: log, warning, error, debug, info
			/// </summary>
			[JsonProperty("level")]
			public string Level;

			/// <summary>
			/// Message text.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("text")]
			public string Text;

			/// <summary>
			/// URL of the message origin.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Line number in the resource that generated this message (1-based).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("line")]
			public int Line;

			/// <summary>
			/// Column number in the resource that generated this message (1-based).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("column")]
			public int Column;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Does nothing.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void ClearMessages() {
			_chrome.Send(
				"Console.clearMessages"
			);
		}

		/// <summary>
		/// Disables console domain, prevents further console messages from being reported to the client.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"Console.disable"
			);
		}

		/// <summary>
		/// Enables console domain, sends the messages collected so far to the client by means of the `messageAdded` notification.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"Console.enable"
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("Console.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "messageAdded":
					if (MessageAdded == null) {
						return;
					}
					MessageAdded(
						_chrome,
						JsonConvert.DeserializeObject
							<MessageAddedEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
