// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// 
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: Runtime</description></item>
	/// </list>
	/// </summary>
	public class IndexedDBDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the IndexedDBDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public IndexedDBDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Database with an array of object stores.
		/// </summary>
		[JsonObject]
		public class DatabaseWithObjectStores {
			/// <summary>
			/// Database name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// Database version (type is not 'integer', as the standard requires the version number to be 'unsigned long long')
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("version")]
			public double Version;

			/// <summary>
			/// Object stores in this database.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("objectStores")]
			public ObjectStore[] ObjectStores;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Object store.
		/// </summary>
		[JsonObject]
		public class ObjectStore {
			/// <summary>
			/// Object store name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// Object store key path.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: KeyPath</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("keyPath")]
			public KeyPath KeyPath;

			/// <summary>
			/// If true, object store has auto increment flag set.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("autoIncrement")]
			public bool AutoIncrement;

			/// <summary>
			/// Indexes in this object store.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("indexes")]
			public ObjectStoreIndex[] Indexes;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Object store index.
		/// </summary>
		[JsonObject]
		public class ObjectStoreIndex {
			/// <summary>
			/// Index name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// Index key path.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: KeyPath</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("keyPath")]
			public KeyPath KeyPath;

			/// <summary>
			/// If true, index is unique.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("unique")]
			public bool Unique;

			/// <summary>
			/// If true, index allows multiple entries for a key.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("multiEntry")]
			public bool MultiEntry;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Key.
		/// </summary>
		[JsonObject]
		public class Key {
			/// <summary>
			/// Key type.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: number, string, date, array
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// Number value.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("number")]
			public double Number;

			/// <summary>
			/// String value.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("string")]
			public string String;

			/// <summary>
			/// Date value.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("date")]
			public double Date;

			/// <summary>
			/// Array value.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("array")]
			public Key[] Array;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Key range.
		/// </summary>
		[JsonObject]
		public class KeyRange {
			/// <summary>
			/// Lower bound.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Key</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("lower")]
			public Key Lower;

			/// <summary>
			/// Upper bound.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Key</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("upper")]
			public Key Upper;

			/// <summary>
			/// If true lower bound is open.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("lowerOpen")]
			public bool LowerOpen;

			/// <summary>
			/// If true upper bound is open.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("upperOpen")]
			public bool UpperOpen;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Data entry.
		/// </summary>
		[JsonObject]
		public class DataEntry {
			/// <summary>
			/// Key object.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("key")]
			public RuntimeDomain.RemoteObject Key;

			/// <summary>
			/// Primary key object.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("primaryKey")]
			public RuntimeDomain.RemoteObject PrimaryKey;

			/// <summary>
			/// Value object.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public RuntimeDomain.RemoteObject Value;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Key path.
		/// </summary>
		[JsonObject]
		public class KeyPath {
			/// <summary>
			/// Key path type.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: null, string, array
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// String value.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("string")]
			public string String;

			/// <summary>
			/// Array value.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("array")]
			public string[] Array;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Clears all entries from an object store.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="securityOrigin">Security origin.</param>
		/// <param name="databaseName">Database name.</param>
		/// <param name="objectStoreName">Object store name.</param>
		public void ClearObjectStore(string securityOrigin, string databaseName, string objectStoreName) {
			_chrome.Send(
				"IndexedDB.clearObjectStore",
				new {
					securityOrigin,
					databaseName,
					objectStoreName
				}
			);
		}

		/// <summary>
		/// Deletes a database.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="securityOrigin">Security origin.</param>
		/// <param name="databaseName">Database name.</param>
		public void DeleteDatabase(string securityOrigin, string databaseName) {
			_chrome.Send(
				"IndexedDB.deleteDatabase",
				new {
					securityOrigin,
					databaseName
				}
			);
		}

		/// <summary>
		/// Delete a range of entries from an object store
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="securityOrigin"></param>
		/// <param name="databaseName"></param>
		/// <param name="objectStoreName"></param>
		/// <param name="keyRange">Range of entry keys to delete</param>
		public void DeleteObjectStoreEntries(string securityOrigin, string databaseName, string objectStoreName, KeyRange keyRange) {
			_chrome.Send(
				"IndexedDB.deleteObjectStoreEntries",
				new {
					securityOrigin,
					databaseName,
					objectStoreName,
					keyRange
				}
			);
		}

		/// <summary>
		/// Disables events from backend.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"IndexedDB.disable"
			);
		}

		/// <summary>
		/// Enables events from backend.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"IndexedDB.enable"
			);
		}

		/// <summary>
		/// Requests data from object store or index.
		/// </summary>
		[JsonObject]
		public class RequestDataResult {
			/// <summary>
			/// Array of object store data entries.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("objectStoreDataEntries")]
			public DataEntry[] ObjectStoreDataEntries;

			/// <summary>
			/// If true, there are more entries to fetch in the given range.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("hasMore")]
			public bool HasMore;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Requests data from object store or index.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="securityOrigin">Security origin.</param>
		/// <param name="databaseName">Database name.</param>
		/// <param name="objectStoreName">Object store name.</param>
		/// <param name="indexName">Index name, empty string for object store data requests.</param>
		/// <param name="skipCount">Number of records to skip.</param>
		/// <param name="pageSize">Number of records to fetch.</param>
		/// <param name="keyRange">Key range.</param>
		public RequestDataResult RequestData(string securityOrigin, string databaseName, string objectStoreName, string indexName, int skipCount, int pageSize, KeyRange keyRange = null) {
			string s = _chrome.Send(
				"IndexedDB.requestData",
				new {
					securityOrigin,
					databaseName,
					objectStoreName,
					indexName,
					skipCount,
					pageSize,
					keyRange
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<RequestDataResult>();
		}

		/// <summary>
		/// Gets metadata of an object store
		/// </summary>
		[JsonObject]
		public class GetMetadataResult {
			/// <summary>
			/// the entries count
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("entriesCount")]
			public double EntriesCount;

			/// <summary>
			/// the current value of key generator, to become the next inserted key into the object store. Valid if objectStore.autoIncrement is true.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("keyGeneratorValue")]
			public double KeyGeneratorValue;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Gets metadata of an object store
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="securityOrigin">Security origin.</param>
		/// <param name="databaseName">Database name.</param>
		/// <param name="objectStoreName">Object store name.</param>
		public GetMetadataResult GetMetadata(string securityOrigin, string databaseName, string objectStoreName) {
			string s = _chrome.Send(
				"IndexedDB.getMetadata",
				new {
					securityOrigin,
					databaseName,
					objectStoreName
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetMetadataResult>();
		}

		/// <summary>
		/// Requests database with given name in given frame.
		/// </summary>
		[JsonObject]
		public class RequestDatabaseResult {
			/// <summary>
			/// Database with an array of object stores.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: DatabaseWithObjectStores</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("databaseWithObjectStores")]
			public DatabaseWithObjectStores DatabaseWithObjectStores;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Requests database with given name in given frame.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="securityOrigin">Security origin.</param>
		/// <param name="databaseName">Database name.</param>
		public RequestDatabaseResult RequestDatabase(string securityOrigin, string databaseName) {
			string s = _chrome.Send(
				"IndexedDB.requestDatabase",
				new {
					securityOrigin,
					databaseName
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<RequestDatabaseResult>();
		}

		/// <summary>
		/// Requests database names for given security origin.
		/// </summary>
		[JsonObject]
		public class RequestDatabaseNamesResult {
			/// <summary>
			/// Database names for origin.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("databaseNames")]
			public string[] DatabaseNames;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Requests database names for given security origin.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="securityOrigin">Security origin.</param>
		public RequestDatabaseNamesResult RequestDatabaseNames(string securityOrigin) {
			string s = _chrome.Send(
				"IndexedDB.requestDatabaseNames",
				new {
					securityOrigin
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<RequestDatabaseNamesResult>();
		}



	}
}
