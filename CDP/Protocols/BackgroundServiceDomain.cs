// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// Defines events for background web platform features.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class BackgroundServiceDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the BackgroundServiceDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public BackgroundServiceDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Called when the recording state for the service has been updated.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class RecordingStateChangedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("isRecording")]
			public bool IsRecording;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ServiceName</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("service")]
			public string Service;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Called when the recording state for the service has been updated.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<RecordingStateChangedEventArgs> RecordingStateChanged;

		/// <summary>
		/// Called with all existing backgroundServiceEvents when enabled, and all new events afterwards if enabled and recording.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class BackgroundServiceEventReceivedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: BackgroundServiceEvent</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("backgroundServiceEvent")]
			public BackgroundServiceEvent BackgroundServiceEvent;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Called with all existing backgroundServiceEvents when enabled, and all new events afterwards if enabled and recording.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<BackgroundServiceEventReceivedEventArgs> BackgroundServiceEventReceived;

		/// <summary>
		/// A key-value pair for additional event information to pass along.
		/// </summary>
		[JsonObject]
		public class EventMetadata {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("key")]
			public string Key;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public string Value;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class BackgroundServiceEvent {
			/// <summary>
			/// Timestamp of the event (in seconds).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Network.TimeSinceEpoch</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// The origin this event belongs to.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("origin")]
			public string Origin;

			/// <summary>
			/// The Service Worker ID that initiated the event.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ServiceWorker.RegistrationID</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("serviceWorkerRegistrationId")]
			public string ServiceWorkerRegistrationId;

			/// <summary>
			/// The Background Service this event belongs to.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ServiceName</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("service")]
			public string Service;

			/// <summary>
			/// A description of the event.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("eventName")]
			public string EventName;

			/// <summary>
			/// An identifier that groups related events together.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("instanceId")]
			public string InstanceId;

			/// <summary>
			/// A list of event-specific information.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("eventMetadata")]
			public EventMetadata[] EventMetadata;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Enables event updates for the service.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="service"></param>
		public void StartObserving(string service) {
			_chrome.Send(
				"BackgroundService.startObserving",
				new {
					service
				}
			);
		}

		/// <summary>
		/// Disables event updates for the service.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="service"></param>
		public void StopObserving(string service) {
			_chrome.Send(
				"BackgroundService.stopObserving",
				new {
					service
				}
			);
		}

		/// <summary>
		/// Set the recording state for the service.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="shouldRecord"></param>
		/// <param name="service"></param>
		public void SetRecording(bool shouldRecord, string service) {
			_chrome.Send(
				"BackgroundService.setRecording",
				new {
					shouldRecord,
					service
				}
			);
		}

		/// <summary>
		/// Clears all stored data for the service.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="service"></param>
		public void ClearEvents(string service) {
			_chrome.Send(
				"BackgroundService.clearEvents",
				new {
					service
				}
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("BackgroundService.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "recordingStateChanged":
					if (RecordingStateChanged == null) {
						return;
					}
					RecordingStateChanged(
						_chrome,
						JsonConvert.DeserializeObject
							<RecordingStateChangedEventArgs>(@params.ToString())
					);
					break;
				case "backgroundServiceEventReceived":
					if (BackgroundServiceEventReceived == null) {
						return;
					}
					BackgroundServiceEventReceived(
						_chrome,
						JsonConvert.DeserializeObject
							<BackgroundServiceEventReceivedEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
