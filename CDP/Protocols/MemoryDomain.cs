// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// 
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class MemoryDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the MemoryDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public MemoryDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Heap profile sample.
		/// </summary>
		[JsonObject]
		public class SamplingProfileNode {
			/// <summary>
			/// Size of the sampled allocation.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("size")]
			public double Size;

			/// <summary>
			/// Total bytes attributed to this sample.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("total")]
			public double Total;

			/// <summary>
			/// Execution stack at the point of allocation.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("stack")]
			public string[] Stack;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Array of heap profile samples.
		/// </summary>
		[JsonObject]
		public class SamplingProfile {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("samples")]
			public SamplingProfileNode[] Samples;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("modules")]
			public Module[] Modules;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Executable module information
		/// </summary>
		[JsonObject]
		public class Module {
			/// <summary>
			/// Name of the module.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// UUID of the module.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("uuid")]
			public string Uuid;

			/// <summary>
			/// Base address where the module is loaded into memory. Encoded as a decimal or hexadecimal (0x prefixed) string.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("baseAddress")]
			public string BaseAddress;

			/// <summary>
			/// Size of the module in bytes.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("size")]
			public double Size;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class GetDOMCountersResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("documents")]
			public int Documents;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodes")]
			public int Nodes;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("jsEventListeners")]
			public int JsEventListeners;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public GetDOMCountersResult GetDOMCounters() {
			string s = _chrome.Send(
				"Memory.getDOMCounters"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetDOMCountersResult>();
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void PrepareForLeakDetection() {
			_chrome.Send(
				"Memory.prepareForLeakDetection"
			);
		}

		/// <summary>
		/// Simulate OomIntervention by purging V8 memory.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void ForciblyPurgeJavaScriptMemory() {
			_chrome.Send(
				"Memory.forciblyPurgeJavaScriptMemory"
			);
		}

		/// <summary>
		/// Enable/disable suppressing memory pressure notifications in all processes.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="suppressed">If true, memory pressure notifications will be suppressed.</param>
		public void SetPressureNotificationsSuppressed(bool suppressed) {
			_chrome.Send(
				"Memory.setPressureNotificationsSuppressed",
				new {
					suppressed
				}
			);
		}

		/// <summary>
		/// Simulate a memory pressure notification in all processes.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="level">Memory pressure level of the notification.</param>
		public void SimulatePressureNotification(string level) {
			_chrome.Send(
				"Memory.simulatePressureNotification",
				new {
					level
				}
			);
		}

		/// <summary>
		/// Start collecting native memory profile.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="samplingInterval">Average number of bytes between samples.</param>
		/// <param name="suppressRandomness">Do not randomize intervals between samples.</param>
		public void StartSampling(int? samplingInterval = null, bool? suppressRandomness = null) {
			_chrome.Send(
				"Memory.startSampling",
				new {
					samplingInterval,
					suppressRandomness
				}
			);
		}

		/// <summary>
		/// Stop collecting native memory profile.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void StopSampling() {
			_chrome.Send(
				"Memory.stopSampling"
			);
		}

		/// <summary>
		/// Retrieve native memory allocations profile collected since renderer process startup.
		/// </summary>
		[JsonObject]
		public class GetAllTimeSamplingProfileResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SamplingProfile</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("profile")]
			public SamplingProfile Profile;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Retrieve native memory allocations profile collected since renderer process startup.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public GetAllTimeSamplingProfileResult GetAllTimeSamplingProfile() {
			string s = _chrome.Send(
				"Memory.getAllTimeSamplingProfile"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetAllTimeSamplingProfileResult>();
		}

		/// <summary>
		/// Retrieve native memory allocations profile collected since browser process startup.
		/// </summary>
		[JsonObject]
		public class GetBrowserSamplingProfileResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SamplingProfile</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("profile")]
			public SamplingProfile Profile;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Retrieve native memory allocations profile collected since browser process startup.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public GetBrowserSamplingProfileResult GetBrowserSamplingProfile() {
			string s = _chrome.Send(
				"Memory.getBrowserSamplingProfile"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetBrowserSamplingProfileResult>();
		}

		/// <summary>
		/// Retrieve native memory allocations profile collected since last `startSampling` call.
		/// </summary>
		[JsonObject]
		public class GetSamplingProfileResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SamplingProfile</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("profile")]
			public SamplingProfile Profile;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Retrieve native memory allocations profile collected since last `startSampling` call.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public GetSamplingProfileResult GetSamplingProfile() {
			string s = _chrome.Send(
				"Memory.getSamplingProfile"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetSamplingProfileResult>();
		}



	}
}
