// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// DOM debugging allows setting breakpoints on particular DOM operations and events. JavaScript execution will stop on these operations as if there was a regular breakpoint set.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: false</description></item>
	/// <item><description>dependencies: DOM, Debugger, Runtime</description></item>
	/// </list>
	/// </summary>
	public class DOMDebuggerDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the DOMDebuggerDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public DOMDebuggerDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Object event listener.
		/// </summary>
		[JsonObject]
		public class EventListener {
			/// <summary>
			/// `EventListener`'s type.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// `EventListener`'s useCapture.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("useCapture")]
			public bool UseCapture;

			/// <summary>
			/// `EventListener`'s passive flag.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("passive")]
			public bool Passive;

			/// <summary>
			/// `EventListener`'s once flag.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("once")]
			public bool Once;

			/// <summary>
			/// Script id of the handler code.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.ScriptId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scriptId")]
			public string ScriptId;

			/// <summary>
			/// Line number in the script (0-based).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("lineNumber")]
			public int LineNumber;

			/// <summary>
			/// Column number in the script (0-based).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("columnNumber")]
			public int ColumnNumber;

			/// <summary>
			/// Event handler function value.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Runtime.RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("handler")]
			public RuntimeDomain.RemoteObject Handler;

			/// <summary>
			/// Event original handler function value.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Runtime.RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("originalHandler")]
			public RuntimeDomain.RemoteObject OriginalHandler;

			/// <summary>
			/// Node the listener is added to (if any).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: DOM.BackendNodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("backendNodeId")]
			public int BackendNodeId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Returns event listeners of the given object.
		/// </summary>
		[JsonObject]
		public class GetEventListenersResult {
			/// <summary>
			/// Array of relevant listeners.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("listeners")]
			public EventListener[] Listeners;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns event listeners of the given object.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="objectId">Identifier of the object to return listeners for.</param>
		/// <param name="depth">The maximum depth at which Node children should be retrieved, defaults to 1. Use -1 for the entire subtree or provide an integer larger than 0.</param>
		/// <param name="pierce">Whether or not iframes and shadow roots should be traversed when returning the subtree (default is false). Reports listeners for all contexts if pierce is enabled.</param>
		public GetEventListenersResult GetEventListeners(string objectId, int? depth = null, bool? pierce = null) {
			string s = _chrome.Send(
				"DOMDebugger.getEventListeners",
				new {
					objectId,
					depth,
					pierce
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetEventListenersResult>();
		}

		/// <summary>
		/// Removes DOM breakpoint that was set using `setDOMBreakpoint`.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Identifier of the node to remove breakpoint from.</param>
		/// <param name="type">Type of the breakpoint to remove.</param>
		public void RemoveDOMBreakpoint(int nodeId, string type) {
			_chrome.Send(
				"DOMDebugger.removeDOMBreakpoint",
				new {
					nodeId,
					type
				}
			);
		}

		/// <summary>
		/// Removes breakpoint on particular DOM event.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="eventName">Event name.</param>
		/// <param name="targetName">EventTarget interface name.</param>
		public void RemoveEventListenerBreakpoint(string eventName, string targetName = null) {
			_chrome.Send(
				"DOMDebugger.removeEventListenerBreakpoint",
				new {
					eventName,
					targetName
				}
			);
		}

		/// <summary>
		/// Removes breakpoint on particular native event.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="eventName">Instrumentation name to stop on.</param>
		public void RemoveInstrumentationBreakpoint(string eventName) {
			_chrome.Send(
				"DOMDebugger.removeInstrumentationBreakpoint",
				new {
					eventName
				}
			);
		}

		/// <summary>
		/// Removes breakpoint from XMLHttpRequest.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="url">Resource URL substring.</param>
		public void RemoveXHRBreakpoint(string url) {
			_chrome.Send(
				"DOMDebugger.removeXHRBreakpoint",
				new {
					url
				}
			);
		}

		/// <summary>
		/// Sets breakpoint on particular operation with DOM.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Identifier of the node to set breakpoint on.</param>
		/// <param name="type">Type of the operation to stop upon.</param>
		public void SetDOMBreakpoint(int nodeId, string type) {
			_chrome.Send(
				"DOMDebugger.setDOMBreakpoint",
				new {
					nodeId,
					type
				}
			);
		}

		/// <summary>
		/// Sets breakpoint on particular DOM event.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="eventName">DOM Event name to stop on (any DOM event will do).</param>
		/// <param name="targetName">EventTarget interface name to stop on. If equal to `"*"` or not provided, will stop on any EventTarget.</param>
		public void SetEventListenerBreakpoint(string eventName, string targetName = null) {
			_chrome.Send(
				"DOMDebugger.setEventListenerBreakpoint",
				new {
					eventName,
					targetName
				}
			);
		}

		/// <summary>
		/// Sets breakpoint on particular native event.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="eventName">Instrumentation name to stop on.</param>
		public void SetInstrumentationBreakpoint(string eventName) {
			_chrome.Send(
				"DOMDebugger.setInstrumentationBreakpoint",
				new {
					eventName
				}
			);
		}

		/// <summary>
		/// Sets breakpoint on XMLHttpRequest.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="url">Resource URL substring. All XHRs having this substring in the URL will get stopped upon.</param>
		public void SetXHRBreakpoint(string url) {
			_chrome.Send(
				"DOMDebugger.setXHRBreakpoint",
				new {
					url
				}
			);
		}



	}
}
