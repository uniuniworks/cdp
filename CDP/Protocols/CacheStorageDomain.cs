// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// 
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class CacheStorageDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the CacheStorageDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public CacheStorageDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Data entry.
		/// </summary>
		[JsonObject]
		public class DataEntry {
			/// <summary>
			/// Request URL.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestURL")]
			public string RequestURL;

			/// <summary>
			/// Request method.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestMethod")]
			public string RequestMethod;

			/// <summary>
			/// Request headers
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("requestHeaders")]
			public Header[] RequestHeaders;

			/// <summary>
			/// Number of seconds since epoch.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("responseTime")]
			public double ResponseTime;

			/// <summary>
			/// HTTP response status code.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("responseStatus")]
			public int ResponseStatus;

			/// <summary>
			/// HTTP response status text.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("responseStatusText")]
			public string ResponseStatusText;

			/// <summary>
			/// HTTP response type
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: CachedResponseType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("responseType")]
			public string ResponseType;

			/// <summary>
			/// Response headers
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("responseHeaders")]
			public Header[] ResponseHeaders;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Cache identifier.
		/// </summary>
		[JsonObject]
		public class Cache {
			/// <summary>
			/// An opaque unique id of the cache.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: CacheId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("cacheId")]
			public string CacheId;

			/// <summary>
			/// Security origin of the cache.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("securityOrigin")]
			public string SecurityOrigin;

			/// <summary>
			/// The name of the cache.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("cacheName")]
			public string CacheName;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class Header {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public string Value;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Cached response
		/// </summary>
		[JsonObject]
		public class CachedResponse {
			/// <summary>
			/// Entry content, base64-encoded.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: binary</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("body")]
			public byte[] Body;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Deletes a cache.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="cacheId">Id of cache for deletion.</param>
		public void DeleteCache(string cacheId) {
			_chrome.Send(
				"CacheStorage.deleteCache",
				new {
					cacheId
				}
			);
		}

		/// <summary>
		/// Deletes a cache entry.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="cacheId">Id of cache where the entry will be deleted.</param>
		/// <param name="request">URL spec of the request.</param>
		public void DeleteEntry(string cacheId, string request) {
			_chrome.Send(
				"CacheStorage.deleteEntry",
				new {
					cacheId,
					request
				}
			);
		}

		/// <summary>
		/// Requests cache names.
		/// </summary>
		[JsonObject]
		public class RequestCacheNamesResult {
			/// <summary>
			/// Caches for the security origin.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("caches")]
			public Cache[] Caches;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Requests cache names.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="securityOrigin">Security origin.</param>
		public RequestCacheNamesResult RequestCacheNames(string securityOrigin) {
			string s = _chrome.Send(
				"CacheStorage.requestCacheNames",
				new {
					securityOrigin
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<RequestCacheNamesResult>();
		}

		/// <summary>
		/// Fetches cache entry.
		/// </summary>
		[JsonObject]
		public class RequestCachedResponseResult {
			/// <summary>
			/// Response read from the cache.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: CachedResponse</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("response")]
			public CachedResponse Response;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fetches cache entry.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="cacheId">Id of cache that contains the entry.</param>
		/// <param name="requestURL">URL spec of the request.</param>
		/// <param name="requestHeaders">headers of the request.</param>
		public RequestCachedResponseResult RequestCachedResponse(string cacheId, string requestURL, Header[] requestHeaders) {
			string s = _chrome.Send(
				"CacheStorage.requestCachedResponse",
				new {
					cacheId,
					requestURL,
					requestHeaders
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<RequestCachedResponseResult>();
		}

		/// <summary>
		/// Requests data from cache.
		/// </summary>
		[JsonObject]
		public class RequestEntriesResult {
			/// <summary>
			/// Array of object store data entries.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("cacheDataEntries")]
			public DataEntry[] CacheDataEntries;

			/// <summary>
			/// Count of returned entries from this storage. If pathFilter is empty, it is the count of all entries from this storage.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("returnCount")]
			public double ReturnCount;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Requests data from cache.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="cacheId">ID of cache to get entries from.</param>
		/// <param name="skipCount">Number of records to skip.</param>
		/// <param name="pageSize">Number of records to fetch.</param>
		/// <param name="pathFilter">If present, only return the entries containing this substring in the path</param>
		public RequestEntriesResult RequestEntries(string cacheId, int skipCount, int pageSize, string pathFilter = null) {
			string s = _chrome.Send(
				"CacheStorage.requestEntries",
				new {
					cacheId,
					skipCount,
					pageSize,
					pathFilter
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<RequestEntriesResult>();
		}



	}
}
