// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// 
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class ApplicationCacheDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the ApplicationCacheDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public ApplicationCacheDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ApplicationCacheStatusUpdatedEventArgs : EventArgs {
			/// <summary>
			/// Identifier of the frame containing document whose application cache updated status.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Page.FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// Manifest URL.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("manifestURL")]
			public string ManifestURL;

			/// <summary>
			/// Updated application cache status.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("status")]
			public int Status;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ApplicationCacheStatusUpdatedEventArgs> ApplicationCacheStatusUpdated;

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class NetworkStateUpdatedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("isNowOnline")]
			public bool IsNowOnline;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<NetworkStateUpdatedEventArgs> NetworkStateUpdated;

		/// <summary>
		/// Detailed application cache resource information.
		/// </summary>
		[JsonObject]
		public class ApplicationCacheResource {
			/// <summary>
			/// Resource url.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Resource size.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("size")]
			public int Size;

			/// <summary>
			/// Resource type.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Detailed application cache information.
		/// </summary>
		[JsonObject]
		public class ApplicationCache {
			/// <summary>
			/// Manifest URL.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("manifestURL")]
			public string ManifestURL;

			/// <summary>
			/// Application cache size.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("size")]
			public double Size;

			/// <summary>
			/// Application cache creation time.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("creationTime")]
			public double CreationTime;

			/// <summary>
			/// Application cache update time.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("updateTime")]
			public double UpdateTime;

			/// <summary>
			/// Application cache resources.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("resources")]
			public ApplicationCacheResource[] Resources;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Frame identifier - manifest URL pair.
		/// </summary>
		[JsonObject]
		public class FrameWithManifest {
			/// <summary>
			/// Frame identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Page.FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// Manifest URL.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("manifestURL")]
			public string ManifestURL;

			/// <summary>
			/// Application cache status.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("status")]
			public int Status;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Enables application cache domain notifications.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"ApplicationCache.enable"
			);
		}

		/// <summary>
		/// Returns relevant application cache data for the document in given frame.
		/// </summary>
		[JsonObject]
		public class GetApplicationCacheForFrameResult {
			/// <summary>
			/// Relevant application cache data for the document in given frame.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ApplicationCache</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("applicationCache")]
			public ApplicationCache ApplicationCache;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns relevant application cache data for the document in given frame.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="frameId">Identifier of the frame containing document whose application cache is retrieved.</param>
		public GetApplicationCacheForFrameResult GetApplicationCacheForFrame(string frameId) {
			string s = _chrome.Send(
				"ApplicationCache.getApplicationCacheForFrame",
				new {
					frameId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetApplicationCacheForFrameResult>();
		}

		/// <summary>
		/// Returns array of frame identifiers with manifest urls for each frame containing a document associated with some application cache.
		/// </summary>
		[JsonObject]
		public class GetFramesWithManifestsResult {
			/// <summary>
			/// Array of frame identifiers with manifest urls for each frame containing a document associated with some application cache.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameIds")]
			public FrameWithManifest[] FrameIds;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns array of frame identifiers with manifest urls for each frame containing a document associated with some application cache.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public GetFramesWithManifestsResult GetFramesWithManifests() {
			string s = _chrome.Send(
				"ApplicationCache.getFramesWithManifests"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetFramesWithManifestsResult>();
		}

		/// <summary>
		/// Returns manifest URL for document in the given frame.
		/// </summary>
		[JsonObject]
		public class GetManifestForFrameResult {
			/// <summary>
			/// Manifest URL for document in the given frame.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("manifestURL")]
			public string ManifestURL;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns manifest URL for document in the given frame.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="frameId">Identifier of the frame containing document whose manifest is retrieved.</param>
		public GetManifestForFrameResult GetManifestForFrame(string frameId) {
			string s = _chrome.Send(
				"ApplicationCache.getManifestForFrame",
				new {
					frameId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetManifestForFrameResult>();
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("ApplicationCache.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "applicationCacheStatusUpdated":
					if (ApplicationCacheStatusUpdated == null) {
						return;
					}
					ApplicationCacheStatusUpdated(
						_chrome,
						JsonConvert.DeserializeObject
							<ApplicationCacheStatusUpdatedEventArgs>(@params.ToString())
					);
					break;
				case "networkStateUpdated":
					if (NetworkStateUpdated == null) {
						return;
					}
					NetworkStateUpdated(
						_chrome,
						JsonConvert.DeserializeObject
							<NetworkStateUpdatedEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
