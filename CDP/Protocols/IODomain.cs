// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// Input/Output operations for streams produced by DevTools.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: false</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class IODomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the IODomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public IODomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}


		/// <summary>
		/// Close the stream, discard any temporary backing storage.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="handle">Handle of the stream to close.</param>
		public void Close(string handle) {
			_chrome.Send(
				"IO.close",
				new {
					handle
				}
			);
		}

		/// <summary>
		/// Read a chunk of the stream
		/// </summary>
		[JsonObject]
		public class ReadResult {
			/// <summary>
			/// Set if the data is base64-encoded
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("base64Encoded")]
			public bool Base64Encoded;

			/// <summary>
			/// Data that were read.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("data")]
			public string Data;

			/// <summary>
			/// Set if the end-of-file condition occured while reading.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("eof")]
			public bool Eof;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Read a chunk of the stream
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="handle">Handle of the stream to read.</param>
		/// <param name="offset">Seek to the specified offset before reading (if not specificed, proceed with offset following the last read). Some types of streams may only support sequential reads.</param>
		/// <param name="size">Maximum number of bytes to read (left upon the agent discretion if not specified).</param>
		public ReadResult Read(string handle, int? offset = null, int? size = null) {
			string s = _chrome.Send(
				"IO.read",
				new {
					handle,
					offset,
					size
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<ReadResult>();
		}

		/// <summary>
		/// Return UUID of Blob object specified by a remote object id.
		/// </summary>
		[JsonObject]
		public class ResolveBlobResult {
			/// <summary>
			/// UUID of the specified Blob.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("uuid")]
			public string Uuid;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Return UUID of Blob object specified by a remote object id.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="objectId">Object id of a Blob object wrapper.</param>
		public ResolveBlobResult ResolveBlob(string objectId) {
			string s = _chrome.Send(
				"IO.resolveBlob",
				new {
					objectId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<ResolveBlobResult>();
		}



	}
}
