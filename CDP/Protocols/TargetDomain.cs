// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// Supports additional targets discovery and allows to attach to them.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: false</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class TargetDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the TargetDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public TargetDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Issued when attached to target because of auto-attach or `attachToTarget` command.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class AttachedToTargetEventArgs : EventArgs {
			/// <summary>
			/// Identifier assigned to the session used to send/receive messages.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SessionID</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sessionId")]
			public string SessionId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: TargetInfo</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("targetInfo")]
			public TargetInfo TargetInfo;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("waitingForDebugger")]
			public bool WaitingForDebugger;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Issued when attached to target because of auto-attach or `attachToTarget` command.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<AttachedToTargetEventArgs> AttachedToTarget;

		/// <summary>
		/// Issued when detached from target for any reason (including `detachFromTarget` command). Can be issued multiple times per target if multiple sessions have been attached to it.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class DetachedFromTargetEventArgs : EventArgs {
			/// <summary>
			/// Detached session identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SessionID</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sessionId")]
			public string SessionId;

			/// <summary>
			/// Deprecated.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: TargetID</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("targetId")]
			public string TargetId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Issued when detached from target for any reason (including `detachFromTarget` command). Can be issued multiple times per target if multiple sessions have been attached to it.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<DetachedFromTargetEventArgs> DetachedFromTarget;

		/// <summary>
		/// Notifies about a new protocol message received from the session (as reported in `attachedToTarget` event).
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ReceivedMessageFromTargetEventArgs : EventArgs {
			/// <summary>
			/// Identifier of a session which sends a message.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SessionID</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sessionId")]
			public string SessionId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("message")]
			public string Message;

			/// <summary>
			/// Deprecated.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: TargetID</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("targetId")]
			public string TargetId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Notifies about a new protocol message received from the session (as reported in `attachedToTarget` event).
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ReceivedMessageFromTargetEventArgs> ReceivedMessageFromTarget;

		/// <summary>
		/// Issued when a possible inspection target is created.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class TargetCreatedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: TargetInfo</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("targetInfo")]
			public TargetInfo TargetInfo;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Issued when a possible inspection target is created.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<TargetCreatedEventArgs> TargetCreated;

		/// <summary>
		/// Issued when a target is destroyed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class TargetDestroyedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: TargetID</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("targetId")]
			public string TargetId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Issued when a target is destroyed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<TargetDestroyedEventArgs> TargetDestroyed;

		/// <summary>
		/// Issued when a target has crashed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class TargetCrashedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: TargetID</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("targetId")]
			public string TargetId;

			/// <summary>
			/// Termination status type.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("status")]
			public string Status;

			/// <summary>
			/// Termination error code.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("errorCode")]
			public int ErrorCode;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Issued when a target has crashed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<TargetCrashedEventArgs> TargetCrashed;

		/// <summary>
		/// Issued when some information about a target has changed. This only happens between `targetCreated` and `targetDestroyed`.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class TargetInfoChangedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: TargetInfo</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("targetInfo")]
			public TargetInfo TargetInfo;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Issued when some information about a target has changed. This only happens between `targetCreated` and `targetDestroyed`.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<TargetInfoChangedEventArgs> TargetInfoChanged;

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class TargetInfo {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: TargetID</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("targetId")]
			public string TargetId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("title")]
			public string Title;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Whether the target has an attached client.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("attached")]
			public bool Attached;

			/// <summary>
			/// Opener target Id
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: TargetID</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("openerId")]
			public string OpenerId;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: BrowserContextID</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("browserContextId")]
			public string BrowserContextId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class RemoteLocation {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("host")]
			public string Host;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("port")]
			public int Port;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Activates (focuses) the target.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="targetId"></param>
		public void ActivateTarget(string targetId) {
			_chrome.Send(
				"Target.activateTarget",
				new {
					targetId
				}
			);
		}

		/// <summary>
		/// Attaches to the target with given id.
		/// </summary>
		[JsonObject]
		public class AttachToTargetResult {
			/// <summary>
			/// Id assigned to the session.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SessionID</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sessionId")]
			public string SessionId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Attaches to the target with given id.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="targetId"></param>
		/// <param name="flatten">Enables "flat" access to the session via specifying sessionId attribute in the commands. We plan to make this the default, deprecate non-flattened mode, and eventually retire it. See crbug.com/991325.</param>
		public AttachToTargetResult AttachToTarget(string targetId, bool? flatten = null) {
			string s = _chrome.Send(
				"Target.attachToTarget",
				new {
					targetId,
					flatten
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<AttachToTargetResult>();
		}

		/// <summary>
		/// Attaches to the browser target, only uses flat sessionId mode.
		/// </summary>
		[JsonObject]
		public class AttachToBrowserTargetResult {
			/// <summary>
			/// Id assigned to the session.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: SessionID</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("sessionId")]
			public string SessionId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Attaches to the browser target, only uses flat sessionId mode.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public AttachToBrowserTargetResult AttachToBrowserTarget() {
			string s = _chrome.Send(
				"Target.attachToBrowserTarget"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<AttachToBrowserTargetResult>();
		}

		/// <summary>
		/// Closes the target. If the target is a page that gets closed too.
		/// </summary>
		[JsonObject]
		public class CloseTargetResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("success")]
			public bool Success;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Closes the target. If the target is a page that gets closed too.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="targetId"></param>
		public CloseTargetResult CloseTarget(string targetId) {
			string s = _chrome.Send(
				"Target.closeTarget",
				new {
					targetId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<CloseTargetResult>();
		}

		/// <summary>
		/// Inject object to the target's main frame that provides a communication channel with browser target.  Injected object will be available as `window[bindingName]`.  The object has the follwing API: - `binding.send(json)` - a method to send messages over the remote debugging protocol - `binding.onmessage = json =&gt; handleMessage(json)` - a callback that will be called for the protocol notifications and command responses.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="targetId"></param>
		/// <param name="bindingName">Binding name, 'cdp' if not specified.</param>
		public void ExposeDevToolsProtocol(string targetId, string bindingName = null) {
			_chrome.Send(
				"Target.exposeDevToolsProtocol",
				new {
					targetId,
					bindingName
				}
			);
		}

		/// <summary>
		/// Creates a new empty BrowserContext. Similar to an incognito profile but you can have more than one.
		/// </summary>
		[JsonObject]
		public class CreateBrowserContextResult {
			/// <summary>
			/// The id of the context created.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: BrowserContextID</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("browserContextId")]
			public string BrowserContextId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Creates a new empty BrowserContext. Similar to an incognito profile but you can have more than one.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public CreateBrowserContextResult CreateBrowserContext() {
			string s = _chrome.Send(
				"Target.createBrowserContext"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<CreateBrowserContextResult>();
		}

		/// <summary>
		/// Returns all browser contexts created with `Target.createBrowserContext` method.
		/// </summary>
		[JsonObject]
		public class GetBrowserContextsResult {
			/// <summary>
			/// An array of browser context ids.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("browserContextIds")]
			public string[] BrowserContextIds;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns all browser contexts created with `Target.createBrowserContext` method.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public GetBrowserContextsResult GetBrowserContexts() {
			string s = _chrome.Send(
				"Target.getBrowserContexts"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetBrowserContextsResult>();
		}

		/// <summary>
		/// Creates a new page.
		/// </summary>
		[JsonObject]
		public class CreateTargetResult {
			/// <summary>
			/// The id of the page opened.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: TargetID</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("targetId")]
			public string TargetId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Creates a new page.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="url">The initial URL the page will be navigated to.</param>
		/// <param name="width">Frame width in DIP (headless chrome only).</param>
		/// <param name="height">Frame height in DIP (headless chrome only).</param>
		/// <param name="browserContextId">The browser context to create the page in.</param>
		/// <param name="enableBeginFrameControl">Whether BeginFrames for this target will be controlled via DevTools (headless chrome only, not supported on MacOS yet, false by default).</param>
		/// <param name="newWindow">Whether to create a new Window or Tab (chrome-only, false by default).</param>
		/// <param name="background">Whether to create the target in background or foreground (chrome-only, false by default).</param>
		public CreateTargetResult CreateTarget(string url, int? width = null, int? height = null, string browserContextId = null, bool? enableBeginFrameControl = null, bool? newWindow = null, bool? background = null) {
			string s = _chrome.Send(
				"Target.createTarget",
				new {
					url,
					width,
					height,
					browserContextId,
					enableBeginFrameControl,
					newWindow,
					background
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<CreateTargetResult>();
		}

		/// <summary>
		/// Detaches session with given id.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="sessionId">Session to detach.</param>
		/// <param name="targetId">Deprecated.</param>
		public void DetachFromTarget(string sessionId = null, string targetId = null) {
			_chrome.Send(
				"Target.detachFromTarget",
				new {
					sessionId,
					targetId
				}
			);
		}

		/// <summary>
		/// Deletes a BrowserContext. All the belonging pages will be closed without calling their beforeunload hooks.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="browserContextId"></param>
		public void DisposeBrowserContext(string browserContextId) {
			_chrome.Send(
				"Target.disposeBrowserContext",
				new {
					browserContextId
				}
			);
		}

		/// <summary>
		/// Returns information about a target.
		/// </summary>
		[JsonObject]
		public class GetTargetInfoResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: TargetInfo</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("targetInfo")]
			public TargetInfo TargetInfo;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns information about a target.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="targetId"></param>
		public GetTargetInfoResult GetTargetInfo(string targetId = null) {
			string s = _chrome.Send(
				"Target.getTargetInfo",
				new {
					targetId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetTargetInfoResult>();
		}

		/// <summary>
		/// Retrieves a list of available targets.
		/// </summary>
		[JsonObject]
		public class GetTargetsResult {
			/// <summary>
			/// The list of targets.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("targetInfos")]
			public TargetInfo[] TargetInfos;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Retrieves a list of available targets.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public GetTargetsResult GetTargets() {
			string s = _chrome.Send(
				"Target.getTargets"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetTargetsResult>();
		}

		/// <summary>
		/// Sends protocol message over session with given id. Consider using flat mode instead; see commands attachToTarget, setAutoAttach, and crbug.com/991325.
		/// <list type="bullet">
		/// <item><description>deprecated: true</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="message"></param>
		/// <param name="sessionId">Identifier of the session.</param>
		/// <param name="targetId">Deprecated.</param>
		[Obsolete]
		public void SendMessageToTarget(string message, string sessionId = null, string targetId = null) {
			_chrome.Send(
				"Target.sendMessageToTarget",
				new {
					message,
					sessionId,
					targetId
				}
			);
		}

		/// <summary>
		/// Controls whether to automatically attach to new targets which are considered to be related to this one. When turned on, attaches to all existing related targets as well. When turned off, automatically detaches from all currently attached targets.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="autoAttach">Whether to auto-attach to related targets.</param>
		/// <param name="waitForDebuggerOnStart">Whether to pause new targets when attaching to them. Use `Runtime.runIfWaitingForDebugger` to run paused targets.</param>
		/// <param name="flatten">Enables "flat" access to the session via specifying sessionId attribute in the commands. We plan to make this the default, deprecate non-flattened mode, and eventually retire it. See crbug.com/991325.</param>
		/// <param name="windowOpen">Auto-attach to the targets created via window.open from current target.</param>
		public void SetAutoAttach(bool autoAttach, bool waitForDebuggerOnStart, bool? flatten = null, bool? windowOpen = null) {
			_chrome.Send(
				"Target.setAutoAttach",
				new {
					autoAttach,
					waitForDebuggerOnStart,
					flatten,
					windowOpen
				}
			);
		}

		/// <summary>
		/// Controls whether to discover available targets and notify via `targetCreated/targetInfoChanged/targetDestroyed` events.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="discover">Whether to discover available targets.</param>
		public void SetDiscoverTargets(bool discover) {
			_chrome.Send(
				"Target.setDiscoverTargets",
				new {
					discover
				}
			);
		}

		/// <summary>
		/// Enables target discovery for the specified locations, when `setDiscoverTargets` was set to `true`.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="locations">List of remote locations.</param>
		public void SetRemoteLocations(RemoteLocation[] locations) {
			_chrome.Send(
				"Target.setRemoteLocations",
				new {
					locations
				}
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("Target.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "attachedToTarget":
					if (AttachedToTarget == null) {
						return;
					}
					AttachedToTarget(
						_chrome,
						JsonConvert.DeserializeObject
							<AttachedToTargetEventArgs>(@params.ToString())
					);
					break;
				case "detachedFromTarget":
					if (DetachedFromTarget == null) {
						return;
					}
					DetachedFromTarget(
						_chrome,
						JsonConvert.DeserializeObject
							<DetachedFromTargetEventArgs>(@params.ToString())
					);
					break;
				case "receivedMessageFromTarget":
					if (ReceivedMessageFromTarget == null) {
						return;
					}
					ReceivedMessageFromTarget(
						_chrome,
						JsonConvert.DeserializeObject
							<ReceivedMessageFromTargetEventArgs>(@params.ToString())
					);
					break;
				case "targetCreated":
					if (TargetCreated == null) {
						return;
					}
					TargetCreated(
						_chrome,
						JsonConvert.DeserializeObject
							<TargetCreatedEventArgs>(@params.ToString())
					);
					break;
				case "targetDestroyed":
					if (TargetDestroyed == null) {
						return;
					}
					TargetDestroyed(
						_chrome,
						JsonConvert.DeserializeObject
							<TargetDestroyedEventArgs>(@params.ToString())
					);
					break;
				case "targetCrashed":
					if (TargetCrashed == null) {
						return;
					}
					TargetCrashed(
						_chrome,
						JsonConvert.DeserializeObject
							<TargetCrashedEventArgs>(@params.ToString())
					);
					break;
				case "targetInfoChanged":
					if (TargetInfoChanged == null) {
						return;
					}
					TargetInfoChanged(
						_chrome,
						JsonConvert.DeserializeObject
							<TargetInfoChangedEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
