// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// Runtime domain exposes JavaScript runtime by means of remote evaluation and mirror objects. Evaluation results are returned as mirror object that expose object type, string representation and unique identifier that can be used for further object reference. Original objects are maintained in memory unless they are either explicitly released or are released along with the other objects in their object group.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: false</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class RuntimeDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the RuntimeDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public RuntimeDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Notification is issued every time when binding is called.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class BindingCalledEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("payload")]
			public string Payload;

			/// <summary>
			/// Identifier of the context where the call was made.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ExecutionContextId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("executionContextId")]
			public int ExecutionContextId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Notification is issued every time when binding is called.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<BindingCalledEventArgs> BindingCalled;

		/// <summary>
		/// Issued when console API was called.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ConsoleAPICalledEventArgs : EventArgs {
			/// <summary>
			/// Type of the call.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: log, debug, info, error, warning, dir, dirxml, table, trace, clear, startGroup, startGroupCollapsed, endGroup, assert, profile, profileEnd, count, timeEnd
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// Call arguments.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("args")]
			public RemoteObject[] Args;

			/// <summary>
			/// Identifier of the context where the call was made.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ExecutionContextId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("executionContextId")]
			public int ExecutionContextId;

			/// <summary>
			/// Call timestamp.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Timestamp</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// Stack trace captured when the call was made. The async stack chain is automatically reported for the following call types: `assert`, `error`, `trace`, `warning`. For other types the async call chain can be retrieved using `Debugger.getStackTrace` and `stackTrace.parentId` field.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: StackTrace</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("stackTrace")]
			public StackTrace StackTrace;

			/// <summary>
			/// Console context descriptor for calls on non-default console context (not console.*): 'anonymous#unique-logger-id' for call on unnamed context, 'name#unique-logger-id' for call on named context.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("context")]
			public string Context;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Issued when console API was called.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ConsoleAPICalledEventArgs> ConsoleAPICalled;

		/// <summary>
		/// Issued when unhandled exception was revoked.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ExceptionRevokedEventArgs : EventArgs {
			/// <summary>
			/// Reason describing why exception was revoked.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("reason")]
			public string Reason;

			/// <summary>
			/// The id of revoked exception, as reported in `exceptionThrown`.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("exceptionId")]
			public int ExceptionId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Issued when unhandled exception was revoked.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ExceptionRevokedEventArgs> ExceptionRevoked;

		/// <summary>
		/// Issued when exception was thrown and unhandled.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ExceptionThrownEventArgs : EventArgs {
			/// <summary>
			/// Timestamp of the exception.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Timestamp</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ExceptionDetails</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("exceptionDetails")]
			public ExceptionDetails ExceptionDetails;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Issued when exception was thrown and unhandled.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ExceptionThrownEventArgs> ExceptionThrown;

		/// <summary>
		/// Issued when new execution context is created.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ExecutionContextCreatedEventArgs : EventArgs {
			/// <summary>
			/// A newly created execution context.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ExecutionContextDescription</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("context")]
			public ExecutionContextDescription Context;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Issued when new execution context is created.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ExecutionContextCreatedEventArgs> ExecutionContextCreated;

		/// <summary>
		/// Issued when execution context is destroyed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ExecutionContextDestroyedEventArgs : EventArgs {
			/// <summary>
			/// Id of the destroyed context
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ExecutionContextId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("executionContextId")]
			public int ExecutionContextId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Issued when execution context is destroyed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ExecutionContextDestroyedEventArgs> ExecutionContextDestroyed;

		/// <summary>
		/// Issued when all executionContexts were cleared in browser
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler ExecutionContextsCleared;

		/// <summary>
		/// Issued when object should be inspected (for example, as a result of inspect() command line API call).
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class InspectRequestedEventArgs : EventArgs {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("object")]
			public RemoteObject Object;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: object</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("hints")]
			public object Hints;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Issued when object should be inspected (for example, as a result of inspect() command line API call).
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<InspectRequestedEventArgs> InspectRequested;

		/// <summary>
		/// Mirror object referencing original JavaScript object.
		/// </summary>
		[JsonObject]
		public class RemoteObject {
			/// <summary>
			/// Object type.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: object, function, undefined, string, number, boolean, symbol, bigint
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// Object subtype hint. Specified for `object` type values only.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: array, null, node, regexp, date, map, set, weakmap, weakset, iterator, generator, error, proxy, promise, typedarray, arraybuffer, dataview
			/// </summary>
			[JsonProperty("subtype")]
			public string Subtype;

			/// <summary>
			/// Object class (constructor) name. Specified for `object` type values only.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("className")]
			public string ClassName;

			/// <summary>
			/// Remote object value in case of primitive values or JSON values (if it was requested).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: any</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public object Value;

			/// <summary>
			/// Primitive value which can not be JSON-stringified does not have `value`, but gets this property.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: UnserializableValue</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("unserializableValue")]
			public string UnserializableValue;

			/// <summary>
			/// String representation of the object.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("description")]
			public string Description;

			/// <summary>
			/// Unique object identifier (for non-primitive values).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: RemoteObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("objectId")]
			public string ObjectId;

			/// <summary>
			/// Preview containing abbreviated property values. Specified for `object` type values only.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: ObjectPreview</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("preview")]
			public ObjectPreview Preview;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: CustomPreview</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("customPreview")]
			public CustomPreview CustomPreview;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class CustomPreview {
			/// <summary>
			/// The JSON-stringified result of formatter.header(object, config) call. It contains json ML array that represents RemoteObject.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("header")]
			public string Header;

			/// <summary>
			/// If formatter returns true as a result of formatter.hasBody call then bodyGetterId will contain RemoteObjectId for the function that returns result of formatter.body(object, config) call. The result value is json ML array.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: RemoteObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("bodyGetterId")]
			public string BodyGetterId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Object containing abbreviated remote object value.
		/// </summary>
		[JsonObject]
		public class ObjectPreview {
			/// <summary>
			/// Object type.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: object, function, undefined, string, number, boolean, symbol, bigint
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// Object subtype hint. Specified for `object` type values only.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: array, null, node, regexp, date, map, set, weakmap, weakset, iterator, generator, error
			/// </summary>
			[JsonProperty("subtype")]
			public string Subtype;

			/// <summary>
			/// String representation of the object.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("description")]
			public string Description;

			/// <summary>
			/// True iff some of the properties or entries of the original object did not fit.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("overflow")]
			public bool Overflow;

			/// <summary>
			/// List of the properties.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("properties")]
			public PropertyPreview[] Properties;

			/// <summary>
			/// List of the entries. Specified for `map` and `set` subtype values only.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("entries")]
			public EntryPreview[] Entries;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class PropertyPreview {
			/// <summary>
			/// Property name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// Object type. Accessor means that the property itself is an accessor property.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: object, function, undefined, string, number, boolean, symbol, accessor, bigint
			/// </summary>
			[JsonProperty("type")]
			public string Type;

			/// <summary>
			/// User-friendly property value string.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public string Value;

			/// <summary>
			/// Nested value preview.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: ObjectPreview</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("valuePreview")]
			public ObjectPreview ValuePreview;

			/// <summary>
			/// Object subtype hint. Specified for `object` type values only.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: array, null, node, regexp, date, map, set, weakmap, weakset, iterator, generator, error
			/// </summary>
			[JsonProperty("subtype")]
			public string Subtype;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class EntryPreview {
			/// <summary>
			/// Preview of the key. Specified for map-like collection entries.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: ObjectPreview</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("key")]
			public ObjectPreview Key;

			/// <summary>
			/// Preview of the value.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ObjectPreview</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public ObjectPreview Value;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Object property descriptor.
		/// </summary>
		[JsonObject]
		public class PropertyDescriptor {
			/// <summary>
			/// Property name or symbol description.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// The value associated with the property.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public RemoteObject Value;

			/// <summary>
			/// True if the value associated with the property may be changed (data descriptors only).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("writable")]
			public bool Writable;

			/// <summary>
			/// A function which serves as a getter for the property, or `undefined` if there is no getter (accessor descriptors only).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("get")]
			public RemoteObject Get;

			/// <summary>
			/// A function which serves as a setter for the property, or `undefined` if there is no setter (accessor descriptors only).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("set")]
			public RemoteObject Set;

			/// <summary>
			/// True if the type of this property descriptor may be changed and if the property may be deleted from the corresponding object.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("configurable")]
			public bool Configurable;

			/// <summary>
			/// True if this property shows up during enumeration of the properties on the corresponding object.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("enumerable")]
			public bool Enumerable;

			/// <summary>
			/// True if the result was thrown during the evaluation.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("wasThrown")]
			public bool WasThrown;

			/// <summary>
			/// True if the property is owned for the object.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("isOwn")]
			public bool IsOwn;

			/// <summary>
			/// Property symbol object, if the property is of the `symbol` type.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("symbol")]
			public RemoteObject Symbol;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Object internal property descriptor. This property isn't normally visible in JavaScript code.
		/// </summary>
		[JsonObject]
		public class InternalPropertyDescriptor {
			/// <summary>
			/// Conventional property name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// The value associated with the property.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public RemoteObject Value;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Object private field descriptor.
		/// </summary>
		[JsonObject]
		public class PrivatePropertyDescriptor {
			/// <summary>
			/// Private property name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// The value associated with the private property.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public RemoteObject Value;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Represents function call argument. Either remote object id `objectId`, primitive `value`, unserializable primitive value or neither of (for undefined) them should be specified.
		/// </summary>
		[JsonObject]
		public class CallArgument {
			/// <summary>
			/// Primitive value or serializable javascript object.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: any</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public object Value;

			/// <summary>
			/// Primitive value which can not be JSON-stringified.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: UnserializableValue</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("unserializableValue")]
			public string UnserializableValue;

			/// <summary>
			/// Remote object handle.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: RemoteObjectId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("objectId")]
			public string ObjectId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Description of an isolated world.
		/// </summary>
		[JsonObject]
		public class ExecutionContextDescription {
			/// <summary>
			/// Unique id of the execution context. It can be used to specify in which execution context script evaluation should be performed.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ExecutionContextId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("id")]
			public int Id;

			/// <summary>
			/// Execution context origin.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("origin")]
			public string Origin;

			/// <summary>
			/// Human readable name describing given context.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// Embedder-specific auxiliary data.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: object</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("auxData")]
			public object AuxData;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Detailed information about exception (or error) that was thrown during script compilation or execution.
		/// </summary>
		[JsonObject]
		public class ExceptionDetails {
			/// <summary>
			/// Exception id.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("exceptionId")]
			public int ExceptionId;

			/// <summary>
			/// Exception text, which should be used together with exception object when available.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("text")]
			public string Text;

			/// <summary>
			/// Line number of the exception location (0-based).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("lineNumber")]
			public int LineNumber;

			/// <summary>
			/// Column number of the exception location (0-based).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("columnNumber")]
			public int ColumnNumber;

			/// <summary>
			/// Script ID of the exception location.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: ScriptId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scriptId")]
			public string ScriptId;

			/// <summary>
			/// URL of the exception location, to be used when the script was not reported.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// JavaScript stack trace if available.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: StackTrace</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("stackTrace")]
			public StackTrace StackTrace;

			/// <summary>
			/// Exception object if available.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("exception")]
			public RemoteObject Exception;

			/// <summary>
			/// Identifier of the context where exception happened.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: ExecutionContextId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("executionContextId")]
			public int ExecutionContextId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Stack entry for runtime errors and assertions.
		/// </summary>
		[JsonObject]
		public class CallFrame {
			/// <summary>
			/// JavaScript function name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("functionName")]
			public string FunctionName;

			/// <summary>
			/// JavaScript script id.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: ScriptId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scriptId")]
			public string ScriptId;

			/// <summary>
			/// JavaScript script name or url.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// JavaScript script line number (0-based).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("lineNumber")]
			public int LineNumber;

			/// <summary>
			/// JavaScript script column number (0-based).
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("columnNumber")]
			public int ColumnNumber;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Call frames for assertions or error messages.
		/// </summary>
		[JsonObject]
		public class StackTrace {
			/// <summary>
			/// String label of this stack trace. For async traces this may be a name of the function that initiated the async call.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("description")]
			public string Description;

			/// <summary>
			/// JavaScript function name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("callFrames")]
			public CallFrame[] CallFrames;

			/// <summary>
			/// Asynchronous JavaScript stack trace that preceded this stack, if available.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: StackTrace</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("parent")]
			public StackTrace Parent;

			/// <summary>
			/// Asynchronous JavaScript stack trace that preceded this stack, if available.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: StackTraceId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("parentId")]
			public StackTraceId ParentId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// If `debuggerId` is set stack trace comes from another debugger and can be resolved there. This allows to track cross-debugger calls. See `Runtime.StackTrace` and `Debugger.paused` for usages.
		/// </summary>
		[JsonObject]
		public class StackTraceId {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("id")]
			public string Id;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: UniqueDebuggerId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("debuggerId")]
			public string DebuggerId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Add handler to promise with given promise object id.
		/// </summary>
		[JsonObject]
		public class AwaitPromiseResult {
			/// <summary>
			/// Promise result. Will contain rejected value if promise was rejected.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("result")]
			public RemoteObject Result;

			/// <summary>
			/// Exception details if stack strace is available.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: ExceptionDetails</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("exceptionDetails")]
			public ExceptionDetails ExceptionDetails;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Add handler to promise with given promise object id.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="promiseObjectId">Identifier of the promise.</param>
		/// <param name="returnByValue">Whether the result is expected to be a JSON object that should be sent by value.</param>
		/// <param name="generatePreview">Whether preview should be generated for the result.</param>
		public AwaitPromiseResult AwaitPromise(string promiseObjectId, bool? returnByValue = null, bool? generatePreview = null) {
			string s = _chrome.Send(
				"Runtime.awaitPromise",
				new {
					promiseObjectId,
					returnByValue,
					generatePreview
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<AwaitPromiseResult>();
		}

		/// <summary>
		/// Calls function with given declaration on the given object. Object group of the result is inherited from the target object.
		/// </summary>
		[JsonObject]
		public class CallFunctionOnResult {
			/// <summary>
			/// Call result.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("result")]
			public RemoteObject Result;

			/// <summary>
			/// Exception details.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: ExceptionDetails</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("exceptionDetails")]
			public ExceptionDetails ExceptionDetails;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Calls function with given declaration on the given object. Object group of the result is inherited from the target object.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="functionDeclaration">Declaration of the function to call.</param>
		/// <param name="objectId">Identifier of the object to call function on. Either objectId or executionContextId should be specified.</param>
		/// <param name="arguments">Call arguments. All call arguments must belong to the same JavaScript world as the target object.</param>
		/// <param name="silent">In silent mode exceptions thrown during evaluation are not reported and do not pause execution. Overrides `setPauseOnException` state.</param>
		/// <param name="returnByValue">Whether the result is expected to be a JSON object which should be sent by value.</param>
		/// <param name="generatePreview">Whether preview should be generated for the result.</param>
		/// <param name="userGesture">Whether execution should be treated as initiated by user in the UI.</param>
		/// <param name="awaitPromise">Whether execution should `await` for resulting value and return once awaited promise is resolved.</param>
		/// <param name="executionContextId">Specifies execution context which global object will be used to call function on. Either executionContextId or objectId should be specified.</param>
		/// <param name="objectGroup">Symbolic group name that can be used to release multiple objects. If objectGroup is not specified and objectId is, objectGroup will be inherited from object.</param>
		public CallFunctionOnResult CallFunctionOn(string functionDeclaration, string objectId = null, CallArgument[] arguments = null, bool? silent = null, bool? returnByValue = null, bool? generatePreview = null, bool? userGesture = null, bool? awaitPromise = null, int? executionContextId = null, string objectGroup = null) {
			string s = _chrome.Send(
				"Runtime.callFunctionOn",
				new {
					functionDeclaration,
					objectId,
					arguments,
					silent,
					returnByValue,
					generatePreview,
					userGesture,
					awaitPromise,
					executionContextId,
					objectGroup
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<CallFunctionOnResult>();
		}

		/// <summary>
		/// Compiles expression.
		/// </summary>
		[JsonObject]
		public class CompileScriptResult {
			/// <summary>
			/// Id of the script.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: ScriptId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("scriptId")]
			public string ScriptId;

			/// <summary>
			/// Exception details.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: ExceptionDetails</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("exceptionDetails")]
			public ExceptionDetails ExceptionDetails;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Compiles expression.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="expression">Expression to compile.</param>
		/// <param name="sourceURL">Source url to be set for the script.</param>
		/// <param name="persistScript">Specifies whether the compiled script should be persisted.</param>
		/// <param name="executionContextId">Specifies in which execution context to perform script run. If the parameter is omitted the evaluation will be performed in the context of the inspected page.</param>
		public CompileScriptResult CompileScript(string expression, string sourceURL, bool persistScript, int? executionContextId = null) {
			string s = _chrome.Send(
				"Runtime.compileScript",
				new {
					expression,
					sourceURL,
					persistScript,
					executionContextId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<CompileScriptResult>();
		}

		/// <summary>
		/// Disables reporting of execution contexts creation.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"Runtime.disable"
			);
		}

		/// <summary>
		/// Discards collected exceptions and console API calls.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void DiscardConsoleEntries() {
			_chrome.Send(
				"Runtime.discardConsoleEntries"
			);
		}

		/// <summary>
		/// Enables reporting of execution contexts creation by means of `executionContextCreated` event. When the reporting gets enabled the event will be sent immediately for each existing execution context.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"Runtime.enable"
			);
		}

		/// <summary>
		/// Evaluates expression on global object.
		/// </summary>
		[JsonObject]
		public class EvaluateResult {
			/// <summary>
			/// Evaluation result.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("result")]
			public RemoteObject Result;

			/// <summary>
			/// Exception details.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: ExceptionDetails</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("exceptionDetails")]
			public ExceptionDetails ExceptionDetails;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Evaluates expression on global object.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="expression">Expression to evaluate.</param>
		/// <param name="objectGroup">Symbolic group name that can be used to release multiple objects.</param>
		/// <param name="includeCommandLineAPI">Determines whether Command Line API should be available during the evaluation.</param>
		/// <param name="silent">In silent mode exceptions thrown during evaluation are not reported and do not pause execution. Overrides `setPauseOnException` state.</param>
		/// <param name="contextId">Specifies in which execution context to perform evaluation. If the parameter is omitted the evaluation will be performed in the context of the inspected page.</param>
		/// <param name="returnByValue">Whether the result is expected to be a JSON object that should be sent by value.</param>
		/// <param name="generatePreview">Whether preview should be generated for the result.</param>
		/// <param name="userGesture">Whether execution should be treated as initiated by user in the UI.</param>
		/// <param name="awaitPromise">Whether execution should `await` for resulting value and return once awaited promise is resolved.</param>
		/// <param name="throwOnSideEffect">Whether to throw an exception if side effect cannot be ruled out during evaluation. This implies `disableBreaks` below.</param>
		/// <param name="timeout">Terminate execution after timing out (number of milliseconds).</param>
		/// <param name="disableBreaks">Disable breakpoints during execution.</param>
		public EvaluateResult Evaluate(string expression, string objectGroup = null, bool? includeCommandLineAPI = null, bool? silent = null, int? contextId = null, bool? returnByValue = null, bool? generatePreview = null, bool? userGesture = null, bool? awaitPromise = null, bool? throwOnSideEffect = null, double? timeout = null, bool? disableBreaks = null) {
			string s = _chrome.Send(
				"Runtime.evaluate",
				new {
					expression,
					objectGroup,
					includeCommandLineAPI,
					silent,
					contextId,
					returnByValue,
					generatePreview,
					userGesture,
					awaitPromise,
					throwOnSideEffect,
					timeout,
					disableBreaks
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<EvaluateResult>();
		}

		/// <summary>
		/// Returns the isolate id.
		/// </summary>
		[JsonObject]
		public class GetIsolateIdResult {
			/// <summary>
			/// The isolate id.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("id")]
			public string Id;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns the isolate id.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public GetIsolateIdResult GetIsolateId() {
			string s = _chrome.Send(
				"Runtime.getIsolateId"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetIsolateIdResult>();
		}

		/// <summary>
		/// Returns the JavaScript heap usage. It is the total usage of the corresponding isolate not scoped to a particular Runtime.
		/// </summary>
		[JsonObject]
		public class GetHeapUsageResult {
			/// <summary>
			/// Used heap size in bytes.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("usedSize")]
			public double UsedSize;

			/// <summary>
			/// Allocated heap size in bytes.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("totalSize")]
			public double TotalSize;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns the JavaScript heap usage. It is the total usage of the corresponding isolate not scoped to a particular Runtime.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public GetHeapUsageResult GetHeapUsage() {
			string s = _chrome.Send(
				"Runtime.getHeapUsage"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetHeapUsageResult>();
		}

		/// <summary>
		/// Returns properties of a given object. Object group of the result is inherited from the target object.
		/// </summary>
		[JsonObject]
		public class GetPropertiesResult {
			/// <summary>
			/// Object properties.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("result")]
			public PropertyDescriptor[] Result;

			/// <summary>
			/// Internal object properties (only of the element itself).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("internalProperties")]
			public InternalPropertyDescriptor[] InternalProperties;

			/// <summary>
			/// Object private properties.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("privateProperties")]
			public PrivatePropertyDescriptor[] PrivateProperties;

			/// <summary>
			/// Exception details.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: ExceptionDetails</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("exceptionDetails")]
			public ExceptionDetails ExceptionDetails;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns properties of a given object. Object group of the result is inherited from the target object.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="objectId">Identifier of the object to return properties for.</param>
		/// <param name="ownProperties">If true, returns properties belonging only to the element itself, not to its prototype chain.</param>
		/// <param name="accessorPropertiesOnly">If true, returns accessor properties (with getter/setter) only; internal properties are not returned either.</param>
		/// <param name="generatePreview">Whether preview should be generated for the results.</param>
		public GetPropertiesResult GetProperties(string objectId, bool? ownProperties = null, bool? accessorPropertiesOnly = null, bool? generatePreview = null) {
			string s = _chrome.Send(
				"Runtime.getProperties",
				new {
					objectId,
					ownProperties,
					accessorPropertiesOnly,
					generatePreview
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetPropertiesResult>();
		}

		/// <summary>
		/// Returns all let, const and class variables from global scope.
		/// </summary>
		[JsonObject]
		public class GlobalLexicalScopeNamesResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("names")]
			public string[] Names;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns all let, const and class variables from global scope.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="executionContextId">Specifies in which execution context to lookup global scope variables.</param>
		public GlobalLexicalScopeNamesResult GlobalLexicalScopeNames(int? executionContextId = null) {
			string s = _chrome.Send(
				"Runtime.globalLexicalScopeNames",
				new {
					executionContextId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GlobalLexicalScopeNamesResult>();
		}

		/// <summary>
		/// 
		/// </summary>
		[JsonObject]
		public class QueryObjectsResult {
			/// <summary>
			/// Array with objects.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("objects")]
			public RemoteObject Objects;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="prototypeObjectId">Identifier of the prototype to return objects for.</param>
		/// <param name="objectGroup">Symbolic group name that can be used to release the results.</param>
		public QueryObjectsResult QueryObjects(string prototypeObjectId, string objectGroup = null) {
			string s = _chrome.Send(
				"Runtime.queryObjects",
				new {
					prototypeObjectId,
					objectGroup
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<QueryObjectsResult>();
		}

		/// <summary>
		/// Releases remote object with given id.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="objectId">Identifier of the object to release.</param>
		public void ReleaseObject(string objectId) {
			_chrome.Send(
				"Runtime.releaseObject",
				new {
					objectId
				}
			);
		}

		/// <summary>
		/// Releases all remote objects that belong to a given group.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="objectGroup">Symbolic object group name.</param>
		public void ReleaseObjectGroup(string objectGroup) {
			_chrome.Send(
				"Runtime.releaseObjectGroup",
				new {
					objectGroup
				}
			);
		}

		/// <summary>
		/// Tells inspected instance to run if it was waiting for debugger to attach.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void RunIfWaitingForDebugger() {
			_chrome.Send(
				"Runtime.runIfWaitingForDebugger"
			);
		}

		/// <summary>
		/// Runs script with given id in a given context.
		/// </summary>
		[JsonObject]
		public class RunScriptResult {
			/// <summary>
			/// Run result.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("result")]
			public RemoteObject Result;

			/// <summary>
			/// Exception details.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: ExceptionDetails</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("exceptionDetails")]
			public ExceptionDetails ExceptionDetails;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Runs script with given id in a given context.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="scriptId">Id of the script to run.</param>
		/// <param name="executionContextId">Specifies in which execution context to perform script run. If the parameter is omitted the evaluation will be performed in the context of the inspected page.</param>
		/// <param name="objectGroup">Symbolic group name that can be used to release multiple objects.</param>
		/// <param name="silent">In silent mode exceptions thrown during evaluation are not reported and do not pause execution. Overrides `setPauseOnException` state.</param>
		/// <param name="includeCommandLineAPI">Determines whether Command Line API should be available during the evaluation.</param>
		/// <param name="returnByValue">Whether the result is expected to be a JSON object which should be sent by value.</param>
		/// <param name="generatePreview">Whether preview should be generated for the result.</param>
		/// <param name="awaitPromise">Whether execution should `await` for resulting value and return once awaited promise is resolved.</param>
		public RunScriptResult RunScript(string scriptId, int? executionContextId = null, string objectGroup = null, bool? silent = null, bool? includeCommandLineAPI = null, bool? returnByValue = null, bool? generatePreview = null, bool? awaitPromise = null) {
			string s = _chrome.Send(
				"Runtime.runScript",
				new {
					scriptId,
					executionContextId,
					objectGroup,
					silent,
					includeCommandLineAPI,
					returnByValue,
					generatePreview,
					awaitPromise
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<RunScriptResult>();
		}

		/// <summary>
		/// Enables or disables async call stacks tracking.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="maxDepth">Maximum depth of async call stacks. Setting to `0` will effectively disable collecting async call stacks (default).</param>
		public void SetAsyncCallStackDepth(int maxDepth) {
			_chrome.Send(
				"Runtime.setAsyncCallStackDepth",
				new {
					maxDepth
				}
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="enabled"></param>
		public void SetCustomObjectFormatterEnabled(bool enabled) {
			_chrome.Send(
				"Runtime.setCustomObjectFormatterEnabled",
				new {
					enabled
				}
			);
		}

		/// <summary>
		/// 
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="size"></param>
		public void SetMaxCallStackSizeToCapture(int size) {
			_chrome.Send(
				"Runtime.setMaxCallStackSizeToCapture",
				new {
					size
				}
			);
		}

		/// <summary>
		/// Terminate current or next JavaScript execution. Will cancel the termination when the outer-most script execution ends.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public void TerminateExecution() {
			_chrome.Send(
				"Runtime.terminateExecution"
			);
		}

		/// <summary>
		/// If executionContextId is empty, adds binding with the given name on the global objects of all inspected contexts, including those created later, bindings survive reloads. If executionContextId is specified, adds binding only on global object of given execution context. Binding function takes exactly one argument, this argument should be string, in case of any other input, function throws an exception. Each binding function call produces Runtime.bindingCalled notification.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="name"></param>
		/// <param name="executionContextId"></param>
		public void AddBinding(string name, int? executionContextId = null) {
			_chrome.Send(
				"Runtime.addBinding",
				new {
					name,
					executionContextId
				}
			);
		}

		/// <summary>
		/// This method does not remove binding function from global object but unsubscribes current runtime agent from Runtime.bindingCalled notifications.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="name"></param>
		public void RemoveBinding(string name) {
			_chrome.Send(
				"Runtime.removeBinding",
				new {
					name
				}
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("Runtime.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "bindingCalled":
					if (BindingCalled == null) {
						return;
					}
					BindingCalled(
						_chrome,
						JsonConvert.DeserializeObject
							<BindingCalledEventArgs>(@params.ToString())
					);
					break;
				case "consoleAPICalled":
					if (ConsoleAPICalled == null) {
						return;
					}
					ConsoleAPICalled(
						_chrome,
						JsonConvert.DeserializeObject
							<ConsoleAPICalledEventArgs>(@params.ToString())
					);
					break;
				case "exceptionRevoked":
					if (ExceptionRevoked == null) {
						return;
					}
					ExceptionRevoked(
						_chrome,
						JsonConvert.DeserializeObject
							<ExceptionRevokedEventArgs>(@params.ToString())
					);
					break;
				case "exceptionThrown":
					if (ExceptionThrown == null) {
						return;
					}
					ExceptionThrown(
						_chrome,
						JsonConvert.DeserializeObject
							<ExceptionThrownEventArgs>(@params.ToString())
					);
					break;
				case "executionContextCreated":
					if (ExecutionContextCreated == null) {
						return;
					}
					ExecutionContextCreated(
						_chrome,
						JsonConvert.DeserializeObject
							<ExecutionContextCreatedEventArgs>(@params.ToString())
					);
					break;
				case "executionContextDestroyed":
					if (ExecutionContextDestroyed == null) {
						return;
					}
					ExecutionContextDestroyed(
						_chrome,
						JsonConvert.DeserializeObject
							<ExecutionContextDestroyedEventArgs>(@params.ToString())
					);
					break;
				case "executionContextsCleared":
					if (ExecutionContextsCleared == null) {
						return;
					}
					ExecutionContextsCleared(_chrome, EventArgs.Empty);
					break;
				case "inspectRequested":
					if (InspectRequested == null) {
						return;
					}
					InspectRequested(
						_chrome,
						JsonConvert.DeserializeObject
							<InspectRequestedEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
