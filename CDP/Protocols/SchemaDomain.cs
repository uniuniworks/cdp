// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// This domain is deprecated.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: true</description></item>
	/// <item><description>experimental: false</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class SchemaDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the SchemaDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public SchemaDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Description of the protocol domain.
		/// </summary>
		[JsonObject]
		public class Domain {
			/// <summary>
			/// Domain name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// Domain version.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("version")]
			public string Version;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Returns supported domains.
		/// </summary>
		[JsonObject]
		public class GetDomainsResult {
			/// <summary>
			/// List of supported domains.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("domains")]
			public Domain[] Domains;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns supported domains.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public GetDomainsResult GetDomains() {
			string s = _chrome.Send(
				"Schema.getDomains"
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetDomainsResult>();
		}



	}
}
