// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// Provides access to log entries.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: false</description></item>
	/// <item><description>dependencies: Runtime, Network</description></item>
	/// </list>
	/// </summary>
	public class LogDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the LogDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public LogDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Issued when new message was logged.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class EntryAddedEventArgs : EventArgs {
			/// <summary>
			/// The entry.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: LogEntry</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("entry")]
			public LogEntry Entry;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Issued when new message was logged.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<EntryAddedEventArgs> EntryAdded;

		/// <summary>
		/// Log entry.
		/// </summary>
		[JsonObject]
		public class LogEntry {
			/// <summary>
			/// Log entry source.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: xml, javascript, network, storage, appcache, rendering, security, deprecation, worker, violation, intervention, recommendation, other
			/// </summary>
			[JsonProperty("source")]
			public string Source;

			/// <summary>
			/// Log entry severity.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: verbose, info, warning, error
			/// </summary>
			[JsonProperty("level")]
			public string Level;

			/// <summary>
			/// Logged text.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("text")]
			public string Text;

			/// <summary>
			/// Timestamp when this entry was added.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.Timestamp</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("timestamp")]
			public double Timestamp;

			/// <summary>
			/// URL of the resource if known.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("url")]
			public string Url;

			/// <summary>
			/// Line number in the resource.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("lineNumber")]
			public int LineNumber;

			/// <summary>
			/// JavaScript stack trace.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Runtime.StackTrace</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("stackTrace")]
			public RuntimeDomain.StackTrace StackTrace;

			/// <summary>
			/// Identifier of the network request associated with this entry.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Network.RequestId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("networkRequestId")]
			public string NetworkRequestId;

			/// <summary>
			/// Identifier of the worker associated with this entry.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("workerId")]
			public string WorkerId;

			/// <summary>
			/// Call arguments.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("args")]
			public RuntimeDomain.RemoteObject[] Args;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Violation configuration setting.
		/// </summary>
		[JsonObject]
		public class ViolationSetting {
			/// <summary>
			/// Violation type.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// Allowed values: longTask, longLayout, blockedEvent, blockedParser, discouragedAPIUse, handler, recurringHandler
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// Time threshold to trigger upon.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("threshold")]
			public double Threshold;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Clears the log.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Clear() {
			_chrome.Send(
				"Log.clear"
			);
		}

		/// <summary>
		/// Disables log domain, prevents further log entries from being reported to the client.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"Log.disable"
			);
		}

		/// <summary>
		/// Enables log domain, sends the entries collected so far to the client by means of the `entryAdded` notification.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"Log.enable"
			);
		}

		/// <summary>
		/// start violation reporting.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="config">Configuration for violations.</param>
		public void StartViolationsReport(ViolationSetting[] config) {
			_chrome.Send(
				"Log.startViolationsReport",
				new {
					config
				}
			);
		}

		/// <summary>
		/// Stop violation reporting.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void StopViolationsReport() {
			_chrome.Send(
				"Log.stopViolationsReport"
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("Log.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "entryAdded":
					if (EntryAdded == null) {
						return;
					}
					EntryAdded(
						_chrome,
						JsonConvert.DeserializeObject
							<EntryAddedEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
