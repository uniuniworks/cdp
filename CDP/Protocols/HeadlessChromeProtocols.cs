// CDP version: 1.3
using CDP.Protocols;
using Newtonsoft.Json.Linq;

namespace CDP {
	public partial class HeadlessChrome {
		/// <summary>
		/// The protocol version.
		/// </summary>
		public static readonly string Version = "1.3";

		/// <summary>
		/// 
		/// </summary>
		public Protocols.AccessibilityDomain Accessibility;

		/// <summary>
		/// 
		/// </summary>
		public Protocols.AnimationDomain Animation;

		/// <summary>
		/// 
		/// </summary>
		public Protocols.ApplicationCacheDomain ApplicationCache;

		/// <summary>
		/// Audits domain allows investigation of page violations and possible improvements.
		/// </summary>
		public Protocols.AuditsDomain Audits;

		/// <summary>
		/// Defines events for background web platform features.
		/// </summary>
		public Protocols.BackgroundServiceDomain BackgroundService;

		/// <summary>
		/// The Browser domain defines methods and events for browser managing.
		/// </summary>
		public Protocols.BrowserDomain Browser;

		/// <summary>
		/// This domain exposes CSS read/write operations. All CSS objects (stylesheets, rules, and styles) have an associated `id` used in subsequent operations on the related object. Each object type has a specific `id` structure, and those are not interchangeable between objects of different kinds. CSS objects can be loaded using the `get*ForNode()` calls (which accept a DOM node id). A client can also keep track of stylesheets via the `styleSheetAdded`/`styleSheetRemoved` events and subsequently load the required stylesheet contents using the `getStyleSheet[Text]()` methods.
		/// </summary>
		public Protocols.CSSDomain CSS;

		/// <summary>
		/// 
		/// </summary>
		public Protocols.CacheStorageDomain CacheStorage;

		/// <summary>
		/// A domain for interacting with Cast, Presentation API, and Remote Playback API functionalities.
		/// </summary>
		public Protocols.CastDomain Cast;

		/// <summary>
		/// This domain exposes DOM read/write operations. Each DOM Node is represented with its mirror object that has an `id`. This `id` can be used to get additional information on the Node, resolve it into the JavaScript object wrapper, etc. It is important that client receives DOM events only for the nodes that are known to the client. Backend keeps track of the nodes that were sent to the client and never sends the same node twice. It is client's responsibility to collect information about the nodes that were sent to the client.<p>Note that `iframe` owner elements will return corresponding document elements as their child nodes.</p>
		/// </summary>
		public Protocols.DOMDomain DOM;

		/// <summary>
		/// DOM debugging allows setting breakpoints on particular DOM operations and events. JavaScript execution will stop on these operations as if there was a regular breakpoint set.
		/// </summary>
		public Protocols.DOMDebuggerDomain DOMDebugger;

		/// <summary>
		/// This domain facilitates obtaining document snapshots with DOM, layout, and style information.
		/// </summary>
		public Protocols.DOMSnapshotDomain DOMSnapshot;

		/// <summary>
		/// Query and modify DOM storage.
		/// </summary>
		public Protocols.DOMStorageDomain DOMStorage;

		/// <summary>
		/// 
		/// </summary>
		public Protocols.DatabaseDomain Database;

		/// <summary>
		/// 
		/// </summary>
		public Protocols.DeviceOrientationDomain DeviceOrientation;

		/// <summary>
		/// This domain emulates different environments for the page.
		/// </summary>
		public Protocols.EmulationDomain Emulation;

		/// <summary>
		/// This domain provides experimental commands only supported in headless mode.
		/// </summary>
		public Protocols.HeadlessExperimentalDomain HeadlessExperimental;

		/// <summary>
		/// Input/Output operations for streams produced by DevTools.
		/// </summary>
		public Protocols.IODomain IO;

		/// <summary>
		/// 
		/// </summary>
		public Protocols.IndexedDBDomain IndexedDB;

		/// <summary>
		/// 
		/// </summary>
		public Protocols.InputDomain Input;

		/// <summary>
		/// 
		/// </summary>
		public Protocols.InspectorDomain Inspector;

		/// <summary>
		/// 
		/// </summary>
		public Protocols.LayerTreeDomain LayerTree;

		/// <summary>
		/// Provides access to log entries.
		/// </summary>
		public Protocols.LogDomain Log;

		/// <summary>
		/// 
		/// </summary>
		public Protocols.MemoryDomain Memory;

		/// <summary>
		/// Network domain allows tracking network activities of the page. It exposes information about http, file, data and other requests and responses, their headers, bodies, timing, etc.
		/// </summary>
		public Protocols.NetworkDomain Network;

		/// <summary>
		/// This domain provides various functionality related to drawing atop the inspected page.
		/// </summary>
		public Protocols.OverlayDomain Overlay;

		/// <summary>
		/// Actions and events related to the inspected page belong to the page domain.
		/// </summary>
		public Protocols.PageDomain Page;

		/// <summary>
		/// 
		/// </summary>
		public Protocols.PerformanceDomain Performance;

		/// <summary>
		/// Security
		/// </summary>
		public Protocols.SecurityDomain Security;

		/// <summary>
		/// 
		/// </summary>
		public Protocols.ServiceWorkerDomain ServiceWorker;

		/// <summary>
		/// 
		/// </summary>
		public Protocols.StorageDomain Storage;

		/// <summary>
		/// The SystemInfo domain defines methods and events for querying low-level system information.
		/// </summary>
		public Protocols.SystemInfoDomain SystemInfo;

		/// <summary>
		/// Supports additional targets discovery and allows to attach to them.
		/// </summary>
		public Protocols.TargetDomain Target;

		/// <summary>
		/// The Tethering domain defines methods and events for browser port binding.
		/// </summary>
		public Protocols.TetheringDomain Tethering;

		/// <summary>
		/// 
		/// </summary>
		public Protocols.TracingDomain Tracing;

		/// <summary>
		/// A domain for letting clients substitute browser's network layer with client code.
		/// </summary>
		public Protocols.FetchDomain Fetch;

		/// <summary>
		/// This domain allows inspection of Web Audio API. https://webaudio.github.io/web-audio-api/
		/// </summary>
		public Protocols.WebAudioDomain WebAudio;

		/// <summary>
		/// This domain allows configuring virtual authenticators to test the WebAuthn API.
		/// </summary>
		public Protocols.WebAuthnDomain WebAuthn;

		/// <summary>
		/// This domain allows detailed inspection of media elements
		/// </summary>
		public Protocols.MediaDomain Media;

		/// <summary>
		/// This domain is deprecated - use Runtime or Log instead.
		/// </summary>
		public Protocols.ConsoleDomain Console;

		/// <summary>
		/// Debugger domain exposes JavaScript debugging capabilities. It allows setting and removing breakpoints, stepping through execution, exploring stack traces, etc.
		/// </summary>
		public Protocols.DebuggerDomain Debugger;

		/// <summary>
		/// 
		/// </summary>
		public Protocols.HeapProfilerDomain HeapProfiler;

		/// <summary>
		/// 
		/// </summary>
		public Protocols.ProfilerDomain Profiler;

		/// <summary>
		/// Runtime domain exposes JavaScript runtime by means of remote evaluation and mirror objects. Evaluation results are returned as mirror object that expose object type, string representation and unique identifier that can be used for further object reference. Original objects are maintained in memory unless they are either explicitly released or are released along with the other objects in their object group.
		/// </summary>
		public Protocols.RuntimeDomain Runtime;

		/// <summary>
		/// This domain is deprecated.
		/// </summary>
		public Protocols.SchemaDomain Schema;


		/// <summary>
		/// Initializes objects that exist in CDP.Protocols.
		/// </summary>
		protected override void InitProtocols() {
			Accessibility = new Protocols.AccessibilityDomain(this);
			Animation = new Protocols.AnimationDomain(this);
			ApplicationCache = new Protocols.ApplicationCacheDomain(this);
			Audits = new Protocols.AuditsDomain(this);
			BackgroundService = new Protocols.BackgroundServiceDomain(this);
			Browser = new Protocols.BrowserDomain(this);
			CSS = new Protocols.CSSDomain(this);
			CacheStorage = new Protocols.CacheStorageDomain(this);
			Cast = new Protocols.CastDomain(this);
			DOM = new Protocols.DOMDomain(this);
			DOMDebugger = new Protocols.DOMDebuggerDomain(this);
			DOMSnapshot = new Protocols.DOMSnapshotDomain(this);
			DOMStorage = new Protocols.DOMStorageDomain(this);
			Database = new Protocols.DatabaseDomain(this);
			DeviceOrientation = new Protocols.DeviceOrientationDomain(this);
			Emulation = new Protocols.EmulationDomain(this);
			HeadlessExperimental = new Protocols.HeadlessExperimentalDomain(this);
			IO = new Protocols.IODomain(this);
			IndexedDB = new Protocols.IndexedDBDomain(this);
			Input = new Protocols.InputDomain(this);
			Inspector = new Protocols.InspectorDomain(this);
			LayerTree = new Protocols.LayerTreeDomain(this);
			Log = new Protocols.LogDomain(this);
			Memory = new Protocols.MemoryDomain(this);
			Network = new Protocols.NetworkDomain(this);
			Overlay = new Protocols.OverlayDomain(this);
			Page = new Protocols.PageDomain(this);
			Performance = new Protocols.PerformanceDomain(this);
			Security = new Protocols.SecurityDomain(this);
			ServiceWorker = new Protocols.ServiceWorkerDomain(this);
			Storage = new Protocols.StorageDomain(this);
			SystemInfo = new Protocols.SystemInfoDomain(this);
			Target = new Protocols.TargetDomain(this);
			Tethering = new Protocols.TetheringDomain(this);
			Tracing = new Protocols.TracingDomain(this);
			Fetch = new Protocols.FetchDomain(this);
			WebAudio = new Protocols.WebAudioDomain(this);
			WebAuthn = new Protocols.WebAuthnDomain(this);
			Media = new Protocols.MediaDomain(this);
			Console = new Protocols.ConsoleDomain(this);
			Debugger = new Protocols.DebuggerDomain(this);
			HeapProfiler = new Protocols.HeapProfilerDomain(this);
			Profiler = new Protocols.ProfilerDomain(this);
			Runtime = new Protocols.RuntimeDomain(this);
			Schema = new Protocols.SchemaDomain(this);
		}

		/// <summary>
		/// Called when an event is received from chrome.
		/// </summary>
		/// <param name="method">The event method name.</param>
		/// <param name="params">The event parameters.</param>
		protected override void OnEventReceived(JToken method, JToken @params) {
			if (method == null || @params == null) {
				return;
			}
			string[] methodArray = method.ToString().Split('.');
			if (methodArray.Length < 2) {
				return;
			}
			switch (methodArray[0]) {
				case "Animation":
					Animation.Emit(methodArray[1], @params);
					break;
				case "ApplicationCache":
					ApplicationCache.Emit(methodArray[1], @params);
					break;
				case "BackgroundService":
					BackgroundService.Emit(methodArray[1], @params);
					break;
				case "CSS":
					CSS.Emit(methodArray[1], @params);
					break;
				case "Cast":
					Cast.Emit(methodArray[1], @params);
					break;
				case "DOM":
					DOM.Emit(methodArray[1], @params);
					break;
				case "DOMStorage":
					DOMStorage.Emit(methodArray[1], @params);
					break;
				case "Database":
					Database.Emit(methodArray[1], @params);
					break;
				case "Emulation":
					Emulation.Emit(methodArray[1], @params);
					break;
				case "HeadlessExperimental":
					HeadlessExperimental.Emit(methodArray[1], @params);
					break;
				case "Inspector":
					Inspector.Emit(methodArray[1], @params);
					break;
				case "LayerTree":
					LayerTree.Emit(methodArray[1], @params);
					break;
				case "Log":
					Log.Emit(methodArray[1], @params);
					break;
				case "Network":
					Network.Emit(methodArray[1], @params);
					break;
				case "Overlay":
					Overlay.Emit(methodArray[1], @params);
					break;
				case "Page":
					Page.Emit(methodArray[1], @params);
					break;
				case "Performance":
					Performance.Emit(methodArray[1], @params);
					break;
				case "Security":
					Security.Emit(methodArray[1], @params);
					break;
				case "ServiceWorker":
					ServiceWorker.Emit(methodArray[1], @params);
					break;
				case "Storage":
					Storage.Emit(methodArray[1], @params);
					break;
				case "Target":
					Target.Emit(methodArray[1], @params);
					break;
				case "Tethering":
					Tethering.Emit(methodArray[1], @params);
					break;
				case "Tracing":
					Tracing.Emit(methodArray[1], @params);
					break;
				case "Fetch":
					Fetch.Emit(methodArray[1], @params);
					break;
				case "WebAudio":
					WebAudio.Emit(methodArray[1], @params);
					break;
				case "Media":
					Media.Emit(methodArray[1], @params);
					break;
				case "Console":
					Console.Emit(methodArray[1], @params);
					break;
				case "Debugger":
					Debugger.Emit(methodArray[1], @params);
					break;
				case "HeapProfiler":
					HeapProfiler.Emit(methodArray[1], @params);
					break;
				case "Profiler":
					Profiler.Emit(methodArray[1], @params);
					break;
				case "Runtime":
					Runtime.Emit(methodArray[1], @params);
					break;
			}
		}
	}
}
