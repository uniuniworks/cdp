// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// This domain exposes DOM read/write operations. Each DOM Node is represented with its mirror object that has an `id`. This `id` can be used to get additional information on the Node, resolve it into the JavaScript object wrapper, etc. It is important that client receives DOM events only for the nodes that are known to the client. Backend keeps track of the nodes that were sent to the client and never sends the same node twice. It is client's responsibility to collect information about the nodes that were sent to the client.<p>Note that `iframe` owner elements will return corresponding document elements as their child nodes.</p>
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: false</description></item>
	/// <item><description>dependencies: Runtime</description></item>
	/// </list>
	/// </summary>
	public class DOMDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the DOMDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public DOMDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Fired when `Element`'s attribute is modified.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class AttributeModifiedEventArgs : EventArgs {
			/// <summary>
			/// Id of the node that has changed.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public int NodeId;

			/// <summary>
			/// Attribute name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// Attribute value.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public string Value;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when `Element`'s attribute is modified.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<AttributeModifiedEventArgs> AttributeModified;

		/// <summary>
		/// Fired when `Element`'s attribute is removed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class AttributeRemovedEventArgs : EventArgs {
			/// <summary>
			/// Id of the node that has changed.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public int NodeId;

			/// <summary>
			/// A ttribute name.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when `Element`'s attribute is removed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<AttributeRemovedEventArgs> AttributeRemoved;

		/// <summary>
		/// Mirrors `DOMCharacterDataModified` event.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class CharacterDataModifiedEventArgs : EventArgs {
			/// <summary>
			/// Id of the node that has changed.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public int NodeId;

			/// <summary>
			/// New text value.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("characterData")]
			public string CharacterData;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Mirrors `DOMCharacterDataModified` event.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<CharacterDataModifiedEventArgs> CharacterDataModified;

		/// <summary>
		/// Fired when `Container`'s child node count has changed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ChildNodeCountUpdatedEventArgs : EventArgs {
			/// <summary>
			/// Id of the node that has changed.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public int NodeId;

			/// <summary>
			/// New node count.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("childNodeCount")]
			public int ChildNodeCount;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when `Container`'s child node count has changed.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ChildNodeCountUpdatedEventArgs> ChildNodeCountUpdated;

		/// <summary>
		/// Mirrors `DOMNodeInserted` event.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ChildNodeInsertedEventArgs : EventArgs {
			/// <summary>
			/// Id of the node that has changed.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("parentNodeId")]
			public int ParentNodeId;

			/// <summary>
			/// If of the previous siblint.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("previousNodeId")]
			public int PreviousNodeId;

			/// <summary>
			/// Inserted node data.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Node</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("node")]
			public Node Node;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Mirrors `DOMNodeInserted` event.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ChildNodeInsertedEventArgs> ChildNodeInserted;

		/// <summary>
		/// Mirrors `DOMNodeRemoved` event.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ChildNodeRemovedEventArgs : EventArgs {
			/// <summary>
			/// Parent id.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("parentNodeId")]
			public int ParentNodeId;

			/// <summary>
			/// Id of the node that has been removed.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public int NodeId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Mirrors `DOMNodeRemoved` event.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ChildNodeRemovedEventArgs> ChildNodeRemoved;

		/// <summary>
		/// Called when distrubution is changed.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class DistributedNodesUpdatedEventArgs : EventArgs {
			/// <summary>
			/// Insertion point where distrubuted nodes were updated.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("insertionPointId")]
			public int InsertionPointId;

			/// <summary>
			/// Distributed nodes for given insertion point.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("distributedNodes")]
			public BackendNode[] DistributedNodes;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Called when distrubution is changed.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<DistributedNodesUpdatedEventArgs> DistributedNodesUpdated;

		/// <summary>
		/// Fired when `Document` has been totally updated. Node ids are no longer valid.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler DocumentUpdated;

		/// <summary>
		/// Fired when `Element`'s inline style is modified via a CSS property modification.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class InlineStyleInvalidatedEventArgs : EventArgs {
			/// <summary>
			/// Ids of the nodes for which the inline styles have been invalidated.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeIds")]
			public int[] NodeIds;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when `Element`'s inline style is modified via a CSS property modification.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<InlineStyleInvalidatedEventArgs> InlineStyleInvalidated;

		/// <summary>
		/// Called when a pseudo element is added to an element.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class PseudoElementAddedEventArgs : EventArgs {
			/// <summary>
			/// Pseudo element's parent element id.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("parentId")]
			public int ParentId;

			/// <summary>
			/// The added pseudo element.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Node</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("pseudoElement")]
			public Node PseudoElement;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Called when a pseudo element is added to an element.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<PseudoElementAddedEventArgs> PseudoElementAdded;

		/// <summary>
		/// Called when a pseudo element is removed from an element.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class PseudoElementRemovedEventArgs : EventArgs {
			/// <summary>
			/// Pseudo element's parent element id.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("parentId")]
			public int ParentId;

			/// <summary>
			/// The removed pseudo element id.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("pseudoElementId")]
			public int PseudoElementId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Called when a pseudo element is removed from an element.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<PseudoElementRemovedEventArgs> PseudoElementRemoved;

		/// <summary>
		/// Fired when backend wants to provide client with the missing DOM structure. This happens upon most of the calls requesting node ids.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class SetChildNodesEventArgs : EventArgs {
			/// <summary>
			/// Parent node id to populate with children.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("parentId")]
			public int ParentId;

			/// <summary>
			/// Child nodes array.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodes")]
			public Node[] Nodes;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Fired when backend wants to provide client with the missing DOM structure. This happens upon most of the calls requesting node ids.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<SetChildNodesEventArgs> SetChildNodes;

		/// <summary>
		/// Called when shadow root is popped from the element.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ShadowRootPoppedEventArgs : EventArgs {
			/// <summary>
			/// Host element id.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("hostId")]
			public int HostId;

			/// <summary>
			/// Shadow root id.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("rootId")]
			public int RootId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Called when shadow root is popped from the element.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ShadowRootPoppedEventArgs> ShadowRootPopped;

		/// <summary>
		/// Called when shadow root is pushed into the element.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class ShadowRootPushedEventArgs : EventArgs {
			/// <summary>
			/// Host element id.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("hostId")]
			public int HostId;

			/// <summary>
			/// Shadow root.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Node</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("root")]
			public Node Root;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Called when shadow root is pushed into the element.
		/// <list type="bullet">
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<ShadowRootPushedEventArgs> ShadowRootPushed;

		/// <summary>
		/// Backend node with a friendly name.
		/// </summary>
		[JsonObject]
		public class BackendNode {
			/// <summary>
			/// `Node`'s nodeType.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeType")]
			public int NodeType;

			/// <summary>
			/// `Node`'s nodeName.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeName")]
			public string NodeName;

			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: BackendNodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("backendNodeId")]
			public int BackendNodeId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// DOM interaction is implemented in terms of mirror objects that represent the actual DOM nodes. DOMNode is a base node mirror type.
		/// </summary>
		[JsonObject]
		public class Node {
			/// <summary>
			/// Node identifier that is passed into the rest of the DOM messages as the `nodeId`. Backend will only push node with given `id` once. It is aware of all requested nodes and will only fire DOM events for nodes known to the client.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public int NodeId;

			/// <summary>
			/// The id of the parent node if any.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("parentId")]
			public int ParentId;

			/// <summary>
			/// The BackendNodeId for this node.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: BackendNodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("backendNodeId")]
			public int BackendNodeId;

			/// <summary>
			/// `Node`'s nodeType.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeType")]
			public int NodeType;

			/// <summary>
			/// `Node`'s nodeName.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeName")]
			public string NodeName;

			/// <summary>
			/// `Node`'s localName.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("localName")]
			public string LocalName;

			/// <summary>
			/// `Node`'s nodeValue.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeValue")]
			public string NodeValue;

			/// <summary>
			/// Child count for `Container` nodes.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("childNodeCount")]
			public int ChildNodeCount;

			/// <summary>
			/// Child nodes of this node when requested with children.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("children")]
			public Node[] Children;

			/// <summary>
			/// Attributes of the `Element` node in the form of flat array `[name1, value1, name2, value2]`.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("attributes")]
			public string[] Attributes;

			/// <summary>
			/// Document URL that `Document` or `FrameOwner` node points to.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("documentURL")]
			public string DocumentURL;

			/// <summary>
			/// Base URL that `Document` or `FrameOwner` node uses for URL completion.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("baseURL")]
			public string BaseURL;

			/// <summary>
			/// `DocumentType`'s publicId.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("publicId")]
			public string PublicId;

			/// <summary>
			/// `DocumentType`'s systemId.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("systemId")]
			public string SystemId;

			/// <summary>
			/// `DocumentType`'s internalSubset.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("internalSubset")]
			public string InternalSubset;

			/// <summary>
			/// `Document`'s XML version in case of XML documents.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("xmlVersion")]
			public string XmlVersion;

			/// <summary>
			/// `Attr`'s name.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("name")]
			public string Name;

			/// <summary>
			/// `Attr`'s value.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("value")]
			public string Value;

			/// <summary>
			/// Pseudo element type for this node.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: PseudoType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("pseudoType")]
			public string PseudoType;

			/// <summary>
			/// Shadow root type.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: ShadowRootType</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("shadowRootType")]
			public string ShadowRootType;

			/// <summary>
			/// Frame ID for frame owner elements.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Page.FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// Content document for frame owner elements.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Node</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("contentDocument")]
			public Node ContentDocument;

			/// <summary>
			/// Shadow root list for given element host.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("shadowRoots")]
			public Node[] ShadowRoots;

			/// <summary>
			/// Content document fragment for template elements.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Node</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("templateContent")]
			public Node TemplateContent;

			/// <summary>
			/// Pseudo elements associated with this node.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("pseudoElements")]
			public Node[] PseudoElements;

			/// <summary>
			/// Import document for the HTMLImport links.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Node</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("importedDocument")]
			public Node ImportedDocument;

			/// <summary>
			/// Distributed nodes for given insertion point.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("distributedNodes")]
			public BackendNode[] DistributedNodes;

			/// <summary>
			/// Whether the node is SVG.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: boolean</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("isSVG")]
			public bool IsSVG;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// A structure holding an RGBA color.
		/// </summary>
		[JsonObject]
		public class RGBA {
			/// <summary>
			/// The red component, in the [0-255] range.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("r")]
			public int R;

			/// <summary>
			/// The green component, in the [0-255] range.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("g")]
			public int G;

			/// <summary>
			/// The blue component, in the [0-255] range.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("b")]
			public int B;

			/// <summary>
			/// The alpha component, in the [0-1] range (default: 1).
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("a")]
			public double A;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Box model.
		/// </summary>
		[JsonObject]
		public class BoxModel {
			/// <summary>
			/// Content box
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Quad</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("content")]
			public double[] Content;

			/// <summary>
			/// Padding box
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Quad</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("padding")]
			public double[] Padding;

			/// <summary>
			/// Border box
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Quad</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("border")]
			public double[] Border;

			/// <summary>
			/// Margin box
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Quad</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("margin")]
			public double[] Margin;

			/// <summary>
			/// Node width
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("width")]
			public int Width;

			/// <summary>
			/// Node height
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("height")]
			public int Height;

			/// <summary>
			/// Shape outside coordinates
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: ShapeOutsideInfo</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("shapeOutside")]
			public ShapeOutsideInfo ShapeOutside;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// CSS Shape Outside details.
		/// </summary>
		[JsonObject]
		public class ShapeOutsideInfo {
			/// <summary>
			/// Shape bounds
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Quad</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("bounds")]
			public double[] Bounds;

			/// <summary>
			/// Shape coordinate details
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("shape")]
			public object[] Shape;

			/// <summary>
			/// Margin shape bounds
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("marginShape")]
			public object[] MarginShape;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Rectangle.
		/// </summary>
		[JsonObject]
		public class Rect {
			/// <summary>
			/// X coordinate
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("x")]
			public double X;

			/// <summary>
			/// Y coordinate
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("y")]
			public double Y;

			/// <summary>
			/// Rectangle width
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("width")]
			public double Width;

			/// <summary>
			/// Rectangle height
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: number</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("height")]
			public double Height;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}


		/// <summary>
		/// Collects class names for the node with given id and all of it's child nodes.
		/// </summary>
		[JsonObject]
		public class CollectClassNamesFromSubtreeResult {
			/// <summary>
			/// Class name list.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("classNames")]
			public string[] ClassNames;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Collects class names for the node with given id and all of it's child nodes.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Id of the node to collect class names.</param>
		public CollectClassNamesFromSubtreeResult CollectClassNamesFromSubtree(int nodeId) {
			string s = _chrome.Send(
				"DOM.collectClassNamesFromSubtree",
				new {
					nodeId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<CollectClassNamesFromSubtreeResult>();
		}

		/// <summary>
		/// Creates a deep copy of the specified node and places it into the target container before the given anchor.
		/// </summary>
		[JsonObject]
		public class CopyToResult {
			/// <summary>
			/// Id of the node clone.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public int NodeId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Creates a deep copy of the specified node and places it into the target container before the given anchor.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Id of the node to copy.</param>
		/// <param name="targetNodeId">Id of the element to drop the copy into.</param>
		/// <param name="insertBeforeNodeId">Drop the copy before this node (if absent, the copy becomes the last child of `targetNodeId`).</param>
		public CopyToResult CopyTo(int nodeId, int targetNodeId, int? insertBeforeNodeId = null) {
			string s = _chrome.Send(
				"DOM.copyTo",
				new {
					nodeId,
					targetNodeId,
					insertBeforeNodeId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<CopyToResult>();
		}

		/// <summary>
		/// Describes node given its id, does not require domain to be enabled. Does not start tracking any objects, can be used for automation.
		/// </summary>
		[JsonObject]
		public class DescribeNodeResult {
			/// <summary>
			/// Node description.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Node</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("node")]
			public Node Node;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Describes node given its id, does not require domain to be enabled. Does not start tracking any objects, can be used for automation.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Identifier of the node.</param>
		/// <param name="backendNodeId">Identifier of the backend node.</param>
		/// <param name="objectId">JavaScript object id of the node wrapper.</param>
		/// <param name="depth">The maximum depth at which children should be retrieved, defaults to 1. Use -1 for the entire subtree or provide an integer larger than 0.</param>
		/// <param name="pierce">Whether or not iframes and shadow roots should be traversed when returning the subtree (default is false).</param>
		public DescribeNodeResult DescribeNode(int? nodeId = null, int? backendNodeId = null, string objectId = null, int? depth = null, bool? pierce = null) {
			string s = _chrome.Send(
				"DOM.describeNode",
				new {
					nodeId,
					backendNodeId,
					objectId,
					depth,
					pierce
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<DescribeNodeResult>();
		}

		/// <summary>
		/// Disables DOM agent for the given page.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Disable() {
			_chrome.Send(
				"DOM.disable"
			);
		}

		/// <summary>
		/// Discards search results from the session with the given id. `getSearchResults` should no longer be called for that search.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="searchId">Unique search session identifier.</param>
		public void DiscardSearchResults(string searchId) {
			_chrome.Send(
				"DOM.discardSearchResults",
				new {
					searchId
				}
			);
		}

		/// <summary>
		/// Enables DOM agent for the given page.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void Enable() {
			_chrome.Send(
				"DOM.enable"
			);
		}

		/// <summary>
		/// Focuses the given element.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Identifier of the node.</param>
		/// <param name="backendNodeId">Identifier of the backend node.</param>
		/// <param name="objectId">JavaScript object id of the node wrapper.</param>
		public void Focus(int? nodeId = null, int? backendNodeId = null, string objectId = null) {
			_chrome.Send(
				"DOM.focus",
				new {
					nodeId,
					backendNodeId,
					objectId
				}
			);
		}

		/// <summary>
		/// Returns attributes for the specified node.
		/// </summary>
		[JsonObject]
		public class GetAttributesResult {
			/// <summary>
			/// An interleaved array of node attribute names and values.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("attributes")]
			public string[] Attributes;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns attributes for the specified node.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Id of the node to retrieve attibutes for.</param>
		public GetAttributesResult GetAttributes(int nodeId) {
			string s = _chrome.Send(
				"DOM.getAttributes",
				new {
					nodeId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetAttributesResult>();
		}

		/// <summary>
		/// Returns boxes for the given node.
		/// </summary>
		[JsonObject]
		public class GetBoxModelResult {
			/// <summary>
			/// Box model for the node.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: BoxModel</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("model")]
			public BoxModel Model;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns boxes for the given node.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Identifier of the node.</param>
		/// <param name="backendNodeId">Identifier of the backend node.</param>
		/// <param name="objectId">JavaScript object id of the node wrapper.</param>
		public GetBoxModelResult GetBoxModel(int? nodeId = null, int? backendNodeId = null, string objectId = null) {
			string s = _chrome.Send(
				"DOM.getBoxModel",
				new {
					nodeId,
					backendNodeId,
					objectId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetBoxModelResult>();
		}

		/// <summary>
		/// Returns quads that describe node position on the page. This method might return multiple quads for inline nodes.
		/// </summary>
		[JsonObject]
		public class GetContentQuadsResult {
			/// <summary>
			/// Quads that describe node layout relative to viewport.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("quads")]
			public double[][] Quads;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns quads that describe node position on the page. This method might return multiple quads for inline nodes.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Identifier of the node.</param>
		/// <param name="backendNodeId">Identifier of the backend node.</param>
		/// <param name="objectId">JavaScript object id of the node wrapper.</param>
		public GetContentQuadsResult GetContentQuads(int? nodeId = null, int? backendNodeId = null, string objectId = null) {
			string s = _chrome.Send(
				"DOM.getContentQuads",
				new {
					nodeId,
					backendNodeId,
					objectId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetContentQuadsResult>();
		}

		/// <summary>
		/// Returns the root DOM node (and optionally the subtree) to the caller.
		/// </summary>
		[JsonObject]
		public class GetDocumentResult {
			/// <summary>
			/// Resulting node.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Node</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("root")]
			public Node Root;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns the root DOM node (and optionally the subtree) to the caller.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="depth">The maximum depth at which children should be retrieved, defaults to 1. Use -1 for the entire subtree or provide an integer larger than 0.</param>
		/// <param name="pierce">Whether or not iframes and shadow roots should be traversed when returning the subtree (default is false).</param>
		public GetDocumentResult GetDocument(int? depth = null, bool? pierce = null) {
			string s = _chrome.Send(
				"DOM.getDocument",
				new {
					depth,
					pierce
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetDocumentResult>();
		}

		/// <summary>
		/// Returns the root DOM node (and optionally the subtree) to the caller.
		/// </summary>
		[JsonObject]
		public class GetFlattenedDocumentResult {
			/// <summary>
			/// Resulting node.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodes")]
			public Node[] Nodes;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns the root DOM node (and optionally the subtree) to the caller.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="depth">The maximum depth at which children should be retrieved, defaults to 1. Use -1 for the entire subtree or provide an integer larger than 0.</param>
		/// <param name="pierce">Whether or not iframes and shadow roots should be traversed when returning the subtree (default is false).</param>
		public GetFlattenedDocumentResult GetFlattenedDocument(int? depth = null, bool? pierce = null) {
			string s = _chrome.Send(
				"DOM.getFlattenedDocument",
				new {
					depth,
					pierce
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetFlattenedDocumentResult>();
		}

		/// <summary>
		/// Returns node id at given location. Depending on whether DOM domain is enabled, nodeId is either returned or not.
		/// </summary>
		[JsonObject]
		public class GetNodeForLocationResult {
			/// <summary>
			/// Resulting node.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: BackendNodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("backendNodeId")]
			public int BackendNodeId;

			/// <summary>
			/// Frame this node belongs to.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Page.FrameId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("frameId")]
			public string FrameId;

			/// <summary>
			/// Id of the node at given coordinates, only when enabled and requested document.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public int NodeId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns node id at given location. Depending on whether DOM domain is enabled, nodeId is either returned or not.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="x">X coordinate.</param>
		/// <param name="y">Y coordinate.</param>
		/// <param name="includeUserAgentShadowDOM">False to skip to the nearest non-UA shadow root ancestor (default: false).</param>
		/// <param name="ignorePointerEventsNone">Whether to ignore pointer-events: none on elements and hit test them.</param>
		public GetNodeForLocationResult GetNodeForLocation(int x, int y, bool? includeUserAgentShadowDOM = null, bool? ignorePointerEventsNone = null) {
			string s = _chrome.Send(
				"DOM.getNodeForLocation",
				new {
					x,
					y,
					includeUserAgentShadowDOM,
					ignorePointerEventsNone
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetNodeForLocationResult>();
		}

		/// <summary>
		/// Returns node's HTML markup.
		/// </summary>
		[JsonObject]
		public class GetOuterHTMLResult {
			/// <summary>
			/// Outer HTML markup.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("outerHTML")]
			public string OuterHTML;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns node's HTML markup.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Identifier of the node.</param>
		/// <param name="backendNodeId">Identifier of the backend node.</param>
		/// <param name="objectId">JavaScript object id of the node wrapper.</param>
		public GetOuterHTMLResult GetOuterHTML(int? nodeId = null, int? backendNodeId = null, string objectId = null) {
			string s = _chrome.Send(
				"DOM.getOuterHTML",
				new {
					nodeId,
					backendNodeId,
					objectId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetOuterHTMLResult>();
		}

		/// <summary>
		/// Returns the id of the nearest ancestor that is a relayout boundary.
		/// </summary>
		[JsonObject]
		public class GetRelayoutBoundaryResult {
			/// <summary>
			/// Relayout boundary node id for the given node.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public int NodeId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns the id of the nearest ancestor that is a relayout boundary.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Id of the node.</param>
		public GetRelayoutBoundaryResult GetRelayoutBoundary(int nodeId) {
			string s = _chrome.Send(
				"DOM.getRelayoutBoundary",
				new {
					nodeId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetRelayoutBoundaryResult>();
		}

		/// <summary>
		/// Returns search results from given `fromIndex` to given `toIndex` from the search with the given identifier.
		/// </summary>
		[JsonObject]
		public class GetSearchResultsResult {
			/// <summary>
			/// Ids of the search result nodes.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeIds")]
			public int[] NodeIds;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns search results from given `fromIndex` to given `toIndex` from the search with the given identifier.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="searchId">Unique search session identifier.</param>
		/// <param name="fromIndex">Start index of the search result to be returned.</param>
		/// <param name="toIndex">End index of the search result to be returned.</param>
		public GetSearchResultsResult GetSearchResults(string searchId, int fromIndex, int toIndex) {
			string s = _chrome.Send(
				"DOM.getSearchResults",
				new {
					searchId,
					fromIndex,
					toIndex
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetSearchResultsResult>();
		}

		/// <summary>
		/// Hides any highlight.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void HideHighlight() {
			_chrome.Send(
				"DOM.hideHighlight"
			);
		}

		/// <summary>
		/// Highlights DOM node.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void HighlightNode() {
			_chrome.Send(
				"DOM.highlightNode"
			);
		}

		/// <summary>
		/// Highlights given rectangle.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public void HighlightRect() {
			_chrome.Send(
				"DOM.highlightRect"
			);
		}

		/// <summary>
		/// Marks last undoable state.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public void MarkUndoableState() {
			_chrome.Send(
				"DOM.markUndoableState"
			);
		}

		/// <summary>
		/// Moves node into the new container, places it before the given anchor.
		/// </summary>
		[JsonObject]
		public class MoveToResult {
			/// <summary>
			/// New id of the moved node.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public int NodeId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Moves node into the new container, places it before the given anchor.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Id of the node to move.</param>
		/// <param name="targetNodeId">Id of the element to drop the moved node into.</param>
		/// <param name="insertBeforeNodeId">Drop node before this one (if absent, the moved node becomes the last child of `targetNodeId`).</param>
		public MoveToResult MoveTo(int nodeId, int targetNodeId, int? insertBeforeNodeId = null) {
			string s = _chrome.Send(
				"DOM.moveTo",
				new {
					nodeId,
					targetNodeId,
					insertBeforeNodeId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<MoveToResult>();
		}

		/// <summary>
		/// Searches for a given string in the DOM tree. Use `getSearchResults` to access search results or `cancelSearch` to end this search session.
		/// </summary>
		[JsonObject]
		public class PerformSearchResult {
			/// <summary>
			/// Unique search session identifier.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("searchId")]
			public string SearchId;

			/// <summary>
			/// Number of search results.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("resultCount")]
			public int ResultCount;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Searches for a given string in the DOM tree. Use `getSearchResults` to access search results or `cancelSearch` to end this search session.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="query">Plain text or query selector or XPath search query.</param>
		/// <param name="includeUserAgentShadowDOM">True to search in user agent shadow DOM.</param>
		public PerformSearchResult PerformSearch(string query, bool? includeUserAgentShadowDOM = null) {
			string s = _chrome.Send(
				"DOM.performSearch",
				new {
					query,
					includeUserAgentShadowDOM
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<PerformSearchResult>();
		}

		/// <summary>
		/// Requests that the node is sent to the caller given its path. // FIXME, use XPath
		/// </summary>
		[JsonObject]
		public class PushNodeByPathToFrontendResult {
			/// <summary>
			/// Id of the node for given path.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public int NodeId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Requests that the node is sent to the caller given its path. // FIXME, use XPath
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="path">Path to node in the proprietary format.</param>
		public PushNodeByPathToFrontendResult PushNodeByPathToFrontend(string path) {
			string s = _chrome.Send(
				"DOM.pushNodeByPathToFrontend",
				new {
					path
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<PushNodeByPathToFrontendResult>();
		}

		/// <summary>
		/// Requests that a batch of nodes is sent to the caller given their backend node ids.
		/// </summary>
		[JsonObject]
		public class PushNodesByBackendIdsToFrontendResult {
			/// <summary>
			/// The array of ids of pushed nodes that correspond to the backend ids specified in backendNodeIds.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeIds")]
			public int[] NodeIds;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Requests that a batch of nodes is sent to the caller given their backend node ids.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="backendNodeIds">The array of backend node ids.</param>
		public PushNodesByBackendIdsToFrontendResult PushNodesByBackendIdsToFrontend(int[] backendNodeIds) {
			string s = _chrome.Send(
				"DOM.pushNodesByBackendIdsToFrontend",
				new {
					backendNodeIds
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<PushNodesByBackendIdsToFrontendResult>();
		}

		/// <summary>
		/// Executes `querySelector` on a given node.
		/// </summary>
		[JsonObject]
		public class QuerySelectorResult {
			/// <summary>
			/// Query selector result.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public int NodeId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Executes `querySelector` on a given node.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Id of the node to query upon.</param>
		/// <param name="selector">Selector string.</param>
		public QuerySelectorResult QuerySelector(int nodeId, string selector) {
			string s = _chrome.Send(
				"DOM.querySelector",
				new {
					nodeId,
					selector
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<QuerySelectorResult>();
		}

		/// <summary>
		/// Executes `querySelectorAll` on a given node.
		/// </summary>
		[JsonObject]
		public class QuerySelectorAllResult {
			/// <summary>
			/// Query selector result.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: array</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeIds")]
			public int[] NodeIds;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Executes `querySelectorAll` on a given node.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Id of the node to query upon.</param>
		/// <param name="selector">Selector string.</param>
		public QuerySelectorAllResult QuerySelectorAll(int nodeId, string selector) {
			string s = _chrome.Send(
				"DOM.querySelectorAll",
				new {
					nodeId,
					selector
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<QuerySelectorAllResult>();
		}

		/// <summary>
		/// Re-does the last undone action.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public void Redo() {
			_chrome.Send(
				"DOM.redo"
			);
		}

		/// <summary>
		/// Removes attribute with given name from an element with given id.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Id of the element to remove attribute from.</param>
		/// <param name="name">Name of the attribute to remove.</param>
		public void RemoveAttribute(int nodeId, string name) {
			_chrome.Send(
				"DOM.removeAttribute",
				new {
					nodeId,
					name
				}
			);
		}

		/// <summary>
		/// Removes node with given id.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Id of the node to remove.</param>
		public void RemoveNode(int nodeId) {
			_chrome.Send(
				"DOM.removeNode",
				new {
					nodeId
				}
			);
		}

		/// <summary>
		/// Requests that children of the node with given id are returned to the caller in form of `setChildNodes` events where not only immediate children are retrieved, but all children down to the specified depth.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Id of the node to get children for.</param>
		/// <param name="depth">The maximum depth at which children should be retrieved, defaults to 1. Use -1 for the entire subtree or provide an integer larger than 0.</param>
		/// <param name="pierce">Whether or not iframes and shadow roots should be traversed when returning the sub-tree (default is false).</param>
		public void RequestChildNodes(int nodeId, int? depth = null, bool? pierce = null) {
			_chrome.Send(
				"DOM.requestChildNodes",
				new {
					nodeId,
					depth,
					pierce
				}
			);
		}

		/// <summary>
		/// Requests that the node is sent to the caller given the JavaScript node object reference. All nodes that form the path from the node to the root are also sent to the client as a series of `setChildNodes` notifications.
		/// </summary>
		[JsonObject]
		public class RequestNodeResult {
			/// <summary>
			/// Node id for given object.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public int NodeId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Requests that the node is sent to the caller given the JavaScript node object reference. All nodes that form the path from the node to the root are also sent to the client as a series of `setChildNodes` notifications.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="objectId">JavaScript object id to convert into node.</param>
		public RequestNodeResult RequestNode(string objectId) {
			string s = _chrome.Send(
				"DOM.requestNode",
				new {
					objectId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<RequestNodeResult>();
		}

		/// <summary>
		/// Resolves the JavaScript node object for a given NodeId or BackendNodeId.
		/// </summary>
		[JsonObject]
		public class ResolveNodeResult {
			/// <summary>
			/// JavaScript object wrapper for given node.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: Runtime.RemoteObject</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("object")]
			public RuntimeDomain.RemoteObject Object;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Resolves the JavaScript node object for a given NodeId or BackendNodeId.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Id of the node to resolve.</param>
		/// <param name="backendNodeId">Backend identifier of the node to resolve.</param>
		/// <param name="objectGroup">Symbolic group name that can be used to release multiple objects.</param>
		/// <param name="executionContextId">Execution context in which to resolve the node.</param>
		public ResolveNodeResult ResolveNode(int? nodeId = null, int? backendNodeId = null, string objectGroup = null, int? executionContextId = null) {
			string s = _chrome.Send(
				"DOM.resolveNode",
				new {
					nodeId,
					backendNodeId,
					objectGroup,
					executionContextId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<ResolveNodeResult>();
		}

		/// <summary>
		/// Sets attribute for an element with given id.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Id of the element to set attribute for.</param>
		/// <param name="name">Attribute name.</param>
		/// <param name="value">Attribute value.</param>
		public void SetAttributeValue(int nodeId, string name, string value) {
			_chrome.Send(
				"DOM.setAttributeValue",
				new {
					nodeId,
					name,
					value
				}
			);
		}

		/// <summary>
		/// Sets attributes on element with given id. This method is useful when user edits some existing attribute value and types in several attribute name/value pairs.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Id of the element to set attributes for.</param>
		/// <param name="text">Text with a number of attributes. Will parse this text using HTML parser.</param>
		/// <param name="name">Attribute name to replace with new attributes derived from text in case text parsed successfully.</param>
		public void SetAttributesAsText(int nodeId, string text, string name = null) {
			_chrome.Send(
				"DOM.setAttributesAsText",
				new {
					nodeId,
					text,
					name
				}
			);
		}

		/// <summary>
		/// Sets files for the given file input element.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="files">Array of file paths to set.</param>
		/// <param name="nodeId">Identifier of the node.</param>
		/// <param name="backendNodeId">Identifier of the backend node.</param>
		/// <param name="objectId">JavaScript object id of the node wrapper.</param>
		public void SetFileInputFiles(string[] files, int? nodeId = null, int? backendNodeId = null, string objectId = null) {
			_chrome.Send(
				"DOM.setFileInputFiles",
				new {
					files,
					nodeId,
					backendNodeId,
					objectId
				}
			);
		}

		/// <summary>
		/// Sets if stack traces should be captured for Nodes. See `Node.getNodeStackTraces`. Default is disabled.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="enable">Enable or disable.</param>
		public void SetNodeStackTracesEnabled(bool enable) {
			_chrome.Send(
				"DOM.setNodeStackTracesEnabled",
				new {
					enable
				}
			);
		}

		/// <summary>
		/// Gets stack traces associated with a Node. As of now, only provides stack trace for Node creation.
		/// </summary>
		[JsonObject]
		public class GetNodeStackTracesResult {
			/// <summary>
			/// Creation stack trace, if available.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: Runtime.StackTrace</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("creation")]
			public RuntimeDomain.StackTrace Creation;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Gets stack traces associated with a Node. As of now, only provides stack trace for Node creation.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Id of the node to get stack traces for.</param>
		public GetNodeStackTracesResult GetNodeStackTraces(int nodeId) {
			string s = _chrome.Send(
				"DOM.getNodeStackTraces",
				new {
					nodeId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetNodeStackTracesResult>();
		}

		/// <summary>
		/// Returns file information for the given File wrapper.
		/// </summary>
		[JsonObject]
		public class GetFileInfoResult {
			/// <summary>
			/// 
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("path")]
			public string Path;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns file information for the given File wrapper.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="objectId">JavaScript object id of the node wrapper.</param>
		public GetFileInfoResult GetFileInfo(string objectId) {
			string s = _chrome.Send(
				"DOM.getFileInfo",
				new {
					objectId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetFileInfoResult>();
		}

		/// <summary>
		/// Enables console to refer to the node with given id via $x (see Command Line API for more details $x functions).
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">DOM node id to be accessible by means of $x command line API.</param>
		public void SetInspectedNode(int nodeId) {
			_chrome.Send(
				"DOM.setInspectedNode",
				new {
					nodeId
				}
			);
		}

		/// <summary>
		/// Sets node name for a node with given id.
		/// </summary>
		[JsonObject]
		public class SetNodeNameResult {
			/// <summary>
			/// New node's id.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public int NodeId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Sets node name for a node with given id.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Id of the node to set name for.</param>
		/// <param name="name">New node's name.</param>
		public SetNodeNameResult SetNodeName(int nodeId, string name) {
			string s = _chrome.Send(
				"DOM.setNodeName",
				new {
					nodeId,
					name
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<SetNodeNameResult>();
		}

		/// <summary>
		/// Sets node value for a node with given id.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Id of the node to set value for.</param>
		/// <param name="value">New node's value.</param>
		public void SetNodeValue(int nodeId, string value) {
			_chrome.Send(
				"DOM.setNodeValue",
				new {
					nodeId,
					value
				}
			);
		}

		/// <summary>
		/// Sets node HTML markup, returns new node id.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="nodeId">Id of the node to set markup for.</param>
		/// <param name="outerHTML">Outer HTML markup to set.</param>
		public void SetOuterHTML(int nodeId, string outerHTML) {
			_chrome.Send(
				"DOM.setOuterHTML",
				new {
					nodeId,
					outerHTML
				}
			);
		}

		/// <summary>
		/// Undoes the last performed action.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		public void Undo() {
			_chrome.Send(
				"DOM.undo"
			);
		}

		/// <summary>
		/// Returns iframe node that owns iframe with the given domain.
		/// </summary>
		[JsonObject]
		public class GetFrameOwnerResult {
			/// <summary>
			/// Resulting node.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: BackendNodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("backendNodeId")]
			public int BackendNodeId;

			/// <summary>
			/// Id of the node at given coordinates, only when enabled and requested document.
			/// <list type="bullet">
			/// <item><description>Optional: true</description></item>
			/// <item><description>Type: NodeId</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("nodeId")]
			public int NodeId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Returns iframe node that owns iframe with the given domain.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: true</description></item>
		/// </list>
		/// </summary>
		/// <param name="frameId"></param>
		public GetFrameOwnerResult GetFrameOwner(string frameId) {
			string s = _chrome.Send(
				"DOM.getFrameOwner",
				new {
					frameId
				}
			);
			if (string.IsNullOrEmpty(s)) {
				return null;
			}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken("result")
				.ToObject<GetFrameOwnerResult>();
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("DOM.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "attributeModified":
					if (AttributeModified == null) {
						return;
					}
					AttributeModified(
						_chrome,
						JsonConvert.DeserializeObject
							<AttributeModifiedEventArgs>(@params.ToString())
					);
					break;
				case "attributeRemoved":
					if (AttributeRemoved == null) {
						return;
					}
					AttributeRemoved(
						_chrome,
						JsonConvert.DeserializeObject
							<AttributeRemovedEventArgs>(@params.ToString())
					);
					break;
				case "characterDataModified":
					if (CharacterDataModified == null) {
						return;
					}
					CharacterDataModified(
						_chrome,
						JsonConvert.DeserializeObject
							<CharacterDataModifiedEventArgs>(@params.ToString())
					);
					break;
				case "childNodeCountUpdated":
					if (ChildNodeCountUpdated == null) {
						return;
					}
					ChildNodeCountUpdated(
						_chrome,
						JsonConvert.DeserializeObject
							<ChildNodeCountUpdatedEventArgs>(@params.ToString())
					);
					break;
				case "childNodeInserted":
					if (ChildNodeInserted == null) {
						return;
					}
					ChildNodeInserted(
						_chrome,
						JsonConvert.DeserializeObject
							<ChildNodeInsertedEventArgs>(@params.ToString())
					);
					break;
				case "childNodeRemoved":
					if (ChildNodeRemoved == null) {
						return;
					}
					ChildNodeRemoved(
						_chrome,
						JsonConvert.DeserializeObject
							<ChildNodeRemovedEventArgs>(@params.ToString())
					);
					break;
				case "distributedNodesUpdated":
					if (DistributedNodesUpdated == null) {
						return;
					}
					DistributedNodesUpdated(
						_chrome,
						JsonConvert.DeserializeObject
							<DistributedNodesUpdatedEventArgs>(@params.ToString())
					);
					break;
				case "documentUpdated":
					if (DocumentUpdated == null) {
						return;
					}
					DocumentUpdated(_chrome, EventArgs.Empty);
					break;
				case "inlineStyleInvalidated":
					if (InlineStyleInvalidated == null) {
						return;
					}
					InlineStyleInvalidated(
						_chrome,
						JsonConvert.DeserializeObject
							<InlineStyleInvalidatedEventArgs>(@params.ToString())
					);
					break;
				case "pseudoElementAdded":
					if (PseudoElementAdded == null) {
						return;
					}
					PseudoElementAdded(
						_chrome,
						JsonConvert.DeserializeObject
							<PseudoElementAddedEventArgs>(@params.ToString())
					);
					break;
				case "pseudoElementRemoved":
					if (PseudoElementRemoved == null) {
						return;
					}
					PseudoElementRemoved(
						_chrome,
						JsonConvert.DeserializeObject
							<PseudoElementRemovedEventArgs>(@params.ToString())
					);
					break;
				case "setChildNodes":
					if (SetChildNodes == null) {
						return;
					}
					SetChildNodes(
						_chrome,
						JsonConvert.DeserializeObject
							<SetChildNodesEventArgs>(@params.ToString())
					);
					break;
				case "shadowRootPopped":
					if (ShadowRootPopped == null) {
						return;
					}
					ShadowRootPopped(
						_chrome,
						JsonConvert.DeserializeObject
							<ShadowRootPoppedEventArgs>(@params.ToString())
					);
					break;
				case "shadowRootPushed":
					if (ShadowRootPushed == null) {
						return;
					}
					ShadowRootPushed(
						_chrome,
						JsonConvert.DeserializeObject
							<ShadowRootPushedEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
