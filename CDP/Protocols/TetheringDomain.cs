// CDP version: 1.3
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {
	/// <summary>
	/// The Tethering domain defines methods and events for browser port binding.
	/// <list type="bullet">
	/// <item><description>version: 1.3</description></item>
	/// <item><description>deprecated: false</description></item>
	/// <item><description>experimental: true</description></item>
	/// <item><description>dependencies: </description></item>
	/// </list>
	/// </summary>
	public class TetheringDomain {
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the TetheringDomain class.
		/// </summary>
		/// <param name="chrome">The HeadlessChrome object.</param>
		public TetheringDomain(HeadlessChrome chrome) {
			_chrome = chrome;
		}

		/// <summary>
		/// Informs that port was successfully bound and got a specified connection id.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class AcceptedEventArgs : EventArgs {
			/// <summary>
			/// Port number that was successfully bound.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: integer</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("port")]
			public int Port;

			/// <summary>
			/// Connection id to be used.
			/// <list type="bullet">
			/// <item><description>Optional: false</description></item>
			/// <item><description>Type: string</description></item>
			/// </list>
			/// </summary>
			[JsonProperty("connectionId")]
			public string ConnectionId;

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}
		}

		/// <summary>
		/// Informs that port was successfully bound and got a specified connection id.
		/// <list type="bullet">
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<AcceptedEventArgs> Accepted;


		/// <summary>
		/// Request browser port binding.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="port">Port number to bind.</param>
		public void Bind(int port) {
			_chrome.Send(
				"Tethering.bind",
				new {
					port
				}
			);
		}

		/// <summary>
		/// Request browser port unbinding.
		/// <list type="bullet">
		/// <item><description>deprecated: false</description></item>
		/// <item><description>experimental: false</description></item>
		/// </list>
		/// </summary>
		/// <param name="port">Port number to unbind.</param>
		public void Unbind(int port) {
			_chrome.Send(
				"Tethering.unbind",
				new {
					port
				}
			);
		}


		internal void Emit(string eventName, JToken @params) {
			_chrome.AddLog(string.Format("Tethering.Emit: {0}, {1}", eventName, @params));
			switch (eventName) {
				case "accepted":
					if (Accepted == null) {
						return;
					}
					Accepted(
						_chrome,
						JsonConvert.DeserializeObject
							<AcceptedEventArgs>(@params.ToString())
					);
					break;

			}
		}

	}
}
