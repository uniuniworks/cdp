﻿using System;
using Newtonsoft.Json;

namespace CDP {
	/// <summary>
	/// Occurs when the WebSocket receives an error.
	/// </summary>
	[JsonObject]
	public class ErrorEventArgs : EventArgs {
		/// <summary>
		/// The method name.
		/// </summary>
		[JsonProperty("method")]
		public string Method;

		/// <summary>
		/// The error object.
		/// </summary>
		[JsonProperty("error")]
		public ErrorJson Error;

		/// <summary>
		/// Initializes a new instance of the ErrorEventArgs class.
		/// </summary>
		/// <param name="method">The method name.</param>
		/// <param name="error">The error object.</param>
		public ErrorEventArgs(string method, ErrorJson error) {
			Method = method;
			Error = error;
		}

		/// <summary>
		/// Returns a JSON string that represents the current object.
		/// </summary>
		/// <returns>A JSON string.</returns>
		public override string ToString() {
			return JsonConvert.SerializeObject(
				this, Formatting.Indented
			);
		}
	}
}
