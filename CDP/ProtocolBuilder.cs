﻿using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace CDP {
	using CommandJson = ProtocolJson.CommandJson;
	using DomainJson = ProtocolJson.DomainJson;
	using PropertyItemsJson = ProtocolJson.PropertyItemsJson;
	using PropertyJson = ProtocolJson.PropertyJson;
	using TypeJson = ProtocolJson.TypeJson;

	/// <summary>
	/// Converts ProtocolJson to C# source files.
	/// </summary>
	public class ProtocolBuilder {
		/// <summary>
		/// Reserved C# keywords.
		/// <para>
		/// Variable names that match this table are prefixed with "@".
		/// </para>
		/// </summary>
		public static List<string> ReservedKeywords = new List<string> {
			"abstract",
			"as",
			"async",
			"await",
			"base",
			"bool",
			"break",
			"byte",
			"case",
			"catch",
			"char",
			"checked",
			"class",
			"const",
			"continue",
			"decimal",
			"default",
			"delegate",
			"do",
			"double",
			"else",
			"enum",
			"event",
			"explicit",
			"extern",
			"false",
			"finally",
			"fixed",
			"float",
			"for",
			"foreach",
			"goto",
			"if",
			"implicit",
			"in",
			"int",
			"interface",
			"internal",
			"is",
			"lock",
			"long",
			"namespace",
			"new",
			"null",
			"object",
			"operator",
			"out",
			"override",
			"params",
			"private",
			"protected",
			"public",
			"readonly",
			"ref",
			"return",
			"sbyte",
			"sealed",
			"short",
			"sizeof",
			"stackalloc",
			"static",
			"string",
			"struct",
			"switch",
			"this",
			"throw",
			"true",
			"try",
			"typeof",
			"uint",
			"ulong",
			"unchecked",
			"unsafe",
			"ushort",
			"using",
			"virtual",
			"volatile",
			"void",
			"while"
		};

		/// <summary>
		/// Templates for outputting source code.
		/// </summary>
		public static class Formats {
			/// <summary>
			/// A class name.
			/// </summary>
			public static string ClassName = "{Domain}Domain";

			/// <summary>
			/// Template for outputting "HeadlessChromeProtocols.cs".
			/// </summary>
			public static string Protocols =
@"// CDP version: {Version}
using CDP.Protocols;
using Newtonsoft.Json.Linq;

namespace CDP {{
	public partial class HeadlessChrome {{
		/// <summary>
		/// The protocol version.
		/// </summary>
		public static readonly string Version = ""{Version}"";

{Protocols}
		/// <summary>
		/// Initializes objects that exist in CDP.Protocols.
		/// </summary>
		protected override void InitProtocols() {{
{ProtocolInits}
		}}

		/// <summary>
		/// Called when an event is received from chrome.
		/// </summary>
		/// <param name=""method"">The event method name.</param>
		/// <param name=""params"">The event parameters.</param>
		protected override void OnEventReceived(JToken method, JToken @params) {{
			if (method == null || @params == null) {{
				return;
			}}
			string[] methodArray = method.ToString().Split('.');
			if (methodArray.Length < 2) {{
				return;
			}}
			switch (methodArray[0]) {{
{ProtocolEvents}
			}}
		}}
	}}
}}
";

			/// <summary>
			/// A property in "Protocols".
			/// </summary>
			public static string Protocol =
@"		/// <summary>
		/// {Description}
		/// </summary>
		public Protocols.{ClassName} {Domain};

";

			/// <summary>
			/// A property initializer in "Protocols".
			/// </summary>
			public static string ProtocolInit =
@"			{Domain} = new Protocols.{ClassName}(this);
";

			/// <summary>
			/// A event handler in "Protocols".
			/// </summary>
			public static string ProtocolEvent =
@"				case ""{Domain}"":
					{Domain}.Emit(methodArray[1], @params);
					break;
";

			/// <summary>
			/// Template for each domain.
			/// </summary>
			public static string Domain =
@"// CDP version: {Version}
using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace CDP.Protocols {{
	/// <summary>
	/// {Description}
	/// <list type=""bullet"">
	/// <item><description>version: {Version}</description></item>
	/// <item><description>deprecated: {Deprecated}</description></item>
	/// <item><description>experimental: {Experimental}</description></item>
	/// <item><description>dependencies: {Dependencies}</description></item>
	/// </list>
	/// </summary>
	public class {ClassName} {{
		HeadlessChrome _chrome;

		/// <summary>
		/// Initializes a new instance of the {ClassName} class.
		/// </summary>
		/// <param name=""chrome"">The HeadlessChrome object.</param>
		public {ClassName}(HeadlessChrome chrome) {{
			_chrome = chrome;
		}}
{Events}
{Types}
{Commands}
{EventEmitter}
	}}
}}
";

			/// <summary>
			/// A type declaration in "Domain".
			/// </summary>
			public static string Type =
@"		/// <summary>
		/// {Description}{EnumComments}
		/// </summary>
		[JsonObject]
		public class {Id} {{
{Properties}

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {{
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}}
		}}

";

			/// <summary>
			/// A property in "Type".
			/// </summary>
			public static string TypeProperty =
@"			/// <summary>
			/// {Description}
			/// <list type=""bullet"">
			/// <item><description>Optional: {Optional}</description></item>
			/// <item><description>Type: {Type}{Ref}</description></item>
			/// </list>{EnumComments}
			/// </summary>
			[JsonProperty(""{name}"")]
			public {ResolvedType} {Name};

";

			/// <summary>
			/// A method without a return value in "Domain".
			/// </summary>
			public static string CommandNoReturn =
@"		/// <summary>
		/// {Description}
		/// <list type=""bullet"">
		/// <item><description>deprecated: {Deprecated}</description></item>
		/// <item><description>experimental: {Experimental}</description></item>
		/// </list>
		/// </summary>{ParamComments}{ObsoleteAttribute}
		public void {Name}({Parameters}) {{
			_chrome.Send(
				""{Domain}.{name}""{CommandParams}
			);
		}}

";

			/// <summary>
			/// A method with a return value in "Domain".
			/// </summary>
			public static string Command =
@"{ReturnType}		/// <summary>
		/// {Description}
		/// <list type=""bullet"">
		/// <item><description>deprecated: {Deprecated}</description></item>
		/// <item><description>experimental: {Experimental}</description></item>
		/// </list>
		/// </summary>{ParamComments}{ObsoleteAttribute}
		public {ReturnTypeName} {Name}({Parameters}) {{
			string s = _chrome.Send(
				""{Domain}.{name}""{CommandParams}
			);
			if (string.IsNullOrEmpty(s)) {{
				return null;
			}}
			JObject jObject = JObject.Parse(s);
			return jObject.SelectToken(""result"")
				.ToObject<{ReturnTypeName}>();
		}}

";

			/// <summary>
			/// A method parameters in "Command".
			/// </summary>
			public static string CommandParams =
@",
				new {{{0}
				}}";

			/// <summary>
			/// A method parameter in "CommandParams".
			/// </summary>
			public static string CommandParam =
@"
					{0}";

			/// <summary>
			/// A ObsoleteAttribute in "Command".
			/// </summary>
			public static string ObsoleteAttribute =
@"
		[Obsolete]";

			/// <summary>
			/// A array variable.
			/// </summary>
			public static string Array = "{0}[]";

			/// <summary>
			/// A event handler in "Type".
			/// </summary>
			public static string Event =
@"{EventArgs}
		/// <summary>
		/// {Description}
		/// <list type=""bullet"">
		/// <item><description>experimental: {Experimental}</description></item>
		/// </list>
		/// </summary>
		public event EventHandler<{Name}EventArgs> {Name};
";

			/// <summary>
			/// A event handler without arguments in "Type".
			/// </summary>
			public static string EventNoArgs =
@"
		/// <summary>
		/// {Description}
		/// <list type=""bullet"">
		/// <item><description>experimental: {Experimental}</description></item>
		/// </list>
		/// </summary>
		public event EventHandler {Name};
";

			/// <summary>
			/// A event arguments in "Event".
			/// </summary>
			public static string EventArgs =
@"
		/// <summary>
		/// {Description}
		/// <list type=""bullet"">
		/// <item><description>experimental: {Experimental}</description></item>
		/// </list>
		/// </summary>
		[JsonObject]
		public class {Name}EventArgs : EventArgs {{
{Properties}

			/// <summary>
			/// Returns a JSON string that represents the current object.
			/// </summary>
			/// <returns>A JSON string.</returns>
			public override string ToString() {{
				return JsonConvert.SerializeObject(
					this, Formatting.Indented
				);
			}}
		}}
";

			/// <summary>
			/// A parameter comment in "Command".
			/// </summary>
			public static string ParamComment =
@"
		/// <param name=""{name}"">{Description}</param>";

			/// <summary>
			/// A comment for enum variable.
			/// </summary>
			public static string EnumComment =
@"
			/// Allowed values: {Items}";

			/// <summary>
			/// A event emitter in "Type".
			/// </summary>
			public static string EventEmitter =
@"		internal void Emit(string eventName, JToken @params) {{
			_chrome.AddLog(string.Format(""{Domain}.Emit: {{0}}, {{1}}"", eventName, @params));
			switch (eventName) {{
{EventEmits}
			}}
		}}
";

			/// <summary>
			/// A event case in "EventEmitter".
			/// </summary>
			public static string EventEmit =
@"				case ""{name}"":
					if ({Name} == null) {{
						return;
					}}
					{Name}(
						_chrome,
						JsonConvert.DeserializeObject
							<{Name}EventArgs>(@params.ToString())
					);
					break;
";

			/// <summary>
			/// A event case without arguments in "EventEmitter".
			/// </summary>
			public static string EventEmitNoArgs =
@"				case ""{name}"":
					if ({Name} == null) {{
						return;
					}}
					{Name}(_chrome, EventArgs.Empty);
					break;
";
		}

		/// <summary>
		/// Table for converting type names
		/// written in JSON text to .NET type names.
		/// </summary>
		public static Dictionary<string, string> TypeNameReplacementList =
			new Dictionary<string, string> {
				{ "integer", "int" },
				{ "number", "double" },
				{ "boolean", "bool" },
				{ "any", "object" },
				{ "binary", "byte[]" }
			};

		/// <summary>
		/// Table for converting .NET type names to Nullable type names.
		/// </summary>
		public static Dictionary<string, string> NullableTypeNameReplacementList =
			new Dictionary<string, string> {
				{ "int", "int?" },
				{ "double", "double?" },
				{ "bool", "bool?" }
			};

		/// <summary>
		/// Table for not escaping HTML tags in comments written in JSON text.
		/// </summary>
		public static List<string> NoEscapeHtmlTags = new List<string> {
			"p"
		};

		Dictionary<string, TypeJson> _types;

		internal ProtocolBuilder() {
		}

		/// <summary>
		/// Converts the ProtocolJson object to C# source files.
		/// <para>
		/// The generated files are output to
		/// the directory of the executable file.
		/// </para>
		/// </summary>
		/// <param name="protocol">The ProtocolJson object.</param>
		public static void Build(ProtocolJson protocol) {
			Build(protocol, null);
		}

		/// <summary>
		/// Converts the ProtocolJson object to C# source files.
		/// <para>
		/// The generated files are output to
		/// the directory of the executable file.
		/// </para>
		/// </summary>
		/// <param name="protocol">The ProtocolJson object.</param>
		/// <param name="outputPath">
		/// Relative path from the directory of the executable file.
		/// </param>
		public static void Build(ProtocolJson protocol, string outputPath) {
			if (protocol == null || protocol.Domains == null) {
				return;
			}
			if (outputPath == null) {
				outputPath = string.Empty;
			}
			string appPath = Assembly.GetExecutingAssembly().Location;
			string appDir = Path.GetDirectoryName(appPath);
			outputPath = Path.Combine(appDir, outputPath);
			outputPath = Path.GetFullPath(outputPath);
			if (Directory.Exists(outputPath) == false) {
				Directory.CreateDirectory(outputPath);
			}
			ProtocolBuilder builder = new ProtocolBuilder();
			string version = protocol.Version.ToString();
			builder._types = builder.CollectAllTypes(protocol);
			foreach (DomainJson domain in protocol.Domains) {
				if (domain == null) {
					continue;
				}
				string domainString = builder.GetDomain(domain, version);
				//Console.WriteLine(domainString);
				string path = Path.Combine(
					outputPath, builder.GetClassName(domain) + ".cs"
				);
				File.WriteAllText(path, domainString);
			}

			string protocols = builder.GetProtocols(protocol);
			string protocolPath = Path.Combine(
				outputPath, "HeadlessChromeProtocols.cs"
			);
			//Console.WriteLine(protocols);
			File.WriteAllText(protocolPath, protocols);
		}

		protected string GetProtocols(ProtocolJson protocol) {
			if (protocol.Domains == null || protocol.Domains.Length <= 0) {
				return string.Empty;
			}
			string version = protocol.Version.ToString();
			StringBuilder protocolsBuilder = new StringBuilder();
			StringBuilder initsBuilder = new StringBuilder();
			StringBuilder eventsBuilder = new StringBuilder();
			Dictionary<string, string> args;
			foreach (DomainJson domain in protocol.Domains) {
				if (domain == null) {
					continue;
				}
				string dependencies = string.Empty;
				if (domain.Dependencies != null) {
					dependencies = string.Join(", ", domain.Dependencies);
				}
				args = new Dictionary<string, string> {
					{ "Version", version },
					{ "ClassName", GetClassName(domain) },
					{ "Domain", domain.Domain },
					{ "Description", EscapeTags(RemoveNewLines(domain.Description)) },
					{ "Deprecated", domain.Deprecated.ToString().ToLower() },
					{ "Experimental", domain.Experimental.ToString().ToLower() },
					{ "Dependencies", dependencies }
				};
				protocolsBuilder.Append(
					StringFormatter.Format(Formats.Protocol, args)
				);
				initsBuilder.Append(
					StringFormatter.Format(Formats.ProtocolInit, args)
				);
				if (domain.Events != null && domain.Events.Length > 0) {
					eventsBuilder.Append(
						StringFormatter.Format(Formats.ProtocolEvent, args)
					);
				}
			}
			args = new Dictionary<string, string> {
				{ "Version", version },
				{ "Protocols", protocolsBuilder.ToString() },
				{ "ProtocolInits", TrimNewLines(initsBuilder.ToString()) },
				{ "ProtocolEvents", TrimNewLines(eventsBuilder.ToString()) }
			};
			string protocols = StringFormatter.Format(Formats.Protocols, args);
			return protocols;
		}

		protected Dictionary<string, TypeJson>CollectAllTypes(ProtocolJson protocol) {
			Dictionary<string, TypeJson> types;
			types = new Dictionary<string, TypeJson>();
			if (protocol == null || protocol.Domains == null) {
				return types;
			}
			foreach (DomainJson domain in protocol.Domains) {
				if (domain == null || domain.Types == null) {
					continue;
				}
				foreach (TypeJson type in domain.Types) {
					if (type == null) {
						continue;
					}
					//if (type.Type != "object") {
						string id = domain.Domain + "." + type.ID;
						types.Add(id, type);
					//}
				}
			}
			return types;
		}

		protected TypeJson FindType(string id) {
			if (_types.ContainsKey(id) == false) {
				return null;
			}
			return _types[id];
		}

		protected string GetDomain(DomainJson domain, string version) {
			string dependencies = string.Empty;
			if (domain.Dependencies != null) {
				dependencies = string.Join(", ", domain.Dependencies);
			}
			Dictionary<string, string> args = new Dictionary<string, string> {
				{ "Version", version },
				{ "ClassName", GetClassName(domain) },
				{ "Domain", domain.Domain },
				{ "Description", EscapeTags(RemoveNewLines(domain.Description)) },
				{ "Deprecated", domain.Deprecated.ToString().ToLower() },
				{ "Experimental", domain.Experimental.ToString().ToLower() },
				{ "Dependencies", dependencies },
				{ "Types", GetTypes(domain) },
				{ "Commands", GetCommands(domain) },
				{ "Events", GetEvents(domain) },
				{ "EventEmitter", GetEventEmitter(domain) }
			};
			return StringFormatter.Format(Formats.Domain, args);
		}

		protected string GetTypes(DomainJson domain) {
			if (domain.Types == null || domain.Types.Length <= 0) {
				return string.Empty;
			}
			StringBuilder builder = new StringBuilder();
			foreach (TypeJson item in domain.Types) {
				builder.Append(GetType(domain, item));
			}
			return builder.ToString();
		}

		protected string GetCommands(DomainJson domain) {
			if (domain.Commands == null || domain.Commands.Length <= 0) {
				return string.Empty;
			}
			StringBuilder builder = new StringBuilder();
			foreach (CommandJson item in domain.Commands) {
				builder.Append(GetCommand(domain, item));
			}
			return builder.ToString();
		}

		protected string GetEvents(DomainJson domain) {
			if (domain.Events == null || domain.Events.Length <= 0) {
				return string.Empty;
			}
			StringBuilder builder = new StringBuilder();
			foreach (CommandJson item in domain.Events) {
				builder.Append(GetEvent(domain, item));
			}
			return builder.ToString();
		}

		protected string GetEventEmitter(DomainJson domain) {
			if (domain.Events == null || domain.Events.Length <= 0) {
				return string.Empty;
			}
			StringBuilder builder = new StringBuilder();
			foreach (CommandJson item in domain.Events) {
				builder.Append(GetEventEmit(domain, item));
			}
			Dictionary<string, string> args = new Dictionary<string, string> {
				{ "Domain", domain.Domain },
				{ "EventEmits", builder.ToString() }
			};
			return StringFormatter.Format(Formats.EventEmitter, args);
		}

		protected string GetEventEmit(DomainJson domain, CommandJson command) {
			Dictionary<string, string> args = new Dictionary<string, string> {
				{ "Domain", domain.Domain },
				{ "name", command.Name },
				{ "Name", Capitalize(command.Name) },
				{ "Description", EscapeTags(RemoveNewLines(command.Description)) },
				{ "Experimental", command.Experimental.ToString().ToLower() },
				{ "Type", command.Type },
				{ "Parameters", GetParams(domain, command.Parameters, true) },
				//{ "ReturnType", GetReturnType(domain, command) },
				//{ "Returns", GetReturnType(domain, command) },
				//{ "CommandParams", GetCommandParams(command.Parameters) },
				//{ "ParamComments", GetParamComments(command.Parameters) }
			};
			if (command.Parameters == null || command.Parameters.Length <= 0) {
				return StringFormatter.Format(Formats.EventEmitNoArgs, args);
			}
			return StringFormatter.Format(Formats.EventEmit, args);
		}

		protected string GetType(DomainJson domain, TypeJson type) {
			if (type.Type != "object") {
				return string.Empty;
			}
			Dictionary<string, string> args = new Dictionary<string, string> {
				{ "id", type.ID },
				{ "Id", Capitalize(type.ID) },
				{ "Description", EscapeTags(RemoveNewLines(type.Description)) },
				{ "Type", type.Type },
				{ "Enum", GetEnumComments(type.Enum) },
				{ "Properties", GetProperties(domain, type.Properties) }
			};
			AddItemsArgs(args, type.Items);
			return StringFormatter.Format(Formats.Type, args);
		}

		protected string GetCommand(DomainJson domain, CommandJson command) {
			string returnTypeName = GetReturnTypeName(command);
			Dictionary<string, string> args = new Dictionary<string, string> {
				{ "Domain", domain.Domain },
				{ "name", command.Name },
				{ "Name", Capitalize(command.Name) },
				{ "Description", EscapeTags(RemoveNewLines(command.Description)) },
				{ "Deprecated", command.Deprecated.ToString().ToLower() },
				{ "Experimental", command.Experimental.ToString().ToLower() },
				{ "Type", command.Type },
				{ "Parameters", GetParams(domain, command.Parameters, true) },
				{ "ReturnTypeName", returnTypeName },
				{ "ReturnType", GetReturnType(domain, command) },
				//{ "Returns", command.Returns },
				{ "CommandParams", GetCommandParams(command.Parameters) },
				{ "ParamComments", GetParamComments(command.Parameters) },
				{ "ObsoleteAttribute", GetObsoleteAttribute(command.Deprecated) }
			};
			if (returnTypeName == "void") {
				return StringFormatter.Format(Formats.CommandNoReturn, args);
			}
			return StringFormatter.Format(Formats.Command, args);
		}

		protected string GetEvent(DomainJson domain, CommandJson command) {
			string eventArgs = GetEventArgs(domain, command);
			Dictionary<string, string> args = new Dictionary<string, string> {
				{ "Domain", domain.Domain },
				{ "name", command.Name },
				{ "Name", Capitalize(command.Name) },
				{ "Description", EscapeTags(RemoveNewLines(command.Description)) },
				{ "Experimental", command.Experimental.ToString().ToLower() },
				{ "Type", command.Type },
				{ "Parameters", GetParams(domain, command.Parameters, true) },
				//{ "Properties", GetProperties(domain, command.Parameters) },
				//{ "ReturnType", GetReturnType(domain, command) },
				//{ "Returns", GetReturnType(domain, command) },
				{ "CommandParams", GetCommandParams(command.Parameters) },
				{ "ParamComments", GetParamComments(command.Parameters) },
				{ "EventArgs", eventArgs }
			};
			if (string.IsNullOrEmpty(eventArgs)) {
				return StringFormatter.Format(Formats.EventNoArgs, args);
			}
			return StringFormatter.Format(Formats.Event, args);
		}

		protected string GetEventArgs(DomainJson domain, CommandJson command) {
			if (command.Parameters == null || command.Parameters.Length <= 0) {
				return string.Empty;
			}
			Dictionary<string, string> args = new Dictionary<string, string> {
				{ "Domain", domain.Domain },
				{ "name", command.Name },
				{ "Name", Capitalize(command.Name) },
				{ "Description", EscapeTags(RemoveNewLines(command.Description)) },
				{ "Experimental", command.Experimental.ToString().ToLower() },
				{ "Type", command.Type },
				{ "Parameters", GetParams(domain, command.Parameters, true) },
				{ "Properties", GetProperties(domain, command.Parameters) },
				//{ "ReturnType", GetReturnType(domain, command) },
				//{ "Returns", GetReturnType(domain, command) },
				{ "CommandParams", GetCommandParams(command.Parameters) },
				{ "ParamComments", GetParamComments(command.Parameters) }
			};
			return StringFormatter.Format(Formats.EventArgs, args);
		}

		protected string GetProperties(
			DomainJson domain, PropertyJson[] properties)
		{
			if (properties == null || properties.Length <= 0) {
				return string.Empty;
			}
			StringBuilder builder = new StringBuilder();
			//int lastIndex = properties.Length - 1;
			for (int i = 0; i < properties.Length; i++) {
				PropertyJson item = properties[i];
				builder.Append(GetProperty(domain, item));
				//if (i < lastIndex) {
				//	builder.AppendLine();
				//}
			}
			return TrimNewLines(builder.ToString());
		}

		protected string GetProperty(DomainJson domain, PropertyJson property) {
			Dictionary<string, string> args = CreatePropertyArgs(property);
			args.Add("ResolvedType", ResolveType(domain, property));
			return StringFormatter.Format(Formats.TypeProperty, args);
		}

		protected Dictionary<string, string> CreatePropertyArgs(
			PropertyJson property)
		{
			Dictionary<string, string> args = new Dictionary<string, string> {
				{ "name", property.Name },
				{ "Name", Capitalize(property.Name) },
				{ "Description", EscapeTags(RemoveNewLines(property.Description)) },
				{ "Optional", property.Optional.ToString().ToLower() },
				{ "Type", property.Type },
				{ "Ref", property.Ref },
				{ "EnumComments", GetEnumComments(property.Enum) }
			};
			AddItemsArgs(args, property.Items);
			return args;
		}

		protected void AddItemsArgs(
			Dictionary<string, string> args, PropertyItemsJson items)
		{
			if (items == null) {
				args.Add("Items.Type", string.Empty);
				args.Add("Items.Ref", string.Empty);
				return;
			}
			args.Add("Items.Type", items.Type);
			args.Add("Items.Ref", items.Ref);
		}

		protected string GetCommandParams(PropertyJson[] properties) {
			if (properties == null || properties.Length <= 0) {
				return string.Empty;
			}
			StringBuilder builder = new StringBuilder();
			int lastIndex = properties.Length - 1;
			for (int i = 0; i < properties.Length; i++) {
				PropertyJson item = properties[i];
				builder.AppendFormat(
					Formats.CommandParam,
					EscapePropertyName(item.Name)
				);
				if (i < lastIndex) {
					builder.Append(',');
				}
			}
			return string.Format(
				Formats.CommandParams,
				builder.ToString()
			);
		}

		protected string GetParams(
			DomainJson domain, PropertyJson[] properties,
			bool setDefault = false)
		{
			if (properties == null || properties.Length <= 0) {
				return string.Empty;
			}
			StringBuilder builder = new StringBuilder();
			int lastIndex = properties.Length - 1;
			for (int i = 0; i < properties.Length; i++) {
				PropertyJson property = properties[i];
				string type = ResolveType(domain, property);
				if (setDefault) {
					if (property.Optional) {
						if (NullableTypeNameReplacementList.ContainsKey(type)) {
							type = NullableTypeNameReplacementList[type];
						}
					}
					builder.Append(type);
					builder.Append(' ');
					builder.Append(EscapePropertyName(property.Name));
					if (property.Optional) {
						builder.Append(" = null");
					}
				} else {
					builder.Append(type);
					builder.Append(' ');
					builder.Append(EscapePropertyName(property.Name));
				}
				if (i < lastIndex) {
					builder.Append(", ");
				}
			}
			return builder.ToString();
		}

		protected string GetParamComments(PropertyJson[] properties) {
			if (properties == null || properties.Length <= 0) {
				return string.Empty;
			}
			StringBuilder builder = new StringBuilder();
			foreach (PropertyJson item in properties) {
				builder.Append(GetParamComment(item));
			}
			return builder.ToString();
		}

		protected string GetParamComment(PropertyJson property) {
			if (property == null) {
				return string.Empty;
			}
			Dictionary<string, string> args = CreatePropertyArgs(property);
			//args.Add("ResolvedType", ResolveType(domain, property));
			return StringFormatter.Format(Formats.ParamComment, args);
		}

		protected string GetReturnTypeName(CommandJson command) {
			if (command == null) {
				return "void";
			}
			if (command.Returns == null || command.Returns.Length <= 0) {
				return "void";
			}
			return Capitalize(command.Name) + "Result";
		}

		protected string GetReturnType(DomainJson domain, CommandJson command) {
			if (command == null) {
				return string.Empty;
			}
			PropertyJson[] returns = command.Returns;
			if (returns == null || returns.Length <= 0) {
				return string.Empty;
			}
			string id = GetReturnTypeName(command);
			Dictionary<string, string> args = new Dictionary<string, string> {
				{ "id", id },
				{ "Id", id },
				{ "Description", EscapeTags(RemoveNewLines(command.Description)) },
				{ "Type", command.Type },
				{ "Properties", GetProperties(domain, returns) }
			};
			return StringFormatter.Format(Formats.Type, args);
		}

		protected string GetObsoleteAttribute(bool value) {
			if (value == false) {
				return string.Empty;
			}
			return Formats.ObsoleteAttribute;
		}

		protected string GetEnumComments(string[] enums) {
			if (enums == null || enums.Length <= 0) {
				return string.Empty;
			}
			StringBuilder builder = new StringBuilder();
			int lastIndex = enums.Length - 1;
			for (int i = 0; i < enums.Length; i++) {
				builder.Append(enums[i]);
				if (i < lastIndex) {
					builder.Append(", ");
				}
			}
			Dictionary<string, string> args = new Dictionary<string, string> {
				{ "Items", builder.ToString() }
			};
			return StringFormatter.Format(Formats.EnumComment, args);
		}

		protected string GetClassName(DomainJson domain) {
			return GetClassName(domain.Domain);
		}

		protected string GetClassName(string name) {
			Dictionary<string, string> args = new Dictionary<string, string> {
				{ "Domain", name }
			};
			return StringFormatter.Format(Formats.ClassName, args);
		}

		protected string GetRefName(string value) {
			if (string.IsNullOrEmpty(value)) {
				return string.Empty;
			}
			if (value.Contains(".")) {
				string[] splited = value.Split('.');
				if (splited.Length != 2) {
					return GetTypeName(value);
				}
				return GetClassName(splited[0]) + "." + splited[1];
			}
			return GetTypeName(value);
		}

		protected string GetArrayType(
			DomainJson domain, PropertyItemsJson items)
		{
			if (items == null) {
				return string.Empty;
			}
			if (items.Type != null) {
				return string.Format(
					Formats.Array,
					GetTypeName(items.Type)
				);
			}
			return string.Format(
				Formats.Array,
				ResolveRefType(domain, items.Ref)
			);
		}

		protected string ResolveRefType(DomainJson domain, string @ref) {
			if (string.IsNullOrEmpty(@ref)) {
				return string.Empty;
			}
			string typeName;
			if (@ref.Contains(".")) {
				typeName = @ref;
			} else {
				typeName = domain.Domain + "." + @ref;
			}
			TypeJson type = FindType(typeName);
			if (type == null) {
				return GetRefName(@ref);
			}
			if (type.Type == "object") {
				return GetRefName(@ref);
			}
			if (type.Type == "array") {
				return GetArrayType(domain, type.Items);
			}
			return GetRefName(type.Type);
		}

		protected string ResolveType(DomainJson domain, PropertyJson property) {
			if (domain == null || property == null) {
				return string.Empty;
			}
			if (string.IsNullOrEmpty(property.Type)) {
				return ResolveRefType(domain, property.Ref);
			}
			if (property.Type == "array") {
				return GetArrayType(domain, property.Items);
			}
			return GetTypeName(property.Type);
		}

		protected string GetTypeName(string value) {
			if (string.IsNullOrEmpty(value)) {
				return string.Empty;
			}
			if (TypeNameReplacementList.ContainsKey(value) == false) {
				return value;
			}
			return TypeNameReplacementList[value];
		}

		protected string EscapePropertyName(string value) {
			if (string.IsNullOrEmpty(value)) {
				return string.Empty;
			}
			if (IsReservedKeyword(value)) {
				value = "@" + value;
			}
			return value;
		}

		protected bool IsReservedKeyword(string value) {
			if (string.IsNullOrEmpty(value)) {
				return false;
			}
			return ReservedKeywords.Contains(value);
		}

		protected string TrimNewLines(string value) {
			if (string.IsNullOrEmpty(value)) {
				return string.Empty;
			}
			return value.Trim('\r', '\n');
		}

		protected string RemoveNewLines(string value) {
			if (string.IsNullOrEmpty(value)) {
				return string.Empty;
			}
			return Regex.Replace(
				value, "\r\n|\r|\n", " "
			);
		}

		protected string EscapeTags(string value) {
			if (string.IsNullOrEmpty(value)) {
				return string.Empty;
			}
			Regex regex = new Regex("<(/?)([^>]+)>|<|>");
			return regex.Replace(value, (Match match) => {
				if (match.Length < 3) {
					if (match.Value == "<") {
						return "&lt;";
					}
					if (match.Value == ">") {
						return "&gt;";
					}
					return match.Value;
				}
				if (NoEscapeHtmlTags.Contains(match.Groups[2].Value)) {
					return match.Value;
				}
				return "&lt;" + match.Groups[1] + match.Groups[2] + "&gt;";
			});
		}

		protected string Capitalize(string value) {
			if (string.IsNullOrEmpty(value)) {
				return string.Empty;
			}
			return char.ToUpper(value[0]) + value.Substring(1);
		}
	}
}
