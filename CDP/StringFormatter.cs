﻿using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace CDP {
	public static class StringFormatter {
		static readonly Regex Regex = new Regex(@"{{|}}|{([^}]+)}");

		public static string Format(string format, Dictionary<string, string> args) {
			if (string.IsNullOrEmpty(format) || args == null) {
				return string.Empty;
			}
			return Regex.Replace(format, (Match match) => {
				switch (match.Value) {
					case "{{":
						return "{";
					case "}}":
						return "}";
				}
				if (match.Groups.Count < 2) {
					return string.Empty;
				}
				string value = match.Groups[1].Value;
				if (args.ContainsKey(value) == false) {
					return string.Empty;
				}
				return args[value];
			});
		}
	}
}
