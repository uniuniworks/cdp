﻿using Newtonsoft.Json.Linq;

namespace CDP {
	/// <summary>
	/// The base class of the HeadlessChrome class.
	/// <para>
	/// To work even if the "Protocols" directory does not exist.
	/// </para>
	/// </summary>
	public class HeadlessChromeBase {
		/// <summary>
		/// Initializes objects that exist in CDP.Protocols.
		/// </summary>
		protected virtual void InitProtocols() {
		}

		/// <summary>
		/// Called when an event is received from chrome.
		/// </summary>
		/// <param name="method">The event method name.</param>
		/// <param name="params">The event parameters.</param>
		protected virtual void OnEventReceived(JToken method, JToken @params) {
		}
	}
}
