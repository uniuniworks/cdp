﻿using System;
using Newtonsoft.Json;

namespace CDP {
	/// <summary>
	/// Occurs when a log is added.
	/// </summary>
	public class LogAddedEventArgs : EventArgs {
		/// <summary>
		/// The log message.
		/// </summary>
		public string Message;

		/// <summary>
		/// Initializes a new instance of the LogAddedEventArgs class.
		/// </summary>
		/// <param name="message">The log message.</param>
		public LogAddedEventArgs(string message) {
			Message = message;
		}

		/// <summary>
		/// Returns a JSON string that represents the current object.
		/// </summary>
		/// <returns>A JSON string.</returns>
		public override string ToString() {
			return JsonConvert.SerializeObject(
				this, Formatting.Indented
			);
		}
	}
}
