# CDP

.NET bindings for the Chrome DevTools Protocol (CDP)

- This package can be used for any kind of browser automation
- .NET Framework 2.0 or later

Please see the [Wiki](https://bitbucket.org/uniuniworks/cdp/wiki/) for more detailed usage

## Download

- [NuGet package](https://www.nuget.org/packages/CDP/)
- [Source code](https://bitbucket.org/uniuniworks/cdp/downloads/)

## Usage

```csharp
using System;
using System.IO;
using CDP;
using CDP.Protocols;

class MainClass {
	public static void Main(string[] args) {
		HeadlessChrome chrome = new HeadlessChrome(
			HeadlessChrome.DefaultAppPathMac
		);
		// "using" clause ensures that close the chrome process
		using (chrome) {
			chrome.Page.FrameNavigated += (sender, e) => {
				Console.WriteLine("FrameNavigated: " + e.Frame.Id);
			};
			chrome.Start();
			chrome.Page.Enable();
			chrome.Page.Navigate("http://google.com");
			var png = chrome.Page.CaptureScreenshot();
			File.WriteAllBytes("Screenshot.png", png.Data);
			chrome.Delay(1000);
		}
	}
}
```

## Dependencies (NuGet)

- [Newtonsoft.Json](https://www.nuget.org/packages/Newtonsoft.Json/)
  (MIT)
- [SuperSocket.ClientEngine.Core](https://www.nuget.org/packages/SuperSocket.ClientEngine.Core/)
  (Apache License 2.0)
- [WebSocket4Net](https://www.nuget.org/packages/WebSocket4Net/)
  (Apache License 2.0)

## License

- MIT
